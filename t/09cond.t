use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 32;

use Thread::Sociable always_lockable => 1;

sociable_enable_debug();

my $Base = 0;

$Base++;

### Start of Testing ###

# test locking
{
    my $lock : Sociable;
    my $tr;

    # test that a subthread can't lock until parent thread has unlocked

    {
        lock($lock);
        ok( 1, "set first lock");
        $tr = async {
            lock($lock);
            ok( 1, "set lock in subthread");
        };
        threads->yield;
        ok( 1, "still got lock");
    }
    $tr->join;

sociable_disable_debug();

    $Base += 3;

    # ditto with ref to thread

    {
        my $lockref = \$lock;
        lock($lockref);
        ok(1,"set first lockref");
        $tr = async {
            lock($lockref);
            ok(1,"set lockref in subthread");
        };
        threads->yield;
        ok(1,"still got lockref");
    }
    $tr->join;

    $Base += 3;

    # make sure recursive locks unlock at the right place
    {
        lock($lock);
        ok(1,"set first recursive lock");
        lock($lock);
        threads->yield;
        {
            lock($lock);
            threads->yield;
        }
        $tr = async {
            lock($lock);
            ok(1,"set recursive lock in subthread");
        };
        {
            lock($lock);
            threads->yield;
            {
                lock($lock);
                threads->yield;
                lock($lock);
                threads->yield;
            }
        }
        ok(1,"still got recursive lock");
    }
    $tr->join;

    $Base += 3;

    # Make sure a lock factory gives out fresh locks each time
    # for both attribute and run-time shares

    sub lock_factory1 { my $lock : Lockable; return \$lock; }
    sub lock_factory2 { my $lock; socialize_with_lock($lock); return \$lock; }

    my (@locks1, @locks2);
    push @locks1, lock_factory1() for 1..2;
    push @locks1, lock_factory2() for 1..2;
    push @locks2, lock_factory1() for 1..2;
    push @locks2, lock_factory2() for 1..2;

    ok(1,"lock factory: locking all locks");
    lock $locks1[0];
    lock $locks1[1];
    lock $locks1[2];
    lock $locks1[3];
    ok(1,"lock factory: locked all locks");
    $tr = async {
        ok(1,"lock factory: child: locking all locks");
        lock $locks2[0];
        lock $locks2[1];
        lock $locks2[2];
        lock $locks2[3];
        ok(1,"lock factory: child: locked all locks");
    };
    $tr->join;

    $Base += 4;
}

# test cond_signal()
{
    my $lock : Lockable;

    sub foo {
        lock($lock);
        ok(1,"cond_signal: created first lock");
        my $tr2 = threads->create(\&bar);
        cond_wait($lock);
        $tr2->join();
        ok(1,"cond_signal: joined");
    }

    sub bar {
        ok(1,"cond_signal: child before lock");
        lock($lock);
        ok(1,"cond_signal: child locked");
        cond_signal($lock);
        ok(1,"cond_signal: signalled");
    }

    my $tr  = threads->create(\&foo);
    $tr->join();

    $Base += 5;

    # ditto, but with lockrefs

    my $lockref = \$lock;
    sub foo2 {
        lock($lockref);
        ok(1,"cond_signal: ref: created first lock");
        my $tr2 = threads->create(\&bar2);
        cond_wait($lockref);
        $tr2->join();
        ok(1,"cond_signal: ref: joined");
    }

    sub bar2 {
        ok(1,"cond_signal: ref: child before lock");
        lock($lockref);
        ok(1,"cond_signal: ref: child locked");
        cond_signal($lockref);
        ok(1,"cond_signal: ref: signalled");
    }

    $tr  = threads->create(\&foo2);
    $tr->join();

    $Base += 5;

}


# test cond_broadcast()
{
    my $counter : Lockable = 0;

    # broad(N) forks off broad(N-1) and goes into a wait, in such a way
    # that it's guaranteed to reach the wait before its child enters the
    # locked region. When N reaches 0, the child instead does a
    # cond_broadcast to wake all its ancestors.

    sub broad {
        my $n = shift;
        my $th;
        {
            lock($counter);
            if ($n > 0) {
                $counter++;
                $th = threads->new(\&broad, $n-1);
                cond_wait($counter);
                $counter += 10;
            }
            else {
                ok( $counter == 3, "cond_broadcast: all three waiting");
                cond_broadcast($counter);
            }
        }
        $th->join if $th;
    }

    threads->new(\&broad, 3)->join;
    ok( $counter == 33, "cond_broadcast: all three threads woken");

    $Base += 2;


    # ditto, but with refs and socialize()

    my $counter2 = 0;
    socialize_with_lock($counter2);
    my $r = \$counter2;

    sub broad2 {
        my $n = shift;
        my $th;
        {
            lock($r);
            if ($n > 0) {
                $$r++;
                $th = threads->new(\&broad2, $n-1);
                cond_wait($r);
                $$r += 10;
            }
            else {
                ok( $$r == 3, "cond_broadcast: ref: all three waiting");
                cond_broadcast($r);
            }
        }
        $th->join if $th;
    }

    threads->new(\&broad2, 3)->join;;
    ok( $$r == 33, "cond_broadcast: ref: all three threads woken");

    $Base += 2;

}


# test warnings;
{
    my $warncount = 0;
    local $SIG{__WARN__} = sub { $warncount++ };

    my $lock : Lockable;

    cond_signal($lock);
    ok( $warncount == 1, 'get warning on cond_signal');
    cond_broadcast($lock);
    ok( $warncount == 2, 'get warning on cond_broadcast');
    no warnings 'threads';
    cond_signal($lock);
    ok( $warncount == 2, 'get no warning on cond_signal');
    cond_broadcast($lock);
    ok( $warncount == 2, 'get no warning on cond_broadcast');

    #$Base += 4;
}

# EOF
