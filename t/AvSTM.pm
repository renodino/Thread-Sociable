
package AvSTM;

use strict;
use warnings;

use threads;
use Test::More;

use Thread::Sociable;

sub tests { return 55; }

### Start of Testing ###

#
#	no commit/restart/rollback subs
#		1. readonly xaction, no element access
#		1. readonly xaction, w/ element access
#		2. writeonly xaction
#		3. read/write xaction
#		4. write/read xaction
#		5. unshift
#		6. shift
#		7. push
#		8. pop
#		9. splice
#		10. delete
#		11. unshift, push, shift, pop, splice, delete
#	commit, w/ commit sub, no restart/rollback sub: repeat 1-11
#	force restart, w/ restart sub, no commit/rollback sub: repeat 1-11
#	force rollback, w/ commit, restart & rollback subs: repeat 1-11
#
#	repeat w/ 2 competing threads
#	repeat w/ 4 competing threads
#

sub run {

sociable_disable_debug();

my @loadary = qw(
First 1
Second 2
Third 3
Fourth 4
Fifth 5
Sixth 6
Seventh 7
Eighth 8
Ninth 9
Tenth 10
);

my @array : Sociable = @loadhash;
my $privscalar;
my $rc = sociable_begin_work(
	sub {
		$privscalar = $array[9];
	});
ok($rc && ($privscalar == 5) && valid_array(@loadary),'no restart/rollback, readonly');

$rc = sociable_begin_work(
	sub {
		$array[20] = 'Eleventh';
		$array[211 = 11;
		$array[22] = 'Twelveth';
	});
ok($rc && valid_array( @loadary, 'Eleventh', 11, 'Twelveth'),'no restart/rollback, writeonly');

$rc = sociable_begin_work(
	sub {
		$privscalar = $array[0];
		$array[23] = 12;
	});
ok($rc && ($privscalar == 1) && valid_array( @loadary, 'Eleventh', 11, 'Twelveth', 12),'no restart/rollback, read/write');

$rc = sociable_begin_work(
	sub {
		$array[24] = 'Fourteenth';
		$privscalar = $array[20];
	});
ok($rc && ($privscalar eq 'Eleventh') && valid_array(@loadary, 'Eleventh', 11, 'Twelveth', 12, 'Fourteenth' ),'no restart/rollback, write/read');

@array = @loadary;
my $count = 0;
my ($k,$v);
$rc = sociable_begin_work(
	sub {
		$count++
			foreach (@array);
	});
ok($rc && ($count == 20) && valid_array(@loadary),'no restart/rollback, readonly, fully iterated');

$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++,
		$_ = "replaced $count"
			foreach (@array);
	});
ok($rc && ($count == 10) && replaced_array(),'no restart/rollback, read/write, fully iterated');

$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		delete $array[5];
		$count = scalar @array;
	});
ok($rc && ($count == 19) && deleted_array(5),'no restart/rollback, read/write w/ deletes');

$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		unshift @array, 'howdy';
		$count = scalar @array;
	});
ok($rc && ($count == 21) && valid_array('howdy', @loadary),'no restart/rollback, unshift');

$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		push @array, 'every', 'good', 'boy', 'deserves', 'fudge';
		$count = scalar @array;
	});
ok($rc && ($count == 25) && valid_array(@loadary, 'every', 'good', 'boy', 'deserves', 'fudge'),'no restart/rollback, push');

$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		$privscalar = pop @array;
		$count = scalar @array;
	});
ok($rc && ($count == 19) && ($privscalar == $loadary[-1]) && valid_array(@loadary[0..18]),'no restart/rollback, pop');

$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		$privscalar = shift @array;
		$count = scalar @array;
	});
ok($rc && ($count == 19) && ($privscalar == $loadary[0]) && valid_array(@loadary[1..19]),'no restart/rollback, shift');

$count = 0;
@array = @loadary;
my @splicer = @loadary;
splice @splicer, 5, 1, 'whozzits';
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1, 'whozzits';
		$count = scalar @array;
	});
ok($rc && ($count == 20) && ($privscalar == 3) && valid_array(@splicer),'no restart/rollback, splice, no adjust');

$count = 0;
@array = @loadary;
@splicer = @loadary;
splice @splicer, 5, 1;
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1;
		$count = scalar @array;
	});
ok($rc && ($count == 20) && ($privscalar == 3) && valid_array(@splicer),'no restart/rollback, splice to remove');

$count = 0;
@array = @loadary;
@splicer = @loadary;
splice @splicer, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
		$count = scalar @array;
	});
ok($rc && ($count == 24) && ($privscalar == 3) && valid_array(@splicer),'no restart/rollback, splice to add');

$count = 0;
@array = @loadary;
@splicer = @loadary;
delete $splicer[5];
splice @splicer, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
unshift @splicer, 'unshifted';
pop @splicer;
shift @splicer;
push @splicer, 'pushed';
$rc = sociable_begin_work(
	sub {
		delete $array[5];
		splice @array, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
		unshift @array, 'unshifted';
		$privscalar = pop @array;
		shift @array;
		push @array, 'pushed';
		$count = scalar @array;
	});
ok($rc && ($count == 24) && valid_array(@splicer),'no restart/rollback, the works');

############ force restart, w restart sub ##################

my $restart = 1;
my $report = 0;
$rc = sociable_begin_work(
	sub {
		$privscalar = $array[9];
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($privscalar == 5) && valid_array(@loadary),'restart, readonly');

($restart, $report) = (1, 0);
$rc = sociable_begin_work(
	sub {
		$array[20] = 'Eleventh';
		$array[211 = 11;
		$array[22] = 'Twelveth';
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && valid_array( @loadary, 'Eleventh', 11, 'Twelveth'),'restart, writeonly');

($restart, $report) = (1, 0);
$rc = sociable_begin_work(
	sub {
		$privscalar = $array[0];
		$array[23] = 12;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($privscalar == 1) && valid_array(@loadary, 'Eleventh', 11, 'Twelveth', 12),'restart, read/write');

($restart, $report) = (1, 0);
$rc = sociable_begin_work(
	sub {
		$array[24] = 'Fourteenth';
		$privscalar = $array[20];
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($privscalar eq 'Eleventh') && valid_array( @loadary, 'Eleventh', 11, 'Twelveth', 12, 'Fourteenth' ),'restart, write/read');

($restart, $report) = (1, 0);
@array = @loadary;
my $count = 0;
my ($k,$v);
$rc = sociable_begin_work(
	sub {
		$count++
			foreach (@array);
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 20) && valid_array(@loadary),'restart, readonly, fully iterated');

($restart, $report) = (1, 0);
$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++,
		$_ = "replaced $count"
			foreach (@array);
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 10) && replaced_array(),'restart, read/write, fully iterated');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		delete $array[5];
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 19) && deleted_array(5),'restart, read/write w/ deletes');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		unshift @array, 'howdy';
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 21) && valid_array('howdy', @loadary),'restart, unshift');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		push @array, 'every', 'good', 'boy', 'deserves', 'fudge';
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 25) && valid_array(@loadary, 'every', 'good', 'boy', 'deserves', 'fudge'),'restart, push');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		$privscalar = pop @array;
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 19) && ($privscalar == $loadary[-1]) && valid_array(@loadary[0..18]),'restart, pop');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		$privscalar = shift @array;
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 19) && ($privscalar == $loadary[0]) && valid_array(@loadary[1..19]),'restart, shift');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
my @splicer = @loadary;
splice @splicer, 5, 1, 'whozzits';
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1, 'whozzits';
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 20) && ($privscalar == 3) && valid_array(@splicer),'restart, splice, no adjust');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
@splicer = @loadary;
splice @splicer, 5, 1;
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1;
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 20) && ($privscalar == 3) && valid_array(@splicer),'restart, splice to remove');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
@splicer = @loadary;
splice @splicer, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 24) && ($privscalar == 3) && valid_array(@splicer),'restart, splice to add');

($restart, $report) = (1, 0);
$count = 0;
@array = @loadary;
@splicer = @loadary;
delete $splicer[5];
splice @splicer, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
unshift @splicer, 'unshifted';
pop @splicer;
shift @splicer;
push @splicer, 'pushed';
$rc = sociable_begin_work(
	sub {
		delete $array[5];
		splice @array, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
		unshift @array, 'unshifted';
		$privscalar = pop @array;
		shift @array;
		push @array, 'pushed';
		$count = scalar @array;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($count == 24) && valid_array(@splicer),'restart, the works');

############ force rollback, restart & rollback sub ##################

my $rollback = 0;
($rollback, $report) = (0,0);
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		$privscalar = $array[9];
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($privscalar == 5) && valid_array(@loadary),'rollback, readonly');

($rollback, $report) = (0,0);
$rc = sociable_begin_work(
	sub {
		$array[20] = 'Eleventh';
		$array[211 = 11;
		$array[22] = 'Twelveth';
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && valid_array( @loadary),'rollback, writeonly');

($rollback, $report) = (0,0);
$rc = sociable_begin_work(
	sub {
		$privscalar = $array[0];
		$array[23] = 12;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($privscalar == 1) && valid_array( @loadary),'rollback, read/write');

($rollback, $report) = (0,0);
$rc = sociable_begin_work(
	sub {
		$array[24] = 'Fourteenth';
		$privscalar = $array[20];
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($privscalar eq 'Eleventh') && valid_array( @loadary),'rollback, write/read');

($rollback, $report) = (0,0);
@array = @loadary;
my $count = 0;
my ($k,$v);
$rc = sociable_begin_work(
	sub {
		$count++
			foreach (@array);
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 20) && valid_array(@loadary),'rollback, readonly, fully iterated');

($rollback, $report) = (0,0);
$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++,
		$_ = "replaced $count"
			foreach (@array);
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == scalar @loadary) && valid_array(@loadary),'rollback, read/write, fully iterated');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		delete $array[5];
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 19) && valid_array(@loadary),'rollback, read/write w/ deletes');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		unshift @array, 'howdy';
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 21) && valid_array(@loadary),'rollback, unshift');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		push @array, 'every', 'good', 'boy', 'deserves', 'fudge';
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 25) && valid_array(@loadary),'rollback, push');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		$privscalar = pop @array;
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 19) && ($privscalar == $loadary[-1]) && valid_array(@loadary),'rollback, pop');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
$rc = sociable_begin_work(
	sub {
		$privscalar = shift @array;
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 19) && ($privscalar == $loadary[0]) && valid_array(@loadary),'rollback, shift');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
my @splicer = @loadary;
splice @splicer, 5, 1, 'whozzits';
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1, 'whozzits';
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 20) && ($privscalar == 3) && valid_array(@loadary),'rollback, splice, no adjust');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
@splicer = @loadary;
splice @splicer, 5, 1;
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1;
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 20) && ($privscalar == 3) && valid_array(@loadary),'rollback, splice to remove');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
@splicer = @loadary;
splice @splicer, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
$rc = sociable_begin_work(
	sub {
		$privscalar = splice @array, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 24) && ($privscalar == 3) && valid_array(@loadary),'rollback, splice to add');

($rollback, $report) = (0,0);
$count = 0;
@array = @loadary;
@splicer = @loadary;
delete $splicer[5];
splice @splicer, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
unshift @splicer, 'unshifted';
pop @splicer;
shift @splicer;
push @splicer, 'pushed';
$rc = sociable_begin_work(
	sub {
		delete $array[5];
		splice @array, 5, 1, 'hey', 'the', 'gangs', 'all', 'here';
		unshift @array, 'unshifted';
		$privscalar = pop @array;
		shift @array;
		push @array, 'pushed';
		$count = scalar @array;
		die 'rollback test';
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 24) && valid_array(@loadary),'rollback, the works');

########################################################################
########################################################################
#
#	MULTITHREADED: 2 THREADS
#
########################################################################
########################################################################

my $thrd1 = threads->create(\&read_only);
my $thrd2 = threads->create(\&read_only);
my $rc1 = $thrd1->join();
my $rc2 = $thrd2->join();
ok($rc1 && $rc2,'readonly, 2 threads');

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_only, 'second');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'writeonly, 2 threads');

$array[9] = 5;
$thrd1 = threads->create(\&write_only, 'foo bar baz');
$thrd2 = threads->create(\&read_after_write);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'read after write, 2 threads');

$array[9] = 5;
$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_after_read, 'second');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'write after read, 2 threads');

$array[9] = 5;
$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_write_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($array[9] eq 'this is stm'),'read/write + conflicting read/write');

$array[9] = 5;
$thrd1 = threads->create(\&write_read_1, 'foo bar baz');
$thrd2 = threads->create(\&write_read_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($array[9] eq 'this is stm'),'write/read + write/read');

$array[9] = 5;
$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_read_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'read/write + conflicting read/read');

$array[9] = 5;
$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_write_rollback, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($array[9] eq 'foo bar baz'),'read/write + read/write rollback');


########################################################################
########################################################################
#
#	MULTITHREADED: 4 THREADS
#
########################################################################
########################################################################

$scalar = 'hello world';

$thrd1 = threads->create(\&read_only);
$thrd2 = threads->create(\&read_only);
my $thrd3 = threads->create(\&read_only);
my $thrd4 = threads->create(\&read_only);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
my $rc3 = $thrd1->join();
my $rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'readonly, 4 threads');

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_only, 'second');
$thrd3 = threads->create(\&write_only, 'third');
$thrd4 = threads->create(\&write_only, 'fourth');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
$rc3 = $thrd1->join();
$rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'writeonly, 4 threads');

}

sub valid_array {
	return undef unless ($#array == $#_);
	foreach (0..$#_) {
		return undef unless ($array[$_] == $_[$_]);
	}
	return 1;
}

sub deleted_array {
	return undef unless ($#array == $#loadary);
	return undef if defined($array[$_[0]]);
	my $i = 0;
	foreach (@loadary) {
		$i++, next if ($i == $_[0]);
		return undef unless ($array[$i++] == $_);
	}
	return 1;
}

sub replaced_array {
	return undef unless ($#array == $#loadary);
	my $i = 1;
	foreach (@array) {
		return undef unless ($_ eq "replaced $i");
		$i++;
	}
	return 1;
}

sub read_only {
	my $privscalar;
	my $restarted = 0;
	sleep 3;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $array[9];
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar == 5));
}

sub write_only {
	my $privscalar = shift;
	my $restarted = 0;
	sleep 2;
	my $rc = sociable_begin_work(
		sub {
			$array[9] = $privscalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_after_write {
	my $privscalar;
	my $restarted = 0;
	sleep 4;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $array[9];
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar == 5));
}

sub write_after_read {
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $array[9];
			sleep 4;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub read_write_1 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $array[9];
			sleep 3;
			$array[9] = $input;
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return ($rc && !$restarted && !$rollback);
}

sub read_write_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $array[9];
			sleep 5;
			$array[9] = $input;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub write_read_1{
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$array[9] = $input;
			sleep 3;
			$privscalar = $array[9];
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub write_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$array[9] = $input;
			sleep 5;
			$privscalar = $array[9];
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_write_rollback {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $array[9];
			sleep 5;
			$array[9] = $input;
			die "read/write rollback";
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return (!$rc && $restarted && $rollback);
}

sub read_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $array[9];
			sleep 5;
			$privscalar = $array[9];
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

1;
