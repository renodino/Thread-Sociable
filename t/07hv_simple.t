use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 16;

use Thread::Sociable;

### Start of Testing ###

my %hash;
socialize(%hash);
$hash{"foo"} = "bar";
ok($hash{"foo"} eq "bar","Check hash get");
threads->create(sub { $hash{"bar"} = "thread1"})->join();
threads->create(sub { ok($hash{"bar"} eq "thread1", "Check thread get and write")})->join();
{
    my $foo = delete($hash{"bar"});
    ok( $foo eq "thread1", "Check delete, want 'thread1' got '$foo'");
    $foo = delete($hash{"bar"});
    ok( !defined $foo, "Check delete on empty value");
}
ok( keys %hash == 1, "Check keys");
$hash{"1"} = 1;
$hash{"2"} = 2;
$hash{"3"} = 3;
ok( keys %hash == 4, "Check keys");
ok( exists($hash{"1"}), "Exist on existing key");
ok( !exists($hash{"4"}), "Exists on non existing key");
my %seen;
foreach my $key ( keys %hash) {
    $seen{$key}++;
}
ok( $seen{1} == 1, "Keys..");
ok( $seen{2} == 1, "Keys..");
ok( $seen{3} == 1, "Keys..");
ok( $seen{"foo"} == 1, "Keys..");

keys %hash = 13;
pass("set hash size");

# bugid #24407: the stringification of the numeric 1 got allocated to the
# wrong thread memory pool, which crashes on Windows.
ok( exists $hash{1}, "Check numeric key");

threads->create(sub { %hash = () })->join();
ok( keys %hash == 0, "Check clear");

ok( is_sociable(%hash), "Check for sociability");

# EOF
