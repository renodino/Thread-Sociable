use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}


use threads;
use Test::More tests => 8;
use Thread::Sociable;
sociable_disable_debug();

### Start of Testing ###

my $test = "bar";
socialize($test);
ok($test eq "bar","Test magic share fetch");
$test = "foo";
ok($test eq "foo","Test magic share assign");

#my $c = Thread::Sociable::_refcnt($test);
threads->create(
                sub {
                    ok($test eq "foo","Test magic share fetch after thread");
                    $test = "baz";
                    })->join();


ok($test eq "baz","Test that value has changed in another thread");
$test = "barbar";
ok(length($test) == 6, "Check length code; $test length is " . length($test));
threads->create(sub { $test = "barbarbar" })->join;
ok(length($test) == 9, "Check length code after different thread modified it; length is " . length($test));
threads->create(sub { undef($test)})->join();
ok(!defined($test), "Check undef value");

ok(is_sociable($test), "Check for sociability");

# EOF
