use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More skip_all => 'Sociable ties not yet implemented';
#use Test::More tests => 53;
use Thread::Sociable;
use TiedApartment;
pass('Loaded');
sociable_disable_debug();

#
#	0. create apartment thread
#	1. tie scalar in apt. thread
#	2. tie array in apt. thread
#	3. tie hash in apt. thread
#	4. write to scalar
#	5. read from scalar
#	6. assign values to array
#	7. read values from array
#	8. unshift onto array
#	9. shift from array
#	10. push onto array
#	11. pop from array
#	12. splice into array
#	13. add tuples to hash
#	14. read tuples from hash
#
### Start of Testing ###

my $scalar : Sociable;
my @array : Sociable;
my %hash : Sociable;

#
#	create a async/closure test object
#
my $tiedapt = Thread::Sociable::Apartment->new(
	AptClass => 'TiedApartment',
	AptTimeout => 10,
	Scalar => \$scalar,
	Array => \@array,
	Hash => \%hash,
);

#	4. write to scalar
#	5. read from scalar
#	6. assign values to array
#	7. read values from array
#	8. unshift onto array
#	9. shift from array
#	10. push onto array
#	11. pop from array
#	12. splice into array
#	13. add tuples to hash
#	14. read tuples from hash

$scalar = 'abcdefghijklmnop';
my $aptreport = $tiedapt->reportLastTiedOp();
ok(($aptreport eq 'STORE "abcdefghijklmnop" to scalar'), 'write to tied scalar');

my $result = $scalar;
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result eq 'ponmlkjihgfedcba') && ($aptreport eq 'FETCHED "ponmlkjihgfedcba" from scalar'), 'read tied scalar');

@array = (1..20);
$aptreport = $tiedapt->reportLastTiedOp();
ok(($aptreport eq 'STORE 20 to array'), 'write to tied array');

$result = join(',', @array);
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result eq join(',', reverse 1..20)) && ($aptreport eq 'FETCH 1 from array'), 'read tied array');

unshift @array, 'abcde';
$aptreport = $tiedapt->reportLastTiedOp();
ok(($aptreport eq 'UNSHIFT "abcde" onto array'), 'unshift tied array');

$result = scalar @array;
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result == 21) && ($aptreport eq 'STORESIZE of array'), 'get tied array length');

$result = shift @array;
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result == 'edcba') && ($aptreport eq 'SHIFT of array'), 'shift tied array');

push @array, 'wxyz';
$aptreport = $tiedapt->reportLastTiedOp();
ok(($aptreport eq 'PUSH onto array'), 'push onto tied array');

$result = pop @array;
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result == 'zyxw') && ($aptreport eq 'POP off array'), 'pop tied array');

$result = splice @array, 0, 1, qw(l m n o);
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result == 20) && ($aptreport eq 'SPLICE of array'), 'splice tied array');

$result = join(',', @array[0..3]);
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result eq 'o,n,m,l') && ($aptreport eq 'FETCH l from array'), 'read spliced tied array');

%hash = ( First => 1, Second => 2, Third => 3, Fourth => 4 );
$aptreport = $tiedapt->reportLastTiedOp();
ok(($aptreport eq 'STORE 4 to key Fourth'), 'write to tied hash');

my ($k, $v);

my %expected = ( First => 101, Second => 102, Third => 103, Fourth => 104);

foreach (qw(First Second Third Fourth)) {
	$result = $hash{$_};
	$aptreport = $tiedapt->reportLastTiedOp();
	ok(($aptreport eq "FETCH $expected{$_} from key $_"), 'read from tied hash');
}

$result = join(',', sort keys %hash);
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result eq 'First,Fourth,Second,Third') && ($aptreport eq "NEXTKEY"), 'keys from tied hash');

$result = delete $hash{Fourth};
$aptreport = $tiedapt->reportLastTiedOp();
ok(($result == 204) && ($aptreport eq "DELETE"), 'delete from tied hash');

$tiedapt->stop();

