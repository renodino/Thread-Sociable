use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
    push @INC, './t';
}

use threads;
use Test::More;
use Thread::Sociable;

### Start of Testing ###
#
#	execute SV, AV, HV test suites using locked, lazy protocols
#
use SvSTM;
use AvSTM;
use HvSTM;

plan tests => 3 + (SvSTM->tests() + AvSTM->tests() + HvSTM->tests());

ok(sociable_stm_locked(),'set STM locked commit');
ok(sociable_stm_lazy(),'set STM lazy detect');
ok((sociable_stm_protocol() eq "locked lazy"),'get updated STM protocols');

SvSTM->run();

AvSTM->run();

HvSTM->run();