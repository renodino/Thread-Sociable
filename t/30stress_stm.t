use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 53;

use Thread::Sociable;
sociable_disable_debug();

### Start of Testing ###

#
#	Need to port LFUCache, maybe RBTree tests from RSTM test suites ?
#