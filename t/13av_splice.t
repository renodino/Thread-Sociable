use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 88;

use Thread::Sociable;

sub cmp_types {
	my ($foo, $scalar, $ary, $ary_result, $hash, $hash_result, $size, $offset) = @_;
	return (scalar @$foo == $size) &&
		($foo->[$offset] eq 'a') &&
			(ref $foo->[$offset + 1] eq 'SCALAR') && (${$foo->[$offset + 1]} == $$scalar) &&
			(ref $foo->[$offset + 2] eq 'ARRAY') && (scalar @{$foo->[$offset + 2]} == scalar @$ary) && (join(',', @{$foo->[$offset + 2]}) eq $ary_result) &&
			(ref $foo->[$offset + 3] eq 'HASH') && (join(',', (map "$_,$foo->[$offset + 3]{$_}", sort keys %{$foo->[$offset + 3]})) eq $hash_result) &&
			(!defined($foo->[$offset + 4]);
}

### Start of Testing ###
#
#	test cases:
#	1. empty array: do both list and scalar context
#		a. splice @foo
#		b. splice @foo, 10
#		c. splice @foo, -10
#		d. splice @foo, 0, 10
#		e. splice @foo, 0, -10
#		f. splice @foo, 10, 10
#		g. splice @foo, -10, 10
#		h. splice @foo, 0, -10
#		i. splice @foo, 10, -10
#		j. splice @foo, -10, -10
#		k. splice @foo, 0, 0, 'a', 'b', 'c', 'd'
#		l. splice @foo, 0, 10, 'a', 'b', 'c', 'd'
#		m. splice @foo, 0, -10, 'a', 'b', 'c', 'd'
#		n. splice @foo, 10, 10, 'a', 'b', 'c', 'd'
#		o. splice @foo, -10, 10, 'a', 'b', 'c', 'd'
#		p. splice @foo, -10, -10, 'a', 'b', 'c', 'd'
#		q. splice @foo, 0, 0, 'a', \$scalar, \@ary, \%hash, undef
#		r. splice @foo, 0, 10, 'a', \$scalar, \@ary, \%hash, undef
#		s. splice @foo, 0, -110, 'a', \$scalar, \@ary, \%hash, undef
#		t. splice @foo, 10, 10, 'a', \$scalar, \@ary, \%hash, undef
#		u. splice @foo, 10, -10, 'a', \$scalar, \@ary, \%hash, undef
#		v. splice @foo, -10, 70, 'a', \$scalar, \@ary, \%hash, undef
#		w. splice @foo, -10, -110, 'a', \$scalar, \@ary, \%hash, undef
#	2. filled array: @foo = (1..100);
#		a. splice @foo
#		b. splice @foo, 10
#		c. splice @foo, -10
#		d. splice @foo, 0, 10
#		e. splice @foo, 0, -10
#		f. splice @foo, 10, 10
#		g. splice @foo, -10, 10
#		h. splice @foo, 0, -10
#		i. splice @foo, 10, -10
#		j. splice @foo, -10, -10
#		k. splice @foo, 0, 0, 'a', \$scalar, \@ary, \%hash, undef
#		l. splice @foo, 0, 10, 'a', \$scalar, \@ary, \%hash, undef
#		m. splice @foo, 0, -110, 'a', \$scalar, \@ary, \%hash, undef
#		n. splice @foo, 10, 10, 'a', \$scalar, \@ary, \%hash, undef
#		o. splice @foo, 10, -10, 'a', \$scalar, \@ary, \%hash, undef
#		p. splice @foo, -10, 70, 'a', \$scalar, \@ary, \%hash, undef
#		q. splice @foo, -10, -110, 'a', \$scalar, \@ary, \%hash, undef
#	3. splice in thread 1, verify and unsplice in thread 2, verify unsplice in thread 1

my @foo;
socialize(@foo);
ok(1,'sociable @foo');
my @bar = splice @foo;
ok(!(scalar @bar),'list splice @foo');
my $bar = splice @foo;
ok(!defined($bar),'scalar splice @foo');

@bar = splice @foo, 0;
ok(!(scalar @bar),'list splice @foo, 0');
$bar = splice @foo, 0;
ok(!defined($bar),'scalar splice @foo, 0');

@bar = splice @foo, 10;
ok(!(scalar @bar),'list splice @foo, 10');
$bar = splice @foo, 10;
ok(!defined($bar),'scalar splice @foo, 10');

@bar = splice @foo, -10;
ok(!(scalar @bar),'list splice @foo, -10');
$bar = splice @foo, -10;
ok(!defined($bar),'scalar splice @foo, -10');

@bar = splice @foo, 0, 0;
ok(!(scalar @bar),'list splice @foo, 0, 0');
$bar = splice @foo, 0, 0;
ok(!defined($bar),'scalar splice @foo, 0, 0');

@bar = splice @foo, 0, 10;
ok(!(scalar @bar),'list splice @foo, 0, 10');
$bar = splice @foo, 0, 10;
ok(!defined($bar),'scalar splice @foo, 0, 10');

@bar = splice @foo, 0, -10;
ok(!(scalar @bar),'list splice @foo, 0, -10');
$bar = splice @foo, 0, -10;
ok(!defined($bar),'scalar splice @foo, 0, -10');

@bar = splice @foo, 10, 10;
ok(!(scalar @bar),'list splice @foo, 10, 10');
$bar = splice @foo, 10, 10;
ok(!defined($bar),'scalar splice @foo, 10, 10');

@bar = splice @foo, 10, -10;
ok(!(scalar @bar),'list splice @foo, 10, -10');
$bar = splice @foo, 10, -10;
ok(!defined($bar),'scalar splice @foo, 10, -10');

@bar = splice @foo, -10, 10;
ok(!(scalar @bar),'list splice @foo, -10, 10');
$bar = splice @foo, -10, 10;
ok(!defined($bar),'scalar splice @foo, -10, 10');

@bar = splice @foo, -10, -10;
ok(!(scalar @bar),'list splice @foo, -10, -10');
$bar = splice @foo, -10, -10;
ok(!defined($bar),'scalar splice @foo, -10, -10');

@bar = splice @foo, 0, 0, qw(a b c d);
ok( ((scalar @bar == 0) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')), 'list splice @foo, 0, 0, qw(a b c d)');
@foo = ();
$bar = splice @foo, 0, 0, qw(a b c d);
ok(((!defined($bar)) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'scalar splice @foo, 0, 0, qw(a b c d)');

@foo = ();
@bar = splice @foo, 0, 10, qw(a b c d);
ok(((scalar @bar == 0) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'list splice @foo, 0, 10', qw(a b c d));
@foo = ();
$bar = splice @foo, 0, 10, qw(a b c d);
ok(((!defined($bar)) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'scalar splice @foo, 0, 10', qw(a b c d));

@foo = ();
@bar = splice @foo, 0, -10, qw(a b c d);
ok(((scalar @bar == 0) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'list splice @foo, 0, -10', qw(a b c d));
@foo = ();
$bar = splice @foo, 0, -10, qw(a b c d);
ok(((!defined($bar)) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'scalar splice @foo, 0, -10, qw(a b c d)');

@foo = ();
@bar = splice @foo, 10, 10, qw(a b c d);
ok(((scalar @bar == 0) && (scalar @foo == 14) && (join(',', @foo) eq 'a,b,c,d')),'list splice @foo, 10, 10, qw(a b c d)');
@foo = ();
$bar = splice @foo, 10, 10, qw(a b c d);
ok(((!defined($bar)) && (scalar @foo == 14) && (join(',', @foo) eq 'a,b,c,d')),'scalar splice @foo, 10, 10, qw(a b c d)');

@foo = ();
@bar = splice @foo, 10, -10, qw(a b c d);
ok(((scalar @bar == 0) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'list splice @foo, 10, -10, qw(a b c d)');
@foo = ();
$bar = splice @foo, 10, -10, qw(a b c d);
ok(((!defined($bar)) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'scalar splice @foo, 10, -10', qw(a b c d));

@foo = ();
@bar = splice @foo, -10, 10, qw(a b c d);
ok(((scalar @bar == 0) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'list splice @foo, -10, 10', qw(a b c d));
@foo = ();
$bar = splice @foo, -10, 10, qw(a b c d);
ok(((!defined($bar)) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'scalar splice @foo, -10, 10', qw(a b c d));

@foo = ();
@bar = splice @foo, -10, -10, qw(a b c d);
ok(((scalar @bar == 0) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'list splice @foo, -10, -10', qw(a b c d));
@foo = ();
$bar = splice @foo, -10, -10, qw(a b c d);
ok(((!defined($bar)) && (scalar @foo == 4) && (join(',', @foo) eq 'a,b,c,d')),'scalar splice @foo, -10, -10', qw(a b c d));

@foo = ();
my $scalar : Sociable = 10;
my @ary : Sociable = (1..5);
my %hash : Sociable =  ( first => 1, second => 2, third => 3, fourth => 4);
my $ary_result = join(',', @ary);
my $hash_result = join(',', (map "$_,$hash{$_}", sort keys %hash));
@bar = splice @foo, 0, 0, ('a', \$scalar, \@ary, \%hash, undef);
ok( ((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)), 'list splice @foo, 0, 0, ("a", \$scalar, \@ary, \%hash, undef)');
@foo = ();
$bar = splice @foo, 0, 0, ("a", \$scalar, \@ary, \%hash, undef);
ok(((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'scalar splice @foo, 0, 0, ("a", \$scalar, \@ary, \%hash, undef)');

@foo = ();
@bar = splice @foo, 0, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok(((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'list splice @foo, 0, 10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = ();
$bar = splice @foo, 0, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok(((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'scalar splice @foo, 0, 10', ("a", \$scalar, \@ary, \%hash, undef));

@foo = ();
@bar = splice @foo, 0, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok(((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'list splice @foo, 0, -10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = ();
$bar = splice @foo, 0, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok(((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'scalar splice @foo, 0, -10, ("a", \$scalar, \@ary, \%hash, undef)');

@foo = ();
@bar = splice @foo, 10, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok(((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'list splice @foo, 10, 10, ("a", \$scalar, \@ary, \%hash, undef)');
@foo = ();
$bar = splice @foo, 10, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok(((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'scalar splice @foo, 10, 10, ("a", \$scalar, \@ary, \%hash, undef)');

@foo = ();
@bar = splice @foo, 10, -10, ("a", \$scalar, \@ary, \%hash, undef);
ok(((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'list splice @foo, 10, -10, ("a", \$scalar, \@ary, \%hash, undef)');
@foo = ();
$bar = splice @foo, 10, -10, ("a", \$scalar, \@ary, \%hash, undef);
ok(((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'scalar splice @foo, 10, -10', ("a", \$scalar, \@ary, \%hash, undef));

@foo = ();
@bar = splice @foo, -10, 70, ("a", \$scalar, \@ary, \%hash, undef);
ok(((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'list splice @foo, -10, 10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = ();
$bar = splice @foo, -10, 70, ("a", \$scalar, \@ary, \%hash, undef);
ok(((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'scalar splice @foo, -10, 10', ("a", \$scalar, \@ary, \%hash, undef));

@foo = ();
@bar = splice @foo, -10, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok(((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'list splice @foo, -10, -10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = ();
$bar = splice @foo, -10, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok(((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0)),'scalar splice @foo, -10, -10', ("a", \$scalar, \@ary, \%hash, undef));

##########################################################################################
#
#	now with existing data
#
##########################################################################################

@foo = (1..50);
@bar = splice @foo;
ok((scalar @bar == 50) && (join(',', @bar) eq join(',', 1..50)) && (!scalar @foo),'list splice @foo(1..50)');
@foo = (1..50);
$bar = splice @foo;
ok(defined($bar) && ($bar == 50) && (!scalar @foo),'scalar splice @foo(1..50)');

@foo = (1..50);
@bar = splice @foo, 0;
ok((scalar @bar == 50) && (join(',', @bar) eq join(',', 1..50)) && (!scalar @foo),'list splice @foo(1..50), 0');
@foo = (1..50);
$bar = splice @foo, 0;
ok(defined($bar) && ($bar == 50) && (!scalar @foo),'scalar splice @foo(1..50), 0');

@foo = (1..50);
@bar = splice @foo, 10;
ok((scalar @bar == 41) && (join(',', @bar) eq join(',', 10..50)) && (scalar @foo == 10),'list splice @foo(1..50), 10');
@foo = (1..50);
$bar = splice @foo, 10;
ok(defined($bar) && ($bar == 50) && (scalar @foo == 10),'scalar splice @foo(1..50), 10');

@foo = (1..50);
@bar = splice @foo, -10;
ok((scalar @bar == 10) && (join(',', @bar) eq join(',', 41..50)) && (scalar @foo == 40),'list splice @foo(1..50), -10');
@foo = (1..50);
$bar = splice @foo, -10;
ok(defined($bar) && ($bar == 50) && (scalar @foo == 40),'scalar splice @foo(1..50), -10');

@foo = (1..50);
@bar = splice @foo, 0, 0;
ok((scalar @bar == 50) && (join(',', @bar) eq join(',', 1..50)) && (!scalar @foo),'list splice @foo(1..50), 0, 0');
@foo = (1..50);
$bar = splice @foo, 0, 0;
ok(defined($bar) && ($bar == 50) && (!scalar @foo),'scalar splice @foo(1..50), 0, 0');

@foo = (1..50);
@bar = splice @foo, 0, 10;
ok((scalar @bar == 10) && (join(',', @bar) eq join(',', 1..10)) && (scalar @foo == 40),'list splice @foo(1..50), 0, 10');
@foo = (1..50);
$bar = splice @foo, 0, 10;
ok(defined($bar) && ($bar == 10) && (scalar @foo == 40),'scalar splice @foo(1..50), 0, 10');

@foo = (1..50);
@bar = splice @foo, 0, -10;
ok((scalar @bar == 40) && (join(',', @bar) eq join(',', 1..40)) && (scalar @foo == 10) && (join(',', @foo) eq join(',', 41..50)),'list splice @foo(1..50), 0, -10');
@foo = (1..50);
$bar = splice @foo, 0, -10;
ok(defined($bar) && ($bar == 40) && (scalar @foo == 10),'scalar splice @foo(1..50), 0, -10');

@foo = (1..50);
@bar = splice @foo, 10, 10;
ok((scalar @bar == 10) && (join(',', @bar) eq join(',', 11..20)) && (scalar @foo == 40) && (join(',', @foo) eq join(',', 1..10, 21..50)),'list splice @foo(1..50), 10, 10');
@foo = (1..50);
$bar = splice @foo, 10, 10;
ok(defined($bar) && ($bar == 20) && (scalar @foo == 40),'scalar splice @foo(1..50), 10, 10');

@foo = (1..50);
@bar = splice @foo, 10, -10;
ok((scalar @bar == 30) && (join(',', @bar) eq join(',', 11..40)) && (scalar @foo == 20) && (join(',', @foo) eq join(',', 1..10, 41..50)),'list splice @foo(1..50), 10, -10');
@foo = (1..50);
$bar = splice @foo, 10, -10;
ok(defined($bar) && ($bar == 40) && (scalar @foo == 20),'scalar splice @foo(1..50), 10, -10');

@foo = (1..50);
@bar = splice @foo, -10, 10;
ok((scalar @bar == 10) && (join(',', @bar) eq join(',', 41..50)) && (scalar @foo == 40) && (join(',', @foo) eq join(',', 1..40)),'list splice @foo(1..50), -10, 10');
@foo = (1..50);
$bar = splice @foo, -10, 10;
ok(defined($bar) && ($bar == 50) && (scalar @foo == 40),'scalar splice @foo(1..50), -10, 10');

@foo = (1..50);
@bar = splice @foo, -10, -10;
ok((scalar @bar == 0) && (scalar @foo == 50) && (join(',', @foo) eq join(',', 1..50)),'list splice @foo(1..50), -10, -10');
@foo = (1..50);
$bar = splice @foo, -10, -10;
ok((!defined($bar)) && (scalar @foo == 50),'scalar splice @foo(1..50), -10, -10');

#
#	!!!FINISH HACKING TESTS FROM HERE!!!
#
#		q. splice @foo, 0, 0, 'a', \$scalar, \@ary, \%hash, undef
#		r. splice @foo, 0, 10, 'a', \$scalar, \@ary, \%hash, undef
#		s. splice @foo, 0, -110, 'a', \$scalar, \@ary, \%hash, undef
#		t. splice @foo, 10, 10, 'a', \$scalar, \@ary, \%hash, undef
#		u. splice @foo, 10, -10, 'a', \$scalar, \@ary, \%hash, undef
#		v. splice @foo, -10, 70, 'a', \$scalar, \@ary, \%hash, undef
#		w. splice @foo, -10, -110, 'a', \$scalar, \@ary, \%hash, undef

@foo = (1..50);
@bar = splice @foo, 0, 0, ('a', \$scalar, \@ary, \%hash, undef);
ok( (scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 55, 0), 'list splice @foo(1..50), 0, 0, ("a", \$scalar, \@ary, \%hash, undef)');
@foo = (1..50);
$bar = splice @foo, 0, 0, ("a", \$scalar, \@ary, \%hash, undef);
ok((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 55, 0),'scalar splice @foo(1..50), 0, 0, ("a", \$scalar, \@ary, \%hash, undef)');

@foo = (1..50);
@bar = splice @foo, 0, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok((scalar @bar == 10) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 45, 0),'list splice @foo(1..50), 0, 10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = (1..50);
$bar = splice @foo, 0, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok((defined($bar) && ($bar == 10) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 45, 0)),'scalar splice @foo(1..50), 0, 10', ("a", \$scalar, \@ary, \%hash, undef));

@foo = (1..50);
@bar = splice @foo, 0, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 55, 0),'list splice @foo(1..50), 0, -10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = (1..50);
$bar = splice @foo, 0, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 55, 0),'scalar splice @foo(1..50), 0, -10, ("a", \$scalar, \@ary, \%hash, undef)');

@foo = (1..50);
@bar = splice @foo, 10, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok((scalar @bar == 10) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 45, 10),'list splice @foo(1..50), 10, 10, ("a", \$scalar, \@ary, \%hash, undef)');
@foo = (1..50);
$bar = splice @foo, 10, 10, ("a", \$scalar, \@ary, \%hash, undef);
ok(defined($bar) && ($bar == 20) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 45, 10),'scalar splice @foo(1..50), 10, 10, ("a", \$scalar, \@ary, \%hash, undef)');

@foo = (1..50);
@bar = splice @foo, 10, -10, ("a", \$scalar, \@ary, \%hash, undef);
ok((scalar @bar == 30) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 25, 10)),'list splice @foo(1..50), 10, -10, ("a", \$scalar, \@ary, \%hash, undef)');
@foo = (1..50);
$bar = splice @foo, 10, -10, ("a", \$scalar, \@ary, \%hash, undef);
ok(defined($bar) && ($bar == 40) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 25, 10)),'scalar splice @foo(1..50), 10, -10', ("a", \$scalar, \@ary, \%hash, undef));

@foo = (1..50);
@bar = splice @foo, -10, 70, ("a", \$scalar, \@ary, \%hash, undef);
ok((scalar @bar == 10) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 45, 40)),'list splice @foo(1..50), -10, 10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = (1..50);
$bar = splice @foo, -10, 70, ("a", \$scalar, \@ary, \%hash, undef);
ok(defined($bar) && ($bar == 50) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 45, 40)),'scalar splice @foo(1..50), -10, 10', ("a", \$scalar, \@ary, \%hash, undef));

@foo = (1..50);
@bar = splice @foo, -10, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok((scalar @bar == 0) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 55, 0)),'list splice @foo(1..50), -10, -10', ("a", \$scalar, \@ary, \%hash, undef));
@foo = (1..50);
$bar = splice @foo, -10, -110, ("a", \$scalar, \@ary, \%hash, undef);
ok((!defined($bar)) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 55, 0)),'scalar splice @foo(1..50), -10, -10', ("a", \$scalar, \@ary, \%hash, undef));


#	3. splice in thread 1, verify and unsplice in thread 2, verify unsplice in thread 1
# EOF

@foo = (1..50);
my @baz : Sociable = ();

my $thrd = thread->create(\&splicer);
sleep 3;
{
	lock(@foo);
	ok((scalar @baz == 30) && cmp_types(\@foo, $scalar, \@ary, $ary_result, \%hash, $hash_result, 25, 10)),'threaded splice @foo(1..50), 10, -10, ("a", \$scalar, \@ary, \%hash, undef)');

	@baz = splice @foo, 10, 5, @baz;
}
$thrd->join();


sub splicer {
	{
		lock(@foo);
		@baz = splice @foo, 10, -10, ("a", \$scalar, \@ary, \%hash, undef);
	}
	sleep 3;
	{
		lock(@foo);
		ok((scalar @baz == 5) && cmp_types(\@baz, $scalar, \@ary, $ary_result, \%hash, $hash_result, 5, 0) &&
			(scalar @foo == 50) && (join(',', @foo) eq join(',', 1..50)), 'threaded unsplice @foo(1..50), 10, -10, ("a", \$scalar, \@ary, \%hash, undef)');
	}
	sleep 3;
}