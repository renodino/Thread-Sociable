use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 14;

use Thread::Sociable;

### Start of Testing ###

sociable_disable_debug();

my $sv;
socialize($sv);
$sv = "hi";

my @av;
socialize(@av);
push(@av, $sv);

ok( $av[0] eq "hi", 'Array holds value');

push(@av, "foo");
ok( $av[1] eq 'foo', 'Array holds 2nd value');

my $av = threads->create(sub {
    my $av;
    my @av2;
    socialize($av);
    socialize(@av2);
    $av = \@av2;
    push(@$av, "bar");
    push(@$av, \@av);
    return ($av);
})->join();

ok($av->[0] eq "bar", 'Thread added to array');
ok($av->[1]->[0] eq 'hi', 'Shared in shared');

#
#	we seem to cause free to wrong pool here when we free up
#	the dummy tie object in sociable_new_private...???
#
threads->create(sub { $av[0] = "hihi"; })->join();

ok($av->[1]->[0] eq 'hihi', 'Replaced shared in shared');

ok( pop(@{$av->[1]}) eq "foo", 'Pop shared array');

ok( scalar(@{$av->[1]}) == 1, 'Array size');

threads->create(sub { @$av = () })->join();

threads->create(sub { ok( scalar @$av == 0, 'Array cleared in thread'); })->join();

threads->create(sub {
    unshift(@$av, threads->create(sub {
                        my @array;
                        socialize(@array);
                        return (\@array);
                  })->join());
})->join();

ok( ref($av->[0]) eq 'ARRAY', 'Array in array');

threads->create(sub { push @{$av->[0]}, \@av })->join();
threads->create(sub { $av[0] = 'testtest'})->join();
threads->create(sub { ok( $av->[0]->[0]->[0] eq 'testtest', 'Nested'); })->join();

ok( is_sociable($sv), "Check for sociability");
ok( is_sociable(@av), "Check for sociability");

my $x : Sociable;
ok( is_sociable($x), "Check for sociability");

# EOF
