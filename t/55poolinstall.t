#
#	Thread::Sociable::Apartment test script
#
BEGIN {
	push @INC, './t';
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More skip_all => "Apartment threading not complete";
#use Test::More tests => 47;

use AptTestCommon;
use Thread::Sociable;
use Thread::Sociable::Queue;
use Thread::Sociable::Apartment;
use Thread::Sociable::Apartment::Server;
use Thread::Sociable::Apartment::MuxServer;
use Thread::Sociable::Apartment::Client;
use Batter;
use ThirdBase;
use Coach;

use strict;
use warnings;

$AptTestCommon::testtype = 'pooled, installed thread';

#
#	prelims: use shared test count for eventual
#	threaded tests
#
my $testno : Sociable = 1;

AptTestCommon::report_result(\$testno, 1, 'load');
#
#	create a async/closure test object
#
my $batter = Thread::Sociable::Apartment->new(
	AptClass => 'Batter',
	AptTimeout => 10
);
AptTestCommon::report_result(\$testno, defined($batter), 'simple constructor', '', $@);

unless ($batter) {
	AptTestCommon::report_result(\$testno, 'skip', 'no test object, skipping')
		foreach ($testno..$tests);
	die "Unable to continue, cannot create an object.";
}
#
#	now use an externally provided/pooled thread/TQD, which will
#	create a new TQD for us
#
my $result = Thread::Sociable::Apartment->create_pool(AptPoolSize => 4, AptMaxPending => 10);
AptTestCommon::report_result(\$testno, defined($result), 'create thread pool', '', $@);

unless ($result) {
	AptTestCommon::report_result(\$testno, 'skip', 'no object, skipping')
		foreach ($testno..$tests);
	die "Unable to continue, cannot create a thread pool.";
}
#
#	installed thread tests: create a T::A, then install our
#	main thread in a T::A which calls run()
#
my $taco = Thread::Sociable::Apartment->install(
	AptClass => 'Coach',
	AptTimeout => 10,
	AptParams => [ 'lc' ]
);

AptTestCommon::report_result(\$testno, defined($taco), 'installed constructor', '', $@);

unless ($taco) {
	AptTestCommon::report_result(\$testno, 'skip', 'no object, skipping')
		foreach ($testno..$tests);
	die "Unable to continue, cannot create an installed object.";
}
#
#	use another thread to execute tests against the mux
#
my $tqd = Thread::Sociable::Queue->new(ListenerRequired => 1);
my $thread = threads->new(\&AptTestCommon::run_thread, $tqd, \$testno);
$tqd->wait_for_listener();
$tqd->enqueue($taco, $batter, 'Coach');

Thread::Sociable::Apartment->run();

$thread->join();
