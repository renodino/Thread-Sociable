package TiedApartment;

use Thread::Sociable::Apartment;
use Thread::Sociable::Apartment::Server;

use base qw(Thread::Sociable::Apartment::Server);

use Test::More;

use strict;
use warnings;

sub new {
	my ($class, $tac) = @_;
	my $obj = bless { _ready => 0 }, $class;
	$obj->set_client($tac);
	return $obj;
}
#
#	called by main harness to check if an async operation completed
#
sub async_ready { return $_[0]->{_ready}; }
#
#	called by main harness to install object under test
#
sub set_test_object { $_[0]->{_obj} = $_[1]; }

sub remove_test_object { delete $_[0]->{_obj}; }

sub run_simple_async {
	my ($self, $testtype) = @_;

#print STDERR "run simple async closure called with ", join(', ', @_),
#	" on ", $self->{_obj}, "\n";

	$self->{_ready} = undef;
	my $obj = $self->{_obj};
	my $id = $obj->ta_async_thirdBase(
		sub {
#			print STDERR "simple async closure called with ", join(', ', @_), "\n";
			$self->{_ready} = 1;
			my $res = shift;
			ok(defined($res) && ($res eq 'thirdbase'), "async closure for $testtype");
		});
	print STDERR "can't async: $@\n" unless defined($id);
	return 1;
}

sub run_override_async {
	my ($self, $testno, $testtype) = @_;

	$self->{_ready} = undef;
	my $obj = $self->{_obj};
	my $id = $obj->ta_async_firstBase(
		sub {
			$self->{_ready} = 1;
			my $res = shift;
			ok(($res eq 'triple'), "async override closure for $testtype");
		});
	return 1;
}

sub run_inherited_async {
	my ($self, $testno, $testtype) = @_;

	$self->{_ready} = undef;
	my $obj = $self->{_obj};
	my $id = $obj->ta_async_secondBase(
		sub {
			$self->{_ready} = 1;
			my $res = shift;
			ok(($res eq 'secondbase'), "async inherited closure for $testtype");
		});
	return 1;
}

sub run_closure_args {
	my ($self, $testno, $testtype) = @_;

	$self->{_ready} = undef;
	my $obj = $self->{_obj};
	my $closure = $obj->get_closure();

	$closure->('first', 'second', 'third', 'home');

	pass("void closure w/ arguments for $testtype");

	my @results = $closure->('first', 'second', 'third', 'home');

#	print STDERR "Result is ", join(', ', @results), "\n";

	ok((($results[3] eq 'first') &&
		($results[2] eq 'second') &&
		($results[1] eq 'third') &&
		($results[0] eq 'home')), "wantarray closure w/ arguments for $testtype");

	my $result = $closure->('first', 'second', 'third', 'home');

#	print STDERR "Result is $result\n";

	ok(($result eq 'emohdrihtdnocestsrif'), "scalar closure w/ arguments for $testtype");
#
#	quick simplex test
#
	$closure = $obj->get_simplex_closure();

	$closure->('first', 'second', 'third', 'home');

	pass("simplex closure for $testtype");
	$self->{_ready} = 1;

	return 1;
}

1;
