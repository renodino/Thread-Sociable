use strict;
use warnings;
use vars qw($loaded);
BEGIN {
	unshift @INC, './t';
	my $tests = 146;
	print STDERR "
 *** Note: some tests have significant delays...
";
	$^W= 1;
	$| = 1;
	print "1..$tests\n";
}

use QTestCommon;
use QTestCommon qw(report_result);

$loaded = 1;
report_result(1, 'load module');
#
#	runs the tests
#
QTestCommon->run_test($ARGV[0] || 0, 1);
