use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 26;

use Thread::Sociable;

### Start of Testing ###

sociable_disable_debug();

my $test = 1;
socialize($test);
ok($test == 1,"IV Numeric context compare");
ok($test eq '1',"IV String context compare");
$test = 1.23456;
ok($test == 1.23456,"NV Numeric context compare");
ok($test eq '1.23456',"NV String context compare");

$test = 1234;
$test++;
ok($test == 1235,"IV increment");
$test--;
ok($test == 1234,"IV decrement");

$test += 10;
ok($test == 1244,"IV +=");
$test -= 10;
ok($test == 1234,"IV -=");

$test *= 10;
ok($test == 12340,"IV *=");
$test /= 10;
ok($test == 1234,"IV /=");
$test %= 10;
ok($test == 4,"IV %=");

$test >>= 1;
ok($test == 2,"IV >>=");

$test <<= 1;
ok($test == 4,"IV <<=");

$test |= 1;
ok($test == 5,"IV |=");

$test &= 1;
ok($test == 1,"IV &=");

$test = 1234;
ok(length($test) == 4,"length(IV)");

### NV tests ###

$test = 1234.567;
$test++;
ok($test == 1235.567,"NV increment");
$test--;
ok($test == 1234.567,"NV decrement");

$test += 10;
ok($test == 1244.567,"NV +=");
$test -= 10;
ok($test == 1234.567,"NV -=");

$test *= 10;
ok($test == 12345.67,"NV *=");
$test /= 10;
ok($test == 1234.567,"NV /=");
$test %= 10;
ok($test == 4,"NV %=");

$test = 1234.567;
ok(int($test) == 1234,"int(NV)");
ok(length($test) == 8,"length(NV)");

# EOF
