package HvSTM;

use strict;
use warnings;

use threads;
use Test::More;
use Thread::Sociable;

sub tests { return 35; }

### Start of Testing ###
#
#	no commit/restart/rollback subs
#		1. readonly
#		2. writeonly
#		3. read/write
#		4. write/read
#		5. readonly w/each, fully iterated
#		6. read/write w/ keys, fully iterated
#		7. exists
#		8. delete
#	commit, w/ commit sub, no restart/rollback sub: repeat 1-9
#	force restart, w/ restart sub, no commit/rollback sub: repeat 1-9
#	force rollback, w/ commit, restart and rollback subs: repeat 1-9
#
#	repeat w/ 2 competing threads
#	repeat w/ 4 competing threads
#

sub run {

sociable_disable_debug();

my %loadhash = qw(
First 1
Second 2
Third 3
Fourth 4
Fifth 5
Sixth 6
Seventh 7
Eighth 8
Ninth 9
Tenth 10
);

my %hash : Sociable = %loadhash;
my $privscalar;
my $rc = sociable_begin_work(
	sub {
		$privscalar = $hash{Fifth};
	});
ok($rc && ($privscalar == 5) && valid_hash(),'no restart/rollback, readonly');

$rc = sociable_begin_work(
	sub {
		$hash{Eleventh} = 11;
		$hash{Twelveth} = 12;
		$hash{Twentieth} = 20;
	});
ok($rc && valid_hash( Eleventh => 11, Twelveth => 12, Twentieth => 20),'no restart/rollback, writeonly');

$rc = sociable_begin_work(
	sub {
		$privscalar = $hash{First};
		$hash{Thirteenth} = 13;
	});
ok($rc && ($privscalar == 1) && valid_hash( Eleventh => 11, Twelveth => 12, Thirteenth => 13, Twentieth => 20),'no restart/rollback, read/write');

$rc = sociable_begin_work(
	sub {
		$hash{Fourteenth} = 14;
		$privscalar = $hash{Twentieth};
	});
ok($rc && ($privscalar == 20) && valid_hash( Eleventh => 11, Twelveth => 12, Thirteenth => 13, Fourteenth => 14, Twentieth => 20),'no restart/rollback, write/read');

%hash = %loadhash;
my $count = 0;
my ($k,$v);
$rc = sociable_begin_work(
	sub {
		$count++
			while (($k, $v) = each %hash);
	});
ok($rc && ($count == 10) && valid_hash(),'no restart/rollback, readonly, fully iterated');

$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++,
		$hash{$k} += 10
			foreach (keys %hash);
	});
ok($rc && ($count == 10) && valid_hash(Increment => 10),'no restart/rollback, read/write, fully iterated');

$count = 0;
%hash = %loadhash;
$rc = sociable_begin_work(
	sub {
		foreach (keys %loadhash) {
			$count++
				if exists $hash{$_};
		}
		$count++
			if exists $hash{Nonexistant};
	});
ok($rc && ($count == 10) && valid_hash(),'no restart/rollback, read w/ exists');

$count = 0;
$rc = sociable_begin_work(
	sub {
		foreach (keys %loadhash) {
			delete $hash{$_} if $hash{$_} & 1;
		}
		$count = scalar keys %hash;
	});
ok($rc && ($count == 5) && deleted_hash(),'no restart/rollback, read/write w/ deletes');

############ force restart, w restart sub ##################

my $restart = 1;
my $report = 0;
%hash = %loadhash;
$rc = sociable_begin_work(
	sub {
		$privscalar = $hash{Fifth};
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($privscalar == 5) && valid_hash(),'restart, readonly');

($restart, $report) = (1,0);
$rc = sociable_begin_work(
	sub {
		die "Thread::Sociable STM RESTART"
			if exists $hash{Eleventh};
		$hash{Eleventh} = 11;
		$hash{Twelveth} = 12;
		$hash{Twentieth} = 20;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && valid_hash( Eleventh => 11, Twelveth => 12, Twentieth => 20),'restart, writeonly');

($restart, $report) = (1,0);
$rc = sociable_begin_work(
	sub {
		die "Thread::Sociable STM RESTART"
			if exists $hash{Thirteenth};
		$privscalar = $hash{First};
		$hash{Thirteenth} = 13;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($privscalar == 1) && valid_hash( Eleventh => 11, Twelveth => 12, Thirteenth => 13, Twentieth => 20),'restart, read/write');

($restart, $report) = (1,0);
$rc = sociable_begin_work(
	sub {
		die "Thread::Sociable STM RESTART"
			if exists $hash{Fourteenth};
		$hash{Fourteenth} = 14;
		$privscalar = $hash{Twentieth};
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; });
ok($rc && ($report == 1) && ($privscalar == 20) && valid_hash( Eleventh => 11, Twelveth => 12, Thirteenth => 13, Fourteenth => 14, Twentieth => 20),'restart, write/read');

($restart, $report) = (1,0);
%hash = %loadhash;
$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++
			while (($k, $v) = each %hash);
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $count = 0; $report++; });
ok($rc && ($report == 1) && ($count == 10) && valid_hash(),'restart, readonly, fully iterated');

($restart, $report) = (1,0);
$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++,
		$hash{$k} += 10
			foreach (keys %hash);
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $count = 0; $report++; });
ok($rc && ($report == 1) && ($count == 10) && valid_hash(Increment => 10),'restart, read/write, fully iterated');

($restart, $report) = (1,0);
$count = 0;
%hash = %loadhash;
$rc = sociable_begin_work(
	sub {
		foreach (keys %loadhash) {
			$count++
				if exists $hash{$_};
		}
		$count++
			if exists $hash{Nonexistant};
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $count = 0; $report++; });
ok($rc && ($report == 1) && ($count == 10) && valid_hash(),'restart, read w/ exists');

($restart, $report) = (1,0);
$count = 0;
$rc = sociable_begin_work(
	sub {
		foreach (keys %loadhash) {
			delete $hash{$_} if $hash{$_} & 1;
		}
		$count = scalar keys %hash;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $count = 0; $report++; });
ok($rc && ($report == 1) && ($count == 5) && deleted_hash(),'restart, read/write w/ deletes');

############ force rollback, restart & rollback sub ##################

my $rollback = 0;
($rollback, $report) = (0,0);
%hash = %loadhash;
$rc = sociable_begin_work(
	sub {
		$privscalar = $hash{Fifth};
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($privscalar == 5) && valid_hash(),'rollback, readonly');

($rollback, $report) = (0,0);
$rc = sociable_begin_work(
	sub {
		$hash{Eleventh} = 11;
		$hash{Twelveth} = 12;
		$hash{Twentieth} = 20;
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && rollback_hash( Eleventh => 11, Twelveth => 12, Twentieth => 20),'rollback, writeonly');

($rollback, $report) = (0,0);
$rc = sociable_begin_work(
	sub {
		$privscalar = $hash{First};
		$hash{Thirteenth} = 13;
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($privscalar == 1) && rollback_hash(Thirteenth => 13),'rollback, read/write');

($rollback, $report) = (0,0);
$rc = sociable_begin_work(
	sub {
		$hash{Fourteenth} = 14;
		$privscalar = $hash{Twentieth};
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && (!defined($privscalar})) && rollback_hash(Fourteenth => 14),'rollback, write/read');

($rollback, $report) = (0,0);
%hash = %loadhash;
$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++
			while (($k, $v) = each %hash);
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 10) && valid_hash(),'rollback last, readonly, fully iterated');

($rollback, $report) = (0,0);
$count = 0;
$rc = sociable_begin_work(
	sub {
		die "force a rollback";
		$count++
			while (($k, $v) = each %hash);
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 0) && valid_hash(),'rollback first, readonly, fully iterated');

($rollback, $report) = (0,0);
$count = 0;
$rc = sociable_begin_work(
	sub {
		$count++,
		$hash{$k} += 10
			foreach (keys %hash);
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 10) && rollback_write_hash(),'rollback, read/write, fully iterated');

($rollback, $report) = (0,0);
$count = 0;
%hash = %loadhash;
$rc = sociable_begin_work(
	sub {
		foreach (keys %loadhash) {
			$count++
				if exists $hash{$_};
		}
		$count++
			if exists $hash{Nonexistant};
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 10) && rollback_hash(),'rollback, read w/ exists');

($rollback, $report) = (0,0);
$count = 0;
$rc = sociable_begin_work(
	sub {
		foreach (keys %loadhash) {
			delete $hash{$_} if $hash{$_} & 1;
		}
		$count = scalar keys %hash;
		die "force a rollback";
	},
	onRestart => sub { $report++; },
	onRollback => sub { $rollback++; });
ok($rc && ($report == 0) && ($rollback == 1) && ($count == 5) && rollback_write_hash(),'rollback, read/write w/ deletes');

########################################################################
########################################################################
#
#	MULTITHREADED: 2 THREADS
#
########################################################################
########################################################################

$scalar = 'hello world';

my $thrd1 = threads->create(\&read_only);
my $thrd2 = threads->create(\&read_only);
my $rc1 = $thrd1->join();
my $rc2 = $thrd2->join();
ok($rc1 && $rc2,'readonly, 2 threads');

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_only, 'second');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'writeonly, 2 threads');

$hash{Fifth} = 5;
$thrd1 = threads->create(\&write_only, 'foo bar baz');
$thrd2 = threads->create(\&read_after_write);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'read after write, 2 threads');

$hash{Fifth} = 5;
$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_after_read, 'second');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'write after read, 2 threads');

$hash{Fifth} = 5;
$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_write_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($scalar eq 'this is stm'),'read/write + conflicting read/write');

$hash{Fifth} = 5;
$thrd1 = threads->create(\&write_read_1, 'foo bar baz');
$thrd2 = threads->create(\&write_read_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($scalar eq 'this is stm'),'write/read + write/read');

$hash{Fifth} = 5;
$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_read_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'read/write + conflicting read/read');

$hash{Fifth} = 5;
$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_write_rollback, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($scalar eq 'foo bar baz'),'read/write + read/write rollback');


########################################################################
########################################################################
#
#	MULTITHREADED: 4 THREADS
#
########################################################################
########################################################################

$scalar = 'hello world';

$thrd1 = threads->create(\&read_only);
$thrd2 = threads->create(\&read_only);
my $thrd3 = threads->create(\&read_only);
my $thrd4 = threads->create(\&read_only);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
my $rc3 = $thrd1->join();
my $rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'readonly, 4 threads');

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_only, 'second');
$thrd3 = threads->create(\&write_only, 'third');
$thrd4 = threads->create(\&write_only, 'fourth');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
$rc3 = $thrd1->join();
$rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'writeonly, 4 threads');

}

sub valid_hash {
	my %extra = @_;
	my $incr = delete $extra{Increment} || 0;
	foreach (keys %loadhash) {
		return undef unless exists $hash{$_} && ($hash{$_} == $loadhash{$_} + $incr);
	}
	if (%extra) {
		foreach (keys %extra) {
			return undef unless exists $hash{$_} && ($hash{$_} == $extra{$_} + $incr);
		}
	}
	return 1;
}

sub deleted_hash {
	foreach (keys %hash) {
		return undef if ($hash{$_} & 1);
		return undef unless exists $loadhash{$_};
	}
	return 1;
}

sub rollback_hash {
	my %extra = @_;
	foreach (keys %extra) {
		return undef if exists $hash{$_};
	}
	return 1;
}

sub rollback_write_hash {
	foreach (keys %loadhash) {
		return undef unless exists $hash{$_} && ($loadhash{$_} == $hash{$_});
	}
	return 1;
}

sub read_only {
	my $privscalar;
	my $restarted = 0;
	sleep 3;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $hash{Fifth};
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar == 5));
}

sub write_only {
	my $privscalar = shift;
	my $restarted = 0;
	sleep 2;
	my $rc = sociable_begin_work(
		sub {
			$hash{Fifth} = $privscalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_after_write {
	my $privscalar;
	my $restarted = 0;
	sleep 4;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $hash{Fifth};
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar == 5));
}

sub write_after_read {
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $hash{Fifth};
			sleep 4;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub read_write_1 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $hash{Fifth};
			sleep 3;
			$hash{Fifth} = $input;
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return ($rc && !$restarted && !$rollback);
}

sub read_write_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $hash{Fifth};
			sleep 5;
			$hash{Fifth} = $input;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub write_read_1{
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$hash{Fifth} = $input;
			sleep 3;
			$privscalar = $hash{Fifth};
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub write_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$hash{Fifth} = $input;
			sleep 5;
			$privscalar = $hash{Fifth};
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_write_rollback {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $hash{Fifth};
			sleep 5;
			$hash{Fifth} = $input;
			die "read/write rollback";
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return (!$rc && $restarted && $rollback);
}

sub read_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $hash{Fifth};
			sleep 5;
			$privscalar = $hash{Fifth};
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

1;