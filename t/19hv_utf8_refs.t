use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 16;

use Thread::Sociable;

### Start of Testing ###

sociable_disable_debug();

my $foo;
socialize($foo);
my %foo;
socialize(%foo);
$foo{"foo"} = \$foo;
ok( !defined ${$foo{foo}}, "Check deref");

#sociable_enable_debug();

$foo = "test";
ok( ${$foo{foo}} eq "test", "Check deref after assign");
threads->create(sub{${$foo{foo}} = "test2";})->join();
ok( $foo eq "test2", "Check after assign in another thread");
my $bar = delete($foo{foo});
ok( $$bar eq "test2", "check delete");
threads->create( sub {
   my $test;
   socialize($test);
   $test = "thread3";
   $foo{test} = \$test;
   })->join();
ok( ${$foo{test}} eq "thread3", "Check reference created in another thread");
my $gg = $foo{test};
$$gg = "test";
ok( ${$foo{test}} eq "test", "Check reference");
my $gg2 = delete($foo{test});
ok( is_sociable($$gg) == is_sociable($$gg2),
       sprintf("Check we get the same thing (%x vs %x)",
       is_sociable($$gg),is_sociable($$gg2)));
ok( $$gg eq $$gg2, "And check the values are the same");
ok( keys %foo == 0, "And make sure we realy have deleted the values");

{
    my (%hash1, %hash2);
    socialize(%hash1);
    socialize(%hash2);
    $hash1{hash} = \%hash2;
    $hash2{"bar"} = "foo";
	my $thash = $hash1{hash};
    ok( $hash1{hash}->{bar} eq "foo", "Check hash references work");
    threads->create(sub { $hash2{"bar2"} = "foo2"})->join();
    ok( $hash1{hash}->{bar2} eq "foo2", "Check hash references work");
    threads->create(sub { my (%hash3); socialize(%hash3); $hash2{hash} = \%hash3; $hash3{"thread"} = "yes"})->join();
    ok( $hash1{hash}->{hash}->{thread} eq "yes", "Check hash created in another thread");
}

# sociable_enable_debug();

{
    my $object : Sociable = &socialize({});
    threads->new(sub {
                     bless $object, 'test1';
                 })->join;
    ok( ref($object) eq 'test1', "blessing does work");
    my %test = (object => $object);
    ok( ref($test{object}) eq 'test1', "and some more work");
    bless $object, 'test2';
    ok( ref($test{object}) eq 'test2', "reblessing works!");
}

ok( is_sociable($foo), "Check for sociability");
ok( is_sociable(%foo), "Check for sociability");

# EOF
