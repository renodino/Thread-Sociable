use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
    push @INC, './t';
}

use threads;
use Test::More;
use Thread::Sociable;

### Start of Testing ###
#
#	execute UTF8 SV, AV, HV test suites using default protocols
#
use UTF8SvSTM;
use UTF8AvSTM;
use UTF8HvSTM;

plan tests => 3 + (UTF8SvSTM->tests() + UTF8AvSTM->tests() + UTF8HvSTM->tests());

ok(sociable_stm_locked(),'set STM locked commit');
ok(sociable_stm_eager(),'set STM eager detect');
ok((sociable_stm_protocol() eq "locked eager"),'get updated STM protocols');

UTF8SvSTM->run();

UTF8AvSTM->run();

UTF8HvSTM->run();