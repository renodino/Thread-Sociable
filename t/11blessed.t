use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

package BlessTest;

sub new {
	my $class = shift;
}

sub report {
	return 'Hello from BlessTest';
}

1;

package main;

use threads;
use Test::More tests => 36;

use Thread::Sociable;

sociable_disable_debug();

### Start of Testing ###

my ($hobj, $aobj, $sobj) : Sociable;

$hobj = &socialize({});
$aobj = &socialize([]);
my $sref = \do{ my $x };
socialize($sref);
$sobj = $sref;

threads->new(sub {
                # Bless objects
                bless $hobj, 'foo';
                bless $aobj, 'bar';
                bless $sobj, 'baz';

                # Add data to objects
                $$aobj[0] = bless(&socialize({}), 'yin');
                $$aobj[1] = bless(&socialize([]), 'yang');
                $$aobj[2] = $sobj;

                $$hobj{'hash'}   = bless(&socialize({}), 'yin');
                $$hobj{'array'}  = bless(&socialize([]), 'yang');
                $$hobj{'scalar'} = $sobj;

                $$sobj = 'abc';

                # Test objects in child thread
                ok( ref($hobj) eq 'foo', "hash blessing does work");
                ok( ref($aobj) eq 'bar', "array blessing does work");
                ok( ref($sobj) eq 'baz', "scalar blessing does work");
                ok( $$sobj eq 'abc', "scalar contents okay");

                ok( ref($$aobj[0]) eq 'yin', "blessed hash in array");
                ok( ref($$aobj[1]) eq 'yang', "blessed array in array");
                ok( ref($$aobj[2]) eq 'baz', "blessed scalar in array");
                ok( ${$$aobj[2]} eq 'abc', "blessed scalar in array contents");

                ok( ref($$hobj{'hash'}) eq 'yin', "blessed hash in hash");
                ok( ref($$hobj{'array'}) eq 'yang', "blessed array in hash");
                ok( ref($$hobj{'scalar'}) eq 'baz', "blessed scalar in hash");
                ok( ${$$hobj{'scalar'}} eq 'abc', "blessed scalar in hash contents");

             })->join;

# Test objects in parent thread
ok( ref($hobj) eq 'foo', "hash blessing does work");
ok( ref($aobj) eq 'bar', "array blessing does work");
ok( ref($sobj) eq 'baz', "scalar blessing does work");
ok( $$sobj eq 'abc', "scalar contents okay");

ok( ref($$aobj[0]) eq 'yin', "blessed hash in array");
ok( ref($$aobj[1]) eq 'yang', "blessed array in array");
ok( ref($$aobj[2]) eq 'baz', "blessed scalar in array");
ok( ${$$aobj[2]} eq 'abc', "blessed scalar in array contents");

ok( ref($$hobj{'hash'}) eq 'yin', "blessed hash in hash");
ok( ref($$hobj{'array'}) eq 'yang', "blessed array in hash");
ok( ref($$hobj{'scalar'}) eq 'baz', "blessed scalar in hash");
ok( ${$$hobj{'scalar'}} eq 'abc', "blessed scalar in hash contents");

threads->new(sub {
                # Rebless objects
                bless $hobj, 'oof';
                bless $aobj, 'rab';
                bless $sobj, 'zab';

                my $data = $$aobj[0];
                bless $data, 'niy';
                $$aobj[0] = $data;
                $data = $$aobj[1];
                bless $data, 'gnay';
                $$aobj[1] = $data;

                $data = $$hobj{'hash'};
                bless $data, 'niy';
                $$hobj{'hash'} = $data;
                $data = $$hobj{'array'};
                bless $data, 'gnay';
                $$hobj{'array'} = $data;

                $$sobj = 'test';
             })->join;

# Test reblessing
ok( ref($hobj) eq 'oof', "hash reblessing does work");
ok( ref($aobj) eq 'rab', "array reblessing does work");
ok( ref($sobj) eq 'zab', "scalar reblessing does work");
ok( $$sobj eq 'test', "scalar contents okay");

ok( ref($$aobj[0]) eq 'niy', "reblessed hash in array");
ok( ref($$aobj[1]) eq 'gnay', "reblessed array in array");

print "\n*** aobj[2] class is ", ref($$aobj[2]), "\n";

ok( ref($$aobj[2]) eq 'zab', "reblessed scalar in array");
ok( ${$$aobj[2]} eq 'test', "reblessed scalar in array contents");

ok( ref($$hobj{'hash'}) eq 'niy', "reblessed hash in hash");
ok( ref($$hobj{'array'}) eq 'gnay', "reblessed array in hash");

print "\n*** hobj{scalar} class is ", ref($$hobj{'scalar'}), "\n";

ok( ref($$hobj{'scalar'}) eq 'zab', "reblessed scalar in hash");
ok( ${$$hobj{'scalar'}} eq 'test', "reblessed scalar in hash contents");

# EOF
