use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 13;
use Thread::Sociable;
sociable_disable_debug();

#
#	test some basic APIs
#
ok((sociable_stm_limit() == 100),'get default STM limit');
ok((sociable_stm_limit(3) == 100) && (sociable_stm_limit() == 3),'set STM limit');

ok((sociable_stm_timeout() == 60),'get default STM timeout');
ok((sociable_stm_limit(30) == 60) && (sociable_stm_limit() == 30),'set STM timeout');

ok((sociable_stm_protocol() eq "locked eager"),'get default STM protocols');

ok((sociable_stm_lockfree() eq "locked"),'set STM lockfree');
ok((sociable_stm_protocol() eq "lockfree eager"),'get updated STM protocols');

ok((sociable_stm_lazy() eq "eager"),'set STM lazy detect');
ok((sociable_stm_protocol() eq "lockfree lazy"),'get updated STM protocols');

ok((sociable_stm_locked() eq "lockfree"),'set STM locked commit');
ok((sociable_stm_protocol() eq "locked lazy"),'get updated STM protocols');

ok((sociable_stm_eager() eq "lazy"),'set STM eager detect');
ok((sociable_stm_protocol() eq "locked eager"),'get updated STM protocols');

