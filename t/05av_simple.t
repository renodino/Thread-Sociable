use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 44;
use Thread::Sociable;

### Start of Testing ###

my @foo;
socialize(@foo);
pass("sociable \@foo");
$foo[0] = "hi";
ok( $foo[0] eq 'hi', "Check assignment works");
$foo[0] = "bar";
ok( $foo[0] eq 'bar', "Check overwriting works");
ok( !defined $foo[1], "Check undef value");
$foo[2] = "test";
ok( $foo[2] eq "test", "Check extending the array works");
ok( !defined $foo[1], "Check undef value again");
ok( scalar(@foo) == 3, "Check the length of the array");
ok($#foo == 2, "Check last element of array");
threads->create(sub { $foo[0] = "thread1" })->join;
ok( $foo[0] eq "thread1", "Check that a value can be changed in another thread");
push(@foo, "another value");
ok( $foo[3] eq "another value", "Check that push works");
push(@foo, 1,2,3);
ok( $foo[-1] == 3, "More push");
ok( $foo[-2] == 2, "More push");
ok( $foo[4] == 1, "More push");
threads->create(sub { push @foo, "thread2" })->join();
ok( $foo[7] eq "thread2", "Check push in another thread");
unshift(@foo, "start");
ok( $foo[0] eq "start", "Check unshift");
unshift(@foo, 1,2,3);
ok( $foo[0] == 1, "Check multiple unshift");
ok( $foo[1] == 2, "Check multiple unshift");
ok( $foo[2] == 3, "Check multiple unshift");
threads->create(sub { unshift @foo, "thread3" })->join();
ok( $foo[0] eq "thread3", "Check unshift from another thread");
my $var = pop(@foo);
ok( $var eq "thread2", "Check pop");
threads->create(sub { my $foo = pop @foo; ok( $foo == 3, "Check pop works in a thread")})->join();
$var = pop(@foo);
ok( $var == 2, "Check pop after thread");
$var = shift(@foo);
ok( $var eq "thread3", "Check shift");
threads->create(sub { my $foo = shift @foo; ok( $foo  == 1, "Check shift works in a thread");
})->join();
$var = shift(@foo);
ok( $var == 2, "Check shift after thread");
{
    my @foo2;
    socialize @foo2;
    my $empty = shift @foo2;
    ok( !defined $empty, "Check shift on empty array");
    $empty = pop @foo2;
    ok( !defined $empty, "Check pop on empty array");
}
my $i = 0;
foreach my $var (@foo) {
    $i++;
}
ok( scalar @foo == $i, "Check foreach");
my $ref = \@foo;
ok( $ref->[0] == 3, "Check reference access");
threads->create(sub { $ref->[0] = "thread4"})->join();
ok( $ref->[0] eq "thread4", "Check that it works after another thread");
undef($ref);
threads->create(sub { @foo = () })->join();
ok( @foo == 0, "Check that array is empty");
ok( exists($foo[0]) == 0, "Check that zero index doesn't index");
@foo = ("sky");
ok( exists($foo[0]) == 1, "Check that zero index exists now");
ok( $foo[0] eq "sky", "And check that it also contains the right value");
$#foo = 20;
ok( $#foo eq 20, "Check sizing works");
$foo[20] = "sky";
ok( delete($foo[20]) eq "sky", "Check delete works");

threads->create(sub { delete($foo[0])})->join();
ok( !defined delete($foo[0]), "Check that delete works from a thread");

@foo = (1,2,3,4,5);

{
    my ($t1,$t2) = @foo[2,3];
    ok( $t1 == 3, "Check slice");
    ok( $t2 == 4, "Check slice again");
    my @t1 = @foo[1...4];
    ok( $t1[0] == 2, "Check slice list");
    ok( $t1[2] == 4, "Check slice list 2");
    threads->create(sub { @foo[0,1] = ("hej","hop") })->join();
    ok($foo[0] eq "hej", "Check slice assign");
}
{
    eval {
        my @t1 = splice(@foo,0,2,"hop", "hej");
    };
    ok( my $temp1 = $@ =~/Splice not implemented for sociable arrays/, "Check that the warning message is correct for non splice");
}

ok( is_sociable(@foo), "Check for sociability");

# EOF
