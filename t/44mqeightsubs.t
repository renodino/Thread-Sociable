use strict;
use warnings;
use vars qw($loaded);
BEGIN {
	unshift @INC, './t';
	my $tests = 146;
	$^W= 1;
	$| = 1;
	print "1..$tests\n";
}

END {print "not ok $QTestCommon::testno\n" unless $loaded;}

use QTestCommon;
use QTestCommon qw(report_result);

$loaded = 1;
report_result(1, 'load module');
#
#	runs the tests
#
QTestCommon->run_test($ARGV[0] || 0, 8);
