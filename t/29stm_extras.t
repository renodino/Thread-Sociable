use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 33;
use Thread::Sociable;
sociable_disable_debug();

### Start of Testing ###

########################################################################
########################################################################
#
#	MULTITHREADED: TEST XACTION LIMIT
#
########################################################################
########################################################################

$scalar = 'hello world';

$thrd1 = threads->create(\&read_only);
$thrd2 = threads->create(\&read_only);
my $thrd3 = threads->create(\&read_only);
my $thrd4 = threads->create(\&read_only);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
my $rc3 = $thrd1->join();
my $rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'readonly, 4 threads');

ok((sociable_stm_limit(20) == 3),'update STM limit');

$thrd1 = threads->create(\&read_only);
$thrd2 = threads->create(\&read_only);
my $thrd3 = threads->create(\&read_only);
my $thrd4 = threads->create(\&read_only);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
my $rc3 = $thrd1->join();
my $rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'readonly, 4 threads');

########################################################################
########################################################################
#
#	SINGLE THREADED: GET LOG
#
########################################################################
########################################################################

########################################################################
########################################################################
#
#	MULTITHREADED: GET LOG
#
########################################################################
########################################################################

########################################################################
########################################################################
#
#	SINGLE THREADED: DUMP LOG
#
########################################################################
########################################################################

########################################################################
########################################################################
#
#	MULTITHREADED: DUMP LOG
#
########################################################################
########################################################################

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_only, 'second');
$thrd3 = threads->create(\&write_only, 'third');
$thrd4 = threads->create(\&write_only, 'fourth');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
$rc3 = $thrd1->join();
$rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'writeonly, 4 threads');



sub first_nested {
	my ($restart, $rollback) = @_;
	sociable_begin_work(sub {
		$other .= 'foo';
		},
		onRestart => $restart,
		onRollback => $rollback
	);
}

sub second_nested {
	my ($restart, $rollback) = @_;
	sociable_begin_work(sub {
		$other .= ' foo';
		third_nested(
			($restart ? sub { $report += ($report ? -1000 : 1000); } : undef),
			($rollback ?  sub { $report += ($report ? -10000 : 10000); } : undef)
		);
		},
		onRestart => $restart,
		onRollback => $rollback
	);
}

sub third_nested {
	my ($restart, $rollback) = @_;
	sociable_begin_work(sub {
		$nested .= ' deeper!';
		},
		onRestart => ($forceroll ? sub { die "Rollback from innermost transaction\n"; } : $restart),
		onRollback => $rollback
	);
}

sub read_only {
	my $privscalar;
	my $restarted = 0;
	sleep 3;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar eq 'hello world'));
}

sub write_only {
	my $privscalar = shift;
	my $restarted = 0;
	sleep 2;
	my $rc = sociable_begin_work(
		sub {
			$scalar = $privscalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_after_write {
	my $privscalar;
	my $restarted = 0;
	sleep 4;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar eq 'foo bar baz'));
}

sub write_after_read {
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 4;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub read_write_1 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 3;
			$scalar = $input;
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return ($rc && !$restarted && !$rollback);
}

sub read_write_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 5;
			$scalar = $input;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub write_read_1{
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$scalar = $input;
			sleep 3;
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub write_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$scalar = $input;
			sleep 5;
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_write_rollback {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 5;
			$scalar = $input;
			die "read/write rollback";
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return (!$rc && $restarted && $rollback);
}

sub read_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 5;
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

