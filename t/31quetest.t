use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
	print STDERR "Note: some tests have significant delays...\n";
}

package Qable;
use Thread::Sociable::Queueable;

use base qw(Thread::Sociable::Queueable);

sub new {
	return bless {}, shift;
}

sub onEnqueue {
	my $obj = shift;
	my $class = ref $obj;
#	print STDERR "$class object enqueued\n";
	return $obj->SUPER::onEnqueue;
}

sub onDequeue {
	my ($class, $obj) = @_;
#	print STDERR "$class object dequeued\n";
	return $class->SUPER::onDequeue($obj);
}

sub onCancel {
	my $obj = shift;
#	print STDERR "Item cancelled.\n";
	1;
}

sub curse {
	my $obj = shift;
	return $obj->SUPER::curse;
}

sub redeem {
	my ($class, $obj) = @_;
	return $class->SUPER::redeem($obj);
}

1;

package SharedQable;
use Thread::Sociable::Queueable;

use base qw(Thread::Sociable::Queueable);

sub new {
	my %obj : Sociable = ( Value => 1);
	return bless \%obj, shift;
}

sub set_value {
	my $obj = shift;
	$obj->{Value}++;
	return 1;
}

sub get_value { return shift->{Value}; }

sub redeem {
	my ($class, $obj) = @_;
	return bless $obj, $class;
}

1;

package main;

use threads;
use Thread::Sociable;
use Thread::Sociable::Queue;

use Test::More skip_all => "Queue support not complete.";
#use Test::More tests => 84;

my $srvtype = 'init';

#
#	test normal dequeue method
#
sub run_dq {
	my $q = shift;

	$q->listen;
	while (1) {
		my $left = $q->pending;

		my $req = $q->dequeue;
#print threads->self()->tid(), " run_dq dq'd\n";
		my $id = shift @$req;

		if ($req->[0] eq 'stop') {
			$q->respond($id, 'stopped');
			$q->ignore();
			last;
		}

		if ($req->[0] eq 'wait') {
			sleep($req->[1]);
		}

		if ($req->[1] && ref $req->[1] && (ref $req->[1] eq 'SharedQable')) {
			$req->[1]->set_value();
		}
#
#	ignore simplex msgs
#
		next
			unless $id;

		$q->marked($id) ?
			$q->respond($id, $q->get_mark($id)) :
			$q->respond($id, @$req);
	}
}
#
#	test nonblocking dequeue method
#
sub run_nb {
	my $q = shift;

	$q->listen;
	while (1) {
		my $req = $q->dequeue_nb;

		sleep 1, next
			unless $req;

#print "run_nb dq'd\n";
		my $id = shift @$req;

		$q->ignore(),
		$q->respond($id, 'stopped'),
		last
			if ($req->[0] eq 'stop');

#print STDERR join(', ', @$req), "\n";
		sleep($req->[1])
			if ($req->[0] eq 'wait');
#
#	ignore simplex msgs
#
		$q->respond($id, @$req)
			if $id;
	}
}
#
#	test timed dequeue method
#
sub run_until {
	my $q = shift;

	my $timeout = 2;
	$q->listen;
	while (1) {

		my $req = $q->dequeue_until($timeout);
		sleep 1, next
			unless $req;

#print "run_until dq'd\n";
		my $id = shift @$req;

		$q->ignore(),
		$q->respond($id, 'stopped'),
		last
			if ($req->[0] eq 'stop');

		sleep($req->[1])
			if ($req->[0] eq 'wait');
#
#	ignore simplex msgs
#
		$q->respond($id, @$req)
			if $id;
	}
}
#
#	acts as a requestor thread for class-level
#	wait tests
#
sub run_requestor {
	my $q = shift;

	$q->wait_for_listener;

	while (1) {
		my $id = $q->enqueue('request');
		my $resp = $q->wait($id);

		last
			if ($resp->[0] eq 'stop');
	}
	return 1;
}
#
#	test urgent dequeue method
#
sub run_urgent {
	my $q = shift;

	$q->listen;
	while (1) {
		my $req = $q->dequeue_urgent;

		sleep 1, next
			unless $req;

#print "run_urgent dq'd\n";
		my $id = shift @$req;

		$q->ignore(),
		$q->respond($id, 'stopped'),
		last
			if ($req->[0] eq 'stop');

		sleep($req->[1])
			if ($req->[0] eq 'wait');
#
#	ignore simplex msgs
#
		$q->respond($id, @$req)
			if $id;
	}
}

$testno = 1;

ok(1, 'load module');
#
#	create queue
#	spawn server thread
#	execute various requests
#	verify responses
#
#	test constructor
#
my $q = Thread::Sociable::Queue->new(ListenerRequired => 1);

ok(defined($q), 'create queue');
#
#	test different kinds of dequeue
#
my @servers = (\&run_dq, \&run_nb, \&run_until);
my @types = ('normal', 'nonblock', 'timed');

my ($result, $id, $server);

my $start = $ARGV[0] || 0;
my $qable = Qable->new();
my $sharedqable = SharedQable->new();

foreach ($start..$#servers) {
	$server = threads->new($servers[$_], $q);
	$srvtype = $types[$_];
#
#	wait for a listener
#
	ok($q->wait_for_listener(), "wait_for_listener() for $srvtype");
#
#	test enqueue_simplex
#
	$id = $q->enqueue_simplex('foo', 'bar');
	ok(defined($id), "enqueue_simplex() for $srvtype");
#
#	test enqueue
#
	$id = $q->enqueue('foo', 'bar');
	ok(defined($id), "enqueue() for $srvtype");
#
#	test ready(); don't care about outcome
#	(prolly need eval here)
#
	eval {
		$result = $q->ready($id);
	};
	ok(!$@, "ready() for $srvtype");
#
#	test wait()
#
	$result = $q->wait($id);

	ok(defined($result) && ($result->[0] eq 'foo') && ($result->[1] eq 'bar'),
		"wait() for $srvtype");
#
#	test dequeue_response
#
	$id = $q->enqueue('foo', 'bar');
	$result = $q->dequeue_response($id);
	ok(defined($result) && ($result->[0] eq 'foo') && ($result->[1] eq 'bar'),
		"dequeue_response() for $srvtype");
#
#	test Queueable enqueue
#
	$id = $q->enqueue('foo', $qable);
	ok(defined($id), "enqueue() Queueable for $srvtype");

	$result = $q->wait($id);

	ok(defined($result) && ($result->[0] eq 'foo') && 
		(ref $result->[1]) && (ref $result->[1] eq 'Qable'),
		"wait() Queueable for $srvtype");
#
#
#	test wait_until, enqueue_urgent
#
	$id = $q->enqueue('wait', 3);
	my $id1 = $q->enqueue('foo', 'bar');
	$result = $q->wait_until($id, 1);
	ok(!defined($result), "wait_until() expires for $srvtype");

	my $id2 = $q->enqueue_urgent('urgent', 'entry');
#
#	should get wait reply here
#
	$result = $q->wait_until($id, 5);
	ok(defined($result) && ($result->[0] eq 'wait'), "wait_until() for $srvtype");
#
#	should get urgent reply here
#
	$result = $q->wait($id2);
	ok(defined($result) && ($result->[0] eq 'urgent'), "enqueue_urgent() for $srvtype");
#
#	should get normal reply here
#
	$result = $q->wait($id1);
	ok(defined($result) && ($result->[0] eq 'foo'), "enqueue() for $srvtype");
#
#	test wait_any: need to queue up several
#
	my %ids = ();

	map { $ids{$q->enqueue('foo', 'bar')} = 1; } (1..10);
#
#	repeat here until all ids respond
#
	my $failed;
	while (keys %ids) {
		$result = $q->wait_any(keys %ids);
		$failed = 1,
		last
			unless defined($result) &&
				(ref $result) &&
				(ref $result eq 'HASH');
		map {
			$failed = 1
				unless delete $ids{$_};
		} keys %$result;
		last
			if $failed;
	}
	ok(!$failed, "wait_any() for $srvtype");
#
#	test wait_any_until
#
	%ids = ();

	$ids{$q->enqueue('wait', '3')} = 1;
	map { $ids{$q->enqueue('foo', 'bar')} = 1; } (2..10);
	$failed = undef;

	$result = $q->wait_any_until(1, keys %ids);
	if ($result) {
		$failed = 1;
	}
	else {
		while (keys %ids) {
			$result = $q->wait_any_until(5, keys %ids);
			$failed = 1,
			last
				unless defined($result) &&
					(ref $result) &&
					(ref $result eq 'HASH');
			map {
				$failed = 1
					unless delete $ids{$_};
			} keys %$result;
			last
				if $failed;
		}
	}
	ok(!$failed, "wait_any_until() for $srvtype");
#
#	test wait_all
#
	%ids = ();
	map { $ids{$q->enqueue('foo', 'bar')} = 1; } (1..10);
#
#	test available()
#
	sleep 1;
	my @avail = $q->available;
	ok(scalar @avail, "available (array) for $srvtype");

	$id = $q->available;
	ok($id, "available (scalar) for $srvtype");

	$id = keys %ids;
	@avail = $q->available($id);
	ok(scalar @avail, "available (id) for $srvtype");
#
#	make sure all ids respond
#
	$result = $q->wait_all(keys %ids);
	unless (defined($result) &&
		(ref $result) &&
		(ref $result eq 'HASH') &&
		(scalar keys %ids == scalar keys %$result)) {
		fail("wait_all() for $srvtype");
	}
	else {
		map { $failed = 1 unless delete $ids{$_}; } keys %$result;
		ok(!($failed || scalar %ids), "wait_all() for $srvtype");
	}
#
#	test wait_all_until
#
	%ids = ();
	map { $ids{$q->enqueue('wait', '1')} = 1; } (1..10);
#
#	make sure all ids respond
#
	$result = $q->wait_all_until(1, keys %ids);
	if (defined($result)) {
		fail("wait_all_until() for $srvtype");
	}
	else {
	# may need a warning print here...
		$result = $q->wait_all_until(20, keys %ids);
		map { $failed = 1 unless delete $ids{$_}; } keys %$result;
		ok(!($failed || scalar keys %ids), "wait_all_until() for $srvtype");
	}
#
#	test cancel()/cancel_all():
#	post a waitop
# 	post a no wait
#	wait a bit for server to pick up the first
#	cancel the nowait
#	check the pending count for zero
#	wait for waitop to finish
#
	$id = $q->enqueue('wait', 5);
	$id1 = $q->enqueue('foo', 'bar');
	$result = $q->wait_until($id, 3);
	$q->cancel($id1);
#print "Cancel: pending :", $q->pending, "\n";
	ok(!$q->pending, "cancel() for $srvtype");
	$result = $q->wait($id);
#
#	do same, but add multiple and cancel all
#
	$id = $q->enqueue('wait', 5);
	$id1 = $q->enqueue('first', 'bar');
	$id2 = $q->enqueue('second', 'bar');
	$result = $q->wait_until($id, 1);
	$q->cancel_all();
#print "Cancel all: pending :", $q->pending, " avail ", $q->available, "\n";
	ok(!($q->pending || $q->available), "cancel_all() for $srvtype");
#
#	kill the thread; also tests urgent i/f
#
	$id = $q->enqueue_urgent('stop');
	ok($id, "enqueue_urgent() for $srvtype");
	$server->join;
#
#	wait for response, then test enqueue wo/ a listener
#
	$result = $q->wait($id);
	ok(!$q->enqueue('no listener'), "enqueue() wo/ listener for $srvtype");

}	#end foreach server method
#
#	now test the class-level waits:
#	create an add'l queue
#	create a listener thread w/ old queue
#	create requestor thread w/ new queue
#
	my $newq = Thread::Sociable::Queue->new(ListenerRequired => 1);
	$server = threads->new($servers[0], $q);
	$srvtype = $types[0];
	$q->wait_for_listener();
#
#	test shared Queueable enqueue
#
	$id = $q->enqueue('foo', $sharedqable);
	ok(defined($id),'enqueue() shared Queueable');

	$result = $q->wait($id);

	ok(defined($result) &&
		($result->[0] eq 'foo') &&
		(ref $result->[1]) &&
		(ref $result->[1] eq 'SharedQable') &&
		($result->[1]->get_value == 2), 'wait() Queueable');

	my $requestor = threads->new(\&run_requestor, $newq);
	$newq->listen();
	my @qs = ();
#
#	post request to listener
#	wait on both queues
#
	$id = $q->enqueue('wait', 3);
print "ID is undef!!!\n" unless defined($id);
#	@qs = Thread::Sociable::Queue->wait_any([$q, $id], [$newq]);
	@qs = Thread::Sociable::Queue->wait_any([$q, $id], $newq);
	unless (scalar @qs) {
		fail('class-level wait_any()');
	}
	else {
#
#	should get the newq only here...
#
		unless ((scalar @qs == 1) && ($qs[0] eq $newq)) {
			fail('class-level wait_any()');
		}
		else {
			my $req = $newq->dequeue();
			$newq->respond(shift @$req, 'ok');
		}
#
#	wait for other queue
#
		my $resp = $q->wait($id);
		pass('class-level wait_any()');
	}
#
#	now timed wait_any
#
	$id = $q->enqueue('wait', 5);
	@qs = Thread::Sociable::Queue->wait_any_until(3, [$q, $id], [$newq]);
	unless (scalar @qs) {
		fail('class-level wait_any_until()');
	}
	else {
#
#	should get the newq only here...
#
		unless ((scalar @qs == 1) && ($qs[0] eq $newq)) {
			fail('class-level wait_any_until()');
		}
		else {
			my $req = $newq->dequeue();
			$newq->respond(shift @$req, 'ok');
		}
#
#	wait for other queue
#
		my $resp = $q->wait($id);
		pass('class-level wait_any_until()');
	}
#
#	now wait_all
#
	$id = $q->enqueue('wait', 3);
	@qs = Thread::Sociable::Queue->wait_all([$q, $id], [$newq]);
	unless (scalar @qs == 2) {
		fail('class-level wait_all()');
	}
	else {
		foreach (@qs) {
			if ($_ eq $newq) {
				my $req = $newq->dequeue();
				$newq->respond(shift @$req, 'ok');
			}
			else {
#
#	wait for other queue
#
				my $resp = $q->wait($id);
			}
		}
		pass('class-level wait_all()');
	}
#
#	now timed wait_all
#
	$id = $q->enqueue('wait', 3);
	@qs = Thread::Sociable::Queue->wait_all_until(1, [$q, $id], [$newq]);
#
#	shouldn't get anything first time thru
#
	if (@qs) {
		fail('class-level wait_all_until()');
	}
	else {
		@qs = Thread::Sociable::Queue->wait_all_until(5, [$q, $id], [$newq]);
		unless (scalar @qs == 2) {
			fail('class-level wait_all_until()');
		}
		else {
			foreach (@qs) {
				if ($_ eq $newq) {
					my $req = $newq->dequeue();
					$newq->respond(shift @$req, 'stop');
				}
				else {
#
#	wait for other queue
#
					my $resp = $q->wait($id);
				}
			}
			ok('class-level wait_all_until()');
		}
	}
	$q->enqueue_simplex('stop');
	$server->join;
	$requestor->join;
#
#	make sure no one else is listening on our queue
#
	$q = Thread::Sociable::Queue->new(ListenerRequired => 1);
#
#	test max pending
#
	$q->set_max_pending(5);
	$server = threads->new(\&run_dq, $q);
	$q->wait_for_listener();
	my @ids = ();
	push @ids, $q->enqueue('wait', 5);
	sleep 1;
#
#	queue up several, then see if we block
#
	push @ids, $q->enqueue('foo', 'bar');
	push @ids, $q->enqueue_urgent('foo', 'bar');
	$q->enqueue_simplex_urgent('foo', 'bar');
	$q->enqueue_simplex('foo', 'bar');
	push @ids, $q->enqueue_urgent('foo', 'bar');
#
#	keep time, we should block at this point
#
	my $started = time();
	push @ids, $q->enqueue('foo', 'bar');
	ok(time() - $started > 1), 'max_pending');
#
#	consume all our responses
#
	$q->wait_all(@ids);
#
#	test mark
#
	my $failed = undef;
	my $id1 = $q->enqueue('wait', 3);
	$q->mark($id1, 'CANCEL');
	unless ($q->get_mark($id1) eq 'CANCEL') {
		fail('mark');
	}
	else {
		sleep 3;	# give thread time to process both
		$result = $q->wait($id1);
		ok($result->[0] eq 'CANCEL', 'mark');
	}

	$q->enqueue_simplex('stop');
	$server->join;
#
#	test dequeue_urgent
#
	$server = threads->new(\&run_urgent, $q);
	$q->wait_for_listener();
	$id1 = $q->enqueue('bar', 'foo');
	my $id2 = $q->enqueue_urgent('foo', 'bar');
	sleep 3;	# give thread time to process both
	$result = $q->wait_any($id1, $id2);
	$failed = undef;
	foreach (keys %$result) {
		$failed = 1, last unless ($_ == $id2);
	}
	ok(!$failed, 'dequeue_urgent');

	$q->enqueue_simplex_urgent('stop');
	$server->join;

