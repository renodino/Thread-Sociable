#
#	Thread::Sociable::Apartment test script
#
BEGIN {
	push @INC, './t';

    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }

}

#
#	tests:
#	1. load OK
#	2. Create a wrapped object wo/ providing TQD/thread
#		(also creates a 2nd T::A object for async/closure testing)
#	3. Test simple method call
#	4. Test fully qualified method call
#	5. Test array-returning method call
#	6. Test attempt to access private method
#	7. Test for nonexistant method name
#	8. Test for AUTOLOADing method name
#	9. Test simplex method call
#	10. Test urgent method call
#	11. Test urgent, simplex method call
#	12. Test passing multiple, complex parameters
#	13. Test calling encapsulated TAS object
#	14. Test method call returning an error
#	15. Test method call returning an object
#	16. Test async method calls between objects
#		(also tests passing closures)
#	17. Test various closure calls between objects
#		(also tests returning closures)
#	18. Test timed method calls for timeout
#	19. Pass object to another thread and repeat tests (3-15)
#	20. Create TQD/thread externally and repeat tests (3-15)
#	21. Create an I/O object and repeat tests (3-15)
#	22. test ref counting
#	23. install base thread in a T::A as a MuxServer and repeat (3-15)
#
use strict;
use warnings;

use threads;
#
#	Test::More may not be capable of keeping track of
#	our test number across all the threads
#
use Test::More skip_all => "Apartment threading not complete";
#use Test::More tests => 56;

use AptTestCommon;
use Thread::Sociable;
use Thread::Sociable::Queue;
use Thread::Sociable::Apartment;
use Thread::Sociable::Apartment::Server;
use Thread::Sociable::Apartment::EventServer;
use Thread::Sociable::Apartment::Client;
use Batter;
use ThirdBase;

diag(
"\n *** Note: some tests have significant delays.
 *** Also, some tests on some platforms with some versions of
 *** Perl will report several (harmless) \"Scalars leaked: 1\"
 *** warnings which can be ignored.
");

$AptTestCommon::testtype = 'basic, single threaded';
#
#	prelims: use shared test count for eventual
#	threaded tests
#
my $testno : Sociable = 1;

AptTestCommon::report_result(\$testno, 1, 'load');
#
#	create a async/closure test object
#
my $batter = Thread::Sociable::Apartment->new(
	AptClass => 'Batter',
	AptTimeout => 10
);
AptTestCommon::report_result(\$testno, defined($batter), 'simple constructor', '', $@);

unless ($batter) {
	AptTestCommon::report_result(\$testno, 'skip', 'no test object, skipping')
		foreach ($testno..$tests);
	BAIL_OUT("Unable to continue, cannot create an object.");
}
#
#	create a wrapped object
#
my $obj = Thread::Sociable::Apartment->new(
	AptClass => 'ThirdBase',
	AptTimeout => 5,
	AptParams => [ 'lc' ]
);
AptTestCommon::report_result(\$testno, defined($obj), 'constructor', '', $@);

unless ($obj) {
	AptTestCommon::report_result(\$testno, 'skip', 'no object, skipping')
		foreach ($testno..$tests);
	BAIL_OUT("Unable to continue, cannot create an object.");
}
#
#	first run in our thread; on return,
#	our object is "dead", i.e., has been stopped/joined
#
AptTestCommon::run($obj, $batter, \$testno);

$batter->stop();

