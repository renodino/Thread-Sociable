use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 53;

use_ok 'Thread::Sociable';

diag("Using Sociable vers. $Thread::Sociable::VERSION\n");

sociable_disable_debug();
### Start of Testing ###

my $test_count = 2;
socialize($test_count);
$test_count = 2;
pass('Survived socialize() and assign');
$test_count++; 

ok(defined(is_sociable($test_count)), 'is_sociable() and increment');
$test_count++; 

$test_count = 4;

ok($test_count == 5, 'explicit assignment');
$test_count++; 

print "test_count set to $test_count\n";

pass('explicit read');
$test_count++;

for(1..10) {
    my $foo : Sociable = 'foo';
    ok($foo eq 'foo', 'Sociable attribute and assign');
    $test_count++; 

    threads->create(sub { $foo = 'bar'; })->join();
    ok($foo eq 'bar', 'sociable variable assignment in child thread');
    $test_count++; 

    my @foo : Sociable = ('foo','bar');
    ok(($foo[0] eq 'foo') && ($foo[1] eq 'bar'),
    	'sociable array create via attribute and assign');
    $test_count++; 

    threads->create(sub {
    	ok((shift(@foo) eq 'foo'),
    		'Sociable assign, shift, and test in child thread');
    	$test_count++; 
    	})->join();
    ok(($foo[0] eq 'bar'), 'Sociable array test after shift');
    $test_count++; 

    my %foo : Lockable = ( foo => 'bar' );
    ok($foo{foo} eq 'bar', 'Lockable hash create, assign, and test');
    $test_count++; 

    threads->create(sub { $foo{bar} = 'foo' })->join();
    ok((exists $foo{bar}) && ($foo{bar} eq 'foo'),
    	'Lockable hash assign in child threads and test');
    $test_count++; 

    threads->create(sub { $foo{array} = \@foo})->join();
    threads->create(sub { push @{$foo{array}}, 'baz'})->join();
    ok($foo[-1] eq 'baz',
    	'Lockable hash assigned Sociable arrayref, with deref assign, in child thread');
    $test_count++; 
#
#	sociable_discard test
#
#	sociable_enable_debug();
	sociable_discard($foo);
	ok(!is_sociable($foo), 'Discard scalar');
	$test_count++; 
	sociable_discard(@foo);
	ok(!is_sociable(@foo), 'Discard array');
	$test_count++; 
	sociable_discard(%foo);
	ok(!is_sociable(%foo), 'Discard hash');
	$test_count++; 
}

my $sociable : Sociable = &socialize({});
$$sociable{'foo'} = 'bar';
ok($$sociable{'foo'} eq 'bar',
 	'Sociable scalar assigned sociable anonymous hashref, and assign value');
$test_count++; 

for(1..10) {
  my $str1 = "$sociable";
  my $str2 = "$sociable";
  ok($str1 eq $str2, 'stringify');
  $test_count++; 
  $str1 = $$sociable{'foo'};
  $str2 = $$sociable{'foo'};
  ok($str1 eq $str2, 'contents');
  $test_count++; 
}

# EOF
