#
#	Thread::Sociable::Apartment test script
#
BEGIN {
	push @INC, './t';

    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More skip_all => "Apartment threading not complete";
#use Test::More tests => 47;

use AptTestCommon;
use Thread::Sociable;
use Thread::Sociable::Queue;
use Thread::Sociable::Apartment;
use Thread::Sociable::Apartment::Server;
use Thread::Sociable::Apartment::Client;
use Batter;
use ThirdBase;

use strict;
use warnings;

$AptTestCommon::testtype = 'basic, multithreaded';

#
#	prelims: use shared test count for eventual
#	threaded tests
#
my $testno : Sociable = 1;

AptTestCommon::report_result(\$testno, 1, 'load');
#
#	create a async/closure test object
#
my $batter = Thread::Sociable::Apartment->new(
	AptClass => 'Batter',
	AptTimeout => 10
);
AptTestCommon::report_result(\$testno, defined($batter), 'simple constructor', '', $@);

unless ($batter) {
	AptTestCommon::report_result(\$testno, 'skip', 'no test object, skipping')
		foreach ($testno..$tests);
	die "Unable to continue, cannot create an object.";
}
#
#	then pass to another thread via a TQD:
#	create a wrapped object
#
my $obj = Thread::Sociable::Apartment->new(
	AptClass => 'ThirdBase',
	AptTimeout => 10,
	AptParams => [ 'lc' ]
);
AptTestCommon::report_result(\$testno, defined($obj), 'constructor', '', $@);

unless ($obj) {
	AptTestCommon::report_result(\$testno, 'skip', 'no object, skipping')
		foreach ($testno..$tests);
	die "Unable to continue, cannot create an object.";
}

my $tqd = Thread::Sociable::Queue->new(ListenerRequired => 1);
my $thread = threads->new(\&AptTestCommon::run_thread, $tqd, \$testno);
$tqd->wait_for_listener();
$tqd->enqueue_and_wait($obj, $batter);
sleep 2;
$thread->join();
