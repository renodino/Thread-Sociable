use strict;
use warnings;

BEGIN {
    use Config;
    if (! $Config{'useithreads'}) {
        print("1..0 # Skip: Perl not compiled with 'useithreads'\n");
        exit(0);
    }
}

use threads;
use Test::More tests => 11;
use Thread::Sociable;

### Start of Testing ###

my $foo;
my $bar = "foo";
socialize($foo);
eval { $foo = \$bar; };
ok(my $temp1 = $@ =~/^Invalid\b.*sociable scalar/, "Wrong error message");

socialize($bar);
$foo = \$bar;
ok($temp1 = $foo =~/SCALAR/, "Check that is a ref");
ok($$foo eq "foo", "Check that it points to the correct value");
$bar = "yeah";
ok($$foo eq "yeah", "Check that assignment works");
$$foo = "yeah2";
ok($$foo eq "yeah2", "Check that deref assignment works");
threads->create(sub {$bar = "yeah3"})->join();
ok($$foo eq "yeah3", "Check that other thread assignemtn works");
threads->create(sub {$foo = "artur"})->join();
ok($foo eq "artur", "Check that uncopupling the ref works");
my $baz;
socialize($baz);
$baz = "original";
$bar = \$baz;
$foo = \$bar;
ok($$$foo eq 'original', "Check reference chain");

my($t1,$t2);
socialize($t1);
socialize($t2);
$t2 = "text";
$t1 = \$t2;
threads->create(sub { $t1 = "bar" })->join();
ok($t1 eq 'bar',"Check that assign to a ROK works");

ok(is_sociable($foo), "Check for sociability");

# EOF
