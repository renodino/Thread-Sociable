package SvSTM;

use strict;
use warnings;

use threads;
use Test::More;

use Thread::Sociable;

sub tests { return 101; }

### Start of Testing ###
#
#	NOTE: REVIEW RESTART TEST LOGIC !!!
#
#
#	single scalar tests:
#		1. readonly xaction
#		2. writeonly xaction
#		3. read/write xaction
#		4. write/read xaction
#
#	commit, no commit/restart/rollback subs: tests 1-4
#	commit, w/ commit sub, no restart/rollback sub: tests 1-4
#	commit, w/ commit & restart sub, no rollback sub: test 4
#	commit, w/ commit sub & rollback sub, no restart sub: test 4
#	commit, w/ commit, restart and rollback subs: test 4
#
#	force restart, no commit/restart/rollback sub: tests 1-4
#	force restart, w/ restart sub, no commit/rollback sub: tests 1-4
#	force restart, w/ rollback sub, no commit/restart sub: test 4
#	force restart, w/ restart & rollback subs, no commit sub: tests 4
#	force restart, w/ commit sub, no restart/rollback sub: test 4
#	force restart, w/ commit & restart sub, no rollback sub: test 4
#	force restart, w/ commit & rollback sub, no restart sub: test 4
#	force restart, w/ commit, restart and rollback subs: test 4
#
#	force rollback, no commit/restart/rollback sub: tests 1-4
#	force rollback, w/ rollback sub, no commit/restart sub: tests 1-4
#	force rollback, w/ restart sub, no commit/rollback sub: test 2
#	force rollback, w/ restart & rollback subs, no commit sub: test 2
#	force rollback, w/ commit sub, no restart/rollback sub: test 2
#	force rollback, w/ commit & rollback sub, no restart sub: test 2
#	force rollback, w/ commit & restart sub, no rollback sub: test 2
#	force rollback, w/ commit, restart and rollback subs: test 2
#
#	commit, commit sub forces restart, no restart/rollback sub: test 4
#	commit, commit sub forces restart, w/ restart sub, no rollback sub: test 4
#	commit, commit sub forces restart, w/ restart & rollback sub: test 4
#
#	commit, commit sub forces rollback, no restart/rollback sub: test 4
#	commit, commit sub forces rollback, w/ rollback sub, no rollback sub: test 4
#	commit, commit sub forces rollback, w/ restart & rollback sub: test 4
#
#	force restart, restart sub forces rollback, no commit/rollback sub: test 4
#	force restart, restart sub forces rollback, w/ rollback sub, no commit sub: test 4
#	force restart, restart sub forces rollback, w/ commit & rollback sub: test 4
#
#	multiple scalar:
#	commit, no commit/restart/rollback subs: tests 1-4
#	commit, w/ commit sub: tests 1-4
#	force restart, no subs: simple read/writes
#	force restart, w/ restart, & rollback subs: simple read/writes
#
#	force rollback, no subs: simple read/writes
#	force rollback, w/ restart/rollback subs: simple read/writes
#
#	repeat multiscalar w/ 1 nested transactions
#	repeat multiscalar w/ 2 nested transactions
#	repeat multiscalar w/ 2 nested transactions, commit subs
#	repeat multiscalar w/ 2 nested consecutive transactions
#	repeat multiscalar w/ 2 competing threads
#	repeat multiscalar w/ 4 competing threads
#

sub run {

sociable_disable_debug();
my $scalar : Sociable = 1234;
my $privscalar;
my $committed = 0;

############ commit, no subs ##################

my $testtype = 'single scalar, commit, no subs,';
my $rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
	});
ok($rc && ($scalar == 1234) && ($privscalar == 1234),"$testtype readonly");

$rc = sociable_begin_work(
	sub {
		$scalar = 5678;
	});
ok($rc && ($scalar == 5678),"$testtype writeonly");

$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$scalar = "foobar";
	});
ok($rc && ($privscalar == 5678) && ($scalar == 'foobar'),"$testtype read/write");

$rc = sociable_begin_work(
	sub {
		$scalar = 'one fine day';
		$privscalar = $scalar;
	});
ok($rc && ($privscalar == 'one fine day') && ($scalar == 'one fine day'),"$testtype write/read");

############ commit, commit sub ##################

$testtype = 'single scalar, commit, commit sub only,';
$scalar = 1234;
$privscalar = undef;
$committed = 0;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
	},
	onCommit => sub {
		$committed = 1;
	}
);
ok($rc && $committed && ($scalar == 1234) && ($privscalar == 1234),"$testtype readonly");

$committed = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = 5678;
	},
	onCommit => sub {
		$committed = 1;
	}
);
ok($rc && $committed && ($scalar == 5678),"$testtype writeonly");

$committed = 0;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$scalar = "foobar";
	},
	onCommit => sub {
		$committed = 1;
	}
);
ok($rc && $committed && ($privscalar == 5678) && ($scalar == 'foobar'),"$testtype read/write");

$committed = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = 'one fine day';
		$privscalar = $scalar;
	},
	onCommit => sub {
		$committed = 1;
	}
);
ok($rc && $committed && ($privscalar == 'one fine day') && ($scalar == 'one fine day'),"$testtype write/read");

############ commit, commit & restart sub ##################

$testtype = 'single scalar, commit, commit+restart subs,';
$scalar = 1234;
$privscalar = undef;
$committed = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = 'one fine day';
		$privscalar = $scalar;
	},
	onCommit => sub {
		$committed = 1;
	},
	onRestart => sub {
		$committed = -1;
	}
);
ok($rc && ($committed > 0) && ($privscalar == 'one fine day') && ($scalar == 'one fine day'),"$testtype write/read");

############ commit, commit & rollback sub ##################

$testtype = 'single scalar, commit, commit+rollback subs,';
$scalar = 1234;
$privscalar = undef;
$committed = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = 'one fine day';
		$privscalar = $scalar;
	},
	onCommit => sub {
		$committed = 1;
	},
	onRollback => sub {
		$committed = -1;
	}
);
ok($rc && ($committed > 0) && ($privscalar == 'one fine day') && ($scalar == 'one fine day'),"$testtype write/read");

############ commit, commit, restart, & rollback sub ##################

$testtype = 'single scalar, commit, commit+restart+rollback subs,';
$scalar = 1234;
$privscalar = undef;
$committed = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = 'one fine day';
		$privscalar = $scalar;
	},
	onCommit => sub {
		$committed = 1;
	},
	onRestart => sub {
		$committed = -1;
	},
	onRollback => sub {
		$committed = -1;
	}
);
ok($rc && ($committed > 0) && ($privscalar == 'one fine day') && ($scalar == 'one fine day'),"$testtype write/read");

############ force restart, no subs ##################

$testtype = 'single scalar, restart, no subs,';
$committed = 0;
my $restart = 1;
$scalar = 1234;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 1234) && ($privscalar == 1234),"$testtype readonly");

$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = 5678;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 5678),"$testtype writeonly");

$restart = 1;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$scalar++;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($privscalar == 5678) && ($scalar == 5679),"$testtype read/write");

$restart = 1;
$privscalar += 10;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});

ok($rc && ($privscalar == 5691) && ($scalar == 5690),"$testtype write/read");

############ force restart, w restart sub ##################

$testtype = 'single scalar, restart, restart sub,';
$restart = 1;
$scalar = 1234;
my $report;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report = 123; }
	);
ok($rc && ($scalar == 1234) && ($privscalar == 1234) && ($report == 123),"$testtype readonly");

$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = 5678;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; }
	);
ok($rc && ($scalar == 5678) && ($report == 124),"$testtype writeonly");

$restart = 1;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$scalar++;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; }
	);
ok($rc && ($privscalar == 5679) && ($scalar == 5680) && ($report == 125),"$testtype read/write");

$restart = 1;
$privscalar += 10;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; }
	);
ok($rc && ($privscalar == 5691) && ($scalar == 5690) && ($report == 126),"$testtype write/read");

############ force restart, w rollback sub ##################

$testtype = 'single scalar, restart, rollback sub,';
$restart = 1;
$privscalar += 10;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRollback => sub { $report++; }
	);
ok($rc && ($privscalar == 5693) && ($scalar == 5692) && ($report == 0),"$testtype write/read");

############ force restart, w restart & rollback sub ##################

$testtype = 'single scalar, restart, restart+rollback sub,';
$restart = 1;
$privscalar += 10;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report = -1; }
	onRollback => sub { $report += 10; }
	);
ok($rc && ($privscalar == 5695) && ($scalar == 5694) && ($report == -1),"$testtype write/read");

############ force restart, w commit sub ##################

$testtype = 'single scalar, restart, commit sub,';
$restart = 1;
$privscalar += 10;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onCommit => sub { $report++; }
	);
ok($rc && ($privscalar == 5693) && ($scalar == 5692) && ($report == 0),"$testtype write/read");

############ force restart, w commit&restart sub ##################

$testtype = 'single scalar, restart, restart sub,';
$restart = 1;
$scalar = 1234;
$privscalar += 10;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onCommit => sub { $report = -1; },
	onRestart => sub { $report++; }
	);
ok($rc && ($privscalar == 5691) && ($scalar == 5690) && ($report == 126),"$testtype write/read");

############ force restart, w commit&rollback sub ##################

$testtype = 'single scalar, restart, commit+rollback sub,';
$restart = 1;
$privscalar += 10;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onCommit => sub { $report++; },
	onRollback => sub { $report++; }
	);
ok($rc && ($privscalar == 5693) && ($scalar == 5692) && ($report == 0),"$testtype write/read");

############ force restart, w commit, restart & rollback sub ##################

$testtype = 'single scalar, restart, commit+restart+rollback sub,';
$restart = 1;
$privscalar += 10;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onCommit => sub { $report += 10; },
	onRestart => sub { $report = -1; }
	onRollback => sub { $report += 10; }
	);
ok($rc && ($privscalar == 5695) && ($scalar == 5694) && ($report == -1),"$testtype write/read");

############ force rollback, no subs ##################

$testtype = 'single scalar, rollback, no subs,';
$scalar = 1234;
$privscalar = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	});
ok((!$rc) && ($scalar == 1234),"$testtype writeonly");

############ force rollback, restart sub ##################

$testtype = 'single scalar, rollback, restart sub,';
$scalar = 1234;
$privscalar = 0;
$report;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report = 1; });
ok((!$rc) && ($scalar == 1234) && (!$report),"$testtype writeonly");

############ force rollback, rollback sub ##################

$testtype = 'single scalar, rollback, rollback sub,';
$scalar = 1234;
$privscalar = 0;
$report;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	},
	onRollback => sub { $report = 1; });
ok((!$rc) && ($scalar == 1234) && ($report == 1),"$testtype writeonly");

############ force rollback, restart & rollback sub ##################

$testtype = 'single scalar, rollback, restart+rollback sub,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report = -1; });
	onRollback => sub { $report += 10; });
ok((!$rc) && ($scalar == 1234) && ($report == 10),"$testtype writeonly");

############ force rollback, commit sub ##################

$testtype = 'single scalar, rollback, commit sub,';
$scalar = 1234;
$privscalar = 0;
$report;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	},
	onCommit => sub { $report = 1; });
ok((!$rc) && ($scalar == 1234) && (!$report),"$testtype writeonly");

############ force rollback, commit+restart sub ##################

$testtype = 'single scalar, rollback, commit+restart sub,';
$scalar = 1234;
$privscalar = 0;
$report;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	},
	onCommit => sub { $report++; },
	onRestart => sub { $report++; }
);
ok((!$rc) && ($scalar == 1234) && (!$report),"$testtype writeonly");

############ force rollback, commit+rollback sub ##################

$testtype = 'single scalar, rollback, commit+rollback sub,';
$scalar = 1234;
$privscalar = 0;
$report;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	},
	onCommit => sub { $report--; },
	onRollback => sub { $report = 1; });
ok((!$rc) && ($scalar == 1234) && ($report == 1),"$testtype writeonly");

############ force rollback, commit,restart & rollback sub ##################

$testtype = 'single scalar, rollback, commit+restart+rollback sub,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		die "Thread::Sociable STM rollback test";
	},
	onCommit => sub { $report = -1; },
	onRestart => sub { $report = -1; },
	onRollback => sub { $report += 10; });
ok((!$rc) && ($scalar == 1234) && ($report == 10),"$testtype writeonly");

############ commit, commit sub forces restart, no restart/rollback sub ##################

$testtype = 'single scalar, commit, commit sub forces restart,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
	},
	onCommit => sub {
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 1) && ($privscalar == 2),"$testtype write/read");

############ commit, commit sub forces restart, restart sub ##################

$testtype = 'single scalar, commit, commit+restart sub, commit forces restart,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
	},
	onCommit => sub {
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; }
);
ok($rc && ($scalar == 1) && ($privscalar == 2) && $report,"$testtype write/read");

############ commit, commit sub forces restart, restart sub ##################

$testtype = 'single scalar, commit, commit+restart sub, commit forces restart,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
	},
	onCommit => sub {
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report++; },
	onRollback => sub { $report--; }
);
ok($rc && ($scalar == 1) && ($privscalar == 2) && ($report > 0),"$testtype write/read");

############ commit, commit sub forces rollback, no restart/rollback sub ##################

$testtype = 'single scalar, commit, commit sub forces rollback,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
	},
	onCommit => sub {
		$restart = 0,
		die "Thread::Sociable STM Commit forces rollback"
			if $restart;
	});
ok((!$rc) && ($scalar == 1234) && ($privscalar == 0),"$testtype write/read");

############ commit, commit sub forces rollback, restart sub ##################

$testtype = 'single scalar, commit, commit+restart sub, commit forces restart,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
	},
	onCommit => sub {
		$restart = 0,
		die "Thread::Sociable STM Commit forces rollback"
			if $restart;
	},
	onRollback => sub { $report++; }
);
ok((!$rc) && ($scalar == 1234) && ($privscalar == 0) && $report,"$testtype write/read");

############ commit, commit sub forces restart, restart sub ##################

$testtype = 'single scalar, commit, commit+restart sub, commit forces restart,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
	},
	onCommit => sub {
		$restart = 0,
		die "Thread::Sociable STM Commit forces rollback"
			if $restart;
	},
	onRestart => sub { $report++; },
	onRollback => sub { $report--; }
);
ok((!$rc) && ($scalar == 1234) && ($privscalar == 0) && ($report < 0),"$testtype write/read");

############ force restart, restart sub forces rollback, no commit/rollback sub ##################

$testtype = 'single scalar, force restart, restart sub, restart sub forces rollback,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { 
		die "Thread::Sociable STM Restart forces rollback";
	}
);
ok((!$rc) && ($scalar == 1234) && ($privscalar == 0),"$testtype write/read");

############ force restart, restart sub forces rollback, rollback sub ##################

$testtype = 'single scalar, force restart, restart+rollback sub, restart sub forces rollback,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { 
		die "Thread::Sociable STM Restart forces rollback";
	},
	onRollback => sub { $report--; }
);
ok((!$rc) && ($scalar == 1234) && ($privscalar == 0) && ($report < 0),"$testtype write/read");

############ force restart, restart sub forces rollback, no commit/rollback sub ##################

$testtype = 'single scalar, force restart, commit+restart+rollback sub, restart sub forces rollback,';
$scalar = 1234;
$privscalar = 0;
$report = 0;
$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onCommit => sub { $report++; }
	onRestart => sub { 
		die "Thread::Sociable STM Restart forces rollback";
	}
	onRollback => sub { $report--; }
);
ok((!$rc) && ($scalar == 1234) && ($privscalar == 0) && ($report < 0),"$testtype write/read");

########################################################################
########################################################################
#
#	MULTIPLE SCALARS
#
########################################################################
########################################################################
$testtype = 'multiscalar, commit, no subs,';
my $other : Sociable = 'abcde';
my $privother;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$privother = $other;
	});
ok($rc && ($scalar == 1234) && ($privscalar == 1234) && ($other eq 'abcde') && ($privother eq 'abcde'),"$testtype readonly");

$rc = sociable_begin_work(
	sub {
		$scalar = 5678;
		$other = 'uvwxyz';
	});
ok($rc && ($scalar == 5678) && ($other eq 'uvwxyz'),"$testtype writeonly");

$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$scalar = "foobar";
		$privother = $other;
		$other = 1245.045e-1;
	});
ok($rc && ($privscalar == 5678) && ($scalar == 'foobar') && ($other == 1245.045e-1) && ($privother eq 'uvwxyz'),"$testtype read/write");

$rc = sociable_begin_work(
	sub {
		$scalar = 'one fine day';
		$privscalar = $scalar;
		$other = -78945;
		$privother = $other;
	});
ok($rc && ($privscalar == 'one fine day') && ($scalar == 'one fine day') && ($other == -78945) && ($privother == -78945),"$testtype write/read");

############ commit, commit sub ##################

$testtype = 'multiscalar, commit, commit sub,';
$other = 'abcde';
$privother = undef;
$report = 0;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$privother = $other;
	},
	onCommit => sub { $report = 1; }
);
ok($rc && $report && ($scalar == 1234) && ($privscalar == 1234) && ($other eq 'abcde') && ($privother eq 'abcde'),"$testtype readonly");

$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = 5678;
		$other = 'uvwxyz';
	},
	onCommit => sub { $report = 1; }
);
ok($rc && $report && ($scalar == 5678) && ($other eq 'uvwxyz'),"$testtype writeonly");

$report = 0;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$scalar = "foobar";
		$privother = $other;
		$other = 1245.045e-1;
	},
	onCommit => sub { $report = 1; }
);
ok($rc && $report && ($privscalar == 5678) && ($scalar == 'foobar') && ($other == 1245.045e-1) && ($privother eq 'uvwxyz'),"$testtype read/write");

$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar = 'one fine day';
		$privscalar = $scalar;
		$other = -78945;
		$privother = $other;
	},
	onCommit => sub { $report = 1; }
);
ok($rc && $report && ($privscalar == 'one fine day') && ($scalar == 'one fine day') && ($other == -78945) && ($privother == -78945),"$testtype write/read");

############ force restart ##################

$testtype = 'multiscalar, restart, no subs,';
my $restart = 1;
$scalar = 1234;
$other = 'abcde';
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$privother = $other;
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 1234) && ($privscalar == 1234) && ($other == 'abcde') && ($privother == 'abcde'),"$testtype readonly");

$restart = 1;
$rc = sociable_begin_work(
	sub {
		$scalar = 5678;
		$other = 'uvwxyz';
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 5678) && ($other == 'uvwxyz'),"$testtype writeonly");

$restart = 1;
$rc = sociable_begin_work(
	sub {
		$privscalar = $scalar;
		$scalar++;
		$privother = $other;
		$other .= '!';
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($privscalar == 5678) && ($scalar == 5679) && ($privother == 'uvwxyz') && ($other == 'uvwxyz!'),"$testtype read/write");

$restart = 1;
$privscalar += 10;
$privother = 'foo bar baz';
$rc = sociable_begin_work(
	sub {
		$scalar = $privscalar;
		$privscalar = $scalar + 1;
		$other = $privother;
		$privother = $other . ' goo';
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});

ok($rc && ($privscalar == 5690) && ($scalar == 5689) && ($privother eq ' 'foo bar baz goo goo') && ($other eq ' 'foo bar baz goo'),"$testtype write/read");

############ force restart, w restart sub ##################

$testtype = 'multiscalar, restart, restart sub,';
$restart = 1;
$scalar = 1234;
$other = 'abcde';
my $report;
$rc = sociable_begin_work(
	sub {
		$scalar++;
		$other .= ' foo';
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report = 123; }
	);
ok($rc && ($scalar == 1235) && ($other == 'abcde foo') && ($report == 123),"$testtype readonly");

############ force restart, w rollback sub ##################

$testtype = 'multiscalar, restart, rollback sub,';
$restart = 1;
$scalar = 1234;
$other = 'abcde';
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar++;
		$other .= ' foo';
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRollback => sub { $report++; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($report == 0),"$testtype write/read");

############ force restart, w restart & rollback sub ##################

$testtype = 'multiscalar, restart, restart+rollback sub,';
$restart = 1;
$scalar = 1234;
$other = 'abcde';
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar++;
		$other .= ' foo';
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report = -1; }
	onRollback => sub { $report += 10; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($report == -1),"$testtype write/read");

############ force rollback, no subs ##################

$testtype = 'multiscalar, rollback, no subs,';
$scalar = 1234;
$other = 'abcde';
$rc = sociable_begin_work(
	sub {
		$scalar++;
		$other .= ' foo';
		die "Thread::Sociable STM rollback test";
	});
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde'),"$testtype readonly");

############ force rollback, restart sub ##################

$testtype = 'multiscalar, rollback, restart sub,';
$scalar = 1234;
$other = 'abcde';
$report;
$rc = sociable_begin_work(
	sub {
		$scalar++;
		$other .= ' foo';
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report = 1; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && (!$report),"$testtype readonly");

############ force rollback, rollback sub ##################

$testtype = 'multiscalar, rollback, rollback sub,';
$scalar = 1234;
$other = 'abcde';
$report;
$rc = sociable_begin_work(
	sub {
		$scalar++;
		$other .= ' foo';
		die "Thread::Sociable STM rollback test";
	},
	onRollback => sub { $report = 1; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($report == 1),"$testtype readonly");

############ force rollback, restart & rollback sub ##################

$testtype = 'multiscalar, rollback, restart+rollback sub,';
$scalar = 1234;
$other = 'abcde';
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar++;
		$other .= ' foo';
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report = -1; });
	onRollback => sub { $report += 10; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($report == 10),"$testtype readonly");

########################################################################
########################################################################
#
#	MULTIPLE SCALARS, SINGLE NESTED TRANSACTION
#
########################################################################
########################################################################

$testtype = 'multiscalar, single nested xaction, ';
############ no restart/rollback, no subs ##################

$scalar = 1234;
$other = 'abcde';
my $nested = 'spelunking';
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested();
	});
ok($rc && ($scalar == 1235) && ($other eq 'abcde foo'),"$testtype commit, no subs");

############ restart, no subs ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested();
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 1235) && ($other eq 'abcde foo'),"$testtype force restart, no subs");

############ force restart, w nested restart sub ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += 1000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report += 123; }
	);
ok($rc && ($scalar == 1235) && ($other == 'abcde foo') && ($report == 1123),"$testtype force restart, restart sub");

############ force restart, w nested rollback sub ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(undef, sub { $report += 10000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRollback => sub { $report++; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($report == 0),"$testtype force restart, rollback sub");

############ force restart, w nested restart & rollback sub ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += 1000; }, sub { $report += 10000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report += 1; }
	onRollback => sub { $report += 10; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($report == 1001),"$testtype force restart, restart & rollback sub");

############ force rollback, no subs ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested();
		die "Thread::Sociable STM rollback test";
	});
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde'),"$testtype force rollback, no subs");

############ force rollback, nested restart sub ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += 1000; });
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report += 1; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && (!$report),"$testtype force rollback, restart sub");

############ force rollback, nested rollback sub ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(undef, sub { $report += 10000; });
		die "Thread::Sociable STM rollback test";
	},
	onRollback => sub { $report += 1; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($report == 10001),"$testtype force rollback, rollback sub");

############ force rollback, nested restart & rollback sub ##################

($restart, $scalar, $other, $report) = (1, 1234, 'abcde', 0);
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += 1000; }, sub { $report += 10000; });
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report += 1; });
	onRollback => sub { $report += 10; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($report == 10010),"$testtype force rollback, restart+rollback subs");


########################################################################
########################################################################
#
#	MULTIPLE SCALARS, 2 NESTED TRANSACTIONS
#
########################################################################
########################################################################

$testtype = 'multiscalar, 2 nested xaction, ';
############ commit, no subs ##################

$scalar = 1234;
$other = 'abcde';
$nested = 'spelunking';
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested();
	});
ok($rc && ($scalar == 1235) && ($other eq 'abcde foo') && ($nested eq 'spelunking deeper!'),"$testtype commit, no subs");

############ commit, commit sub ##################

$scalar = 1234;
$other = 'abcde';
$nested = 'spelunking';
$report = 0;
$rc = sociable_begin_work(
	sub {
		$scalar++;
		commit_nested(\$report);
	},
	onCommit => { $report++; }
);
ok($rc && ($report == 2) && ($scalar == 1235) && ($other eq 'abcde foo') && ($nested eq 'spelunking deeper!'),"$testtype commit, commit subs");

############ restart, no subs ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested();
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 1235) && ($other eq 'abcde foo') && ($nested eq 'spelunking deeper!'),"$testtype force restart, no subs");

############ force restart, w nested restart sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested(sub { $report += 1000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report += 123; }
	);
ok($rc && ($scalar == 1235) && ($other == 'abcde foo') && ($nested eq 'spelunking deeper!') && ($report == 2123),"$testtype force restart, restart sub");

############ force restart, w nested rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested(undef, sub { $report += 10000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRollback => sub { $report++; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($nested eq 'spelunking deeper!') && ($report == 0),"$testtype force restart, rollback sub");

############ force restart, w nested restart & rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested(sub { $report += 1000; }, sub { $report += 10000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report += 1; }
	onRollback => sub { $report += 10; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($nested eq 'spelunking deeper!') && ($report == 2001),"$testtype force restart, restart+rollback sub");

############ force rollback, no subs ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested();
		die "Thread::Sociable STM rollback test";
	});
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking'),"$testtype force rollback, no subs");

############ force rollback, nested restart sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested(sub { $report += 1000; });
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report += 1; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && (!$report),"$testtype force rollback, restart sub");

############ force rollback, nested rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested(undef, sub { $report += 10000; });
		die "Thread::Sociable STM rollback test";
	},
	onRollback => sub { $report += 1; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && ($report == 20001),"$testtype force rollback, rollback sub");

############ force rollback, nested restart & rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		second_nested(sub { $report += 1000; }, sub { $report += 10000; });
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report += 1; });
	onRollback => sub { $report += 10; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && ($report == 20010),"$testtype force rollback, restart+rollback subs");

########################################################################
########################################################################
#
#	MULTIPLE SCALARS, CONSECUTIVE NESTED TRANSACTIONS
#
########################################################################
########################################################################

$testtype = 'multiscalar, consecutive nested xaction ';
############ commit, no subs ##################

$scalar = 1234;
$other = 'abcde';
$nested = 'spelunking';
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested();
		second_nested();
	});
ok($rc && ($scalar == 1235) && ($other eq 'abcde foo') && ($nested eq 'spelunking deeper!'),"$testtype, no subs");

############ restart, no subs ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested();
		second_nested();
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	});
ok($rc && ($scalar == 1235) && ($other eq 'abcde foo') && ($nested eq 'spelunking deeper!'),"$testtype, force restart, no subs");

############ force restart, w nested restart sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += ($report == 2000) ? 1000 : -1; });
		second_nested(sub { $report += 1000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report += ($report == 3000) ? 123 : -1; }
	);
ok($rc && ($scalar == 1235) && ($other == 'abcde foo') && ($nested eq 'spelunking deeper!') && ($report == 3123),"$testtype, force restart, restart sub");

############ force restart, w nested rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(undef, sub { $report += 10000; });
		second_nested(undef, sub { $report += 10000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRollback => sub { $report++; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($nested eq 'spelunking deeper!') && ($report == 0),"$testtype, force restart, rollback sub");

############ force restart, w nested restart & rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += ($report == 2000) ? 1000 : -1; }, sub { $report += 10000; });
		second_nested(sub { $report += 1000; }, sub { $report += 10000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report += ($report == 3000) ? 1 : -1; }
	onRollback => sub { $report += 10; }
	);
ok($rc && ($scalar == 1235)  && ($other == 'abcde foo') && ($nested eq 'spelunking deeper!') && ($report == 3001),"$testtype, force restart, restart+rollback subs");

############ force rollback, no subs ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested();
		second_nested();
		die "Thread::Sociable STM rollback test";
	});
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking'),"$testtype, force rollback, no subs");

############ force rollback, nested restart sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += 1000; });
		second_nested(sub { $report += 1000; });
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report += 1; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && (!$report),"$testtype, force rollback, restart sub");

############ force rollback, nested rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(undef, sub { $report += ($report == 20000) ? 10000 : -1; });
		second_nested(undef, sub { $report += 10000; });
		die "Thread::Sociable STM rollback test";
	},
	onRollback => sub { $report += ($report == 30000) ? 1 : -100000; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && ($report == 30001),"$testtype, force rollback, rollback subs");

############ force rollback, nested restart & rollback sub ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += 1000; }, sub { $report += ($report == 20000) ? 10000 : -1; });
		second_nested(sub { $report += 1000; }, sub { $report += 10000; });
		die "Thread::Sociable STM rollback test";
	},
	onRestart => sub { $report += 1; });
	onRollback => sub { $report += ($report == 30000) ? 1 : -100000; });
ok((!$rc) && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && ($report == 30001),"$testtype, force rollback, restart+rollback subs");


############ force restart, consecutive nested subs, rollback from innermost ##################

my $rolled = 0;
my $forceroll = 1;
($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += ($report == 2000) ? 1000 : -1; }, sub { $rolled += 10000; });
		second_nested(sub { $report += 1000; }, sub { $rolled += 10000; });
		$restart = 0,
		die "Thread::Sociable STM RESTART"
			if $restart;
	},
	onRestart => sub { $report += ($report == 3000) ? 1 : -1; }
	onRollback => sub { $report += 10; }
	);
ok($rc && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && ($report == 0) && ($rolled == 30010),'consecutive nested xaction, w/ restart, rollback from innermost');

############ force restart, consecutive nested subs, rollback from outermost restart ##################

($restart, $scalar, $other, $report, $nested) = (1, 1234, 'abcde', 0, 'spelunking');
$rc = sociable_begin_work(
	sub {
		$scalar++;
		first_nested(sub { $report += ($report == 2000) ? 1000 : -1; }, sub { $report += 10000; });
		second_nested(sub { $report += 1000; }, sub { $report += 10000; });
		$restart = 0,
		die "Thread::Sociable nested rollback from restart"
			if $restart;
	},
	onRestart => sub {
		$report += ($report == 3000) ? 1 : -1;
		die "Rollback from outermost transaction\n";
	},
	onRollback => sub { $report += 10; }
	);
ok($rc && ($scalar == 1234)  && ($other == 'abcde') && ($nested eq 'spelunking') && ($report == 3001) && ($rolled == 30010),'consecutive nested xaction, w/ restart, rollback from outermost');

########################################################################
########################################################################
#
#	MULTITHREADED: 2 THREADS
#
########################################################################
########################################################################

$scalar = 'hello world';

my $thrd1 = threads->create(\&read_only);
my $thrd2 = threads->create(\&read_only);
my $rc1 = $thrd1->join();
my $rc2 = $thrd2->join();
ok($rc1 && $rc2,'readonly, 2 threads');

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_only, 'second');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'writeonly, 2 threads');

$thrd1 = threads->create(\&write_only, 'foo bar baz');
$thrd2 = threads->create(\&read_after_write);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'read after write, 2 threads');

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_after_read, 'second');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'write after read, 2 threads');

$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_write_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($scalar eq 'this is stm'),'read/write + conflicting read/write');

$thrd1 = threads->create(\&write_read_1, 'foo bar baz');
$thrd2 = threads->create(\&write_read_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($scalar eq 'this is stm'),'write/read + write/read');

$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_read_2, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2,'read/write + conflicting read/read');

$thrd1 = threads->create(\&read_write_1, 'foo bar baz');
$thrd2 = threads->create(\&read_write_rollback, 'this is stm');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
ok($rc1 && $rc2 && ($scalar eq 'foo bar baz'),'read/write + read/write rollback');


########################################################################
########################################################################
#
#	MULTITHREADED: 4 THREADS
#
########################################################################
########################################################################

$scalar = 'hello world';

$thrd1 = threads->create(\&read_only);
$thrd2 = threads->create(\&read_only);
my $thrd3 = threads->create(\&read_only);
my $thrd4 = threads->create(\&read_only);
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
my $rc3 = $thrd1->join();
my $rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'readonly, 4 threads');

$thrd1 = threads->create(\&write_only, 'first');
$thrd2 = threads->create(\&write_only, 'second');
$thrd3 = threads->create(\&write_only, 'third');
$thrd4 = threads->create(\&write_only, 'fourth');
$rc1 = $thrd1->join();
$rc2 = $thrd2->join();
$rc3 = $thrd1->join();
$rc4 = $thrd2->join();
ok($rc1 && $rc2 && $rc4 && $rc4,'writeonly, 4 threads');


}

sub first_nested {
	my ($restart, $rollback) = @_;
	sociable_begin_work(sub {
		$other .= 'foo';
		},
		onRestart => $restart,
		onRollback => $rollback
	);
}

sub commit_nested {
	my ($report) = @_;
	sociable_begin_work(sub {
		$other .= ' foo';
		third_nested();
		},
		onCommit => { $$report++; }
	);
}

sub second_nested {
	my ($restart, $rollback) = @_;
	sociable_begin_work(sub {
		$other .= ' foo';
		third_nested(
			($restart ? sub { $report += ($report ? -1000 : 1000); } : undef),
			($rollback ?  sub { $report += ($report ? -10000 : 10000); } : undef)
		);
		},
		onRestart => $restart,
		onRollback => $rollback
	);
}

sub third_nested {
	my ($restart, $rollback) = @_;
	sociable_begin_work(sub {
		$nested .= ' deeper!';
		},
		onRestart => ($forceroll ? sub { die "Rollback from innermost transaction\n"; } : $restart),
		onRollback => $rollback
	);
}

sub read_only {
	my $privscalar;
	my $restarted = 0;
	sleep 3;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar eq 'hello world'));
}

sub write_only {
	my $privscalar = shift;
	my $restarted = 0;
	sleep 2;
	my $rc = sociable_begin_work(
		sub {
			$scalar = $privscalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_after_write {
	my $privscalar;
	my $restarted = 0;
	sleep 4;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted && ($privscalar eq 'foo bar baz'));
}

sub write_after_read {
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 4;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub read_write_1 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 3;
			$scalar = $input;
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return ($rc && !$restarted && !$rollback);
}

sub read_write_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 5;
			$scalar = $input;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

sub write_read_1{
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$scalar = $input;
			sleep 3;
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub write_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$scalar = $input;
			sleep 5;
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && !$restarted);
}

sub read_write_rollback {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rollback = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 5;
			$scalar = $input;
			die "read/write rollback";
		},
		onRestart => sub { $restarted = 1; },
		onRollback => sub { $rollback = 1; }
		);
	return (!$rc && $restarted && $rollback);
}

sub read_read_2 {
	my $input = shift;
	my $privscalar;
	my $restarted = 0;
	my $rc = sociable_begin_work(
		sub {
			$privscalar = $scalar;
			sleep 5;
			$privscalar = $scalar;
		},
		onRestart => sub { $restarted = 1; }
		);
	return ($rc && $restarted);
}

1;