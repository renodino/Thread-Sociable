#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#ifdef HAS_PPPORT_H
#  include "ppport.h"
#endif
/*
 *	kiped from Time::HiRes; only works for Win32 and POSIX
 */
#ifdef WIN32

typedef union {
    unsigned __int64	ft_i64;
    FILETIME		ft_val;
} FT_t;

/* Number of 100 nanosecond units from 1/1/1601 to 1/1/1970 */
#ifdef __GNUC__
#define Const64(x)	x##LL
#else
#define Const64(x)	x##i64
#endif
# define EPOCH_BIAS Const64(116444736000000000)

#endif

/*
 *	don't give a damn about all the fancy drift and skew adjustments
 *	just give us a reasonable hires timestamp
 */
double hires_time()
{
#ifdef WIN32

    FT_t ft;
    unsigned long tv_sec;
    unsigned long tv_usec;
    double retval;

	GetSystemTimeAsFileTime(&ft.ft_val);

    /* seconds since epoch */
    tv_sec = (long) ( (ft.ft_i64 - EPOCH_BIAS) / Const64(10000000) );

    /* microseconds remaining */
    tv_usec = (long)( (ft.ft_i64 / Const64(10)) % Const64(1000000) );

	retval = tv_sec + (tv_usec / 1000000.);

#else

	struct timeval Tp;
	int status = gettimeofday (&Tp, NULL);
	double retval = -1.0;

	if (! status)
		retval = Tp.tv_sec + (Tp.tv_usec / 1000000.);

#endif

	return retval;
}
