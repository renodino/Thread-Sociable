#ifndef _CRT_MEMMGT_H_

#define _CRT_MEMMGT_H_ 1
/*
 *	Because Perl CORE, in its infinite wisdom, has hijacked
 *	CRT's malloc() and free() symbols, without providing
 *	any replacement symbols for them, we have
 *	to create a separate object to link into our XS code
 */

#define CRT_Newz(v,n,t) \
	if ((v = (t *)crt_malloc((n) * sizeof(t))) == NULL) \
		Perl_croak_nocontext("Out of CRT memory!"); \
	memzero((char*)(v), (n)*sizeof(t))

#define CRT_Dup(v,n,t,s,l) \
	if ((v = (t *)crt_malloc((n) * sizeof(t))) == NULL) \
		Perl_croak_nocontext("Out of CRT memory!"); \
	memcpy((char*)(v), (char*)(s), (l) * sizeof(t)); \
	if ((l) < (n)) \
		memzero((char*)(&v[(n)]), ((n) - (l)) * sizeof(t))

#define CRT_Realloc(v,n,t,l) \
	if ((v = crt_realloc(v, n, sizeof(t), l)) == NULL) \
		Perl_croak_nocontext("Out of CRT memory!");

#define CRT_Extend(v,n,t,l) \
	if ((v = crt_realloc(v, n, sizeof(t), (n + l))) == NULL) \
		Perl_croak_nocontext("Out of CRT memory!");

// #define CRT_DEBUG 1
#ifdef CRT_DEBUG
#define crt_malloc(v) crt_malloc_dbg(v, __LINE__)
#define crt_free(v) crt_free_dbg(v, __LINE__)
#define crt_strdup(v) crt_strdup_dbg(v, __LINE__)
#define crt_realloc(v,n,t,l) crt_realloc_dbg(v,n,t,l, __LINE__)

void *crt_malloc_dbg(int size, int line);

void *crt_free_dbg(void *ptr, int line);

void *crt_strdup_dbg(void *str, int line);

void *crt_realloc_dbg(void *ptr, int cursize, int elemsize, int count, int line);

#else
void *crt_malloc(int size);

void *crt_free(void *ptr);

char *crt_strdup(char *str);

void *crt_realloc(void *ptr, int cursize, int elemsize, int count);

#endif

void crt_check();

#endif