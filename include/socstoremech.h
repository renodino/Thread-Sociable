#ifndef _SOCSTOREMECH_H_

#define _SOCSTOREMECH_H_

#include "atomic_ops.h"
/*
 *	storage mechanism dispatcher
 *
 *	Each storage mechanism keeps its own element/structure pool,
 *	and provides its own set of memory alloc/free methods,
 *	as well as read/write methods. If a mechanism is marked "native",
 *	then direct memory access operations are used when possible
 *	(i.e., for process-private and process-shared mechanisms).
 *
 *	Each mechanism also provides its own locking/conditional methods,
 *	spinlock methods, and various methods related to the tie()d
 *	vtbls.
 *
 *	A mechanism author creates a library
 *	which must provide an implementation of the init() method,
 *	and which is responsible for populating the provided empty version of
 *	this structure. The application installs a mechanism via the
 *	Thread::Sociable::sociable_install_mechanism() method call; the native
 *	process-local heap based mechanism is always installed by default.
 *	Mechanisms are installed into a fixed size array of available
 *	mechanisms at a fixed index for all threads/processes/nodes using
 *  the mechanism. It is the responsibility of the application
 *	to assure consistent mechanism indexes across processes/nodes
 *	(a future release may provide a method for publishing the
 *	mechanism map thru all mechanisms).
 *
 *	Add'l debug signature of some methods are provided which
 *	provide the line number within Sociable.xs from which they are
 *	called. If a mechanism leaves these entries NULL, the macros
 *	will fallback to the non-debug versions.
 *
 *	SV's access mechanism storage via proxy elements, which contain
 *	element "locators" specifying the mechanism ID, access level
 *	(process-private, process-shared, or foreign), and a union
 *	providing either a pointer (for process-private), a segment ID/offset
 *	tuple (for process-shared), or a simple 8 byte element ID (for foreign).
 *	The proxy's are manufactured by the mechanism's new_array(),
 *	new_hash(), and new_scalar() methods (for new sociables), and, for
 *	associating with existing sociables, the create_proxy() method.
 *
 *	For native mechanisms, the proxy factory methods are expected to
 *	is provide a directly addressable pointer to the associated element.
 *	For process-private, the pointer to the same as the locator's
 *	pointer field; for process-shared, the pointer is computed from
 *	a base segment address identified by the locator's segment ID
 *	(usually a shmid) and an offset from the base.
 *
 *	Native mechanisms are expected to use the provided sociable_element_t
 *	and sociable_structure_t type definitions as the base for their
 *	elements; they may use extended definitions of these types,
 *	but such extensions should always physically follow the standard type definition.
 *	Also note that native mechanism do not need to provide implementations
 *	of the following methods, as they are provided by direct memory
 *	access to the element structures:
 *		- spinlock_acquire
 *		- spinlock_release
 *		- get_scalar_len
 *		- clear_scalar
 *		- get_sv_type
 *		- is_elem_updated
 *		- incr_elem_seqno
 *		- bless
 *		- is_encoded
 *		- set_encoding
 *		- soc_sv_save
 *		- soc_get_seqno
 *		- expired
 *
 *	Note that locators are expected to be identical across all instances
 *	of a mechanism within an application, so that a ref to a sociable
 *	element can be passed between threads/processes/nodes without
 *	extra translation. Also note that assigning refs of one mechanism
 *	to sociable's of another mechanism is possible, so long as the
 *	receiving mechanism's level is less than or equal to the referent's
 *	mechanism (i.e., process private can accept all mechanisms,
 *	process shared can accept all BUT process private, and
 *	foreign can only accept foreign)
 *
 *	Multi-mechanism transactions:
 *		Each mechanism is separately committed, starting with the
 *		higher level (ergo, largest overhead) mechanisms first.
 *		The result is a minimal 2 phase commit; if any mechanism
 *		cannot acquire all transactional references, then all
 *		contexts are restarted or aborted.
 *		Each mechanism may use its own conflict detection and
 *		locking protocol within the transaction; however,
 *		all transactions referencing the same mechanism
 *		MUST use the same locking protocol.
 *
 *  Multi-mechanism queueing:
 *		queues of lower levels can accept elements of a higher or same
 *		level; higher level queues cannot accept elements of lower
 *		levels.
 *
 *	implements AppleSeeds mechanism
 */

typedef struct sociable_stm_detector sociable_stm_detector_t;

/*
 *	mechanism levels: used to coordinate compatible
 *	levels for storage of references
 */
#define SOC_PROCESS_PRIVATE_MECH (1)
#define SOC_PROCESS_SHARED_MECH (2)
#define SOC_FOREIGN_MECH (3)
#define SOC_COMPATIBLE_MECH(target, source) (target->mech->level <= source->mech->level)

typedef struct sociable_storage_mech {
	char *name;			/* name of mechanism */
	char *classname;	/* name of class implementing this mechanism */
	bool can_stm;		/* true => can be transactionally referenced */
	bool can_queue;		/* true => implements native queueing */
	bool debug;
	bool warn;
	bool always_lockable;
	bool eager;
	bool lockfree;
	byte level;			/* mechanism level */
	int mechid;			/* this structure's index in global sociable_mechs array */
	void *tsdq_global_lock;
	sociable_stm_detector_t *detector;

	int (*is_lockable)(sociable_proxy_t *proxy);
/*
	int (*lock_alloc)(pTHX_ void *lock);
	int (*lock_free)(pTHX_ void *lock);
*/
#ifdef SOCIABLE_DEBUG_LOCKS
	int (*lock)(pTHX_ void *lock, int line);
#else
	int (*lock)(pTHX_ void *lock);
#endif
	int (*unlock)(pTHX_ void *lock);
	int (*cond_wait)(pTHX_ sociable_proxy_t *proxy, sociable_proxy_t *cond_proxy);
	int (*cond_timedwait)(pTHX_  sociable_proxy_t *proxy, double timeout, sociable_proxy_t *cond_proxy);
	int (*cond_signal)(pTHX_  sociable_proxy_t *proxy);
	int (*cond_broadcast)(pTHX_  sociable_proxy_t *proxy);

	int (*spinlock_acquire)(int spinlock);
	int (*spinlock_release)(int spinlock);

	sociable_proxy_t *(*create_proxy)(sociable_locator_t *elemloc);
	sociable_proxy_t *(*new_array)(pTHX_ int sv_flags, int withlock);
	sociable_proxy_t *(*new_hash)(pTHX_  int sv_flags, int withlock);
	sociable_proxy_t *(*new_scalar)(pTHX_ SV *sv, int withlock);
	void (*destroy)(pTHX_ sociable_proxy_t *proxy);
	int (*get_scalar_len)(pTHX_ sociable_proxy_t *proxy);
	void (*clear_scalar)(pTHX_ sociable_proxy_t *proxy);
	int (*get_sv_type)(sociable_proxy_t *proxy);

	int (*is_elem_updated)(sociable_proxy_t *proxy);
	int (*incr_elem_seqno)(sociable_proxy_t *proxy);

	int (*bless)(sociable_proxy_t *proxy, char *stash);

	bool (*set_debug)(bool flag);
	bool (*set_warn)(bool flag);
	bool (*set_always_lockable)(bool flag);

	bool (*debug_enabled)();
	bool (*warn_enabled)();
	bool (*always_lockable_enabled)();

	int (*get_extent)(sociable_proxy_t *proxy);
	int (*get_himark)(sociable_proxy_t *proxy);
	int (*set_extent)(sociable_proxy_t *proxy, int extent, int himark);
	int (*default_extent)(pTHX_ int extent, int himark);

	int (*is_encoded)(sociable_proxy_t *proxy);
	void (*set_encoding)(sociable_proxy_t *proxy, int useUTF8);

	void (*sv_recover)(pTHX_ SV *sv, sociable_proxy_t *proxy);
	void (*sv_save)(pTHX_ SV *sv, sociable_proxy_t *proxy);
	U32 *(*get_seqno)(sociable_proxy_t *proxy);
	U32 (*get_seqnolo)(char *elemid);
	U32 (*get_seqnohi)(char *elemid);
	int (*expired)(sociable_proxy_t *proxy, int cycleno);
	void (*get_properties)(HV *mechhv);
	SV *(*get_property)(char *property);
	SV *(*set_property)(char *property, SV *val);
	sociable_locator_t *(*get_reference)(sociable_proxy_t *proxy);
	int (*is_rv)(sociable_proxy_t *proxy);
	int (*is_tied)(sociable_proxy_t *proxy);
	void *(*addr_to_ptr)(sociable_addr_t *loc);
	int (*is_same_elem)(char *newelemid, char *elemid);
	sociable_locator_t *(*get_rv)(sociable_locator_t *elemloc);
	char *(*get_pkgname)(sociable_locator_t *elemloc);
	int (*get_array_head)(char *elemid);
	void (*set_ref)(sociable_proxy_t *proxy, sociable_locator_t *refloc);
	int (*sv_ok)(sociable_proxy_t *proxy);
/*
 *	structure tie methods
 */
	int (*array_fetch)(pTHX_ sociable_proxy_t *proxy, int index, int for_write, sociable_proxy_t *valproxy);
	int (*hash_lookup)(pTHX_ sociable_proxy_t *proxy, char *key, int len, int for_write, sociable_proxy_t *valproxy);
	int (*array_delete)(pTHX_ sociable_proxy_t *proxy, int index);
	int (*remove_hash_element)(pTHX_ sociable_proxy_t *proxy, char *key, int len);
	int (*array_get_size)(pTHX_ sociable_proxy_t *proxy);
	int (*array_clear)(pTHX_ sociable_proxy_t *proxy);
	int (*hash_clear)(pTHX_ sociable_proxy_t *proxy);
	SV *(*splice)(pTHX_ sociable_proxy_t *proxy, int items, int offset, int len, int listlen, SV **ary);
	int (*push)(pTHX_ sociable_proxy_t *proxy, int listlen, SV **ary);
	SV *(*pop)(pTHX_ sociable_proxy_t *proxy);
	SV *(*shift)(pTHX_ sociable_proxy_t *proxy);
	int (*unshift)(pTHX_ sociable_proxy_t *proxy, int listlen, SV **ary);
	int (*extend)(pTHX_ sociable_proxy_t *proxy, int listlen);
	int (*storesize)(pTHX_ sociable_proxy_t *proxy, int count);
	int (*array_elem_exists)(pTHX_ sociable_proxy_t *proxy, int elem_index);
	int (*hash_elem_exists)(pTHX_ sociable_proxy_t *proxy, char *key, int key_len);
	SV *(*firstkey)(pTHX_ sociable_proxy_t *proxy);
	SV *(*nextkey)(pTHX_ sociable_proxy_t *proxy, char *prevkey, int prevkeylen);
/*
 *	STM methods
 */
	int (*stm_update_karma)();
	int (*stm_get_karma)();
	int (*stm_make_owner)();
	int (*stm_am_owner)();
	sociable_stm_ctxt_t *(*stm_get_owner)(sociable_proxy_t *proxy);
	void (*stm_merge_element)(pTHX_ sociable_addr_t *refloc, sociable_element_t *elemp);
	void (*stm_acquire_commit_lock)();
	int (*stm_acquire_for_commit)(pTHX_ sociable_stm_log_t *wal);
	void (*stm_release_commit_lock)();
	/* allocate a transaction context for the specified mechanism */
	int (*stm_alloc_ctxt)(sociable_addr_t *ptr);
	void (*stm_free_ctxt)(sociable_addr_t *ptr);
	void (*commit_with_lock)(bool flag);
	void (*eager_detect)(bool flag);
/*
 *	heap mgmt methods
 */
	int (*mem_alloc)(sociable_addr_t *loc, int size);
	int (*mem_free)(sociable_addr_t *loc);
	int (*mem_realloc)(sociable_addr_t *loc, int cursize, int newsize);
	int (*mem_strdup)(sociable_addr_t *loc, char *str);
	int (*mem_set)(sociable_addr_t *loc, char fill, int size);
	int (*mem_read)(void *to, sociable_addr_t *from, int size);
	int (*mem_write)(sociable_addr_t *to, void *from, int size);

	int (*mem_alloc_dbg)(sociable_addr_t *loc, int size, int line);
	int (*mem_free_dbg)(sociable_addr_t *loc, int line);
	int (*mem_realloc_dbg)(sociable_addr_t *loc, int cursize, int newsize, int line);
	int (*mem_strdup_dbg)(sociable_addr_t *loc, char *str, int line);
	int (*mem_check)();
} sociable_storage_mech_t;

#ifdef SOCIABLE_DEBUG
/*
 *	debug versions of basic memory functions
 *
 *	allocates a buffer; returns a pointer to physical memory
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_ALLOC(mech, to, size) (*mech->mem_alloc_dbg)(to, size, __LINE__)
#define SOC_LOC_ALLOC(mech, loc, size) \
	(*mech->mem_alloc_dbg)(&(loc)->un, size, __LINE__); \
	(loc)->mechid = mech->mechid; (loc)->level = mech->level
/*
 *	frees a buffer indicated by a pointer to physical memory;
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_FREE(mech, addr) (*mech->mem_free_dbg)(addr, __LINE__)
#define SOC_LOC_FREE(mech, loc) (*mech->mem_free_dbg)(&(loc)->un, __LINE__)

/*
 *	reallocates a buffer indicated by a pointer to physical memory;
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_REALLOC(mech, addr, count, size, newcount) \
	(*mech->mem_realloc_dbg)(addr, (count) * (size), (newcount) * (size), __LINE__)
#define SOC_LOC_REALLOC(mech, loc, count, size, newcount) \
	(*mech->mem_realloc_dbg)(&(loc)->un, (count) * (size), (newcount) * (size), __LINE__)

/*
 *	duplicate a NUL terminated string, returning a pointer to physical memory;
 *	source string is physical memory pointer
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_STRDUP(mech, to, from) (*mech->mem_strdup_dbg)(to, from, __LINE__)
#define SOC_LOC_STRDUP(mech, to, from) (*mech->mem_strdup_dbg)(&(to)->un, &(from)->un, __LINE__)

/*
 *	allocates a buffer and clears to zeros; returns a pointer to physical memory
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_NEWZ(mech, str, count, stype) \
	if ((str = (stype *)(*mech->mem_alloc_dbg)((count) * sizeof(stype), __LINE__) == NULL) \
		Perl_croak_nocontext("MECH_NEWZ: Storage mechanism %s out of memory!", mech->name); \
	if (mech->level != SOC_FOREIGN) \
		memzero((char*)(str), (count)*sizeof(stype)); \
	else \
		(*mech->mem_set)((char*)(str), '\0', (count)*sizeof(stype))
/*
 *	duplicate a structure, returning a pointer to physical memory;
 *	source structure is physical memory pointer
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_DUP(mech, str, count, stype, src, len) \
	if ((str = (stype *)(*mech->mem_alloc_dbg)((count) * sizeof(stype), __LINE__)) == NULL) \
		Perl_croak_nocontext("MECH_DUP: Storage mechanism %s out of memory!", mech->name); \
	if (mech->level != SOC_FOREIGN) {\
		memcpy((char*)str, (char*)src, len * sizeof(stype)); \
		if ((len) < (count)) \
			memzero((char*)(&str[(count)]), ((count) - (len)) * sizeof(stype)); \
	else {\
		(*mech->mem_write)((char*)str, (char*)src, len * sizeof(stype)); \
		if ((len) < (count)) \
			(*mech->mem_set)((char*)(&str[(count)]), '\0', ((count) - (len)) * sizeof(stype)); \
	}

#else

/*
 *	non-debug versions of basic memory functions
 *
 *	allocates a buffer; returns a pointer to physical memory
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_ALLOC(mech, to, size) (*mech->mem_alloc)(to, size)
#define SOC_LOC_ALLOC(mech, loc, size) \
	(*mech->mem_alloc)(&(loc)->un, size); (loc)->mechid = mech->mechid; (loc)->level = mech->level

/*
 *	frees a buffer indicated by a pointer to physical memory;
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_FREE(mech, ptr) (*mech->mem_free)(ptr)
#define SOC_LOC_FREE(mech, loc) (*mech->mem_free)(&(loc)->un)

/*
 *	reallocates a buffer indicated by a pointer to physical memory;
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_REALLOC(mech, ptr, count, size, newcount) \
	(*mech->mem_realloc)(ptr, (count) * (size), (newcount) * (size))
#define SOC_LOC_REALLOC(mech, loc, count, size, newcount) \
	(*mech->mem_realloc)(&(loc)->un, (count) * (size), (newcount) * (size))

/*
 *	duplicate a NUL terminated string, returning a pointer to physical memory;
 *	source string is physical memory pointer
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_STRDUP(mech, to, from) (*mech->mem_strdup)(to, from)
#define SOC_LOC_STRDUP(mech, to, from) (*mech->mem_strdup)(&(to)->un, &(from)->un)

/*
 *	allocates a buffer and clears to zeros; returns a pointer to physical memory
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_NEWZ(mech, str, count, stype, line) \
	if ((str = (stype *)(*mech->mem_alloc_dbg)((count) * sizeof(stype))) == NULL) \
		Perl_croak_nocontext("MECH_NEWZ: Storage mechanism %s out of memory!", mech->name); \
	(*mech->mem_set)((char*)(str), '\0', (count)*sizeof(stype))
/*
 *	duplicate a structure, returning a pointer to physical memory;
 *	source structure is physical memory pointer
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_DUP(mechnum, str, count, stype, src, len) \
	if ((str = (stype *)(*mech->mem_alloc_dbg)((count) * sizeof(stype))) == NULL) \
		Perl_croak_nocontext("MECH_DUP: Storage mechanism %s out of memory!", mech->name); \
	if (mech->level != SOC_FOREIGN) {\
		memcpy((char*)str, (char*)src, len * sizeof(stype)); \
		if ((len) < (count)) \
			memzero((char*)(&str[(count)]), ((count) - (len)) * sizeof(stype)); \
	else {\
		(*mech->mem_write)((char*)str, (char*)src, len * sizeof(stype)); \
		if ((len) < (count)) \
			(*mech->mem_set)((char*)(&str[(count)]), '\0', ((count) - (len)) * sizeof(stype)); \
	}

#endif

/*
 *	set a buffer to a specific byte value
 *	source structure is physical memory pointer
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_MEMSET(mech, str, val, len) \
	if (mech->level != SOC_FOREIGN) \
		memset(str, val, len); \
	else \
		(*mech->mem_set)(str, val, len)

/*
 *	read a buffer; target is physical memory pointer
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_READ(mech, to, from, size) \
	if (mech->level != SOC_FOREIGN) \
		memcpy(to, from, size); \
	else \
		(*mech->mem_read)(to, from, size)

/*
 *	write a buffer; source is physical memory pointer
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_MEM_WRITE(mech, to, from, size) \
	if (mech->level != SOC_FOREIGN) \
		memcpy(to, from, size); \
	else \
		(*mech->mem_write)(to, from, size)

/*
 *	allocate a lock structure
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_LOCK_ALLOC(mech, lock) (*mech->lock_alloc)(aTHX_ lock)

/*
 *	free a lock structure
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_LOCK_FREE(mech, lock) (*mech->lock_free)(aTHX_ lock)

/*
 *	apply lock of a lock structure
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_LOCK(mech, lock) (*mech->lock)(aTHX_ lock)

/*
 *	unlock a lock structure
 *	mechanism is responsible for any redirection to alternate structures
 */
#define SOC_UNLOCK(mech, lock) (*mech->unlock)(aTHX_ lock)

#define SOC_COND_WAIT(mech, lock) (*mech->cond_wait)(aTHX_ lock)

#define SOC_COND_TIMEDWAIT(mech, lock, timeout) (*mech->cond_timedwait)(aTHX lock, timeout)

#define SOC_COND_SIGNAL(mech, lock) (*mech->cond_signal)(aTHX_ lock)

#define SOC_COND_BROADCAST(mech, lock) (*mech->cond_broadcast)(aTHX_ lock)

#define SOC_SPIN_LOCK(proxy) \
	if (proxy->sociable.level != SOC_FOREIGN) \
		SOC_ACQUIRE_SPINLOCK(&proxy->elemp->header.spinlock); \
	else \
		(*proxy->mech->spinlock_acquire)(proxy)

#define SOC_SPIN_RELEASE(proxy) \
	if (proxy->sociable.level != SOC_FOREIGN) \
		SOC_RELEASE_SPINLOCK(&proxy->elemp->header.spinlock); \
	else \
		(*proxy->mech->spinlock_release)(proxy)


#endif