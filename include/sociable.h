#ifndef _SOCIABLE_H_
#define _SOCIABLE_H_

#ifndef HvNAME_get
#  define HvNAME_get(hv)        (0 + ((XPVHV*)SvANY(hv))->xhv_name)
#endif

/*
 *	trace prototype
 */
STATIC void sociable_trace(const char *pat, ...);

#ifdef SOCIABLE_DEBUG

#define SOCIABLE_TRACE(fmt) \
	sociable_trace(fmt)

#define SOCIABLE_TRACE_1(fmt, arg) \
	sociable_trace(fmt, arg)

#define SOCIABLE_TRACE_2(fmt, arg1, arg2) \
	sociable_trace(fmt, arg1, arg2)

#define SOCIABLE_TRACE_3(fmt, arg1, arg2, arg3) \
	sociable_trace(fmt, arg1, arg2, arg3)

#define SOCIABLE_TRACE_4(fmt, arg1, arg2, arg3, arg4) \
	sociable_trace(fmt, arg1, arg2, arg3, arg4)

#define SOCIABLE_TRACE_5(fmt, arg1, arg2, arg3, arg4, arg5) \
	sociable_trace(fmt, arg1, arg2, arg3, arg4, arg5)

#define SOCIABLE_TRACE_6(fmt, arg1, arg2, arg3, arg4, arg5, arg6) \
	sociable_trace(fmt, arg1, arg2, arg3, arg4, arg5, arg6)

#else

#define SOCIABLE_TRACE(fmt)

#define SOCIABLE_TRACE_1(fmt, arg)

#define SOCIABLE_TRACE_2(fmt, arg1, arg2)

#define SOCIABLE_TRACE_3(fmt, arg1, arg2, arg3)

#define SOCIABLE_TRACE_4(fmt, arg1, arg2, arg3, arg4)

#define SOCIABLE_TRACE_5(fmt, arg1, arg2, arg3, arg4, arg5)

#define SOCIABLE_TRACE_6(fmt, arg1, arg2, arg3, arg4, arg5, arg6)

#endif


/*
 *******************************
 *	to setup the element reference mechanism on a variable:
 *
	sv_magicext(sv, NULL, PERL_MAGIC_ext,
		&sociable_scalar_vtbl, (char *)elemp, 0);

 * then to recover the element...

    mg = mg_find(sv, PERL_MAGIC_ext);
    if (mg) {
        elemp = (sociable_element_t *)(mg->mg_ptr);

 * verify the element is valid

        if ((elemp == NULL) || (!elemp->inuse))
        	return NULL;
    }
 *******************************
*/

/*
 *	SOCIABLE FLAG DEFINITIONS
 */
/*
 * element is in use
 */
#define SOC_INUSE (1)
#define SOC_IS_INUSE(elemp) ((elemp)->header.header.soc_flags & 1)
/*
 * element has lock resources
 */
#define SOC_LOCKABLE (2)
#define SOC_IS_LOCKABLE(container) (container->header.soc_flags & 2)
/*
 * element is a transient queue element
 */
#define SOC_TRANSIENT (4)
#define SOC_IS_TRANSIENT(elemp) (elemp->header.header.soc_flags & 4)
/*
 * element can participate in STM logging
 */
#define SOC_STMABLE (8)
#define SOC_CAN_STM(elemp) (elemp->header.header.soc_flags & 8)
#define SOC_USE_STM(proxy) \
	(MY_CXT.stm_xact_count && (!MY_CXT.stm_reconciled) && (proxy->header.soc_flags & 8))
/*
 * element has been transactionally read
 */
#define SOC_STM_READ (16)
#define SOC_STM_WAS_READ(elemp) (elemp->header.header.soc_flags & 16)
/*
 * element has been transactionally written
 */
#define SOC_STM_WRITE (32)
#define SOC_STM_WAS_WRITTEN(elemp) (elemp->header.header.soc_flags & 32)
#define SOC_STM_WAS_ACCESSED(elemp) (elemp->header.header.soc_flags & 48)
/*
 * element is thread-private (ie, alloc'd from CRT heap)
 */
#define SOC_STM_PRIVATE (64)
#define SOC_STM_IS_PRIVATE(elemp) (elemp->header.header.soc_flags & 64)
#define SOC_STM_NOT_SOCIABLE(elemp) (!(elemp->header.header.soc_flags & 64))
/*
 * element is process-addressable (ie, "native")
 */
#define SOC_NATIVE (128)
#define SOC_IS_NATIVE(container) (container->header.soc_flags & 128)
/*
 * element is a proxy (which is also private)
 */
#define SOC_STM_PROXY (192)
#define SOC_STM_IS_PROXY(elemp) (elemp->header.header.soc_flags & 192)
/*
 * element is a queue structure
 */
#define SOC_STM_QUEUE (256)
#define SOC_IS_QUEUE(elemp) (elemp->header.header.soc_flags & 256)
/*
 * a transactional write has occured on an element of the structure
 */
#define SOC_STM_ELEM_WRITE (512)
#define SOC_STM_HAS_WRITES(elemp) (elemp->header.header.soc_flags & 512)
/*
 * its a structure, not an element
 */
#define SOC_STRUCTURE (1024)
#define SOC_IS_STRUCTURE(elemp) (elemp->header.header.soc_flags & 1024)
#define SOC_IS_ELEMENT(elemp) (!(elemp->header.header.soc_flags & 1024))
/*
 * a transactional delete has occured on an element of a hash structure
 */
#define SOC_STM_ELEM_DELETED (2048)
#define SOC_STM_DELETED(elemp) (elemp->header.header.soc_flags & 2048)
#define SOC_STM_UNDELETE(elemp) (elemp->header.header.soc_flags & (~2048))
/*
 *	UTF8 encoding cloning
 */
#define SOC_ENCODE_SV(sv, proxy) \
	if (((proxy->mech->level == SOC_FOREIGN_MECH) \
		? (*proxy->mech->is_encoded)(proxy) \
		: SvUTF8(&proxy->elemp->header.header))) \
		SvUTF8_on(sv);\
	else SvUTF8_off(sv)

#define SOC_ENCODE_ELEM(sv, proxy) \
	if (((proxy->mech->level != SOC_FOREIGN_MECH) \
		if (SvUTF8(sv)) SvUTF8_on(&proxy->elemp->header);\
		else SvUTF8_off(&proxy->elemp->header); \
	} \
	else (*elemp->header.mech->set_encoding)(proxy, SvUTF8(sv))
/*
 * default number of element slots to allocate;
 * can be overridden
 */
#define SOCIABLE_DFLT_ELEMS (1024)

/*
 * default number of structure slots to allocate;
 * can be overridden
 */
#define SOCIABLE_DFLT_STRUCTS (64)

/*
 *	default number of elements for an array
 *	also used as the extent when increasing size;
 *	can be overridden
 */
#define SOCIABLE_DFLT_ARRAYSZ (10)
/*
 *	number of spare slots included when creating/extending an array;
 *	helps avoid frequent re-alloc/copying
 */
#define SOCIABLE_ARRAY_SPARE (10)

typedef struct sociable_element sociable_element_t;
typedef struct sociable_storage_mech sociable_storage_mech_t;
typedef struct sociable_proxy sociable_proxy_t;
typedef struct sociable_stm_log sociable_stm_log_t;
typedef struct sociable_stm_bucket sociable_stm_bucket_t;
typedef struct sociable_stm_detector sociable_stm_detector_t;
/*
 *	generic element locator for process private,
 *	process shared, and distributed elements
 */
typedef union {
	void *elemp;		/* for process private references */
	struct {			/* for process shared references */
		U32 segmentid;	/* assumed to be a shmid or equivalent */
		U32 offset;		/* offset from base of segment */
	} elemloc;
	char elemid[8];		/* for distributed references */
} sociable_addr_t;

typedef struct {
	unsigned char mechid;		/* assoc. mechanism id */
	unsigned char level;		/* level of the referent */
	sociable_addr_t un;
} sociable_locator_t;
/*
 *	fully qualified reference
 */
typedef struct {
	U32 cycleno;				/* cycle no of referent when assigned */
	sociable_locator_t elemloc;
} sociable_ref_t;

/*
 *	common header for proxies, and process-native
 *	elements/structures
 *	(nonnative elements likely have a similar header)
 */
typedef struct {
	volatile U32 soc_flags;	/* sociable control flags: */
	volatile U32 cycleno;	/* recycle counter */
	volatile U32 updcntlo;	/* update sequence counter */
	volatile U32 updcnthi;
	U32 sv_flags;			/* SV type; see sv.h:svtypes */
	union {
		SV *svu_rv;
	} sv_u;
} sociable_header_t;

typedef unsigned char byte;
/*
 *	common element fields for process-native elements/structures
 */
typedef struct {
	byte mechid;					/* index of mechanism in process-local list */
	byte level;
	sociable_header_t header;
	sociable_addr_t next;			/* freelist locators */
	sociable_addr_t prev;
	sociable_addr_t pkg_name;		/* name of any package its blessed into */

#ifdef SOCIABLE_STM
	volatile U32 spinlock;			/* atomically set/cleared for spinlocked access */
	sociable_addr_t owner;			/* current element owner (mech/level implied) */
#endif

#ifdef SOCIABLE_TIE
	sociable_locator_t ties;		/* head of any tie structure chain */
#endif
} sociable_native_t;

typedef struct {
	IV iv;					/* when its an IV; also, slot index for deleted array element */
	UV uv;					/* when its an UV */
	NV nv;					/* when its an NV */
	U32 bufsz;				/* length of buffer in bytes */
	U32 buflen;				/* length of buffer contents in bytes */
	sociable_locator_t bufptr;	/* buffer pointer */
} sociable_scalar_t;

typedef struct crt_hash_tuple_t {
	I32 hashval;			/* computed hash value for key */
	I32 keylen;				/* length of the key */
	sociable_addr_t keyptr;	/* keystring; variable length */
} sociable_hash_tuple_t;

typedef struct {
	I32 extent;				/* allocation cluster size */
	I32 himark;				/* high water mark */
	sociable_addr_t pool;	/* head of extent pool */
	sociable_addr_t tail;	/* tail of extent pool */
	I32 pool_cnt;			/* current pool size */
} sociable_struct_extent_t;

#define SOCIABLE_BUCKETS (23)

typedef struct {
	I32 keycount;			/* total tuples in the hash */
	I32 bucket_count;		/* number of buckets */
	sociable_addr_t buckets;
} sociable_hash_t;

#define hash_bucket_count un.hash.bucket_count
#define hash_keycount un.hash.keycount
#define hash_buckets un.hash.buckets
#define hash_extent extent.extent
#define hash_himark extent.himark
#define hash_pool extent.pool
#define hash_pool_cnt extent.pool_cnt

typedef struct {
	I32 arysz;				/* current number of slots */
	I32 aryhead;			/* current top of array (ie, next free slot) */
	I32 arytail;			/* current bottom of array (ie, first inuse slot) */
	sociable_addr_t aryptr; /* buffer pointer */
#ifdef SOCIABLE_STM
	I32 *slot_map;			/* slot index map of original element positions */
	sociable_element_t *deletes; /* list of read 1st, then deleted elements */
	char *delete_map;		/* bit map of deleted elements (stm only) */
#endif
} sociable_array_t;

#ifdef SOCIABLE_STM
#define SOC_ARRAY_ELEM_DELETED(structp, slot) \
	(structp->un.array.delete_map[((slot) >> 3) & 0x1FFFFFFF] & (1 << ((slot) & 7)))

#define SOC_DELETE_ARRAY_ELEM(structp, slot) \
	structp->un.array.delete_map[((slot) >> 3) & 0x1FFFFFFF] |= (1 << ((slot) & 7))

#define SOC_STM_FOR_WRITE (1)
#define SOC_STM_FOR_DELETE (3)
#define ary_slot_map un.array.slot_map
#define ary_deletes un.array.deletes
#define ary_delete_map un.array.delete_map
#endif

#define ary_head un.array.aryhead
#define ary_tail un.array.arytail
#define ary_size un.array.arysz
#define ary_ptr un.array.aryptr.elemp
#define ary_loc un.array.aryptr

#define ary_extent extent.extent
#define ary_himark extent.himark
#define ary_pool extent.pool
#define ary_pool_cnt extent.pool_cnt

#define NATIVE_ARRAY_LOOKUP(proxy, i, elemp) \
	elemp = (proxy->mech->level == SOC_PROCESS_PRIVATE_MECH) \
			? (sociable_addr_t *)((sociable_structure_t *)proxy->elemp)->un.array.aryptr.elemp \
			: (sociable_addr_t *)(*proxy->mech->addr_to_ptr)(&((sociable_structure_t *)proxy->elemp)->un.array.aryptr); \
	memcpy(elemp, &elemp[i], sizeof(sociable_addr_t))

#define FOREIGN_ARRAY_LOOKUP(proxy, i, elemp) \
	(*proxy->mech->array_elem_lookup)(proxy, i, elemp)

/*
 *	!!!ORDER OF DECLARATION IS IMPORTANT!!!
 *	Meta must come first !
 */
struct sociable_element {
	sociable_native_t header;	/* common stuff */
	union {
		sociable_scalar_t scalar;	/* if its a plain ol' scalar */
		sociable_ref_t ref;			/* if its a ref, ptr to referent */
		sociable_locator_t queue;	/* if a queue ref */
	} un;
	sociable_hash_tuple_t hash_tuple; /* key and value link info for hash element */
};

/*
 *	!!!ORDER OF DECLARATION IS IMPORTANT!!!
 *	Meta  must come first !
 */
typedef struct {
	sociable_native_t header;	/* common stuff */

	sociable_struct_extent_t extent;
	union {
		sociable_hash_t hash;
		sociable_array_t array;
	} un;
} sociable_structure_t;

#define NEXT_PTR header.next.elemp
#define PREV_PTR header.prev.elemp
#define SOCHDR header.header
/*
 *	process private proxy version of element
 */
struct sociable_proxy {
	sociable_header_t header;
	sociable_locator_t sociable;	/* locator for assoc. element */
	sociable_storage_mech_t *mech;	/* optimized for your convenience */
	unsigned int stm_hash;			/* STM WAL hash bucket */
	char *pkg_name;					/* name of any package its blessed into */
	sociable_element_t *elemp;		/* process local element pointer
										(for process-native elements only) */
#ifdef SOCIABLE_DEBUG
	char loc_string[24];			/* a displayable version of the locator */
#endif
};

#ifdef SOCIABLE_DEBUG
#define SOC_PRINTABLE_LOC(sym) char sym[24]
#define PRINTABLE_LOC(loc, locstr) \
	if ((loc)->level == SOC_PROCESS_PRIVATE_MECH) \
		sprintf(locstr, "P:%p", (loc)->un.elemp); \
	else if ((loc)->level == SOC_PROCESS_SHARED_MECH) \
		sprintf(locstr, "S:%08X:%08X", &(loc)->un.elemloc.segmentid, &(loc)->un.elemloc.offset); \
	else \
		sprintf(locstr, "F:%016X", (loc)->un.elemid)
#define PRINTABLE_ADDR(level, addr, locstr) \
	if (level == SOC_PROCESS_PRIVATE_MECH) \
		sprintf(locstr, "P:%p", (addr)->elemp); \
	else if (level == SOC_PROCESS_SHARED_MECH) \
		sprintf(locstr, "S:%08X:%08X", &(addr)->elemloc.segmentid, &(addr)->elemloc.offset); \
	else \
		sprintf(locstr, "F:%016X", (addr)->elemid)
#else
#define SOC_PRINTABLE_LOC(sym)
#define PRINTABLE_LOC(proxy)
#define PRINTABLE_ADDR(level, addr, locstr)
#endif
/*
 * translate input SV into a element (for AV/HV's )
 */
/*
 *	Old way...
 *
#define SOCIABLE_FROM_OBJ(sv) (SvROK(sv) ? INT2PTR(sociable_element_t*, SvIV(SvRV(sv))) : NULL)
 */

#define SOC_PROXY_FROM_OBJ(sv) (SvROK(sv) ? INT2PTR(sociable_proxy_t*, SvIV(SvRV(sv))) : NULL)

#define SOC_SAME_ELEM(mgproxy, proxy) \
	(((mgproxy)->mech == proxy->mech) && \
	(((mgproxy)->mech->level == SOC_FOREIGN_MECH) \
		? (*proxy->mech->is_same_elem)((mgproxy)->sociable.un.elemid, proxy->sociable.un.elemid) \
		: ((mgproxy)->elemp == proxy->elemp)))

#define SOC_DESOCIALIZE(proxy, sv) \
	if (proxy->pkg_name) crt_free(proxy->pkg_name); \
 	crt_free(proxy); \
	sv_unmagic(sv, PERL_MAGIC_ext)


/*
 *	BEGIN NATIVE MACROS
 */
#define NATIVE_NOT_SOCIABLE(proxy) \
	(!(proxy->elemp->header.header.soc_flags & SOC_INUSE) || \
		(proxy->elemp->header.header.cycleno != proxy->header.cycleno))

#define NATIVE_GET_REF(proxy) \
	((proxy->mech->level == SOC_PROCESS_PRIVATE_MECH) \
		? &proxy->sociable.elemp->un.ref \
		: (*proxy->mech->get_rv)(&proxy->sociable.un.elemloc))

#define SOC_GET_PKG_NAME(proxy) \
	((proxy->mech->level == SOC_PROCESS_PRIVATE_MECH) \
		? proxy->sociable.elemp->header.pkg_name.elemp \
		: ((proxy->mech->level == SOC_PROCESS_SHARED_MECH) \
			? (*proxy->mech->addr_to_ptr)(proxy->sociable.elemp->header.pkg_name.elemp) \
			: (*proxy->mech->get_pkgname)(&proxy->sociable.un.elemloc)))


#define NATIVE_ELEM_UPDATED(proxy, elemp) \
		((proxy->header.cycleno != elemp->header.header.cycleno) || \
		((elemp->header.header.updcntlo == 0) && \
			(elemp->header.header.updcnthi == 0)) || \
		(proxy->header.updcntlo != elemp->header.header.updcntlo) || \
		(proxy->header.updcnthi != elemp->header.header.updcnthi))

/*
 *	macros to update or test sequence number
 * Note that seqno of zero is reserved to force a reload
 */
#define NATIVE_INCR_PROXY_SEQNO(proxy) \
	proxy->elemp->header.header.updcntlo++; \
	if (!proxy->elemp->header.header.updcntlo) { \
		if (proxy->elemp->header.header.updcnthi == 0xFFFFFFFF) { \
			proxy->elemp->header.header.updcnthi = 0; \
			proxy->elemp->header.header.updcntlo = 1; \
		} \
		else proxy->elemp->header.header.updcnthi++; \
	}

#define NATIVE_INCR_ELEM_SEQNO(elemp) \
	elemp->header.header.updcntlo++; \
	if (!elemp->header.header.updcntlo) { \
		if (elemp->header.header.updcnthi == 0xFFFFFFFF) { \
			elemp->header.header.updcnthi = 0; \
			elemp->header.header.updcntlo = 1; \
		} \
		else elemp->header.header.updcnthi++; \
	}

#define NATIVE_GET_ELEM_SEQNO(proxy) \
	proxy->header.updcntlo = proxy->elemp->header.header.updcntlo; \
	proxy->header.updcnthi = proxy->elemp->header.header.updcnthi

#define NATIVE_GET_SVTYPE(proxy) (SvTYPE(&proxy->elemp->header.header))

#define NATIVE_ARRAY_HEAD(proxy) (((sociable_structure_t *)(proxy->elemp))->un.array.aryhead)

/*
 *	UTF8 encoding cloning
 */
#define NATIVE_ENCODE_SV_PROXY(sv, proxy) \
	if (SvUTF8(&proxy->elemp->header.header)) SvUTF8_on(sv); \
	else SvUTF8_off(sv)

#define NATIVE_ENCODE_SV_ELEM(sv, elemp) \
	if (SvUTF8(&elemp->header.header)) SvUTF8_on(sv); \
	else SvUTF8_off(sv)

#define NATIVE_ENCODE_PROXY(sv, proxy) \
	if (SvUTF8(sv)) SvUTF8_on(&proxy->elemp->header.header);\
	else SvUTF8_off(&proxy->elemp->header.header);

#define NATIVE_ENCODE_ELEM(sv, elemp) \
	if (SvUTF8(sv)) SvUTF8_on(&elemp->header.header);\
	else SvUTF8_off(&elemp->header.header);

#define NATIVE_GET_RV(sv, proxy) \
       sociable_get_RV(aTHX_ sv, &(proxy)->elemp->un.ref.elemloc, (proxy)->loc_string)
/*
 *	BEGIN FOREIGN MACROS
 */
#define FOREIGN_NOT_SOCIABLE(proxy) \
	((*proxy->mech->expired)(proxy, proxy->header.cycleno))

#define FOREIGN_GET_REF(proxy) \
	((*proxy->mech->get_rv)(proxy))

#define FOREIGN_GET_PKG_NAME(proxy) \
	((*proxy->mech->get_pkgname)(proxy))

#define FOREIGN_GET_SVTYPE(proxy) \
	(*proxy->mech->get_sv_type)(proxy->sociable.un.elemid)

#define FOREIGN_ARRAY_HEAD(proxy) \
	(*proxy->mech->get_array_head)(proxy->sociable.un.elemid)

#define FOREIGN_ELEM_UPDATED(proxy) \
	(*proxy->mech->is_elem_updated)(proxy)

#define FOREIGN_INCR_ELEM_SEQNO(proxy) \
	(*proxy->mech->incr_elem_seqno)(proxy)

#define FOREIGN_GET_ELEM_SEQNO(proxy) \
	(*proxy->mech->get_seqno)(proxy)

#define FOREIGN_ARRAY_HEAD(proxy) \
	(*proxy->mech->get_array_head)(proxy->sociable.un.elemid)

#define FOREIGN_GET_SEQNOHI(proxy) \
	(*proxy->mech->get_seqnohi)(proxy->sociable.un.elemid)

#define FOREIGN_GET_SEQNOLO(proxy) \
	(*proxy->mech->get_seqnolo)(proxy->sociable.un.elemid)
/*
 *	UTF8 encoding cloning
 */
#define FOREIGN_ENCODE_SV(sv, proxy) \
	if ((*proxy->mech->is_encoded)(proxy->sociable.un.elemid)) SvUTF8_on(sv);\
	else SvUTF8_off(sv)

#define FOREIGN_ENCODE_ELEM(sv, proxy) \
	(*proxy->mech->set_encoding)(proxy->sociable.un.elemid, SvUTF8(sv))

#define FOREIGN_GET_RV(sv, proxy) \
       sociable_get_RV(aTHX_ sv, (*(proxy)->mech->get_reference)(proxy), (proxy)->loc_string)
/*
 *	shorthand for common splices
 */
#define SOC_SPLICE_REMOVE(ary, key, extent) sociable_array_splice(aTHX_ ary, key, 1, 0, 1, extent, NULL)

#define SOC_SPLICE_RETURN(ary, key, extent) sociable_array_splice(aTHX_ ary, key, 1, 0, 0, extent, NULL)

#define SOC_GET_SVTYPE(proxy) \
	((proxy->mech->level == SOC_FOREIGN_MECH) \
		? (*proxy->mech->get_sv_type)(proxy) \
		: SvTYPE(&proxy->elemp->header.header))

/*
 *	define per-interpretter (aka thread-private) context
 */
#define MY_CXT_KEY "Thread::Sociable::_guts" XS_VERSION

#ifdef SOCIABLE_STM

typedef struct {
	sociable_locator_t elemloc;
/*
 *	only used for process-shared elements
 *	for quick access to the element wo/
 *	doing a addr_to_ptr xlation
 */
	union {
		sociable_element_t *elemp;
		sociable_structure_t *structp;
	} shared;
	union {
		sociable_element_t *elemp;
		sociable_structure_t *structp;
	} priv;
} sociable_stm_proxy_t;

struct sociable_stm_bucket {
	int avail;
	int count;
	sociable_stm_proxy_t *bucket;
};

/*
 *	STM transaction context: currently, just the karma and an unused timestamp;
 *	mechanism may extend this...
 */
typedef struct {
	U32 karma;				/* karma rating; incr. for each new variabla access */
	time_t started;			/* timestamp when transaction started; for eventual lifetime limiter */
} sociable_stm_ctxt_t;

struct sociable_stm_log {
	I32 WAL_count;				/* count of unique sociable referents */
	I32 spinlocked;				/* count of spinlocked log members */
	sociable_addr_t *WAL;		/* list of unique sociable referents; filled at commit */
	sociable_proxy_t **WAL_proxies;
	sociable_addr_t ctxt;		/* transaction context */
	sociable_stm_ctxt_t *ctxtp;	/* optimize for native, esp process-shared */
};
#endif

#define SOC_CXT_TMP_SVS (30)
#define SOCIABLE_MECHS (16)

typedef struct {
	void (*lockchain)();	/* prior value of PL_lockhook */
	int svtmp_count;		/* number of SVs in svtmps */
	SV *svtmps[SOC_CXT_TMP_SVS];	/* optimization: to avoid heap thrashing */
#ifdef SOCIABLE_STM
	sociable_stm_log_t stm_ctxts[SOCIABLE_MECHS];
	U32 stm_karma;			/* current xaction karma */
	I32 stm_xact_count;		/* count of nested transaction calls */
	I32 stm_xact_avail;		/* available slots in closure array */
	SV **stm_commits;		/* onCommit method chain */
	SV **stm_restarts;		/* onRestart method chain */
	SV **stm_rollbacks;		/* onRollback method chain */
	bool stm_reconciled;	/* true => in commit after reconcile phase, or in restart/rollback processing */
	sociable_stm_bucket_t stm_referents[SOCIABLE_BUCKETS];
							/* hash maps referent's sociable address to thread-private proxy */
#endif
} my_cxt_t;

#define SOC_ALLOC_SV_TMPS(svary, count) \
	if (MY_CXT.svtmp_count || (count > SOC_CXT_TMP_SVS)) \
		svary = (SV **)crt_malloc(sizeof(SV*) * count); \
	else { \
		MY_CXT.svtmp_count = count; \
		svary = MY_CXT.svtmps; \
	}

#define SOC_FREE_SV_TMPS(svary) \
	if ((void *)svary != (void *)&MY_CXT.svtmps) \
		svary = crt_free(svary); \
	else { \
		MY_CXT.svtmp_count = 0; \
		svary = NULL; \
	}

#ifndef SOCIABLE_STM

#define SOC_STM_IN_XACT (0)
#define SOC_CAN_STM(proxy) (0)

#endif

#ifdef SOCIABLE_DEBUG_LOCKS

#define ENTER_ELEM_LOCK(proxy) \
    STMT_START { \
        (*proxy->mech->lock)(aTHX_ proxy, __LINE__);\
    } STMT_END

#define SOCIABLE_TRACELOCK(fmt, arg) \
	sociable_trace(fmt, arg)

#define SOCIABLE_TRACELOCK_4(fmt, arg1, arg2, arg3, arg4) \
	sociable_trace(fmt, arg1, arg2, arg3, arg4)

#else

#define ENTER_ELEM_LOCK(proxy) \
    STMT_START { \
        (*proxy->mech->lock)(aTHX_ proxy);\
    } STMT_END

#define SOCIABLE_TRACELOCK(fmt, arg)
#define SOCIABLE_TRACELOCK_4(fmt, arg1, arg2, arg3, arg4)

#endif

#define LEAVE_ELEM_LOCK(proxy) \
    STMT_START { \
        (*proxy->mech->unlock)(aTHX_ proxy);\
    } STMT_END

/*
 *	method declarations for dynamically loaded libraries
 */
extern SV *native_sv_from_elem(pTHX_ SV *sv, sociable_element_t *elemp, char *loc_string);
extern int native_sv_save(pTHX_ SV *sv, sociable_element_t *elemp);

#ifdef SOCIABLE_STM

extern sociable_element_t *native_save_referent(pTHX_ sociable_proxy_t *proxy, int for_write);
extern void native_merge_element(pTHX_ sociable_addr_t *elemp, sociable_element_t *priv_elemp);
extern int private_acquire_for_commit(pTHX_ sociable_stm_log_t *wal);
extern int shared_acquire_for_commit(pTHX_ sociable_stm_log_t *wal);
extern sociable_stm_detector_t native_eager;
extern sociable_stm_detector_t native_lazy;

#endif

#endif
