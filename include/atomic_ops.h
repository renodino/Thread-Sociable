#ifndef __ATOMIC_OPS_H__
#define __ATOMIC_OPS_H__

#include "windows.h"

/*
 *	Windows version of CAS, w exponential backoff
 *	NOTE: to permit inlining, InterlockedCompareExchange() needs
 *	to be executed outside any conditional expression.
 */
#define SOC_ACQUIRE_SPINLOCK(spinlock) \
{ \
    int i, backoff = 64; \
    unsigned long result; \
    while (1) { \
    	result = InterlockedCompareExchange(spinlock, 1, 0); \
		if (!result) break; \
	    for (i = backoff; i; i--); \
	    if (backoff < 4096) backoff <<= 1; \
	} \
}

#define SOC_RELEASE_SPINLOCK(spinlock) *spinlock = 0

#endif // __ATOMIC_OPS_H__
