/******************************************************************
 ******************************************************************
 *
 *	STM NATIVE EAGER ACQUIRE/CONFLICT DETECTOR METHODS
 *
 ******************************************************************
 ******************************************************************/
STATIC int
native_eager_acquire(pTHX_ sociable_proxy_t *proxy, int for_write)
{
	sociable_stm_ctxt_t *owner;
	dMY_CXT;

	SOC_BUMP_KARMA;
	SOC_ACQUIRE_SPINLOCK(&proxy->elemp->header.spinlock)
/*
 *	make ourselves owner if possible
 */
	owner = NATIVE_GET_OWNER(proxy);
	if (NATIVE_CAN_ACQUIRE(proxy, owner)) {
		NATIVE_MAKE_OWNER(proxy);
		return 1;
	}
/*
 *	restart if can't acquire for write
 */
	if (for_write)
		SOC_RELEASE_SPINLOCK(&proxy->elemp->header.spinlock);

	return (!for_write);
}

/*
 *	eager conflict detector:
 *	if prior read in conflict
 *		return "conflict detected"
 *	if 1st access
 *		bump our karma
 *	if for write and can't (re)acquire
 *		return "conflict detected"
 *	else return "no conflict"
 */
STATIC int
native_eager_has_conflict(pTHX_ sociable_locator_t *proxy, int for_write)
{
	sociable_element_t *elemp = proxy->elemp;
	sociable_stm_ctxt_t *owner;
	dMY_CXT;

	if (SOC_STM_WAS_READ(proxy) && (!NATIVE_VALID_READ(proxy)))
		return 1;

	if (NATIVE_AM_OWNER(proxy))
		return 0;

	SOC_ACQUIRE_SPINLOCK(&elemp->header.spinlock);
	owner = NATIVE_GET_OWNER(proxy);

	if (!NATIVE_CAN_ACQUIRE(proxy, owner)) {
		SOC_RELEASE_SPINLOCK(&elemp->header.spinlock);
		return (!for_write);
	}
	NATIVE_MAKE_OWNER(proxy);
	SOC_RELEASE_SPINLOCK(&elemp->header.spinlock);
	return 0;
}

/*
 *	eager conflict detector for array elements
 */
STATIC int
native_eager_ary_elem_conflict(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int tkey, int for_write)
{
	sociable_element_t *elemp;
	sociable_element_t *priv_elemp = NULL;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
	sociable_addr_t *ary_ptr;
	sociable_addr_t *ary = (sociable_addr_t *)priv_structp->ary_ptr;
	dMY_CXT;
/*
 *	if unaccessed element, create a new one
 */
	if (SOC_IS_UNACCESSED(ary, key)) {
		SOC_BUMP_KARMA;
		CRT_Newz(ary[key].elemp, 1, sociable_element_t);
		if (for_write & SOC_STM_FOR_WRITE)
			priv_structp->header.soc_flags |= SOC_STM_ELEM_WRITE;
	}
	priv_elemp = ary[key].elemp;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, tkey);

	if (SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp))) {
/*
 *	element was read and has changed: conflict
 */
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
		return 1;
	}
/*
 *	if unaccessed, clone element before trying to acquire,
 *	since it may be for a read and will return OK on failure;
 *	mark as read 1st if for read or delete operation
 */
	if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
		native_clone_element(priv_elemp, elemp);
		priv_elemp->header.soc_flags =
			(for_write == SOC_STM_FOR_WRITE)
				? SOC_INUSE | SOC_STM_WRITE | SOC_STM_PRIVATE
				: (for_write == SOC_STM_FOR_DELETE)
					? SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE
					: SOC_INUSE | SOC_STM_READ | SOC_STM_PRIVATE;
	}
/*
 *	for now, use structure lock to protect elements;
 *	in future, use hierarchical lock protocol
 */
 	owner = NATIVE_ELEM_GET_OWNER(elemp);
	if (!NATIVE_ELEM_CAN_ACQUIRE(elemp, owner)) {
		SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
		return for_write;
	}
	NATIVE_MAKE_ELEM_OWNER(elemp);
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

/*
 *	eager conflict detector for array splice
 *	NOTE: ignore lazy flag here, otherwise we may access
 *	invalid elements
 */
 /**** CONTINUE HERE !!!! ****/
STATIC int
native_eager_splice_conflict(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int len)
{
	sociable_element_t *elemp;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	sociable_element_t *priv_elemp = NULL;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_addr_t *ary_ptr;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
	int i;
	dMY_CXT;
/*
 *	for each unaccessed element, create a new empty element
 */
 	for (i = key; i < key + len; i++) {
		if (SOC_IS_UNACCESSED(ary, i)) {
			SOC_BUMP_KARMA;
			CRT_Newz(ary[i].elemp, 1, sociable_element_t);
			elemp = ary[i].elemp;
			elemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
	}
	proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
/*
 *	scan for conflicts with the sociables
 */
 	for (i = key; i < key + len; i++) {
 		if (slot_map[i] == SOC_STM_UNMAPPED)
 			continue;
	 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, slot_map[i]);
 		priv_elemp = ary[i].elemp;

	 	owner = NATIVE_ELEM_GET_OWNER(elemp);
		if ((SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp))) ||
			(!NATIVE_ELEM_CAN_ACQUIRE(elemp, owner))) {
/*
 *	element was read and has changed, OR cannot be acquired: conflict
 */
		 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
			return 1;
		}
		NATIVE_MAKE_ELEM_OWNER(elemp);
/*
 *	if unaccessed, clone the element
 *	mark as read 1st
 */
		if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
			native_clone_element(priv_elemp, elemp);
			priv_elemp->header.soc_flags = SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE;
		}
	}
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

STATIC int
native_eager_hash_elem_acquire(pTHX_ sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	dMY_CXT;
	SOC_BUMP_KARMA;
	if (NATIVE_CAN_ACQUIRE(sh)) {
		sh->header.owner = MY_CXT.stm_ctxt;
		return 0;
	}
	if (for_write)
		SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return for_write;
}

STATIC int
native_eager_hash_elem_conflict(pTHX_ sociable_element_t *h, sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	dMY_CXT;
	if (SOC_STM_WAS_READ(h) && (!NATIVE_VALID_READ(h)))
		return 1;

	if (NATIVE_CAN_ACQUIRE(sh)) {
		sociable->header.owner = MY_CXT.stm_ctxt;
		return 0;
	}

	return (for_write);
}

/******************************************************************
 ******************************************************************
 *
 *	STM NATIVE LAZY ACQUIRE/CONFLICT DETECTOR METHODS
 *
 ******************************************************************
 ******************************************************************/
STATIC int
native_lazy_acquire(pTHX_ sociable_element_t *elemp, int for_write)
{
	dMY_CXT;

	SOC_BUMP_KARMA;
	SOC_ACQUIRE_SPINLOCK(&elemp->header.spinlock);
	return 1;
}
/*
 *	lazy conflict detector:
 *	always return "no conflict"
 */
STATIC int
native_lazy_has_conflict(pTHX_ sociable_element_t *proxy, int for_write)
{
	return 0;
}

/*
 *	lazy conflict detector for array elements
 */
STATIC int
native_lazy_ary_elem_conflict(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int tkey, int for_write)
{
	sociable_element_t *elemp;
	sociable_element_t *priv_elemp = NULL;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
	sociable_addr_t *ary_ptr;
	sociable_addr_t *ary = (sociable_addr_t *)priv_structp->ary_ptr;
	dMY_CXT;
/*
 *	if unaccessed element, create a new one
 */
	if (SOC_IS_UNACCESSED(ary, key)) {
		SOC_BUMP_KARMA;
		CRT_Newz(ary[key].elemp, 1, sociable_element_t);
		if (for_write & SOC_STM_FOR_WRITE)
			priv_structp->header.soc_flags |= SOC_STM_ELEM_WRITE;
	}
	priv_elemp = ary[key].elemp;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, tkey);
/*
 *	if unaccessed, copy metadata before trying to acquire,
 *	since it may be for a read and will return OK on failure;
 *	mark as read 1st if for read or delete operation
 */
	if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
		native_clone_element(priv_elemp, elemp);
		priv_elemp->header.soc_flags =
			(for_write == SOC_STM_FOR_WRITE) ? SOC_INUSE | SOC_STM_WRITE | SOC_STM_PRIVATE :
			(for_write == SOC_STM_FOR_DELETE) ? SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE :
			SOC_INUSE | SOC_STM_READ | SOC_STM_PRIVATE;
	}
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

/*
 *	lazy conflict detector for array splice
 */
STATIC int
native_lazy_splice_conflict(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int len)
{
	sociable_element_t *elemp;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	sociable_element_t *priv_elemp = NULL;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_addr_t *ary_ptr;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
	int i;
	dMY_CXT;
/*
 *	for each unaccessed element, create a new empty element
 */
 	for (i = key; i < key + len; i++) {
		if (SOC_IS_UNACCESSED(ary, i)) {
			SOC_BUMP_KARMA;
			CRT_Newz(ary[i].elemp, 1, sociable_element_t);
			elemp = ary[i].elemp;
			elemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
	}
	proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
/*
 *	clone any unaccessed elements
 */
 	for (i = key; i < key + len; i++) {
 		if (slot_map[i] == SOC_STM_UNMAPPED)
 			continue;
	 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, slot_map[i]);
 		priv_elemp = ary[i].elemp;
/*
 *	if unaccessed, clone the element
 *	mark as read 1st
 */
		if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
			native_clone_element(priv_elemp, elemp);
			priv_elemp->header.soc_flags = SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE;
		}
	}
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

STATIC int
native_lazy_hash_elem_acquire(pTHX_ sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	dMY_CXT;
	SOC_BUMP_KARMA;
	return 0;
}

STATIC int
native_lazy_hash_elem_conflict(pTHX_ sociable_element_t *h, sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	return 0;
}

sociable_stm_detector_t native_eager = {
	&native_eager_acquire,
	&native_eager_has_conflict,
	&native_eager_ary_elem_conflict,
	&native_eager_splice_conflict,
	&native_eager_hash_elem_acquire,
	&native_eager_hash_elem_conflict
};

sociable_stm_detector_t native_lazy = {
	&native_lazy_acquire,
	NULL, /* &native_lazy_has_conflict, */
	&native_lazy_ary_elem_conflict,
	&native_lazy_splice_conflict,
	&native_lazy_hash_elem_acquire,
	NULL /* &native_lazy_hash_elem_conflict */
};

/*
 *	create a complete private clone of a sociable element
 *	assumes caller holds spinlock on elemp (or the parent structure)
 */
STATIC void
native_clone_element(sociable_element_t *privelemp, sociable_element_t *sociable)
{
	char *ptr = NULL;
	char *keyptr = NULL;
	char *bufptr = NULL;
	sociable_storage_mech_t *mech = sociable_mechs[sociable->header.mechid];

	memcpy(&privelemp->header, &sociable->header, sizeof(sociable_native_t));
	if (sociable->header.level == SOC_PROCESS_PRIVATE) {
		if (sociable->header.pkg_name.elemp)
			ptr = (sociable->header.pkg_name);
		if (sociable->hash_tuple.keylen)
			keyptr = sociable->hash_tuple.keyptr.elemp;
		if (!SvROK(&sociable->SOCHDR))
			bufptr = sociable->un.scalar.bufptr.elemp;
	}
	else {
		if (sociable->header.pkg_name.elemp)
			ptr = (*mech->addr_to_ptr)(&sociable->header.pkg_name);
		if (sociable->hash_tuple.keylen)
			keyptr = (*mech->addr_to_ptr)(&sociable->hash_tuple.keyptr);
		if (!SvROK(&sociable->SOCHDR))
			bufptr = (*mech->addr_to_ptr)(&sociable->un.scalar.bufptr);
	}
	if (ptr)
		privelemp->header.pkg_name.elemp = crt_strdup(sociable->header.pkg_name.elemp);
	if (keyptr) {
		privelemp->hash_tuple.keylen = sociable->hash_tuple.keylen;
		privelemp->hash_tuple.hashval = sociable->hash_tuple.hashval;
		CRT_Dup(privelemp->hash_tuple.keyptr.elemp, privelemp->hash_tuple.keylen + 1, char, keyptr, privelemp->hash_tuple.keylen);
	}

#ifdef SOCIABLE_TIE
/*
 *	copy tie info in future
 */
#endif
	if (SvROK(&sociable->header))
		memcpy(&privelemp->un.ref, &sociable->un.ref, sizeof(sociable_ref_t));
	else {
		memcpy(&privelemp->un.scalar, &sociable->un.scalar, sizeof(sociable_scalar_t));
		if (privelemp->un.scalar.bufsz)
			CRT_Dup(proxy->un.scalar.bufptr.elemp, proxy->un.scalar.bufsz, char, bufptr, proxy->un.scalar.buflen);
		else
			proxy->un.scalar.bufptr.elemp = NULL;
	}
}

STATIC void
native_clone_array(sociable_structure_t *proxy, sociable_structure_t *elemp)
{
	sociable_element_t **ary;
	I32 count;
	I32 *slot_map;
	I32 i;

	count = proxy->ary_head = elemp->ary_head;
	proxy->header.updcntlo = elemp->header.updcntlo;
	proxy->header.updcnthi = elemp->header.updcnthi;
	proxy->header.cycleno = elemp->header.cycleno;
	proxy->header.sv_flags = elemp->header.sv_flags;
	proxy->ary_extent = elemp->ary_extent;
	proxy->ary_himark = elemp->ary_himark;
	SOC_RELEASE_SPINLOCK(&elemp->header.spinlock);
/*
 *	create array slots, a map to current slot
 *	positions, and a delete map bitmask.
 *	Slot map is used to locate original element when elements
 *	get movedaround; Delete map is used to mark position of
 *	deleted elements.
 *	Alloc a few extra slots to try to avoid frequent
 *	copies
 */
	proxy->header.sociable = (sociable_element_t *)elemp;
	CRT_Newz(ary, count + SOCIABLE_ARRAY_SPARE, sociable_element_t*);
	CRT_Newz(slot_map, count + SOCIABLE_ARRAY_SPARE, int);
	CRT_Newz(proxy->un.array.delete_map, (((count >> 3) & 0x1FFFFFFF) + 1), char);
	proxy->ary_size = proxy->ary_head + SOCIABLE_ARRAY_SPARE;
	for (i = 0; i < proxy->ary_head; slot_map[i] = i, memcpy(&ary[i++], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)));
	for (;i < proxy->ary_size; slot_map[i++] = SOC_STM_UNMAPPED);
	proxy->ary_ptr = ary;
	proxy->ary_slot_map = slot_map;
}

STATIC void
native_clone_hash(sociable_structure_t *proxy, sociable_structure_t *elemp)
{
	sociable_hash_t *thash = &proxy->un.hash;

	proxy->header.updcntlo = elemp->header.updcntlo;
	proxy->header.updcnthi = elemp->header.updcnthi;
	proxy->header.cycleno = elemp->header.cycleno;
	proxy->header.sv_flags = elemp->header.sv_flags;
	proxy->hash_bucket_count = elemp->hash_bucket_count;
	proxy->extent.extent = elemp->extent.extent;
	proxy->extent.himark = elemp->extent.himark;
	SOC_RELEASE_SPINLOCK(&elemp->header.spinlock);
/*
 *	elements are not added to the proxy until accessed
 */
	proxy->header.sociable = (sociable_element_t *)elemp;
	CRT_Newz(proxy->hash_buckets, proxy->hash_bucket_count, sociable_element_t*);
}

/*
 *	lookup in WAL hash;
 *	if found, conflict detect before returning the private version
 *  else attempt to acquire and create private version
 */
sociable_element_t *
native_save_referent(pTHX_ sociable_proxy_t *proxy, int for_write)
{
/*
 *	simple hash here: divide by 32, then mod 23. May need to verify the skew.
 */
 	sociable_element_t *sociable = proxy->elemp;
	sociable_stm_bucket_t *bucket = &MY_CXT.stm_referents[proxy->stm_hash];
	sociable_structure_t *priv_structp;
	sociable_structure_t *priv_elemp;
	sociable_stm_detector_t *detector = proxy->mech->detector;
	sociable_stm_ctxt_t *owner;
	int mechid = proxy->sociable.mechid;
	SV *errsv;
	int i;
	dMY_CXT;
/*
 *	verify the referent is still sociable
 */
 	if (SOC_NOT_SOCIABLE(proxy)) {
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, "Variable is not sociable.");
		return NULL;
 	}
/*
 *	make sure we've got a context for this mechanism
 */
	if ((MY_CXT.stm_ctxts[mechid].ctxt.segmentid == 0) &&
		(MY_CXT.stm_ctxts[mechid].ctxt.offset == 0) &&
		(!(*proxy->mech->stm_alloc_ctxt)(&MY_CXT.stm_ctxts[mechid].ctxt)))
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, "Cannot allocate transaction context.");
		return NULL;
	}

	for (i = 0; (i < bucket->count) &&
		memcmp(&bucket->bucket[i].elemloc, &proxy->sociable, sizeof(sociable_locator_t)); i++);
	if (i < bucket->count) {
/*
 *	already logged, so check for conflicts (if eager)
 */
		if (detector->has_conflict &&
			(*detector->has_conflict)(aTHX_ bucket->bucket[i].elemloc, for_write)) {
			errsv = get_sv("@", TRUE);
			sv_setpv(errsv, SOC_STM_RESTART_MSG);
			return NULL;
		}

		if (for_write)
			sociable->header.soc_flags |= SOC_STM_WRITE;

		return bucket->bucket[i].priv.elemp;
	}
/*
 *	never logged before, so attempt to acquire and add it
 */
	if (!bucket->avail) {
		CRT_Extend(bucket->bucket, bucket->count + bucket->avail, sociable_stm_proxy_t, SOCIABLE_ARRAY_SPARE);
		bucket->avail += SOCIABLE_ARRAY_SPARE;
	}
	SOC_BUMP_KARMA;
/*
 *	attempt to acquire it, then create private clone of it
 */
	switch (SvTYPE(&proxy->header)) {
		case SVt_PVAV:
		case SVt_PVHV:
			CRT_Newz(priv_structp, 1, sociable_structure_t);
			bucket->bucket[bucket->count].priv.structp = priv_structp;
			break;

		default:
			CRT_Newz(priv_elemp, 1, sociable_element_t);
			bucket->bucket[bucket->count].priv.elemp = priv_elemp;
	}
	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
	owner = (sociable->header.level == SOX_PROCESS_PRIVATE)
		? sociable->header.owner.elemp
		: (*proxy->mech->addr_to_ptr)(&sociable->header.owner);
	if (NATIVE_ELEM_CAN_ACQUIRE(sociable, owner))
		memcpy(&sociable->header.owner, &MY_CXT.stm_ctxts[mechid].ctxt, sizeof(sociable_addr_t));
	else if (for_write) {
		SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
		crt_free(bucket->bucket[bucket->count].priv.elemp);
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, SOC_STM_RESTART_MSG);
		return NULL;
	}
	switch (SvTYPE(proxy)) {
		case SVt_PVAV:
			native_clone_array(priv_structp, (sociable_structure_t *)sociable); /* releases spinlock */
			break;

		case SVt_PVHV:
			native_clone_hash(priv_structp, (sociable_structure_t *)sociable); /* releases spinlock */
			break;

		default:
			native_clone_element(priv_elemp, sociable); /* does NOT release spinlock */
			SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	}
	memcpy(&bucket->bucket[bucket->count].elemloc, &proxy->sociable, sizeof(sociable_locator_t));
	bucket->avail--;
	bucket->count++;
	bucket->bucket[bucket->count].priv.elemp->header.soc_flags =
		SOC_INUSE | SOC_STM_PRIVATE | (for_write ? SOC_STM_WRITE : SOC_STM_READ);
	return bucket->bucket[bucket->count - 1].priv.elemp;
}

int
private_acquire_for_commit(pTHX_ sociable_stm_log_t *wal)
{
	int count = wal->WAL_count;
	int i, j = 0;
	sociable_element_t *elemp, *priv_elemp;
	int rc;

	wal->spinlocked = 0;
	for (i = 0; i < count; i++) {
		elemp = wal->WAL[i].elemp;
		priv_elemp = wal->WAL_proxies[i];
		SOC_ACQUIRE_SPINLOCK(elemp);
		wal->spinlocked++;
		owner = elemp->header.owner.elemp;
		if ((SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp)) ||
			(SOC_STM_WAS_WRITTEN(priv_elemp) && (!NATIVE_CAN_ACQUIRE(elemp, owner))))
			return 0;
/*
 *	don't actually have to acquire, since spinlock is held
 *	until fully committed
 *
 *	if an array or hash that was written, acquire written elements
 */
		if (!SOC_STM_HAS_WRITES(priv_elemp))
			continue;
		rc = (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVAV)
			? private_acquire_array_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp)
			: private_acquire_hash_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp);
		if (!rc)
			return 0;
	}
	return 1;
}

int
shared_acquire_for_commit(pTHX_ sociable_stm_log_t *wal)
{
	int count = wal->WAL_count;
	int i, j = 0;
	sociable_element_t *elemp, *priv_elemp;
	int rc;

	wal->spinlocked = 0;
	for (i = 0; i < count; i++) {
		elemp = wal->WAL[i].elemp;
		priv_elemp = wal->WAL_proxies[i];
		SOC_ACQUIRE_SPINLOCK(elemp);
		wal->spinlocked++;
		owner = (*mech->addr_to_ptr)(&elemp->header.owner);
		if ((SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp)) ||
			(SOC_STM_WAS_WRITTEN(priv_elemp) && (!NATIVE_CAN_ACQUIRE(elemp, owner))))
			return 0;
/*
 *	don't actually have to acquire, since spinlock is held
 *	until fully committed
 *
 *	if an array or hash that was written, acquire written elements
 */
		if (!SOC_STM_HAS_WRITES(priv_elemp))
			continue;
		rc = (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVAV)
			? shared_acquire_array_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp)
			: shared_acquire_hash_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp);
		if (!rc)
			return 0;
	}
	return 1;
}

STATIC void
native_clear_log(pTHX_ sociable_stm_log_t *wal)
{
	int i;
	sociable_proxy_t *proxy;
	dMY_CXT;
/*
 *	for each spinlocked element
 *		if we own it
 *			release it
 */
	for (i = 0; i < wal->spinlocked; i++) {
		proxy = wal->WAL_proxies[i];
		if (NATIVE_AM_OWNER(proxy))
			NATIVE_CLEAR_OWNER(proxy);
		SOC_RELEASE_SPINLOCK(&proxy->elemp->header.spinlock);
	}
	wal->spinlocked = 0;
/*
 *	for each unlocked element
 *		if we might own it
 *			spinlock it
 *			if still owned
 *				release ownership
 *			release spinlock
 *
 */
	for (; i < wal->WAL_count; i++) {
		proxy = wal->WAL[i];
/*
 *	a quick bit equivalence test outside spinlock can
 *	move us along faster
 */
		if (NATIVE_AM_OWNER(proxy)) {
			SOC_ACQUIRE_SPINLOCK(&proxy->elemp->header.spinlock);
			if (NATIVE_AM_OWNER(proxy))
				NATIVE_CLEAR_OWNER(proxy);
			SOC_RELEASE_SPINLOCK(&proxy->elemp->header.spinlock);
		}
	}
/*
 *	discard private copies and clear the WAL
 */
 	stm_reset_elements(aTHX_ wal);
}

/*
 *	merge a private copy into the sociable version
 *	(assumes element is spinlocked and acquireable)
 */
void
native_merge_element(pTHX_ sociable_addr_t *elemp, sociable_element_t *priv_elemp)
{
	SOCIABLE_TRACE_2("native_merge_element: merging proxy %p into elemp %p\n", priv_elemp, elemp);

	sociable_element_t *telemp = (elemp->header.level == SOC_PROCESS_PRIVATE)
		? elemp.elemp
		: (*sociable_mechs[priv_elemp->header.mechid]->addr_to_ptr)(elemp);
	if (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVAV)
		native_merge_array(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)telemp);
	else if (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVHV)
		native_merge_hash(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)telemp);
	else
		native_update_element(aTHX_ priv_elemp, telemp);

	SOCIABLE_TRACE("native_merge_element: returning\n");
}

/*
 *	merge (aka commit) a private array into the sociable version
 *	(assumes array spinlocked and acquireable)
 *	create new array pointer
 *	scan slot_map
 *		if mapped
 *			move ptr from original slot to new slot
 *			if accessed
 *				update the original element
 *			else
 *				create new element in new slot from private element
 *
 *		scan deleted bitmap
 *			if bit set
 *				free the assoc. original element
 */
STATIC void
native_merge_array(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int i,j,k;
	sociable_element_t **ary = priv_structp->ary_ptr;
	int arylen = priv_structp->ary_head;
	int *slot_map = priv_structp->ary_slot_map;
	sociable_element_t *deleted = priv_structp->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = priv_structp->ary_delete_map;
/*
 *	we should probably scan for and create new unmapped elements
 *	in a group to optimize
 */
	for (i = 0; i < arylen; i++) {
		telemp = ary[i];
		if (slot_map[i] == SOC_STM_UNMAPPED) {
			/* create sociable from private element */
			ary[i].elemp = stm_new_sociable(aTHX_ telemp);
		}
		else {
			memcpy(&ary[i], &structp->ary_ptr[slot_map[i]], sizeof(sociable_addr_t));
			if (SOC_STM_WAS_WRITTEN(ary[i].elemp))
				native_update_element(aTHX_ telemp, ary[i].elemp);
		}
		stm_free_element(aTHX_ telemp, NULL);
	}
/*
 *	remove deletes: we should cluster the removed elements
 *	and return to the pool (or extent) in a single locked operation
 */
	arylen = ((structp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			if (delete_map[i] & j)
				sociable_destroy(aTHX_ structp->ary_ptr[(i << 3) + k]);
		}
	}
/*
 *	move everything to the sociable array
 */
	priv_structp->ary_ptr = NULL;
	crt_free(structp->ary_ptr);
	structp->ary_ptr = ary;
	structp->ary_size = priv_structp->ary_size;
	structp->ary_head = priv_structp->ary_head;
}

/*
 *	merge a private hash into the sociable version
 *	(assumes hash spinlocked and acquireable)
 *
 *	if an element is marked deleted
 *		delete original
 *	else if element was written or new
 *		update or add
 */
STATIC void
native_merge_hash(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int bucket;
	sociable_element_t *h;
	int bucketlen = priv_structp->hash_bucket_count;
	sociable_element_t **buckets = priv_structp->hash_buckets;
	sociable_element_t *telemp;

	for (bucket = 0; bucket < bucketlen; bucket++) {
		h = buckets[bucket];
		while (h) {
			if (SOC_STM_DELETED(h)) {
				sociable_remove_hash_element(aTHX_ &structp->un.hash, h->hash_tuple.keyptr.elemp, h->hash_tuple.keylen, &structp->extent);
			}
			else if (!SOC_STM_WAS_READ(h)) {
			/*
			 *	either write to existing element, or create a new one
			 */
				telemp = sociable_hash_lookup(aTHX_ &structp->un.hash, h->hash_tuple.keyptr.elemp, h->hash_tuple.keylen, 1, &structp->extent);
				native_update_element(aTHX_ h, telemp);
			}
			h = h->header.next;
		}
	}
}

STATIC void
native_update_element(pTHX_ sociable_element_t *priv_elemp, sociable_element_t *elemp)
{
	sociable_element_t *refp;
	sociable_storage_mech_t *mech = (priv_elemp->header.level == SOC_PROCESS_PRIVATE)
		? NULL
		: sociable_mechs[priv_elemp->header.mechid];
	SOC_INCR_ELEM_SEQNO(elemp);

	SOCIABLE_TRACE_2("stm_update_element: updating elemp %p from private elem %p\n", elemp, priv_elemp);
	if (!SvOK(&priv_elemp->SOCHDR)) {
		stm_clear_element(elemp);
		return;
	}

    if (SvROK(&priv_elemp->SOCHDR)) {
/*
 * if its a ref, then get its referent and verify
 * its still sociable
 */
		SOCIABLE_TRACE_1("native_update_element: assigning a ref to elemp %p\n", elemp);
        refp = mech
        	? (*mech->addr_to_ptr)(&priv_elemp->un.ref.refelemp.un)
        	: priv_elemp->un.ref.refelemp.un.elemp;

		SOCIABLE_TRACE_1("native_update_element: ref is %p\n", refp);

		if (refp && (refp->SOCHDR.cycleno != priv_elemp->un.ref.cycleno)) {
/*
 *	referent has been recycled
 */
			SOCIABLE_TRACE("native_update_element: not a sociable ref!!!\n");
	        Perl_croak(aTHX_ "Invalid value for sociable scalar");
		}

		SOCIABLE_TRACE_1("native_update_element: storing for a ref for refp %p\n", refp);
/*
 * need to flag an element as a ref
 */
		memcpy(&elemp->un.ref.refelemp, &priv_elemp->un.ref.refelemp, sizeof(sociable_locator_t));
		SvROK_on(&elemp->SOCHDR);
/*
 * if the contents were blessed after
 * the ref was created, we need to copy the package
 */
		if (refp->header.pkg_name) {
	        if (mech) {
				(*mech->mem_free)(&elemp->header.pkg_name);
				(*mech->mem_strdup)(&elemp->header.pkg_name, refp->header.pkg_name.elemp);
			}
			else {
				crt_free(elemp->header.pkg_name.elemp);
				elemp->header.pkg_name = crt_strdup(refp->header.pkg_name);
			}
		}
    }
    else {
    	priv_elemp->SOCHDR.soc_flags = elemp->SOCHDR.soc_flags;
    	sociable_scalar_t *scalar = &elemp->un.scalar;
    	sociable_scalar_t *priv_scalar = &elemp->un.scalar;
    	char *ptr;
/*
 *	!!! order is important ! must test for unsigned before signed
 */
		if (SvUOK(&priv_elemp->SOCHDR)) {
/*
 *	need to check for an unsigned!
 */
			SOCIABLE_TRACE("native_update_element: save'ing UV SV\n");
			scalar->uv	= priv_scalar->uv;
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK|SVf_IVisUV;
		}
		else if (SvIOK(&priv_elemp->SOCHDR)) {
/*
 *	need to check for an unsigned!
 */
			SOCIABLE_TRACE("native_update_element: save'ing IV SV\n");
			scalar->iv	= priv_scalar->iv;
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK;
		}
		else if (SvNOK(&priv_elemp->SOCHDR)) {
			SOCIABLE_TRACE("native_update_element: save'ing NV SV\n");
			scalar->nv	= priv_scalar->nv;
			elemp->SOCHDR.sv_flags = SVt_NV|SVf_NOK;
		}
		else if (SvPOK(&priv_elemp->SOCHDR) || (SvTYPE(&priv_elemp->SOCHDR) <= SVt_PVLV)) {
			scalar->buflen = priv_scalar->buflen;
			SOCIABLE_TRACE_1("native_update_element: save'ing PV SV length %d\n", scalar->buflen);
			if (!scalar->bufsz) {
				scalar->bufsz = priv_scalar->bufsz;
				if (mech)
					(*mech->mem_alloc)(&scalar->bufptr, scalar->bufsz);
				else
					scalar->bufptr.elemp = crt_malloc(scalar->bufsz);
			}
			else if (scalar->bufsz < priv_scalar->buflen + 1) {
				SOCIABLE_TRACE_2("native_update_element: changing buffers: freeing buffer %p for elemp %p\n", scalar->bufptr.elemp, elemp);
				scalar->bufsz = priv_scalar->bufsz;
				if (mech) {
					(*mech->mem_free)(&scalar->bufptr);
					(*mech->mem_alloc)(&scalar->bufptr, scalar->bufsz);
				}
				else {
					crt_free(scalar->bufptr.elemp);
					CRT_Newz(scalar->bufptr.elemp, scalar->bufsz, char);
				}
			}
			ptr = mech ? (*mech->addr_to_ptr)(&scalar->bufptr) : scalar->bufptr.elemp;
			memcpy(ptr, priv_scalar->bufptr.elemp, scalar->buflen);
			elemp->SOCHDR.sv_flags = SVt_PV|SVf_POK;		/* !!! NEED UTF8ness here !!! */
		}
		else {
			SOCIABLE_TRACE_1("native_update_element: unknown SV type %d!!!\n", SvTYPE(&priv_elemp->SOCHDR));
		}
	}
/*************************************************
 *
 * don't know what to do with these as yet
 *
	    case SVt_PVIV:
			elemp->iv = SvIVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;

	    case SVt_PVNV:
			elemp->iv = SvIVX(sv);
			elemp->nv = SvNVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;
 *************************************************/
	SOCIABLE_TRACE("native_update_element: returning\n");
}

/*
 *	clear contents of a private element to undef state
 */
STATIC void
native_stm_clear_element(sociable_element_t *priv_elemp)
{
#ifdef SOCIABLE_TIE
/*
 *	invoke tie to clear ???
 */
#endif
	if (SvROK(&priv_elemp->header))
		memzero(&priv_elemp->un.ref, sizeof(sociable_ref_t));
	else {
		crt_free(elemp->un.scalar.bufptr.elemp);
		elemp->un.scalar.buflen = elemp->un.scalar.bufsz = 0;
		elemp->un.scalar.iv = 0;
		elemp->un.scalar.nv = 0.0;
	}
	elemp->header.sv_flags = 0;
}

/*
 *	acquire written elements of array for commit
 *	(assumes array spinlocked and acquireable)
 *		1) scan deleted list:
 *			if deleted element has changed or cannot be acquired
 *				restart
 *		2) scan the slot_map
 *			if has an original slot and was accessed
 *				if (1st access was read AND element has changed) OR
 * 					(1st access was write AND cannot acquire)
 *					restart
 *		3) scan deleted bitmap
 *			if bit set
 *				if cannot acquire associated element
 *					restart
 */
STATIC int
private_acquire_array_elems(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int i, j, k;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	int arylen = priv_structp->ary_head;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_element_t *deleted = priv_structp->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = priv_structp->ary_delete_map;
	dMY_CXT;
/*
 *	check for accessed deletes
 */
	while (deleted) {
		telemp = structp->ary_ptr[deleted->un.scalar.iv].elemp;
		owner = telemp->header.owner.elemp;
		if ((SOC_STM_WAS_READ(deleted) && (!NATIVE_VALID_STRUCT_ELEM(deleted, telemp))) ||
			(SOC_STM_WAS_WRITTEN(deleted) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
		deleted = deleted->NEXT_PTR;
	}
/*
 *	check for updates
 */
	for (i = 0; i < arylen; i++) {
		if ((slot_map[i] == SOC_STM_UNMAPPED) ||
			(ary[i].elemp == NULL) ||
			(!memcmp(&ary[i], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)))
			continue;
		telemp = structp->ary_ptr[slot_map[i]].elemp;
		owner = telemp->header.owner.elemp;
		if ((SOC_STM_WAS_READ(telemp) && (!NATIVE_VALID_STRUCT_ELEM(ary[i].elemp, telemp))) ||
			(SOC_STM_WAS_WRITTEN(telemp) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
	}
/*
 *	check for unaccessed deletes
 */
	arylen = ((structp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			telemp = structp->ary_ptr[(i << 3) + k].elemp;
			owner = telemp->header.owner.elemp;
			if ((delete_map[i] & j) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner)))
				return 0;
		}
	}
	return 1;
}

/*
 *	acquire written elements of array for commit
 *	(assumes array spinlocked and acquireable)
 *		1) scan deleted list:
 *			if deleted element has changed or cannot be acquired
 *				restart
 *		2) scan the slot_map
 *			if has an original slot and was accessed
 *				if (1st access was read AND element has changed) OR
 * 					(1st access was write AND cannot acquire)
 *					restart
 *		3) scan deleted bitmap
 *			if bit set
 *				if cannot acquire associated element
 *					restart
 */
STATIC int
shared_acquire_array_elems(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int i, j, k;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	int arylen = priv_structp->ary_head;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_element_t *deleted = priv_structp->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = priv_structp->ary_delete_map;
	sociable_storage_mech *mech = sociable_mechs[structp->header.mechid];
	dMY_CXT;
/*
 *	check for accessed deletes
 */
	while (deleted) {
		telemp = (*mech->addr_to_ptr)(&structp->ary_ptr[deleted->un.scalar.iv]);
		owner = (*mech->addr_to_ptr)(&telemp->header.owner);
		if ((SOC_STM_WAS_READ(deleted) && (!NATIVE_VALID_STRUCT_ELEM(deleted, telemp))) ||
			(SOC_STM_WAS_WRITTEN(deleted) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
		deleted = deleted->NEXT_PTR;
	}
/*
 *	check for updates
 */
	for (i = 0; i < arylen; i++) {
		if ((slot_map[i] == SOC_STM_UNMAPPED) ||
			(ary[i].elemp == NULL) ||
			(!memcmp(&ary[i], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)))
			continue;
		telemp = (*mech->addr_to_ptr)(&structp->ary_ptr[slot_map[i]]);
		owner = (*mech->addr_to_ptr)(&telemp->header.owner);
		if ((SOC_STM_WAS_READ(telemp) && (!NATIVE_VALID_STRUCT_ELEM(ary[i].elemp, telemp))) ||
			(SOC_STM_WAS_WRITTEN(telemp) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
	}
/*
 *	check for unaccessed deletes
 */
	arylen = ((structp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			telemp = (*mech->addr_to_ptr)(&structp->ary_ptr[(i << 3) + k]);
			owner = (*mech->addr_to_ptr)(&telemp->header.owner);
			if ((delete_map[i] & j) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner)))
				return 0;
		}
	}
	return 1;
}

/*
 *	lookup value for the specified key; returns the element for the value
 */
STATIC sociable_element_t *
native_stm_hash_lookup(pTHX_ sociable_proxy_t *proxy, char *key, I32 len, I32 for_write, sociable_proxy_t *valproxy)
{
	sociable_structure_t *priv_structp =
		(sociable_structure_t *)(*proxy->mech->stm_save_referent)(aTHX_ proxy, for_write);
	sociable_hash_t *hash;
	sociable_addr_t *sociable = (sociable_structure_t *)proxy->elemp;
	UV hashval = 0;
	I32 bucket = 0;
	sociable_element_t *h = NULL;
	sociable_element_t *sh = NULL;
	sociable_element_t *telemp = NULL;

	SOCIABLE_TRACE_1("in stm_hash_lookup: key %s\n", key);
	memzero(valproxy, sizeof(sociable_proxy_t));
/*
 *	check for conflicts, or log if 1st access;
 *	but don't check/log for write until we know
 *	we're adding a new key
 */
	if (!priv_structp)
		return NULL;

	hash = &priv_structp->un.hash.elemp;
	PERL_HASH(hashval, key, len);
	bucket = hashval %  hash->bucket_count;
/*
 *	scan private version first
 */
	h = hash->buckets[bucket];
	while ((h != NULL) &&
		((h->hash_tuple.hashval != hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr, len)))
		h = h->header.next;

	SOC_SPIN_LOCK(sociable);
	sh = sociable->hash_buckets[bucket];
	while ((sh != NULL) &&
		((hashval != sh->hash_tuple.hashval) ||
		(len != sh->hash_tuple.keylen) ||
		memcmp(key, sh->hash_tuple.keyptr.elemp, len)))
		sh = sh->header.next;
/*
 *	if no sociable, release ASAP
 */
	if (!sh)
		SOC_SPIN_RELEASE(sociable);
/*
 *	if private not found
 *		if sociable not found
 *			if not for write
 *				return
 *			create empty element
 *		else
 *			create private clone of sociable
 *			mark private version w/ proper access (read or write)
 *		insert and return private version
 */
 	if (!h) {
 		if (!sh) {
			if (!for_write)
				return NULL;
/*
 *	create new private element; need to acquire for write
 */
			if (detector->has_conflict &&
				(*detector->has_conflict)(aTHX_ (sociable_element_t *)priv_structp, 1))
				return NULL;
			CRT_Newz(telemp, 1, sociable_element_t);
			telemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
		else {
/*
 *	create private clone
 *	implicitly its 1st access: try to find in the sociable version
 */
			if (detector->hash_elem_acquire &&
				(*detector->hash_elem_acquire)(aTHX_ sh, sociable, for_write))
				return NULL;

			CRT_Newz(telemp, 1, sociable_element_t);
			native_clone_element(telemp, sh);
			SOC_SPIN_RELEASE(sociable);
			telemp->header.soc_flags = for_write ?
				SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_WRITE :
				SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_READ;
		}
/*
 *	insert into hash
 */
		telemp->hash_tuple.hashval = hashval;
		telemp->hash_tuple.keylen = len;
		CRT_Dup(telemp->hash_tuple.keyptr.elemp, len + 1, char, key, len);
		telemp->header.next.elemp = hash->buckets[hashval].elemp;
		if (hash->buckets[hashval].elemp)
			((sociable_element_t *)hash->buckets[hashval].elemp)->PREV_PTR = telemp;
		hash->buckets[hashval].elemp = telemp;
		return telemp;
 	}
/*
 *	private exists:
 *	if marked deleted
 *		undelete it
 *		set as an undef
 *	if no sociable
 *		return private
 *
 *	check for conflicts
 */
	if (sh && detector->hash_elem_conflict &&
		(*detector->hash_elem_conflict)(aTHX_ h, sh, sociable, for_write)) {
		SOC_SPIN_RELEASE(sociable);
		return NULL;
	}
	SOC_SPIN_RELEASE(sociable);

	if (SOC_STM_DELETED(h)) {
		h->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_WRITE;
		stm_clear_element(h);
	}

	SOCIABLE_TRACE_1("stm_hash_lookup: returning %p\n", h);
	return h;
}

