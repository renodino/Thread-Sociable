#define PERL_NO_GET_CONTEXT
#include "crt_memmgt.h"
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#ifdef HAS_PPPORT_H
#  include "ppport.h"
#endif
#include "sociable.h"
#include "socstoremech.h"
#include "crt_memmgt.h"
#include "socstm.h"
#include "atomic_ops.h"
#include "time.h"

static sociable_addr_t soc_stm_pseudo_elem;
static volatile int stm_ctxt_lock = 0;	/* spinlock */
static int stm_limit = SOC_STM_LIMIT;
static int stm_avail = 0;
static int stm_inuse = 0;
/* static sociable_stm_t *stm_pool = NULL; */
/*
 *	default commit protocol is locked, eager
 */
int stm_commit_with_lock = 1;
int stm_eager_detect = 1;
int stm_timeout = 60;

extern sociable_storage_mech_t **sociable_mechs;
extern int *ordered_mechs;
extern int sociable_next_mech;

STATIC void *stm_free_array(pTHX_ sociable_structure_t *proxy);
STATIC void *stm_free_hash(pTHX_ sociable_structure_t *proxy);
STATIC void stm_merge_array(pTHX_ sociable_structure_t *proxy);
STATIC void stm_merge_hash(pTHX_ sociable_structure_t *proxy);
STATIC void stm_clear_log(pTHX_ sociable_storage_mech_t *mech, sociable_stm_log_t *wal);

/* sociable_element_t *stm_save_referent(pTHX_ sociable_proxy_t *proxy, int for_write); */
STATIC void stm_clone_array(sociable_structure_t *proxy, sociable_structure_t *elemp);
STATIC void stm_clone_hash(sociable_structure_t *proxy, sociable_structure_t *elemp);

/**************************************************************
 **************************************************************
 * GENERAL STM METHODS
 **************************************************************
 **************************************************************/
/*
 *	free the specified sets of transaction closures
 */
STATIC void
stm_free_closures(pTHX_ pMY_CXT_ int kind)
{
	int i;
	SV **commits = MY_CXT.stm_commits;
	SV **restarts = MY_CXT.stm_restarts;
	SV **rollbacks = MY_CXT.stm_rollbacks;
	for (i = MY_CXT.stm_xact_count - 1; i >= 0 ; i--) {
		if ((kind & SOC_STM_COMMIT) && (commits[i] != NULL))
			SvREFCNT_dec(commits[i]);
		if ((kind & SOC_STM_RESTART) && (restarts[i] != NULL))
			SvREFCNT_dec(restarts[i]);
		if ((kind & SOC_STM_ROLLBACK) && (rollbacks[i] != NULL))
			SvREFCNT_dec(rollbacks[i]);
	}
	if (kind & SOC_STM_COMMIT)
		memset(commits, 0, (MY_CXT.stm_xact_count * sizeof(SV *)));
	if (kind & SOC_STM_RESTART)
		memset(restarts, 0, (MY_CXT.stm_xact_count * sizeof(SV *)));
	if (kind & SOC_STM_ROLLBACK)
		memset(rollbacks, 0, (MY_CXT.stm_xact_count * sizeof(SV *)));
}

/*
 *	call the specified set of transaction closures;
 *	then discard both sets of closures
 */
int
stm_call_closures(pTHX_ pMY_CXT_ int kind)
{
	int i, abort = 0;
	STRLEN n_a;
	SV **closures;
	dSP;

	closures =
		(kind == SOC_STM_RESTART) ? MY_CXT.stm_restarts :
		(kind == SOC_STM_COMMIT)  ?  MY_CXT.stm_commits :
		/* kind == SOC_STM_ROLLBACK */ MY_CXT.stm_rollbacks;
/*
 *	restart or rollback: clear the WAL
 */
	if (kind != SOC_STM_COMMIT)
 		stm_clear_all_logs(aTHX_ (kind == SOC_STM_ROLLBACK));
/*
 *	rollback: clear transaction context
 */
	if (kind == SOC_STM_ROLLBACK) {
		for (i = 0; i < SOCIABLE_MECHS; i++) {
			if (MY_CXT.stm_ctxts[i].ctxt.elemloc.segmentid || MY_CXT.stm_ctxts[i].ctxt.elemloc.offset)
 				(*sociable_mechs[i]->stm_free_ctxt)(&MY_CXT.stm_ctxts[i].ctxt);
		}
	}

	for (i = MY_CXT.stm_xact_count - 1; i >= 0 ; i--) {
		if (closures[i] != NULL) {
/*
 *	call a Perl closure reference in eval;
 *	if callback generates error
 *		if doing commits OR
 *			(doing restarts AND error != 'Thread::Sociable STM RESTART')
 *			return 0
 */
        	PUSHMARK(SP);
        	call_sv(closures[i], G_VOID|G_DISCARD|G_NOARGS|G_EVAL|G_KEEPERR);
       		if (SvTRUE(ERRSV) &&
				((kind == SOC_STM_COMMIT) ||
				((kind == SOC_STM_RESTART) &&
					strncmp(SvPV(ERRSV, n_a), SOC_STM_RESTART_MSG, strlen(SOC_STM_RESTART_MSG)))))
	        	return 0;
			SvREFCNT_dec(closures[i]);
			closures[i] = NULL;
		}
	}
/*
 *	clear out all the other closures
 */
	kind =
		(kind == SOC_STM_RESTART) ? (SOC_STM_COMMIT | SOC_STM_ROLLBACK) :
		(kind == SOC_STM_COMMIT) ? (SOC_STM_RESTART | SOC_STM_ROLLBACK) :
	 	(SOC_STM_RESTART | SOC_STM_COMMIT);
	 stm_free_closures(aTHX_ aMY_CXT_ kind);
	 MY_CXT.stm_xact_avail += MY_CXT.stm_xact_count;
	 MY_CXT.stm_xact_count = 0;
	 return 1;
}

/*
 *	add empty slots to the xaction closure lists
 */
STATIC void
stm_extend_xacts(pMY_CXT)
{
	dTHX;
	MY_CXT.stm_xact_avail += SOC_STM_EXTENT;
	CRT_Extend(MY_CXT.stm_commits, MY_CXT.stm_xact_count, SV*, SOC_STM_EXTENT);
	CRT_Extend(MY_CXT.stm_restarts, MY_CXT.stm_xact_count, SV*, SOC_STM_EXTENT);
	CRT_Extend(MY_CXT.stm_rollbacks, MY_CXT.stm_xact_count, SV*, SOC_STM_EXTENT);
}

/**************************************************************
 **************************************************************
 * MERGESORT FOR LOCKFREE COMMIT
 **************************************************************
 **************************************************************/
STATIC void
stm_private_compare(int i, int j, int size, int *in, int *out, sociable_addr_t *WAL)
{
	int x = j, z = j;
	int end1 = (((j + i) <= size) ? (j + i) : size);
	int end2 = ((j + (i<<1)) <= size ? (j + (i<<1)) : size);
	int y = j + i;
/*
 * each iteration merges two length- i pairs
 */
	while ((x < end1) && (y < end2))
		out[z++] = (WAL[in[x]].elemp <= WAL[in[y]].elemp) ? in[x++] : in[y++];
/*
 *	copy whatever is leftover in either set
 */
	if (x < end1)
		memcpy(&out[z], &in[x], (end1 - x) * sizeof(int));
	else if (y < end2)
		memcpy(&out[z], &in[y], (end2 - y) * sizeof(int));
}

STATIC void
stm_shared_compare(int i, int j, int size, int *in, int *out, sociable_addr_t *WAL)
{
	int x = j, z = j;
	int end1 = (((j + i) <= size) ? (j + i) : size);
	int end2 = ((j + (i<<1)) <= size ? (j + (i<<1)) : size);
	int y = j + i;
/*
 * each iteration merges two length- i pairs
 */
	while ((x < end1) && (y < end2))
		out[z++] = ((WAL[in[x]].elemloc.segmentid < WAL[in[y]].elemloc.segmentid) ||
				((WAL[in[x]].elemloc.segmentid == WAL[in[y]].elemloc.segmentid) &&
					(WAL[in[x]].elemloc.offset <= WAL[in[y]].elemloc.offset))) ?
			in[x++] : in[y++];
/*
 *	copy whatever is leftover in either set
 */
	if (x < end1)
		memcpy(&out[z], &in[x], (end1 - x) * sizeof(int));
	else if (y < end2)
		memcpy(&out[z], &in[y], (end2 - y) * sizeof(int));
}

STATIC void
stm_foreign_compare(int i, int j, int size, int *in, int *out, sociable_addr_t *WAL)
{
	int x = j, z = j;
	int end1 = (((j + i) <= size) ? (j + i) : size);
	int end2 = ((j + (i<<1)) <= size ? (j + (i<<1)) : size);
	int y = j + i;
/*
 * each iteration merges two length- i pairs
 */
	while ((x < end1) && (y < end2))
		out[z++] = (memcmp(WAL[in[x]].elemid, WAL[in[y]].elemid, sizeof(sociable_addr_t)) <= 0)
			? in[x++] : in[y++];
/*
 *	copy whatever is leftover in either set
 */
	if (x < end1)
		memcpy(&out[z], &in[x], (end1 - x) * sizeof(int));
	else if (y < end2)
		memcpy(&out[z], &in[y], (end2 - y) * sizeof(int));
}
/*
 *	Applies nonrecursive merge sort to elemid's of WAL entries, along
 *	with keeping proxy addresses in corresponding order
 *	Assumes the referent map has already been collected into the WAL
 */
void
stm_sort_WAL(pTHX_ pMY_CXT_ sociable_stm_log_t *wal, int level)
{
	int *in = NULL; // make a new temporary array
	int *out = NULL; // output array
	int *temp = NULL; // temp array reference used for swapping
	int i, j;
	int size = wal->WAL_count;
	void (*comparator)(int i, int j, int size, int *in, int *out, sociable_addr_t *WAL) =
		(level == SOC_PROCESS_PRIVATE_MECH)
			? &stm_private_compare
			: (level == SOC_PROCESS_SHARED_MECH)
				? &stm_shared_compare
				: &stm_foreign_compare;

	sociable_addr_t *outWAL = NULL; // output array
	sociable_proxy_t **out_proxies = NULL;
/*
 *	traverse referent map, accumulating counts of each bucket
 *	alloc array of element ptrs
 *	fill array from map buckets
 */
 	CRT_Newz(in, wal->WAL_count, int);
 	CRT_Newz(out, wal->WAL_count, int);
 	i = 0;
 	while (i < wal->WAL_count) {
 		in[i] = i;
 		i++;
	}

	for (i = 1; i < size; i <<= 1) {
/*
 * each iteration sorts all length - 2*i runs
 */
		for (j = 0; j < size; j += (i<<1))
			(*comparator)(i, j, size, in, out, wal->WAL);
/*
 * swap arrays for next iteration
 */
		temp = in;
		in = out;
		out = temp;
	}
	crt_free(out);
/*
 * the "in" array contains the sorted array, copy out to context WAL
 * copy the reordered sociable ptrs into the WAL as well
 */
	CRT_Newz(outWAL, wal->WAL_count, sociable_addr_t);
	CRT_Newz(out_proxies, wal->WAL_count, sociable_proxy_t *);
 	for (i = 0; i < wal->WAL_count; i++) {
		memcpy(&outWAL[i], &wal->WAL[in[i]], sizeof(sociable_addr_t));
		out_proxies[i] = wal->WAL_proxies[in[i]];
 	}
	crt_free(in);
	crt_free(wal->WAL);
	crt_free(wal->WAL_proxies);
	wal->WAL = outWAL;
	wal->WAL_proxies = out_proxies;
}

/******************************************************************
 ******************************************************************
 *
 *	STM NATIVE EAGER ACQUIRE/CONFLICT DETECTOR METHODS
 *
 ******************************************************************
 ******************************************************************/
STATIC int
native_eager_acquire(pTHX_ pMY_CXT_ sociable_proxy_t *proxy, int for_write)
{
	sociable_stm_ctxt_t *owner;

	SOC_BUMP_KARMA;
	SOC_ACQUIRE_SPINLOCK(&proxy->elemp->header.spinlock)
/*
 *	make ourselves owner if possible
 */
	owner = NATIVE_GET_OWNER(proxy);
	if (NATIVE_ELEM_CAN_ACQUIRE(proxy->elemp, owner)) {
		NATIVE_MAKE_ELEM_OWNER(proxy->elemp);
		return 1;
	}
/*
 *	restart if can't acquire for write
 */
	if (for_write)
		SOC_RELEASE_SPINLOCK(&proxy->elemp->header.spinlock);

	return (!for_write);
}

/*
 *	eager conflict detector:
 *	if prior read in conflict
 *		return "conflict detected"
 *	if 1st access
 *		bump our karma
 *	if for write and can't (re)acquire
 *		return "conflict detected"
 *	else return "no conflict"
 */
STATIC int
native_eager_has_conflict(pTHX_ pMY_CXT_ sociable_stm_proxy_t *proxy, int for_write)
{
	sociable_element_t *priv = proxy->priv.elemp;
	sociable_element_t *shared = proxy->shared.elemp;
	sociable_stm_ctxt_t *owner;

	if (SOC_STM_WAS_READ(priv) && (!NATIVE_VALID_READ(proxy)))
		return 1;

	if (NATIVE_ELEM_AM_OWNER(shared))
		return 0;

	SOC_ACQUIRE_SPINLOCK(&shared->header.spinlock);
	owner = NATIVE_ELEM_GET_OWNER(shared);

	if (!NATIVE_ELEM_CAN_ACQUIRE(shared, owner)) {
		SOC_RELEASE_SPINLOCK(&shared->header.spinlock);
		return (!for_write);
	}
 	NATIVE_MAKE_ELEM_OWNER(shared);
	SOC_RELEASE_SPINLOCK(&shared->header.spinlock);
	return 0;
}

/*
 *	eager conflict detector for array elements
 */
STATIC int
native_eager_ary_elem_conflict(pTHX_ pMY_CXT_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int tkey, int for_write)
{
	sociable_element_t *elemp;
	sociable_element_t *priv_elemp = NULL;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
/*	sociable_addr_t *ary_ptr; */
	sociable_addr_t *ary = (sociable_addr_t *)priv_structp->ary_ptr;
/*
 *	if unaccessed element, create a new one
 */
	if (SOC_IS_UNACCESSED(ary, key)) {
		SOC_BUMP_KARMA;
		CRT_Newz(ary[key].elemp, 1, sociable_element_t);
		if (for_write & SOC_STM_FOR_WRITE)
			priv_structp->header.soc_flags |= SOC_STM_ELEM_WRITE;
	}
	priv_elemp = ary[key].elemp;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, tkey);

	if (SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp))) {
/*
 *	element was read and has changed: conflict
 */
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
		return 1;
	}
/*
 *	if unaccessed, clone element before trying to acquire,
 *	since it may be for a read and will return OK on failure;
 *	mark as read 1st if for read or delete operation
 */
	if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
		native_clone_element(priv_elemp, elemp);
		priv_elemp->header.soc_flags =
			(for_write == SOC_STM_FOR_WRITE)
				? SOC_INUSE | SOC_STM_WRITE | SOC_STM_PRIVATE
				: (for_write == SOC_STM_FOR_DELETE)
					? SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE
					: SOC_INUSE | SOC_STM_READ | SOC_STM_PRIVATE;
	}
/*
 *	for now, use structure lock to protect elements;
 *	in future, use hierarchical lock protocol
 */
 	owner = NATIVE_ELEM_GET_OWNER(elemp);
	if (!NATIVE_ELEM_CAN_ACQUIRE(elemp, owner)) {
		SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
		return for_write;
	}
	NATIVE_MAKE_ELEM_OWNER(elemp);
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

/*
 *	eager conflict detector for array splice
 *	NOTE: ignore lazy flag here, otherwise we may access
 *	invalid elements
 */
 /**** CONTINUE HERE !!!! ****/
STATIC int
native_eager_splice_conflict(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int len)
{
	sociable_element_t *elemp;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	sociable_element_t *priv_elemp = NULL;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_addr_t *ary_ptr;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
	int i;
	dMY_CXT;
/*
 *	for each unaccessed element, create a new empty element
 */
 	for (i = key; i < key + len; i++) {
		if (SOC_IS_UNACCESSED(ary, i)) {
			SOC_BUMP_KARMA;
			CRT_Newz(ary[i].elemp, 1, sociable_element_t);
			elemp = ary[i].elemp;
			elemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
	}
	proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
/*
 *	scan for conflicts with the sociables
 */
 	for (i = key; i < key + len; i++) {
 		if (slot_map[i] == SOC_STM_UNMAPPED)
 			continue;
	 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, slot_map[i]);
 		priv_elemp = ary[i].elemp;

	 	owner = NATIVE_ELEM_GET_OWNER(elemp);
		if ((SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp))) ||
			(!NATIVE_ELEM_CAN_ACQUIRE(elemp, owner))) {
/*
 *	element was read and has changed, OR cannot be acquired: conflict
 */
		 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
			return 1;
		}
		NATIVE_MAKE_ELEM_OWNER(elemp);
/*
 *	if unaccessed, clone the element
 *	mark as read 1st
 */
		if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
			native_clone_element(priv_elemp, elemp);
			priv_elemp->header.soc_flags = SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE;
		}
	}
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

STATIC int
native_eager_hash_elem_acquire(pTHX_ sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	sociable_addr_t *owner;
	dMY_CXT;
	SOC_BUMP_KARMA;
 	owner = NATIVE_ELEM_GET_OWNER(sh);
	if (NATIVE_ELEM_CAN_ACQUIRE(sh, owner)) {
		NATIVE_MAKE_ELEM_OWNER(sh);
		return 0;
	}
	if (for_write)
		SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return for_write;
}

STATIC int
native_eager_hash_elem_conflict(pTHX_ sociable_element_t *h, sociable_element_t *sh, int for_write)
{
	sociable_addr_t *owner;
	dMY_CXT;
	if (SOC_STM_WAS_READ(h) && (!NATIVE_VALID_ELEM_READ(h, sh)))
		return 1;

 	owner = NATIVE_ELEM_GET_OWNER(sh);
	if (NATIVE_ELEM_CAN_ACQUIRE(sh, owner)) {
		NATIVE_MAKE_ELEM_OWNER(sh);
		return 0;
	}

	return for_write;
}

/******************************************************************
 ******************************************************************
 *
 *	STM NATIVE LAZY ACQUIRE/CONFLICT DETECTOR METHODS
 *
 ******************************************************************
 ******************************************************************/
STATIC int
native_lazy_acquire(pTHX_ sociable_element_t *elemp, int for_write)
{
	dMY_CXT;

	SOC_BUMP_KARMA;
	SOC_ACQUIRE_SPINLOCK(&elemp->header.spinlock);
	return 1;
}
/*
 *	lazy conflict detector:
 *	always return "no conflict"
 */
STATIC int
native_lazy_has_conflict(pTHX_ sociable_stm_proxy_t *proxy, int for_write)
{
	return 0;
}

/*
 *	lazy conflict detector for array elements
 */
STATIC int
native_lazy_ary_elem_conflict(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int tkey, int for_write)
{
	sociable_element_t *elemp;
	sociable_element_t *priv_elemp = NULL;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
	sociable_addr_t *ary_ptr;
	sociable_addr_t *ary = (sociable_addr_t *)priv_structp->ary_ptr;
	dMY_CXT;
/*
 *	if unaccessed element, create a new one
 */
	if (SOC_IS_UNACCESSED(ary, key)) {
		SOC_BUMP_KARMA;
		CRT_Newz(ary[key].elemp, 1, sociable_element_t);
		if (for_write & SOC_STM_FOR_WRITE)
			priv_structp->header.soc_flags |= SOC_STM_ELEM_WRITE;
	}
	priv_elemp = ary[key].elemp;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, tkey);
/*
 *	if unaccessed, copy metadata before trying to acquire,
 *	since it may be for a read and will return OK on failure;
 *	mark as read 1st if for read or delete operation
 */
	if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
		native_clone_element(priv_elemp, elemp);
		priv_elemp->header.soc_flags =
			(for_write == SOC_STM_FOR_WRITE) ? SOC_INUSE | SOC_STM_WRITE | SOC_STM_PRIVATE :
			(for_write == SOC_STM_FOR_DELETE) ? SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE :
			SOC_INUSE | SOC_STM_READ | SOC_STM_PRIVATE;
	}
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

/*
 *	lazy conflict detector for array splice
 */
STATIC int
native_lazy_splice_conflict(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *priv_structp, int key, int len)
{
	sociable_element_t *elemp;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	sociable_element_t *priv_elemp = NULL;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_addr_t *ary_ptr;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *owner;
	int i;
	dMY_CXT;
/*
 *	for each unaccessed element, create a new empty element
 */
 	for (i = key; i < key + len; i++) {
		if (SOC_IS_UNACCESSED(ary, i)) {
			SOC_BUMP_KARMA;
			CRT_Newz(ary[i].elemp, 1, sociable_element_t);
			elemp = ary[i].elemp;
			elemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
	}
	proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
 	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!NATIVE_VALID_READ(proxy)) {
	 	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
 		return 1;
 	}
/*
 *	clone any unaccessed elements
 */
 	for (i = key; i < key + len; i++) {
 		if (slot_map[i] == SOC_STM_UNMAPPED)
 			continue;
	 	NATIVE_GET_ARRAY_ELEM(elemp, proxy, sociable, ary_ptr, slot_map[i]);
 		priv_elemp = ary[i].elemp;
/*
 *	if unaccessed, clone the element
 *	mark as read 1st
 */
		if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
			native_clone_element(priv_elemp, elemp);
			priv_elemp->header.soc_flags = SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE;
		}
	}
	SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	return 0;
}

STATIC int
native_lazy_hash_elem_acquire(pTHX_ sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	dMY_CXT;
	SOC_BUMP_KARMA;
	return 0;
}

STATIC int
native_lazy_hash_elem_conflict(pTHX_ sociable_element_t *h, sociable_element_t *sh, int for_write)
{
	return 0;
}

sociable_stm_detector_t native_eager = {
	&native_eager_acquire,
	&native_eager_has_conflict,
	&native_eager_ary_elem_conflict,
	&native_eager_splice_conflict,
	&native_eager_hash_elem_acquire,
	&native_eager_hash_elem_conflict
};

sociable_stm_detector_t native_lazy = {
	&native_lazy_acquire,
	NULL, /* &native_lazy_has_conflict, */
	&native_lazy_ary_elem_conflict,
	&native_lazy_splice_conflict,
	&native_lazy_hash_elem_acquire,
	NULL /* &native_lazy_hash_elem_conflict */
};

/******************************************************************
 ******************************************************************
 *
 *	NATIVE STM METHODS
 *
 ******************************************************************
 ******************************************************************/
/*
 *	create a complete private clone of a sociable element
 *	assumes caller holds spinlock on elemp (or the parent structure)
 */
STATIC void
native_clone_element(sociable_element_t *privelemp, sociable_element_t *sociable)
{
	char *ptr = NULL;
	char *keyptr = NULL;
	char *bufptr = NULL;
	sociable_storage_mech_t *mech = sociable_mechs[sociable->header.mechid];

	memcpy(&privelemp->header, &sociable->header, sizeof(sociable_native_t));
	if (sociable->header.level == SOC_PROCESS_PRIVATE_MECH) {
		if (sociable->header.pkg_name.elemp)
			ptr = (sociable->header.pkg_name);
		if (sociable->hash_tuple.keylen)
			keyptr = sociable->hash_tuple.keyptr.elemp;
		if (!SvROK(&sociable->SOCHDR))
			bufptr = sociable->un.scalar.bufptr.elemp;
	}
	else {
		if (sociable->header.pkg_name.elemp)
			ptr = (*mech->addr_to_ptr)(&sociable->header.pkg_name);
		if (sociable->hash_tuple.keylen)
			keyptr = (*mech->addr_to_ptr)(&sociable->hash_tuple.keyptr);
		if (!SvROK(&sociable->SOCHDR))
			bufptr = (*mech->addr_to_ptr)(&sociable->un.scalar.bufptr);
	}
	if (ptr)
		privelemp->header.pkg_name.elemp = crt_strdup(sociable->header.pkg_name.elemp);
	if (keyptr) {
		privelemp->hash_tuple.keylen = sociable->hash_tuple.keylen;
		privelemp->hash_tuple.hashval = sociable->hash_tuple.hashval;
		CRT_Dup(privelemp->hash_tuple.keyptr.elemp, privelemp->hash_tuple.keylen + 1, char, keyptr, privelemp->hash_tuple.keylen);
	}

#ifdef SOCIABLE_TIE
/*
 *	copy tie info in future
 */
#endif
	if (SvROK(&sociable->header))
		memcpy(&privelemp->un.ref, &sociable->un.ref, sizeof(sociable_ref_t));
	else {
		memcpy(&privelemp->un.scalar, &sociable->un.scalar, sizeof(sociable_scalar_t));
		if (privelemp->un.scalar.bufsz)
			CRT_Dup(proxy->un.scalar.bufptr.elemp, proxy->un.scalar.bufsz, char, bufptr, proxy->un.scalar.buflen);
		else
			proxy->un.scalar.bufptr.elemp = NULL;
	}
}

STATIC void
native_clone_array(sociable_structure_t *proxy, sociable_structure_t *elemp)
{
	sociable_element_t **ary;
	I32 count;
	I32 *slot_map;
	I32 i;

	count = proxy->ary_head = elemp->ary_head;
	proxy->header.updcntlo = elemp->header.updcntlo;
	proxy->header.updcnthi = elemp->header.updcnthi;
	proxy->header.cycleno = elemp->header.cycleno;
	proxy->header.sv_flags = elemp->header.sv_flags;
	proxy->ary_extent = elemp->ary_extent;
	proxy->ary_himark = elemp->ary_himark;
	SOC_RELEASE_SPINLOCK(&elemp->header.spinlock);
/*
 *	create array slots, a map to current slot
 *	positions, and a delete map bitmask.
 *	Slot map is used to locate original element when elements
 *	get movedaround; Delete map is used to mark position of
 *	deleted elements.
 *	Alloc a few extra slots to try to avoid frequent
 *	copies
 */
	proxy->header.sociable = (sociable_element_t *)elemp;
	CRT_Newz(ary, count + SOCIABLE_ARRAY_SPARE, sociable_element_t*);
	CRT_Newz(slot_map, count + SOCIABLE_ARRAY_SPARE, int);
	CRT_Newz(proxy->un.array.delete_map, (((count >> 3) & 0x1FFFFFFF) + 1), char);
	proxy->ary_size = proxy->ary_head + SOCIABLE_ARRAY_SPARE;
	for (i = 0; i < proxy->ary_head; slot_map[i] = i, memcpy(&ary[i++], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)));
	for (;i < proxy->ary_size; slot_map[i++] = SOC_STM_UNMAPPED);
	proxy->ary_ptr = ary;
	proxy->ary_slot_map = slot_map;
}

STATIC void
native_clone_hash(sociable_structure_t *proxy, sociable_structure_t *elemp)
{
	sociable_hash_t *thash = &proxy->un.hash;

	proxy->header.updcntlo = elemp->header.updcntlo;
	proxy->header.updcnthi = elemp->header.updcnthi;
	proxy->header.cycleno = elemp->header.cycleno;
	proxy->header.sv_flags = elemp->header.sv_flags;
	proxy->hash_bucket_count = elemp->hash_bucket_count;
	proxy->extent.extent = elemp->extent.extent;
	proxy->extent.himark = elemp->extent.himark;
	SOC_RELEASE_SPINLOCK(&elemp->header.spinlock);
/*
 *	elements are not added to the proxy until accessed
 */
	proxy->header.sociable = (sociable_element_t *)elemp;
	CRT_Newz(proxy->hash_buckets, proxy->hash_bucket_count, sociable_element_t*);
}

/*
 *	lookup in WAL hash;
 *	if found, conflict detect before returning the private version
 *  else attempt to acquire and create private version
 */
sociable_element_t *
native_save_referent(pTHX_ sociable_proxy_t *proxy, int for_write)
{
/*
 *	simple hash here: divide by 32, then mod 23. May need to verify the skew.
 */
 	byte is_private = (proxy->mech->level == SOC_PROCESS_PRIVATE_MECH);
 	sociable_element_t *sociable = is_private
 		? proxy->sociable.un.elemp
 		: (*proxy->mech->addr_to_ptr)(&proxy->sociable.un);
	sociable_stm_bucket_t *bucket = &MY_CXT.stm_referents[proxy->stm_hash];
	sociable_structure_t *priv_structp;
	sociable_structure_t *priv_elemp;
	sociable_stm_detector_t *detector = proxy->mech->detector;
	sociable_stm_ctxt_t *owner;
	int mechid = proxy->sociable.mechid;
	SV *errsv;
	int i;
	dMY_CXT;
/*
 *	verify the referent is still sociable
 */
 	if (SOC_STM_NOT_SOCIABLE(sociable)) {
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, "Variable is not sociable.");
		return NULL;
 	}
/*
 *	make sure we've got a context for this mechanism
 */
	if (MY_CXT.stm_ctxts[mechid].ctxtp == NULL) {
		if (!(*proxy->mech->stm_alloc_ctxt)(&MY_CXT.stm_ctxts[mechid].ctxt))) {
			errsv = get_sv("@", TRUE);
			sv_setpv(errsv, "Cannot allocate transaction context.");
			return NULL;
		}
		MY_CXT.stm_ctxts[mechid].ctxtp = is_private
			? MY_CXT.stm_ctxts[mechid].ctxt.elemloc.elemp
			: (*proxy->mech->addr_to_ptr)(&MY_CXT.stm_ctxts[mechid].ctxt.elemloc);
	}

	for (i = 0; (i < bucket->count) &&
		memcmp(&bucket->bucket[i].elemloc, &proxy->sociable, sizeof(sociable_locator_t)); i++);
	if (i < bucket->count) {
/*
 *	already logged, so check for conflicts (if eager)
 */
		if (detector->has_conflict &&
			(*detector->has_conflict)(aTHX_ &bucket->bucket[i], for_write)) {
			errsv = get_sv("@", TRUE);
			sv_setpv(errsv, SOC_STM_RESTART_MSG);
			return NULL;
		}

		if (for_write)
			bucket->bucket[i].priv.elemp->header.header.soc_flags |= SOC_STM_WRITE;

		return bucket->bucket[i].priv.elemp;
	}
/*
 *	never logged before, so attempt to acquire and add it
 */
	if (!bucket->avail) {
		CRT_Extend(bucket->bucket, bucket->count + bucket->avail, sociable_stm_proxy_t, SOCIABLE_ARRAY_SPARE);
		bucket->avail += SOCIABLE_ARRAY_SPARE;
	}
	SOC_BUMP_KARMA;
/*
 *	attempt to acquire it, then create private clone of it
 */
	switch (SvTYPE(&proxy->header)) {
		case SVt_PVAV:
		case SVt_PVHV:
			CRT_Newz(priv_structp, 1, sociable_structure_t);
			bucket->bucket[bucket->count].priv.structp = priv_structp;
			break;

		default:
			CRT_Newz(priv_elemp, 1, sociable_element_t);
			bucket->bucket[bucket->count].priv.elemp = priv_elemp;
	}
	SOC_ACQUIRE_SPINLOCK(&sociable->header.spinlock);
	owner = is_private
		? sociable->header.owner.elemp
		: (*proxy->mech->addr_to_ptr)(&sociable->header.owner);
	if (NATIVE_ELEM_CAN_ACQUIRE(sociable, owner))
		memcpy(&sociable->header.owner, &MY_CXT.stm_ctxts[mechid].ctxt, sizeof(sociable_addr_t));
	else if (for_write) {
		SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
		crt_free(bucket->bucket[bucket->count].priv.elemp);
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, SOC_STM_RESTART_MSG);
		return NULL;
	}
	switch (SvTYPE(proxy)) {
		case SVt_PVAV:
			native_clone_array(priv_structp, (sociable_structure_t *)sociable); /* releases spinlock */
			break;

		case SVt_PVHV:
			native_clone_hash(priv_structp, (sociable_structure_t *)sociable); /* releases spinlock */
			break;

		default:
			native_clone_element(priv_elemp, sociable); /* does NOT release spinlock */
			SOC_RELEASE_SPINLOCK(&sociable->header.spinlock);
	}
	memcpy(&bucket->bucket[bucket->count].elemloc, &proxy->sociable, sizeof(sociable_locator_t));
	bucket->bucket[bucket->count].shared.elemp = is_private
		? sociable->un.elemp
		: (*proxy->mech->addr_to_ptr)(&sociable->un);
	bucket->avail--;
	bucket->count++;
	bucket->bucket[bucket->count].priv.elemp->header.header.soc_flags =
		SOC_INUSE | SOC_STM_PRIVATE | (for_write ? SOC_STM_WRITE : SOC_STM_READ);
	return bucket->bucket[bucket->count - 1].priv.elemp;
}

int
private_acquire_for_commit(pTHX_ sociable_stm_log_t *wal)
{
	int count = wal->WAL_count;
	int i, j = 0;
	sociable_element_t *elemp, *priv_elemp;
	int rc;

	wal->spinlocked = 0;
	for (i = 0; i < count; i++) {
		elemp = wal->WAL[i].elemp;
		priv_elemp = wal->WAL_proxies[i]->elemp;
		SOC_ACQUIRE_SPINLOCK(elemp);
		wal->spinlocked++;
		owner = elemp->header.owner.elemp;
		if ((SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp)) ||
			(SOC_STM_WAS_WRITTEN(priv_elemp) && (!NATIVE_CAN_ACQUIRE(elemp, owner))))
			return 0;
/*
 *	don't actually have to acquire, since spinlock is held
 *	until fully committed
 *
 *	if an array or hash that was written, acquire written elements
 */
		if (!SOC_STM_HAS_WRITES(priv_elemp))
			continue;
		rc = (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVAV)
			? private_acquire_array_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp)
			: private_acquire_hash_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp);
		if (!rc)
			return 0;
	}
	return 1;
}

int
shared_acquire_for_commit(pTHX_ sociable_stm_log_t *wal)
{
	int count = wal->WAL_count;
	int i, j = 0;
	sociable_element_t *elemp, *priv_elemp;
	int rc;

	wal->spinlocked = 0;
	for (i = 0; i < count; i++) {
		elemp = wal->WAL[i].elemp;
		priv_elemp = wal->WAL_proxies[i]->elemp;
		SOC_ACQUIRE_SPINLOCK(elemp);
		wal->spinlocked++;
		owner = (*mech->addr_to_ptr)(&elemp->header.owner);
		if ((SOC_STM_WAS_READ(priv_elemp) && (!NATIVE_VALID_STRUCT_ELEM(priv_elemp, elemp)) ||
			(SOC_STM_WAS_WRITTEN(priv_elemp) && (!NATIVE_CAN_ACQUIRE(elemp, owner))))
			return 0;
/*
 *	don't actually have to acquire, since spinlock is held
 *	until fully committed
 *
 *	if an array or hash that was written, acquire written elements
 */
		if (!SOC_STM_HAS_WRITES(priv_elemp))
			continue;
		rc = (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVAV)
			? shared_acquire_array_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp)
			: shared_acquire_hash_elems(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)elemp);
		if (!rc)
			return 0;
	}
	return 1;
}

STATIC void
native_clear_log(pTHX_ sociable_stm_log_t *wal)
{
	int i;
	sociable_proxy_t *proxy;
	dMY_CXT;
/*
 *	for each spinlocked element
 *		if we own it
 *			release it
 */
	for (i = 0; i < wal->spinlocked; i++) {
		proxy = wal->WAL_proxies[i];
		if (NATIVE_AM_OWNER(proxy))
			NATIVE_CLEAR_OWNER(proxy);
		SOC_RELEASE_SPINLOCK(&proxy->elemp->header.spinlock);
	}
	wal->spinlocked = 0;
/*
 *	for each unlocked element
 *		if we might own it
 *			spinlock it
 *			if still owned
 *				release ownership
 *			release spinlock
 *
 */
	for (; i < wal->WAL_count; i++) {
		proxy = wal->WAL[i];
/*
 *	a quick bit equivalence test outside spinlock can
 *	move us along faster
 */
		if (NATIVE_AM_OWNER(proxy)) {
			SOC_ACQUIRE_SPINLOCK(&proxy->elemp->header.spinlock);
			if (NATIVE_AM_OWNER(proxy))
				NATIVE_CLEAR_OWNER(proxy);
			SOC_RELEASE_SPINLOCK(&proxy->elemp->header.spinlock);
		}
	}
/*
 *	discard private copies and clear the WAL
 */
 	stm_reset_elements(aTHX_ wal);
}

/*
 *	merge a private copy into the sociable version
 *	(assumes element is spinlocked and acquireable)
 */
void
native_merge_element(pTHX_ sociable_addr_t *elemp, sociable_element_t *priv_elemp)
{
	SOCIABLE_TRACE_2("native_merge_element: merging proxy %p into elemp %p\n", priv_elemp, elemp);

	sociable_element_t *telemp = (elemp->header.level == SOC_PROCESS_PRIVATE_MECH)
		? elemp.elemp
		: (*sociable_mechs[priv_elemp->header.mechid]->addr_to_ptr)(elemp);
	if (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVAV)
		native_merge_array(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)telemp);
	else if (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVHV)
		native_merge_hash(aTHX_ (sociable_structure_t *)priv_elemp, (sociable_structure_t *)telemp);
	else
		native_update_element(aTHX_ priv_elemp, telemp);

	SOCIABLE_TRACE("native_merge_element: returning\n");
}

/*
 *	merge (aka commit) a private array into the sociable version
 *	(assumes array spinlocked and acquireable)
 *	create new array pointer
 *	scan slot_map
 *		if mapped
 *			move ptr from original slot to new slot
 *			if accessed
 *				update the original element
 *			else
 *				create new element in new slot from private element
 *
 *		scan deleted bitmap
 *			if bit set
 *				free the assoc. original element
 */
STATIC void
native_merge_array(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int i,j,k;
	sociable_element_t **ary = priv_structp->ary_ptr;
	int arylen = priv_structp->ary_head;
	int *slot_map = priv_structp->ary_slot_map;
	sociable_element_t *deleted = priv_structp->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = priv_structp->ary_delete_map;
/*
 *	we should probably scan for and create new unmapped elements
 *	in a group to optimize
 */
	for (i = 0; i < arylen; i++) {
		telemp = ary[i];
		if (slot_map[i] == SOC_STM_UNMAPPED) {
			/* create sociable from private element */
			ary[i].elemp = stm_new_sociable(aTHX_ telemp);
		}
		else {
			memcpy(&ary[i], &structp->ary_ptr[slot_map[i]], sizeof(sociable_addr_t));
			if (SOC_STM_WAS_WRITTEN(ary[i].elemp))
				native_update_element(aTHX_ telemp, ary[i].elemp);
		}
		stm_free_element(aTHX_ telemp, NULL);
	}
/*
 *	remove deletes: we should cluster the removed elements
 *	and return to the pool (or extent) in a single locked operation
 */
	arylen = ((structp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			if (delete_map[i] & j)
				sociable_destroy(aTHX_ structp->ary_ptr[(i << 3) + k]);
		}
	}
/*
 *	move everything to the sociable array
 */
	priv_structp->ary_ptr = NULL;
	crt_free(structp->ary_ptr);
	structp->ary_ptr = ary;
	structp->ary_size = priv_structp->ary_size;
	structp->ary_head = priv_structp->ary_head;
}

/*
 *	merge a private hash into the sociable version
 *	(assumes hash spinlocked and acquireable)
 *
 *	if an element is marked deleted
 *		delete original
 *	else if element was written or new
 *		update or add
 */
STATIC void
native_merge_hash(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int bucket;
	sociable_element_t *h;
	int bucketlen = priv_structp->hash_bucket_count;
	sociable_element_t **buckets = priv_structp->hash_buckets;
	sociable_element_t *telemp;

	for (bucket = 0; bucket < bucketlen; bucket++) {
		h = buckets[bucket];
		while (h) {
			if (SOC_STM_DELETED(h)) {
				sociable_remove_hash_element(aTHX_ &structp->un.hash, h->hash_tuple.keyptr.elemp, h->hash_tuple.keylen, &structp->extent);
			}
			else if (!SOC_STM_WAS_READ(h)) {
			/*
			 *	either write to existing element, or create a new one
			 */
				telemp = sociable_hash_lookup(aTHX_ &structp->un.hash, h->hash_tuple.keyptr.elemp, h->hash_tuple.keylen, 1, &structp->extent);
				native_update_element(aTHX_ h, telemp);
			}
			h = h->header.next;
		}
	}
}

STATIC void
native_update_element(pTHX_ sociable_element_t *priv_elemp, sociable_element_t *elemp)
{
	sociable_element_t *refp;
	sociable_storage_mech_t *mech = (priv_elemp->header.level == SOC_PROCESS_PRIVATE_MECH)
		? NULL
		: sociable_mechs[priv_elemp->header.mechid];
	SOC_INCR_ELEM_SEQNO(elemp);

	SOCIABLE_TRACE_2("stm_update_element: updating elemp %p from private elem %p\n", elemp, priv_elemp);
	if (!SvOK(&priv_elemp->SOCHDR)) {
		stm_clear_element(elemp);
		return;
	}

    if (SvROK(&priv_elemp->SOCHDR)) {
/*
 * if its a ref, then get its referent and verify
 * its still sociable
 */
		SOCIABLE_TRACE_1("native_update_element: assigning a ref to elemp %p\n", elemp);
        refp = mech
        	? (*mech->addr_to_ptr)(&priv_elemp->un.ref.refelemp.un)
        	: priv_elemp->un.ref.refelemp.un.elemp;

		SOCIABLE_TRACE_1("native_update_element: ref is %p\n", refp);

		if (refp && (refp->SOCHDR.cycleno != priv_elemp->un.ref.cycleno)) {
/*
 *	referent has been recycled
 */
			SOCIABLE_TRACE("native_update_element: not a sociable ref!!!\n");
	        Perl_croak(aTHX_ "Invalid value for sociable scalar");
		}

		SOCIABLE_TRACE_1("native_update_element: storing for a ref for refp %p\n", refp);
/*
 * need to flag an element as a ref
 */
		memcpy(&elemp->un.ref.refelemp, &priv_elemp->un.ref.refelemp, sizeof(sociable_locator_t));
		SvROK_on(&elemp->SOCHDR);
/*
 * if the contents were blessed after
 * the ref was created, we need to copy the package
 */
		if (refp->header.pkg_name) {
	        if (mech) {
				(*mech->mem_free)(&elemp->header.pkg_name);
				(*mech->mem_strdup)(&elemp->header.pkg_name, refp->header.pkg_name.elemp);
			}
			else {
				crt_free(elemp->header.pkg_name.elemp);
				elemp->header.pkg_name = crt_strdup(refp->header.pkg_name);
			}
		}
    }
    else {
    	priv_elemp->SOCHDR.soc_flags = elemp->SOCHDR.soc_flags;
    	sociable_scalar_t *scalar = &elemp->un.scalar;
    	sociable_scalar_t *priv_scalar = &elemp->un.scalar;
    	char *ptr;
/*
 *	!!! order is important ! must test for unsigned before signed
 */
		if (SvUOK(&priv_elemp->SOCHDR)) {
/*
 *	need to check for an unsigned!
 */
			SOCIABLE_TRACE("native_update_element: save'ing UV SV\n");
			scalar->uv	= priv_scalar->uv;
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK|SVf_IVisUV;
		}
		else if (SvIOK(&priv_elemp->SOCHDR)) {
/*
 *	need to check for an unsigned!
 */
			SOCIABLE_TRACE("native_update_element: save'ing IV SV\n");
			scalar->iv	= priv_scalar->iv;
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK;
		}
		else if (SvNOK(&priv_elemp->SOCHDR)) {
			SOCIABLE_TRACE("native_update_element: save'ing NV SV\n");
			scalar->nv	= priv_scalar->nv;
			elemp->SOCHDR.sv_flags = SVt_NV|SVf_NOK;
		}
		else if (SvPOK(&priv_elemp->SOCHDR) || (SvTYPE(&priv_elemp->SOCHDR) <= SVt_PVLV)) {
			scalar->buflen = priv_scalar->buflen;
			SOCIABLE_TRACE_1("native_update_element: save'ing PV SV length %d\n", scalar->buflen);
			if (!scalar->bufsz) {
				scalar->bufsz = priv_scalar->bufsz;
				if (mech)
					(*mech->mem_alloc)(&scalar->bufptr, scalar->bufsz);
				else
					scalar->bufptr.elemp = crt_malloc(scalar->bufsz);
			}
			else if (scalar->bufsz < priv_scalar->buflen + 1) {
				SOCIABLE_TRACE_2("native_update_element: changing buffers: freeing buffer %p for elemp %p\n", scalar->bufptr.elemp, elemp);
				scalar->bufsz = priv_scalar->bufsz;
				if (mech) {
					(*mech->mem_free)(&scalar->bufptr);
					(*mech->mem_alloc)(&scalar->bufptr, scalar->bufsz);
				}
				else {
					crt_free(scalar->bufptr.elemp);
					CRT_Newz(scalar->bufptr.elemp, scalar->bufsz, char);
				}
			}
			ptr = mech ? (*mech->addr_to_ptr)(&scalar->bufptr) : scalar->bufptr.elemp;
			memcpy(ptr, priv_scalar->bufptr.elemp, scalar->buflen);
			elemp->SOCHDR.sv_flags = SVt_PV|SVf_POK;		/* !!! NEED UTF8ness here !!! */
		}
		else {
			SOCIABLE_TRACE_1("native_update_element: unknown SV type %d!!!\n", SvTYPE(&priv_elemp->SOCHDR));
		}
	}
/*************************************************
 *
 * don't know what to do with these as yet
 *
	    case SVt_PVIV:
			elemp->iv = SvIVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;

	    case SVt_PVNV:
			elemp->iv = SvIVX(sv);
			elemp->nv = SvNVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;
 *************************************************/
	SOCIABLE_TRACE("native_update_element: returning\n");
}

/*
 *	clear contents of a private element to undef state
 */
STATIC void
native_stm_clear_element(sociable_element_t *priv_elemp)
{
#ifdef SOCIABLE_TIE
/*
 *	invoke tie to clear ???
 */
#endif
	if (SvROK(&priv_elemp->header))
		memzero(&priv_elemp->un.ref, sizeof(sociable_ref_t));
	else {
		crt_free(elemp->un.scalar.bufptr.elemp);
		elemp->un.scalar.buflen = elemp->un.scalar.bufsz = 0;
		elemp->un.scalar.iv = 0;
		elemp->un.scalar.nv = 0.0;
	}
	elemp->header.sv_flags = 0;
}

/*
 *	acquire written elements of array for commit
 *	(assumes array spinlocked and acquireable)
 *		1) scan deleted list:
 *			if deleted element has changed or cannot be acquired
 *				restart
 *		2) scan the slot_map
 *			if has an original slot and was accessed
 *				if (1st access was read AND element has changed) OR
 * 					(1st access was write AND cannot acquire)
 *					restart
 *		3) scan deleted bitmap
 *			if bit set
 *				if cannot acquire associated element
 *					restart
 */
STATIC int
private_acquire_array_elems(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int i, j, k;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	int arylen = priv_structp->ary_head;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_element_t *deleted = priv_structp->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = priv_structp->ary_delete_map;
	dMY_CXT;
/*
 *	check for accessed deletes
 */
	while (deleted) {
		telemp = structp->ary_ptr[deleted->un.scalar.iv].elemp;
		owner = telemp->header.owner.elemp;
		if ((SOC_STM_WAS_READ(deleted) && (!NATIVE_VALID_STRUCT_ELEM(deleted, telemp))) ||
			(SOC_STM_WAS_WRITTEN(deleted) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
		deleted = deleted->NEXT_PTR;
	}
/*
 *	check for updates
 */
	for (i = 0; i < arylen; i++) {
		if ((slot_map[i] == SOC_STM_UNMAPPED) ||
			(ary[i].elemp == NULL) ||
			(!memcmp(&ary[i], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)))
			continue;
		telemp = structp->ary_ptr[slot_map[i]].elemp;
		owner = telemp->header.owner.elemp;
		if ((SOC_STM_WAS_READ(telemp) && (!NATIVE_VALID_STRUCT_ELEM(ary[i].elemp, telemp))) ||
			(SOC_STM_WAS_WRITTEN(telemp) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
	}
/*
 *	check for unaccessed deletes
 */
	arylen = ((structp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			telemp = structp->ary_ptr[(i << 3) + k].elemp;
			owner = telemp->header.owner.elemp;
			if ((delete_map[i] & j) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner)))
				return 0;
		}
	}
	return 1;
}

/*
 *	acquire written elements of array for commit
 *	(assumes array spinlocked and acquireable)
 *		1) scan deleted list:
 *			if deleted element has changed or cannot be acquired
 *				restart
 *		2) scan the slot_map
 *			if has an original slot and was accessed
 *				if (1st access was read AND element has changed) OR
 * 					(1st access was write AND cannot acquire)
 *					restart
 *		3) scan deleted bitmap
 *			if bit set
 *				if cannot acquire associated element
 *					restart
 */
STATIC int
shared_acquire_array_elems(pTHX_ sociable_structure_t *priv_structp, sociable_structure_t *structp)
{
	int i, j, k;
	sociable_addr_t *ary = priv_structp->ary_ptr;
	int arylen = priv_structp->ary_head;
	I32 *slot_map = priv_structp->ary_slot_map;
	sociable_element_t *deleted = priv_structp->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = priv_structp->ary_delete_map;
	sociable_storage_mech *mech = sociable_mechs[structp->header.mechid];
	dMY_CXT;
/*
 *	check for accessed deletes
 */
	while (deleted) {
		telemp = (*mech->addr_to_ptr)(&structp->ary_ptr[deleted->un.scalar.iv]);
		owner = (*mech->addr_to_ptr)(&telemp->header.owner);
		if ((SOC_STM_WAS_READ(deleted) && (!NATIVE_VALID_STRUCT_ELEM(deleted, telemp))) ||
			(SOC_STM_WAS_WRITTEN(deleted) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
		deleted = deleted->NEXT_PTR;
	}
/*
 *	check for updates
 */
	for (i = 0; i < arylen; i++) {
		if ((slot_map[i] == SOC_STM_UNMAPPED) ||
			(ary[i].elemp == NULL) ||
			(!memcmp(&ary[i], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)))
			continue;
		telemp = (*mech->addr_to_ptr)(&structp->ary_ptr[slot_map[i]]);
		owner = (*mech->addr_to_ptr)(&telemp->header.owner);
		if ((SOC_STM_WAS_READ(telemp) && (!NATIVE_VALID_STRUCT_ELEM(ary[i].elemp, telemp))) ||
			(SOC_STM_WAS_WRITTEN(telemp) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner))))
			return 0;
	}
/*
 *	check for unaccessed deletes
 */
	arylen = ((structp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			telemp = (*mech->addr_to_ptr)(&structp->ary_ptr[(i << 3) + k]);
			owner = (*mech->addr_to_ptr)(&telemp->header.owner);
			if ((delete_map[i] & j) && (!NATIVE_ELEM_CAN_ACQUIRE(telemp, owner)))
				return 0;
		}
	}
	return 1;
}

/*
 *	lookup value for the specified key; returns the element for the value
 */
sociable_element_t *
native_stm_hash_lookup(pTHX_ sociable_proxy_t *proxy, char *key, I32 len, I32 for_write, sociable_proxy_t *valproxy)
{
	sociable_structure_t *priv_structp =
		(sociable_structure_t *)(*proxy->mech->stm_save_referent)(aTHX_ proxy, for_write);
	sociable_hash_t *hash;
	sociable_locator_t *sociable = &proxy->sociable;
	UV hashval = 0;
	I32 bucket = 0;
	sociable_element_t *h = NULL;
	sociable_element_t *sh = NULL;
	sociable_element_t *telemp = NULL;

	SOCIABLE_TRACE_1("in stm_hash_lookup: key %s\n", key);
	memzero(valproxy, sizeof(sociable_proxy_t));
/*
 *	check for conflicts, or log if 1st access;
 *	but don't check/log for write until we know
 *	we're adding a new key
 */
	if (!priv_structp)
		return NULL;

	hash = &priv_structp->un.hash.elemp;
	PERL_HASH(hashval, key, len);
	bucket = hashval %  hash->bucket_count;
/*
 *	scan private version first
 */
	h = hash->buckets[bucket];
	while ((h != NULL) &&
		((h->hash_tuple.hashval != hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr, len)))
		h = h->header.next;

	SOC_SPIN_LOCK(proxy);
	sh = sociable->hash_buckets[bucket];
	while ((sh != NULL) &&
		((hashval != sh->hash_tuple.hashval) ||
		(len != sh->hash_tuple.keylen) ||
		memcmp(key, sh->hash_tuple.keyptr.elemp, len)))
		sh = sh->header.next;
/*
 *	if no sociable, release ASAP
 */
	if (!sh)
		SOC_SPIN_RELEASE(proxy);
/*
 *	if private not found
 *		if sociable not found
 *			if not for write
 *				return
 *			create empty element
 *		else
 *			create private clone of sociable
 *			mark private version w/ proper access (read or write)
 *		insert and return private version
 */
 	if (!h) {
 		if (!sh) {
			if (!for_write)
				return NULL;

			CRT_Newz(telemp, 1, sociable_element_t);
			telemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
		else {
/*
 *	create private clone
 *	implicitly its 1st access: try to find in the sociable version
 */
			if (detector->hash_elem_acquire &&
				(*detector->hash_elem_acquire)(aTHX_ sh, sociable, for_write))
				return NULL;

			CRT_Newz(telemp, 1, sociable_element_t);
			native_clone_element(telemp, sh);
			SOC_SPIN_RELEASE(proxy);
			telemp->header.soc_flags = for_write ?
				SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_WRITE :
				SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_READ;
		}
/*
 *	insert into hash
 */
		telemp->hash_tuple.hashval = hashval;
		telemp->hash_tuple.keylen = len;
		CRT_Dup(telemp->hash_tuple.keyptr.elemp, len + 1, char, key, len);
		telemp->header.next.elemp = hash->buckets[hashval].elemp;
		if (hash->buckets[hashval].elemp)
			((sociable_element_t *)hash->buckets[hashval].elemp)->PREV_PTR = telemp;
		hash->buckets[hashval].elemp = telemp;
		return telemp;
 	}
/*
 *	private exists:
 *	if marked deleted
 *		undelete it
 *		set as an undef
 *	if no sociable
 *		return private
 *
 *	check for conflicts
 */
	if (sh && detector->hash_elem_conflict &&
		(*detector->hash_elem_conflict)(aTHX_ h, sh, for_write)) {
		SOC_SPIN_RELEASE(sociable);
		return NULL;
	}
	SOC_SPIN_RELEASE(sociable);

	if (SOC_STM_DELETED(h)) {
		h->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_WRITE;
		stm_clear_element(h);
	}

	SOCIABLE_TRACE_1("stm_hash_lookup: returning %p\n", h);
	return h;
}

/******************************************************************
 ******************************************************************
 *
 *	STM ELEMENT UTILITY METHODS
 *
 ******************************************************************
 ******************************************************************/
/*
 *	create a complete private clone of a sociable element
 *	assumes caller holds spinlock on elemp (or the parent structure)
 */
STATIC void
stm_clone_element(sociable_element_t *proxy, sociable_element_t *sociable)
{
	(*proxy->mech->copy_header)(proxy, sociable);
	proxy->header.cycleno = sociable->header.cycleno;
	proxy->header.updcnthi = sociable->header.updcnthi;
	proxy->header.updcntlo = sociable->header.updcntlo;
	proxy->header.sv_flags = sociable->header.sv_flags;
	proxy->header.soc_flags = sociable->header.soc_flags;
	if (sociable->header.pkg_name)
		proxy->header.pkg_name = strdup(sociable->header.pkg_name);
	if (sociable->hash_tuple.keylen) {
		proxy->hash_tuple.keylen = sociable->hash_tuple.keylen;
		proxy->hash_tuple.hashval = sociable->hash_tuple.hashval;
		CRT_Dup(proxy->hash_tuple.keyptr, proxy->hash_tuple.keylen + 1, char, sociable->hash_tuple.keyptr, proxy->hash_tuple.keylen);
	}
#ifdef SOCIABLE_TIE
/*
 *	copy tie info in future
 */
#endif
	if (SvROK(&sociable->header)) {
		proxy->un.ref.cycleno = sociable->un.ref.cycleno;
		proxy->un.ref.refelemp = sociable->un.ref.refelemp;
	}
	else {
		proxy->un.scalar.bufsz = sociable->un.scalar.bufsz;
		proxy->un.scalar.buflen = sociable->un.scalar.buflen;
		if (proxy->un.scalar.bufsz) {
			CRT_Dup(proxy->un.scalar.bufptr, proxy->un.scalar.bufsz, char, sociable->un.scalar.bufptr, proxy->un.scalar.buflen);
		}
		else
			proxy->un.scalar.bufptr = NULL;
		proxy->un.scalar.iv = sociable->un.scalar.iv;
		proxy->un.scalar.nv = sociable->un.scalar.nv;
	}
}

/*
 *	clear contents of a private element to undef state
 */
STATIC void
stm_clear_element(sociable_element_t *priv_elemp)
{
#ifdef SOCIABLE_TIE
/*
 *	invoke tie to clear ???
 */
#endif
	if (!SvROK(&priv_elemp->header) && priv_elemp->un.scalar.bufsz)
		crt_free(priv_elemp->un.scalar.bufptr.elemp);
	memzero(&priv_elemp->un.scalar, sizeof(sociable_scalar_t));
	priv_elemp->SOCHDR.sv_flags = 0;
}

/*
 *	reset all logged private elements
 *	(called during restart/rollback)
 */
STATIC void
stm_reset_elements(pTHX_ sociable_stm_log_t *wal)
{
	int i;

	for (i = 0; i < wal->WAL_count; i++) {
		stm_free_element(aTHX_ wal->WAL_proxies[i]->elemp, NULL);
		crt_free(wal->WAL_proxies[i]);
	}
	wal->WAL_count = 0;
	wal->WAL = crt_free(wal->WAL);
	wal->WAL_proxies = crt_free(wal->WAL_proxies);
}
/*
 *	free a private element (including any contents)
 *	if proxy provided, then save element in deleted list
 */
STATIC void *
stm_free_element(pTHX_ sociable_element_t *elemp, sociable_structure_t *structp)
{
	if (SvTYPE(&elemp->SOCHDR) == SVt_PVAV)
		return stm_free_array(aTHX_ (sociable_structure_t *)elemp);

	if (SvTYPE(&elemp->SOCHDR) == SVt_PVHV)
		return stm_free_hash(aTHX_ (sociable_structure_t *)elemp);

	crt_free(elemp->header.pkg_name.elemp);
	crt_free(elemp->hash_tuple.keyptr.elemp);

	if (SvROK(&elemp->SOCHDR))
		memzero(&elemp->un.ref, sizeof(sociable_ref_t));
	else if (elemp->un.scalar.bufsz) {
		crt_free(elemp->un.scalar.bufptr.elemp);
		elemp->un.scalar.bufsz = elemp->un.scalar.buflen = 0;
	}
	if (structp && (elemp->un.scalar.iv != SOC_STM_UNMAPPED) && SOC_STM_WAS_READ(elemp)) {
		elemp->NEXT_PTR = structp->ary_deletes;
		structp->ary_deletes = elemp;
		SOC_DELETE_ARRAY_ELEM(structp, elemp->un.scalar.iv);
		return NULL;
	}
	return crt_free(elemp);
}

STATIC void
stm_update_element(pTHX_ sociable_element_t *priv_elemp, sociable_element_t *elemp)
{
	sociable_element_t *refp;
	SOC_INCR_ELEM_SEQNO(elemp);

	SOCIABLE_TRACE_2("stm_update_element: updating elemp %p from proxy %p\n", elemp, priv_elemp);
	if (!SvOK(&priv_elemp->SOCHDR)) {
		stm_clear_element(elemp);
		return;
	}

    if (SvROK(&priv_elemp->SOCHDR)) {
/*
 * if its a ref, then get its referent and verify
 * its still sociable
 */
		SOCIABLE_TRACE_1("stm_update_element: assigning a ref to elemp %p\n", elemp);
        refp = priv_elemp->un.ref.refelemp;

		SOCIABLE_TRACE_1("stm_update_element: ref is %p\n", refp);

		if (refp && (refp->header.cycleno != priv_elemp->un.ref.cycleno)) {
/*
 *	referent has been recycled
 */
			SOCIABLE_TRACE("stm_update_element: not a sociable ref!!!\n");
	        Perl_croak(aTHX_ "Invalid value for sociable scalar");
		}

		SOCIABLE_TRACE_1("stm_update_element: storing for a ref for refp %p\n", refp);
/*
 * need to flag an element as a ref
 */
		elemp->un.ref.refelemp = refp;
		SvROK_on(&elemp->SOCHDR);
/*
 * if the contents were blessed after
 * the ref was created, we need to copy the package
 */
		if (refp->header.pkg_name) {
			crt_free(elemp->header.pkg_name);
			elemp->header.pkg_name = crt_strdup(refp->header.pkg_name);
		}
    }
    else {
    	priv_elemp->SOCHDR.soc_flags = elemp->SOCHDR.soc_flags;
/*
 *	!!! order is important ! must test for unsigned before signed
 */
		if (SvUOK(&priv_elemp->SOCHDR)) {
/*
 *	need to check for an unsigned!
 */
			SOCIABLE_TRACE("stm_update_element: save'ing UV SV\n");
			elemp->un.scalar.uv	= priv_elemp->un.scalar.uv;
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK|SVf_IVisUV;
		}
		else if (SvIOK(&priv_elemp->SOCHDR)) {
/*
 *	need to check for an unsigned!
 */
			SOCIABLE_TRACE("stm_update_element: save'ing IV SV\n");
			elemp->un.scalar.iv	= priv_elemp->un.scalar.iv;
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK;
		}
		else if (SvNOK(&priv_elemp->SOCHDR)) {
			SOCIABLE_TRACE("stm_update_element: save'ing NV SV\n");
			elemp->un.scalar.nv	= priv_elemp->un.scalar.nv;
			elemp->SOCHDR.sv_flags = SVt_NV|SVf_NOK;
		}
		else if (SvPOK(&priv_elemp->SOCHDR) || (SvTYPE(&priv_elemp->SOCHDR) <= SVt_PVLV)) {
			elemp->un.scalar.buflen = priv_elemp->un.scalar.buflen;
			SOCIABLE_TRACE_1("stm_update_element: save'ing PV SV length %d\n", elemp->un.scalar.buflen);
			if (!elemp->un.scalar.bufsz) {
				elemp->un.scalar.bufsz = priv_elemp->un.scalar.bufsz;
				CRT_Newz(elemp->un.scalar.bufptr, elemp->un.scalar.bufsz, char);
			}
			else if (elemp->un.scalar.bufsz < priv_elemp->un.scalar.buflen + 1) {
				SOCIABLE_TRACE_2("stm_update_element: changing buffers: freeing buffer %p for elemp %p\n", elemp->un.scalar.bufptr, elemp);
				crt_free(elemp->un.scalar.bufptr);
				elemp->un.scalar.bufsz = priv_elemp->un.scalar.bufsz;
				CRT_Newz(elemp->un.scalar.bufptr, elemp->un.scalar.bufsz, char);
			}
			memcpy(elemp->un.scalar.bufptr, priv_elemp->un.scalar.bufptr, elemp->un.scalar.buflen);
			elemp->SOCHDR.sv_flags = SVt_PV|SVf_POK;		/* !!! NEED UTF8ness here !!! */
		}
		else {
			SOCIABLE_TRACE_1("stm_update_element: unknown SV type %d!!!\n", SvTYPE(&priv_elemp->SOCHDR));
		}
	}
/*************************************************
 *
 * don't know what to do with these as yet
 *
	    case SVt_PVIV:
			elemp->iv = SvIVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;

	    case SVt_PVNV:
			elemp->iv = SvIVX(sv);
			elemp->nv = SvNVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;
 *************************************************/
	SOCIABLE_TRACE("stm_update_element: returning\n");
}

STATIC void
stm_elem_from_sv(pTHX_ sociable_element_t *elemp, SV *sv)
{
	char * pv;
	SV *obj = NULL;
	sociable_element_t *refelemp = NULL;

	if (!SvOK(sv))	/* already undef */
		return;

    if (SvROK(sv)) {
/*
 * if its a ref, then get its referent and verify
 * its sociable
 */
        obj = SvRV(sv);
        refelemp = sociable_find(aTHX_ obj);
        if (refelemp == NULL)
	        Perl_croak(aTHX_ "Invalid value for sociable scalar");
/*
 * need to flag an element as a ref
 */
		elemp->un.ref.refelemp = refelemp;
		elemp->un.ref.cycleno = refelemp->SOCHDR.cycleno;
		SvROK_on(&elemp->SOCHDR);
/*
 * if the contents were blessed after
 * the ref was created, we need to copy the package
 *	...BUT HOW CAN THIS EVER HAPPEN ???? We only permit
 *
 */
		if (SvOBJECT(obj)) {	/* is it blessed ? */
			crt_free(elemp->header.pkg_name);
			elemp->header.pkg_name = crt_strdup(HvNAME(SvSTASH(obj)));
		}
    }
    else {
    	SOC_ENCODE_ELEM(sv, elemp);
/*
 *	!!! order is important here ! unsigned before signed
 */
		if (SvUOK(sv)) {
			elemp->un.scalar.uv	= SvUVX(sv);
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK|SVf_IVisUV;
		}
		else if (SvIOK(sv)) {
			elemp->un.scalar.iv	= SvIVX(sv);
			elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK;
		}
		else if (SvNOK(sv)) {
			elemp->un.scalar.nv	= SvNVX(sv);
			elemp->SOCHDR.sv_flags = SVt_NV|SVf_NOK;
		}
		else if (SvPOK(sv) || (SvTYPE(sv) <= SVt_PVLV)) {
			pv = SvPV_nomg(sv, elemp->un.scalar.buflen);
			if (!elemp->un.scalar.bufsz) {
				elemp->un.scalar.bufsz = SvLEN(sv);
				CRT_Newz( elemp->un.scalar.bufptr, elemp->un.scalar.bufsz, char);
			}
			else if (elemp->un.scalar.bufsz && (elemp->un.scalar.bufsz < SvLEN(sv))) {
				crt_free(elemp->un.scalar.bufptr);
				elemp->un.scalar.bufsz = SvLEN(sv);
				CRT_Newz( elemp->un.scalar.bufptr, elemp->un.scalar.bufsz, char);
			}
			memcpy(elemp->un.scalar.bufptr, pv, elemp->un.scalar.buflen);
			elemp->un.scalar.buflen = SvCUR(sv);
			elemp->SOCHDR.sv_flags = SVt_PV|SVf_POK;	/* !!! NEED UTF8ness HERE !!! */
		}
		else {
			SOCIABLE_TRACE_1("stm_elem_from_sv: unknown SV type %d!!!\n", SvTYPE(sv));
		}
	}
}

STATIC SV *
stm_sv_from_elem(pTHX_ sociable_element_t *elemp)
{
	SV *sv = newSV(0);
	SV *obj;
	SvUPGRADE(sv, SvTYPE(&elemp->SOCHDR));

	if (SvROK(&elemp->SOCHDR)) {
        obj = sociable_new_private(aTHX_ elemp->un.ref.refelemp);
        SvRV_set(sv, obj);
/*
 * If it was blessed, remove the blessing
 * NOTE: shouldn't we only do this if the current package
 * is not same as new package ???
 */
	    if (SvOBJECT(obj)) {
	        SvREFCNT_dec(SvSTASH(obj));
	        SvOBJECT_off(obj);
	    }
/*
 * if the referent is blessed, bless the private instance
 */
	    if (elemp->header.pkg_name) {
	        /* Add any new old blessing */
	        HV* stash = gv_stashpv(elemp->header.pkg_name, TRUE);
	        SvOBJECT_on(obj);
	        SvSTASH_set(obj, (HV*)SvREFCNT_inc(stash));
	    }
	    return sv;
	}

	SOC_ENCODE_SV(sv, elemp);
    switch (SvTYPE(&elemp->SOCHDR)) {
	    case SVt_NULL:
			sv_setsv(sv, &PL_sv_undef);
			break;

	    case SVt_IV:
    		if (SvIsUV(&elemp->SOCHDR)) {
    			sv_setuv(sv, elemp->un.scalar.uv);
    		}
    		else {
    			sv_setiv(sv, elemp->un.scalar.iv);
    		}
    		break;

	    case SVt_NV:
			sv_setnv(sv, elemp->un.scalar.nv);
			break;

	    case SVt_PV:
			sv_setpvn(sv, elemp->un.scalar.bufptr.elemp, elemp->un.scalar.buflen);
			break;

		default:
			Perl_croak(aTHX_ "Unrecognized element type\n");
    }
    return sv;
}

/*
 *	merge a private copy into the sociable version
 *	(assumes element is spinlocked and acquireable)
 */
void
stm_merge_element(pTHX_ sociable_addr_t *elemp, sociable_element_t *priv_elemp)
{
	SOCIABLE_TRACE_2("stm_merge_element: merging proxy %p into elemp %p\n", priv_elemp, elemp);

	if (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVAV)
		stm_merge_array(aTHX_ (sociable_structure_t *)priv_elemp);
	else if (SvTYPE(&priv_elemp->SOCHDR) == SVt_PVHV)
		stm_merge_hash(aTHX_ (sociable_structure_t *)priv_elemp);
	else
		stm_update_element(aTHX_ priv_elemp, elemp);

	SOCIABLE_TRACE("stm_merge_element: returning\n");
}

/******************************************************************
 ******************************************************************
 *
 *	STM NON-NATIVE EAGER ACQUIRE/CONFLICT DETECTOR METHODS
 *
 ******************************************************************
 ******************************************************************/
STATIC int
stm_eager_acquire(pTHX_ sociable_proxy_t *proxy, int for_write)
{
	dMY_CXT;

	SOC_BUMP_KARMA;
	SOC_SPIN_LOCK(proxy);
/*
 *	make ourselves owner if possible
 */
	if (SOC_STM_CAN_ACQUIRE(proxy)) {
		SOC_MAKE_OWNER(proxy);
		return 1;
	}
	if (for_write)
/*
 *	restart if can't acquire for write
 */
		SOC_SPIN_RELEASE(proxy);

	return (!for_write);
}

/*
 *	eager conflict detector:
 *	if prior read in conflict
 *		return "conflict detected"
 *	if 1st access
 *		bump our karma
 *	if for write and can't (re)acquire
 *		return "conflict detected"
 *	else return "no conflict"
 */
STATIC int
stm_eager_has_conflict(pTHX_ sociable_stm_proxy_t *proxy, int for_write)
{
	sociable_locator_t *elemp = &proxy->sociable;
	dMY_CXT;

	if (SOC_STM_WAS_READ(proxy) && (!SOC_STM_VALID_READ(proxy)))
		return 1;

	if ((*proxy->mech->is_owner)(elemp, MY_CXT.stm_ctxts[elemp->mechid]))
		return 0;

	SOC_SPIN_LOCK(proxy);
	if (!SOC_STM_CAN_ACQUIRE(proxy)) {
		SOC_SPIN_RELEASE(proxy);
		return (!for_write);
	}
	SOC_MAKE_OWNER(proxy);
	SOC_SPIN_RELEASE(proxy);
	return 0;
}

/*
 *	eager conflict detector for array elements
 */
STATIC int
stm_eager_ary_elem_conflict(pTHX_ sociable_proxy_t *proxy, int key, int tkey, int for_write)
{
	sociable_addr_t *elemp;
	sociable_element_t *priv_elemp = NULL;
	sociable_addr_t *sociable = (sociable_structure_t *)proxy->sociable;
	dMY_CXT;
/*
 *	if unaccessed element, create a new one
 */
	if (proxy->ary_ptr[key] == SOC_STM_UNACCESSED) {
		SOC_BUMP_KARMA;
		CRT_Newz(proxy->ary_ptr[key], 1, sociable_element_t);
		if (for_write & SOC_STM_FOR_WRITE)
			proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
	}
	priv_elemp = proxy->ary_ptr[key];
 	SOC_SPIN_LOCK(sociable);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!SOC_STM_VALID_READ(proxy)) {
	 	SOC_SPIN_RELEASE(sociable);
 		return 1;
 	}
 	elemp = sociable->ary_ptr[tkey];
 	assert(elemp);	/* damn well better exist ! */

	if (SOC_STM_WAS_READ(priv_elemp) && (!SOC_STM_VALID_ELEM(priv_elemp, elemp))) {
/*
 *	element was read and has changed: conflict
 */
	 	SOC_SPIN_RELEASE(sociable);
		return 1;
	}
/*
 *	if unaccessed, clone element before trying to acquire,
 *	since it may be for a read and will return OK on failure;
 *	mark as read 1st if for read or delete operation
 */
	if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
		(*proxy->mech->stm_clone_element)(priv_elemp, elemp);
		priv_elemp->header.soc_flags =
			(for_write == SOC_STM_FOR_WRITE) ? SOC_INUSE | SOC_STM_WRITE | SOC_STM_PRIVATE :
			(for_write == SOC_STM_FOR_DELETE) ? SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE :
			SOC_INUSE | SOC_STM_READ | SOC_STM_PRIVATE;
	}
/*
 *	for now, use structure lock to protect elements;
 *	in future, use hierarchical lock protocol
 */
	if (!SOC_STM_CAN_ACQUIRE(elemp)) {
		SOC_SPIN_RELEASE(sociable);
		return for_write;
	}
	SOC_MAKE_OWNER(elemp);
	SOC_SPIN_RELEASE(sociable);
	return 0;
}

/*
 *	eager conflict detector for array splice
 *	NOTE: ignore lazy flag here, otherwise we may access
 *	invalid elements
 */
STATIC int
stm_eager_splice_conflict(pTHX_ sociable_structure_t *proxy, int key, int len)
{
	sociable_element_t *elemp;
	sociable_element_t **ary = proxy->ary_ptr;
	sociable_element_t *priv_elemp = NULL;
	I32 *slot_map = proxy->ary_slot_map;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->header.sociable;
	int i;
	dMY_CXT;
/*
 *	for each unaccessed element, create a new empty element
 */
 	for (i = key; i < key + len; i++) {
		if (ary[i] == SOC_STM_UNACCESSED) {
			SOC_BUMP_KARMA;
			CRT_Newz(ary[key], 1, sociable_element_t);
			ary[i]->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
	}
	proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
 	SOC_SPIN_LOCK(sociable);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!SOC_STM_VALID_READ(proxy)) {
	 	SOC_SPIN_RELEASE(sociable);
 		return 1;
 	}
/*
 *	scan for conflicts with the sociables
 */
 	for (i = key; i < key + len; i++) {
 		if (slot_map[i] == SOC_STM_UNMAPPED)
 			continue;
 		elemp = sociable->ary_ptr[slot_map[i]];
 		assert(elemp);	/* damn well better exist ! */
 		priv_elemp = ary[i];

		if ((SOC_STM_WAS_READ(priv_elemp) && (!SOC_STM_VALID_ELEM(priv_elemp, elemp))) ||
			(!SOC_STM_CAN_ACQUIRE(elemp))) {
/*
 *	element was read and has changed, OR cannot be acquired: conflict
 */
		 	SOC_SPIN_RELEASE(sociable);
			return 1;
		}
		elemp->header.owner = MY_CXT.stm_ctxt;
/*
 *	if unaccessed, clone the element
 *	mark as read 1st
 */
		if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
			stm_clone_element(priv_elemp, elemp);
			priv_elemp->header.soc_flags = SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE;
		}
	}
	SOC_SPIN_RELEASE(sociable);
	return 0;
}

STATIC int
stm_eager_hash_elem_acquire(pTHX_ sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	dMY_CXT;
	SOC_BUMP_KARMA;
	if (SOC_STM_CAN_ACQUIRE(sh)) {
		sh->header.owner = MY_CXT.stm_ctxt;
		return 0;
	}
	if (for_write)
		SOC_SPIN_RELEASE(sociable);
	return for_write;
}

STATIC int
stm_eager_hash_elem_conflict(pTHX_ sociable_element_t *h, sociable_element_t *sh, int for_write)
{
	dMY_CXT;
	if (SOC_STM_WAS_READ(h) && (!SOC_STM_VALID_READ(h)))
		return 1;

	if (SOC_STM_CAN_ACQUIRE(sh)) {
		sociable->header.owner = MY_CXT.stm_ctxt;
		return 0;
	}

	return (for_write);
}

sociable_stm_detector_t stm_eager = {
	&stm_eager_acquire,
	&stm_eager_has_conflict,
	&stm_eager_ary_elem_conflict,
	&stm_eager_splice_conflict,
	&stm_eager_hash_elem_acquire,
	&stm_eager_hash_elem_conflict
};

/******************************************************************
 ******************************************************************
 *
 *	STM LAZY ACQUIRE/CONFLICT DETECTOR METHODS
 *
 ******************************************************************
 ******************************************************************/
STATIC int
stm_lazy_acquire(pTHX_ sociable_element_t *elemp, int for_write)
{
	dMY_CXT;

	SOC_BUMP_KARMA;
	SOC_SPIN_LOCK(elemp);
	return 1;
}
/*
 *	lazy conflict detector:
 *	always return "no conflict"
 */
STATIC int
stm_lazy_has_conflict(pTHX_ sociable_stm_element_t *proxy, int for_write)
{
	return 0;
}

/*
 *	lazy conflict detector for array elements
 */
STATIC int
stm_lazy_ary_elem_conflict(pTHX_ sociable_structure_t *proxy, int key, int tkey, int for_write)
{
	sociable_element_t *elemp;
	sociable_element_t *priv_elemp = NULL;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->header.sociable;
	dMY_CXT;
/*
 *	if unaccessed element, create a new one
 */
	if (proxy->ary_ptr[key] == SOC_STM_UNACCESSED) {
		SOC_BUMP_KARMA;
		CRT_Newz(proxy->ary_ptr[key], 1, sociable_element_t);
		if (for_write & SOC_STM_FOR_WRITE)
			proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
	}
	priv_elemp = proxy->ary_ptr[key];
 	SOC_SPIN_LOCK(sociable);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!SOC_STM_VALID_READ(proxy)) {
	 	SOC_SPIN_RELEASE(sociable);
 		return 1;
 	}
 	elemp = sociable->ary_ptr[tkey];
 	assert(elemp);	/* damn well better exist ! */
/*
 *	if unaccessed, copy metadata before trying to acquire,
 *	since it may be for a read and will return OK on failure;
 *	mark as read 1st if for read or delete operation
 */
	if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
		stm_clone_element(priv_elemp, elemp);
		priv_elemp->header.soc_flags =
			(for_write == SOC_STM_FOR_WRITE) ? SOC_INUSE | SOC_STM_WRITE | SOC_STM_PRIVATE :
			(for_write == SOC_STM_FOR_DELETE) ? SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE :
			SOC_INUSE | SOC_STM_READ | SOC_STM_PRIVATE;
	}
	SOC_SPIN_RELEASE(sociable);
	return 0;
}

/*
 *	lazy conflict detector for array splice
 */
STATIC int
stm_lazy_splice_conflict(pTHX_ sociable_structure_t *proxy, int key, int len)
{
	sociable_element_t *elemp;
	sociable_element_t **ary = proxy->ary_ptr;
	sociable_element_t *priv_elemp = NULL;
	I32 *slot_map = proxy->ary_slot_map;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->header.sociable;
	int i;
	dMY_CXT;
/*
 *	for each unaccessed element, create a new empty element
 */
 	for (i = key; i < key + len; i++) {
		if (ary[i] == SOC_STM_UNACCESSED) {
			SOC_BUMP_KARMA;
			CRT_Newz(ary[key], 1, sociable_element_t);
			ary[i]->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
	}
	proxy->header.soc_flags |= SOC_STM_ELEM_WRITE;
 	SOC_SPIN_LOCK(sociable);
/*
 *	validate the container array is still valid
 *	(which implies the sociable's mapped index is also still valid)
 *	NOTE: required for both eager and lazy
 */
 	if (!SOC_STM_VALID_READ(proxy)) {
	 	SOC_SPIN_RELEASE(sociable);
 		return 1;
 	}
/*
 *	clone any unaccessed elements
 */
 	for (i = key; i < key + len; i++) {
 		if (slot_map[i] == SOC_STM_UNMAPPED)
 			continue;
 		elemp = sociable->ary_ptr[slot_map[i]];
 		assert(elemp);	/* damn well better exist ! */
 		priv_elemp = ary[i];
/*
 *	if unaccessed, clone the element
 *	mark as read 1st
 */
		if (!SOC_STM_WAS_ACCESSED(priv_elemp)) {
			stm_clone_element(priv_elemp, elemp);
			priv_elemp->header.soc_flags = SOC_INUSE | SOC_STM_READ | SOC_STM_WRITE | SOC_STM_PRIVATE;
		}
	}
	SOC_SPIN_RELEASE(sociable);
	return 0;
}

STATIC int
stm_lazy_hash_elem_acquire(pTHX_ sociable_element_t *sh, sociable_structure_t *sociable, int for_write)
{
	dMY_CXT;
	SOC_BUMP_KARMA;
	return 0;
}

STATIC int
stm_lazy_hash_elem_conflict(pTHX_ sociable_element_t *h, sociable_element_t *sh, int for_write)
{
	return 0;
}

sociable_stm_detector_t stm_lazy = {
	&stm_lazy_acquire,
	NULL, /* &stm_lazy_has_conflict, */
	&stm_lazy_ary_elem_conflict,
	&stm_lazy_splice_conflict,
	&stm_lazy_hash_elem_acquire,
	NULL /* &stm_lazy_hash_elem_conflict */
};

/******************************************************************
 ******************************************************************
 *
 *	STM WAL CLEANUP METHODS
 *
 ******************************************************************
 ******************************************************************/

void
stm_collect_WALs(pTHX)
{
	int i, j;
	int size;
	sociable_stm_bucket_t *bucket;
	int mech_idx[SOCIABLE_MECHS];
	dMY_CXT;
/*
 *	each mechanism keeps count of variables logged
 *	alloc array of element ptrs for each mechanism in xaction
 *	traverse map buckets, mapping the elements to the logs for
 *		their mechanism
 */
 	for (i = 0; i < SOCIABLE_MECHS; i++) {
 		mech_idx[i] = 0;
 		if (!MY_CXT.stm_ctxts[i].WAL_count)
 			continue;
		CRT_Newz(MY_CXT.stm_ctxts[i].stm_WAL, size, sociable_element_t *);
		CRT_Newz(MY_CXT.stm_ctxts[i].stm_WAL_proxies, size, sociable_element_t *);
 	}
	for (i = 0; i < SOCIABLE_BUCKETS; i++) {
		bucket = &MY_CXT.stm_referents[i];
		for (j = 0; j < bucket->count; size++, j++) {
			elemp = bucket->bucket[j++];
			size = mech_idx[elemp->header.mech_index]++;
			MY_CXT.stm_ctxts[elemp->header.mech_index].stm_WAL[size] = elemp->header.sociable;
			MY_CXT.stm_ctxts[elemp->header.mech_index].stm_WAL_proxies[size] = elemp;
		}
	}
}

STATIC void
stm_clear_log(pTHX_ sociable_storage_mech_t *mech, sociable_stm_log_t *wal)
{
	int i;
	sociable_proxy_t *proxy;
	dMY_CXT;

/*
 *	for each spinlocked element
 *		if we own it
 *			release it
 *	NOTE: assumes spinocked field is only set by COMMIT processing,
 *	which always starts from the beginning of the WAL arrays
 */
	for (i = 0; i < wal->spinlocked; i++) {
		proxy = wal->WAL_proxies[i];
		if (SOC_AM_OWNER(proxy))
			SOC_CLEAR_OWNER(proxy);
		SOC_SPIN_RELEASE(proxy);
	}
	wal->spinlocked = 0;
/*
 *	for each unlocked element
 *		if we might own it
 *			spinlock it
 *			if still owned
 *				release ownership
 *			release spinlock
 *
 */
	for (; i < wal->WAL_count; i++) {
		proxy = wal->WAL_proxies[i];
/*
 *	a quick bit equivalence test outside spinlock can
 *	move us along faster
 */
		if (SOC_AM_OWNER(proxy)) {
			SOC_SPIN_LOCK(proxy);
			if (SOC_AM_OWNER(proxy))
				SOC_CLEAR_OWNER(proxy);
			SOC_SPIN_RELEASE(elemp);
		}
	}
/*
 *	discard private copies and clear the WAL
 */
	stm_reset_elements(aTHX_ wal);
}

void
stm_clear_all_logs(pTHX_ int reset_karma)
{
	int i,j;
	sociable_stm_log_t *wal;
	dMY_CXT;
	for (i = sociable_next_mech - 1, j = ordered_mechs[i]; i >= 0; j = ordered_mechs[--i]) {
		wal = MY_CXT.stm_ctxts[j];
		if (!(wal && wal->WAL_count))
			continue;
		if (sociable_mechs[j]->level != SOC_FOREIGN_MECH)
			native_clear_log(aTHX_ wal);
		else
			stm_clear_log(aTHX_ sociable_mechs[j], wal);
		if (reset_karma) {
			(*sociable_mechs[j]->stm_free_ctxt)(&wal->ctxt);
			memzero(&wal->ctxt, sizeof(sociable_addr_t));
		}
	}
	for (i = 0; i < SOCIABLE_BUCKETS; crt_free(MY_CXT.stm_referents[i++].bucket));
	memzero(&MY_CXT.stm_referents, sizeof(sociable_stm_bucket_t) * SOCIABLE_BUCKETS);
	if (reset_karma)
		MY_CXT.stm_karma = 0;
}

/******************************************************************
 ******************************************************************
 *
 *	STM REFERENT MAP METHODS
 *
 ******************************************************************
 ******************************************************************/
/*
 *	hash into WAL element list
 */
/*
 *	lookup in WAL hash;
 *	if found, conflict detect before returning the private version
 *  else attempt to acquire and create private version
 */
STATIC sociable_structure_t *
stm_lookup_referent(pTHX_ sociable_proxy_t *proxy)
{
	dMY_CXT;
/*
 *	simple hash here: divide by 32, the mod 23. May need to verify the skew.
 */
	sociable_stm_bucket_t *bucket = &MY_CXT.stm_referents[proxy->stm_hash];
	int i;

	for (i = 0; (i < bucket->count) &&
		memcmp(&bucket->bucket[i].elemloc, &proxy->sociable, sizeof(sociable_locator_t)); i++);
	return ((i < bucket->count) ? bucket->bucket[i].priv.structp : NULL);
}

STATIC sociable_stm_proxy_t *
stm_lookup_stm_proxy(pTHX_ sociable_proxy_t *proxy)
{
	dMY_CXT;
/*
 *	simple hash here: divide by 32, the mod 23. May need to verify the skew.
 */
	sociable_stm_bucket_t *bucket = &MY_CXT.stm_referents[proxy->stm_hash];
	int i;

	for (i = 0; (i < bucket->count) &&
		memcmp(&bucket->bucket[i].elemloc, &proxy->sociable, sizeof(sociable_locator_t)); i++);
	return ((i < bucket->count) ? &bucket->bucket[i] : NULL);
}

/*
 *	lookup in WAL hash;
 *	if found, conflict detect before returning the private version
 *  else attempt to acquire and create private version
 */
sociable_element_t *
stm_save_referent(pTHX_ sociable_proxy_t *proxy, int for_write)
{
	dMY_CXT;
 	sociable_locator_t *elemloc = &proxy->sociable;
	sociable_stm_bucket_t *bucket = &MY_CXT.stm_referents[proxy->stm_hash];
	sociable_proxy_t **elemp = bucket->bucket;
	sociable_structure_t *priv_avhv;
	sociable_element_t *priv_elemp;
	sociable_stm_detector_t *detector = proxy->mech->detector;
	SV *errsv;
	sociable_proxy_t *stmproxy;
	int i;
/*
 *	verify the referent is still sociable
 */
 	if (SOC_NOT_SOCIABLE(proxy)) {
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, "Variable is not sociable.");
		return NULL;
 	}
/*
 *	make sure we've got a context for this mechanism
 */
	if ((MY_CXT.stm_ctxts[elemloc->mechid].ctxt.segmentid == 0) &&
		MY_CXT.stm_ctxts[elemloc->mechid].ctxt.offset == 0) &&
		(!(*proxy->mech->stm_alloc_ctxt)(&MY_CXT.stm_ctxts[mechid].ctxt)))
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, "Cannot allocate transaction context.");
		return NULL;
	}
	for (i = 0; (i < bucket->count) &&
		memcmp(&bucket->bucket[i]->sociable, elemloc, sizeof(sociable_locator_t)); i++);
	if (i < bucket->count) {
/*
 *	already logged, so check for conflicts
 */
		if (detector->has_conflict &&
			(*detector->has_conflict)(aTHX_ &bucket->bucket[i], for_write)) {
			errsv = get_sv("@", TRUE);
			sv_setpv(errsv, SOC_STM_RESTART_MSG);
			return NULL;
		}

		if (for_write)
			bucket->bucket[i]->header.soc_flags |= SOC_STM_WRITE;

		return bucket->bucket[i];
	}
/*
 *	never logged before, so attempt to acquire and add it
 */
	if (!bucket->avail) {
		CRT_Extend(bucket->bucket, bucket->count + bucket->avail, sociable_proxy_t *, SOCIABLE_ARRAY_SPARE);
		bucket->avail += SOCIABLE_ARRAY_SPARE;
	}
	SOC_BUMP_KARMA;
/*
 *	attempt to acquire it, then create clone of it
 */
	switch (SvTYPE(proxy)) {
		case SVt_PVAV:
			stmproxy = stm_new_array(int sv_flags, 0);
			break;

		case SVt_PVHV:
			stmproxy = stm_new_hash(int sv_flags, 0);
			break;

		default:
			stmproxy = stm_new_scalar(SV *sv, 0);
			break;
	}
	bucket->bucket[bucket->count] = stmproxy;
	SOC_SPIN_LOCK(sociable);
	if (SOC_STM_CAN_ACQUIRE(sociable)) {
		memcpy(&sociable->header.owner, MY_CXT.stm_ctxts[proxy->mechid], sizeof(sociable_addr_t));
	}
	else if (for_write) {
		SOC_SPIN_RELEASE(sociable);
		crt_free(stmproxy);
		errsv = get_sv("@", TRUE);
		sv_setpv(errsv, SOC_STM_RESTART_MSG);
		return NULL;
	}
	switch (SvTYPE(proxy)) {
		case SVt_PVAV:
			stm_clone_array(stmproxy, proxy); /* releases spinlock */
			break;

		case SVt_PVHV:
			stm_clone_hash(stmproxy, proxy); /* releases spinlock */
			break;

		default:
			stm_clone_element(stmproxy, proxy); /* does NOT release spinlock */
			SOC_SPIN_RELEASE(sociable);
			stmproxy->sociable = sociable;
	}
	bucket->avail--;
	bucket->count++;
	stmproxy->header.soc_flags =
		SOC_INUSE | SOC_STM_PRIVATE | (for_write ? SOC_STM_WRITE : SOC_STM_READ);
	return stmproxy;
}

STATIC int
stm_set_proxy(sociable_proxy_t *proxy, sociable_element_t *priv_elemp)
{
	memcpy(&proxy->header, elemp->SOCHDR, sizeof(sociable_header_t));
	proxy->sociable.mechid = 0;
	proxy->sociable.level = SOC_PROCESS_PRIVATE_MECH;
	proxy->sociable.un.elemp = elemp;
	return 1;
}

/******************************************************************
 ******************************************************************
 *
 *	STM SCALAR ACCESS METHODS:
 *		just use regular sociable scalar methods, but
 *		grab (and maybe log/acquire) the private version first
 *
 ******************************************************************
 ******************************************************************/
/******************************************************************
 ******************************************************************
 *
 *	STM ARRAY ACCESS METHODS
 *
 ******************************************************************
 ******************************************************************/
/*
 *	add a private AV/HV to the WAL, and create its proxy structure if needed
 *
 *	create empty proxy structure
 *	spinlock sociable version
 *	copy sociable ptr/seqno/cycleno to proxy
 *	copy structure metadata to proxy
 *	copy sociable slot ptrs to private version
 *	release spinlock
 *	log it
 *	returns proxy if successful, else NULL
 *
 * Further explication seems in order:
 *	When an element is deleted:
 *		if 1st access to element was read
 *			acquire ownership
 *			move private version to deleted list with original slot index in IV field
 *		else (unmapped OR not accessed or 1st access was write)
 *			discard the private element
 *		set bit in deleted bitmap
 *
 *	NOTE: elements deleted due to delete() or splice() are considered READ 1st
 *		(unless the element is already marked as write 1st), since subsequent
 *		operations in the xaction may be dependent on the returned value
 *
 *	At commit,
 *		1) scan deleted list:
 *			if deleted element has changed or cannot be acquired
 *				restart
 *		2) scan the slot_map
 *			if has an original slot and was accessed
 *				if (1st access was read AND element has changed) OR
 * 					(1st access was write AND cannot acquire)
 *					restart
 *		3) scan deleted bitmap
 *			if bit set
 *				if cannot acquire associated element
 *					restart
 *		4) once acquisition is complete, apply changes:
 *			create new array pointer
 *			scan slot_map
 *				if mapped
 *					move ptr from original slot to new slot
 *					if accessed
 *						update the original element
 *				else
 *					create new element in new slot from private element
 *
 *			scan deleted bitmap
 *				if bit set
 *					free the assoc. original element
 */
STATIC void
stm_native_clone_array(sociable_proxy_t *stmproxy, sociable_proxy_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	sociable_structure_t *stmelemp = (sociable_structure_t *)stmproxy->elemp;
	sociable_element_t **ary;
	I32 count;
	I32 *slot_map;
	I32 i;

	count = stmpelemp->ary_head = elemp->ary_head;
	stmproxy->header.updcntlo = elemp->header.updcntlo;
	stmproxy->header.updcnthi = elemp->header.updcnthi;
	stmproxy->header.cycleno = elemp->header.cycleno;
	stmproxy->header.sv_flags = elemp->header.sv_flags;
	stmelemp->ary_extent = elemp->ary_extent;
	stmelemp->ary_himark = elemp->ary_himark;
	SOC_SPIN_RELEASE(elemp);
/*
 *	create array slots, a map to current slot
 *	positions, and a delete map bitmask.
 *	Slot map is used to locate original element when elements
 *	get movedaround; Delete map is used to mark position of
 *	deleted elements.
 *	Alloc a few extra slots to try to avoid frequent
 *	copies
 */
	stmproxy->sociable = (sociable_element_t *)elemp;
	CRT_Newz(ary, count + SOCIABLE_ARRAY_SPARE, sociable_element_t*);
	CRT_Newz(slot_map, count + SOCIABLE_ARRAY_SPARE, int);
	CRT_Newz(stmelemp->un.array.delete_map, (((count >> 3) & 0x1FFFFFFF) + 1), char);
	stmelemp->ary_size = stmelemp->ary_head + SOCIABLE_ARRAY_SPARE;
	for (i = 0; i < stmelemp->ary_head; slot_map[i] = i, ary[i++] = SOC_STM_UNACCESSED);
	for (;i < stmelemp->ary_size; slot_map[i++] = SOC_STM_UNMAPPED);
	stmelemp->ary_ptr = ary;
	stmelemp->ary_slot_map = slot_map;
}

int
stm_array_get_size(pTHX_ sociable_proxy_t *proxy)
{
	sociable_structure_t *structp =
		(sociable_structure_t *)(*proxy->mech->stm_save_referent)(aTHX_ proxy, 0);
	if (!structp)
		Perl_croak(aTHX_ Nullch);
	return structp->ary_head;
}

/*
 * transactionally extend an array to permit pushing (or just adding new elements)
 */
STATIC sociable_element_t **
stm_extend_array(pTHX_ sociable_structure_t *priv_structp, I32 needed)
{
	sociable_element_t ** newary;
	sociable_element_t ** oldary;
	I32 *oldmap;
	I32 *newmap;
	int i;
/*
 *	for now assume we'll extend, which means a write
 *	in future, be more conservative, and verify we
 *	really need to extend first
 */
	if (priv_structp->ary_size < needed) {
		oldary = priv_structp->ary_ptr;
		oldmap = priv_structp->ary_slot_map;
		SOCIABLE_TRACE_2("in stm_extend_array: need %d have %d\n", needed, priv_structp->ary_size);
/*
 *	alloc needed plus some spare
 */
		priv_structp->ary_size = needed + SOCIABLE_ARRAY_SPARE;
		CRT_Newz( newary, priv_structp->ary_size, sociable_element_t *);
		CRT_Newz( newmap, priv_structp->ary_size, int);
		SOCIABLE_TRACE_3("stm_extend_array: copy old array %p to new array %p for %d elems\n", oldary, newary, priv_structp->ary_head);
		if ((priv_structp->ary_head > 0) && (oldary != NULL)) {
			priv_structp->ary_head = needed;
			memcpy(newary, oldary, (sizeof(sociable_element_t *) * priv_structp->ary_head));
			memcpy(newmap, oldmap, (sizeof(int) * priv_structp->ary_head));
			SOCIABLE_TRACE_1("stm_extend_array: freeing old array %p\n", oldary);
			crt_free(oldary);
			crt_free(oldmap);
		}
		else
			priv_structp->ary_head = 0;

		for (i = priv_structp->ary_head; i < priv_structp->ary_size; newmap[i++] = SOC_STM_UNMAPPED);

		priv_structp->ary_ptr = newary;
		priv_structp->ary_slot_map = newmap;
		priv_structp->header.soc_flags |= SOC_STM_WRITE;
	}
	SOCIABLE_TRACE("stm_extend_array: returning\n");
    return priv_structp->ary_ptr;
}

int
stm_array_elem_exists(pTHX_ sociable_proxy_t *proxy, int key)
{
	sociable_structure_t *priv_structp =
		(sociable_structure_t *)(*proxy->mech->stm_save_referent)(aTHX_ proxy, 0);
	if (!priv_structp)
		Perl_croak(aTHX_ Nullch);

	return ((key < priv_structp->ary_head) &&
		(priv_structp->ary_ptr[key].elemp != NULL) &&
		(memcmp(&priv_structp->ary_ptr[key], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)) ||
		(priv_structp->ary_slot_map[key] != SOC_STM_UNMAPPED)));
}
/*
 *	fetch the specified element from an array;
 *	if flags set, extend the array if needed to prepare
 *	for storage
 */
int
stm_array_fetch(pTHX_ sociable_proxy_t *proxy, I32 key, I32 for_write, sociable_proxy_t *valproxy)
{
	sociable_structure_t *priv_structp =
		(sociable_structure_t *)(proxy->mech->stm_save_referent)(aTHX_ proxy, for_write);
	sociable_addr_t *ary;
	sociable_stm_detector_t *detector;
	I32 tkey;

	SOCIABLE_TRACE_2("in stm_array_fetch for key %d for_write 0x%04X\n", key, for_write);
	memzero(valproxy, sizeof(sociable_proxy_t));
	if (!priv_structp)
		return 0;
/*
 *	NOTE: elemp is always thread-private, so access directly
 */
	ary = priv_structp->ary_ptr;

	if (for_write) {
		priv_structp->SOCHDR.soc_flags |= SOC_STM_ELEM_WRITE;
		if (key >= priv_structp->ary_size) {
/*
 *	exceeds space in array, and vivify requested, so extend
 */
			ary = stm_extend_array(aTHX_ priv_structp, key);
			if (ary == NULL)
				return 0;
		}
		if (key >= priv_structp->ary_head)
			priv_structp->ary_head = key;
/*
 *	at this point we've got space; create new undef elements as needed
 */
		if (ary[key].elemp == NULL)
			CRT_Newz(ary[key].elemp, 1, sociable_element_t);
	}
/*
 *	if not for write and (exceeds range or is null)
 */
	else if ((key >= priv_structp->ary_head) || (ary[key].elemp == NULL))
		return 0;

	tkey = priv_structp->ary_slot_map[key];
	if (tkey == SOC_STM_UNMAPPED) {
/*
 *	its a new uncommitted element, create an element and return it
 */
		return stm_set_proxy(valproxy, &ary[key]);
	}
	SOCIABLE_TRACE("stm_array_fetch: check for element conflict\n");
	detector = proxy->mech->detector;
	if (detector->ary_elem_conflict &&
		(*detector->ary_elem_conflict)(aTHX_ proxy, priv_structp, key, tkey, for_write))
		return 0;
	SOCIABLE_TRACE("stm_array_fetch: returning\n");
	return stm_set_proxy(valproxy, &ary[key]);
}

/*
 *	delete the specified element from an array;
 */
int
stm_array_delete(pTHX_ sociable_proxy_t *proxy, I32 key)
{
	sociable_structure_t *priv_structp =
		(sociable_structure_t *)(*proxy->mech->stm_save_referent)(aTHX_ proxy, 0));
	sociable_addr_t *ary;
	sociable_stm_detector_t *detector;
	I32 tkey;

	SOCIABLE_TRACE_1("in stm_array_delete for element %d\n", key);
	if (!priv_structp)
		return 0;
/*
 *	if we're deleting off the end, we're writing the array,
 *	and need write ownership
 */
	if ((key == priv_structp->ary_head - 1) &&
		((*proxy->mech->stm_save_referent)(aTHX_ proxy, 1) == NULL))
		return 0;

	ary = priv_structp->ary_ptr;

	if ((key >= priv_structp->ary_head) || (ary[key].elemp == NULL))
		return 1;

	tkey = priv_structp->ary_slot_map[key];
	if (tkey == SOC_STM_UNMAPPED) {
/*
 *	no equivalent in sociable version, just delete it
 */
		ary[key].elemp = stm_free_element(aTHX_ ary[key].elemp, NULL);
		if (key == (priv_structp->ary_head - 1))
			priv_structp->ary_head--;
		return 1;
 	}

	detector = proxy->mech->detector;
	if (key != (priv_structp->ary_head - 1)) {
/*
 *	if not at end, then check conflict and update
 *	conflict check vivifies the private element for us
 *	NOTE: treats the update as a read 1st, then write NOT a delete operation
 */
		SOCIABLE_TRACE("stm_array_delete: not end of array, checking for conflict\n");
		if (detector->ary_elem_conflict &&
			(*detector->ary_elem_conflict)(aTHX_ proxy, priv_structp, key, tkey, SOC_STM_FOR_DELETE))
			return 0;
		if (SvROK(&ary[key]->header)) {
		/* do undef logic */
		}
		else
			ary[key]->un.scalar.buflen = 0;
		ary[key]->header.sv_flags = 0;
	}
	else {
		SOCIABLE_TRACE("stm_array_delete: end of array, trimming\n");
/*
 *	must treat as read 1st access
 */
		if (detector->ary_elem_conflict &&
			(*detector->ary_elem_conflict)(aTHX_ proxy, priv_structp, key, tkey, SOC_STM_FOR_DELETE))
			return 0;
/*
 *	move to deleted list w/ original slot number
 */
		(sociable_element_t *)(ary[key].elemp)->un.scalar.iv = tkey;
		ary[key].elemp = stm_free_element(aTHX_ ary[key].elemp, priv_structp);
		priv_structp->ary_slot_map[key] = SOC_STM_UNMAPPED;
		priv_structp->ary_head--;
		priv_structp->header.soc_flags |= SOC_STM_WRITE;
	}
	SOCIABLE_TRACE("stm_array_delete: returning\n");
 	return 1;
}
/*
 *	clear an array, and destroy all the contents
 */
int
stm_array_clear(pTHX_ sociable_proxy_t *proxy)
{
	sociable_structure_t *priv_structp =
		(sociable_structure_t *)(*proxy->mech->stm_save_referent)(aTHX_ proxy, 1);
 	sociable_addr_t *ary;
	I32 i;

	SOCIABLE_TRACE("in stm_array_clear\n");

	if (!priv_structp)
		return 0;

	priv_structp->SOCHDR.soc_flags |= SOC_STM_WRITE;

 	ary = priv_structp->ary_ptr;
	for (i = 0; i < priv_structp->ary_head; i++) {
/*
 *	move to deleted list w/ original slot number;
 *	no attempted conflict detect here...
 */
		if (!SOC_IS_UNACCESSED(ary, i)) {
			(sociable_element_t *)(ary[i].elemp)->un.scalar.iv = priv_structp->ary_slot_map[i];
			stm_free_element(aTHX_ ary[i].elemp, priv_structp);
		}
	}
/*
 *	in the interest of memory housekeeping, we'll
 *	coerce the array slots into the minimum
 */
	priv_structp->ary_size = SOCIABLE_ARRAY_SPARE;
	priv_structp->ary_head = 0;
	crt_free(priv_structp->un.array.aryptr.elemp);
	crt_free(priv_structp->ary_slot_map);
	CRT_Newz(priv_structp->ary_slot_map, SOCIABLE_ARRAY_SPARE, int);
	CRT_Newz(priv_structp->ary_ptr, SOCIABLE_ARRAY_SPARE, sociable_addr_t);
	for (i = 0; i < SOCIABLE_ARRAY_SPARE; priv_structp->ary_slot_map[i++] = SOC_STM_UNMAPPED);
	SOCIABLE_TRACE("stm_array_clear: returning\n");
	return 1;
}
/*
 *	Remove and return the specified element(s) from an array.
 *	If a replacement list is provided, inserts the replacements
 *	at the specified offset.
 *	If items is -1, then its for a PUSH (key == -1) or UNSHIFT (key == 0)
 *	Returns either a AV or SV or NULL, depending on call context
 *	and success of operation
 *
 */
SV *
stm_splice(pTHX_ sociable_proxy_t *proxy, I32 items, I32 key, I32 len, I32 replace, SV **replacements)
{
	sociable_structure_t *priv_structp =
		(sociable_structure_t *)(*proxy->mech->stm_save_referent)(aTHX_ proxy, 1);
	sociable_array_t *array = NULL;
	AV *av = NULL;
	SV **sv = NULL;
	SV *retsv = NULL;
	sociable_element_t **telemp = NULL;
	sociable_addr_t *ary = NULL;
	sociable_element_t *sociable = NULL;
	int i,j;
	int inserts = replace;
	I32 *slot_map;
	SV *errsv;
	sociable_detector_t *detector;

	if (!priv_structp)
		return NULL;

	array = &priv_structp->un.array;
	ary = priv_structp->ary_ptr.elemp;
	slot_map = priv_structp->ary_slot_map;

	SOCIABLE_TRACE_5("in stm_splice for array %p key %d len %d replace %d replacements %p\n",
		array, key, len, replace, replacements);
/*
 *	if items == -1, then its for a PUSH: splice onto end
 */
 	if (items == -1) {
 		key = (key < 0) ? priv_structp->ary_head : 0;
		retsv = &PL_sv_undef;
	}
 	else {
/*
 *	can't validate params until we've logged/acquired
 */
		if (key < 0)
			key = priv_structp->ary_head + key;
		else if (key >= priv_structp->ary_head) {
			Perl_warn(aTHX_ "Offset exceeds array length");
			key = priv_structp->ary_head;
		}
/*
 *	SPLICE and no LENGTH param was specified, use remainder
 */
		if (items <= 2)
			len = priv_structp->ary_head - key;
/*
 *	if LENGTH is negative,
 */
		if (len < 0)
			len = (priv_structp->ary_head + len) - key;
/*
 *	if LENGTH exceeds current array length, then adjust
 */
		if (len > priv_structp->ary_head - key)
			len = priv_structp->ary_head - key;
/*
 *	if OFFSET exceeds current length, throw error
 */
		if (key >= priv_structp->ary_head) {
 			errsv = get_sv("@", TRUE);
 			sv_setpv(errsv, "Thread::Sociable::SPLICE: OFFSET exceeds array length.");
			return NULL;
		}
/*
 *	if OFFSET + LENGTH exceeds current length, adjust
 */
		if (key + len > priv_structp->ary_head)
			len = priv_structp->ary_head - key;

	 	if (len > 0) {
/*
 *	detect conflicts, and recover any unaccessed elements
 *	then convert to SV or AV
 */
 			detector = proxy->mech->detector;
			if (detector->ary_splice_conflict &&
				(*detector->ary_splice_conflict)(aTHX_ proxy, priv_structp, key, len)) {
 				errsv = get_sv("@", TRUE);
				sv_setpv(errsv, SOC_STM_RESTART_MSG);
				return NULL;
			}

	 		if (GIMME_V == G_ARRAY) {
/*
 *	create empty elements before acquiring lock
 */
	 			CRT_Newz(sv, len, SV *);
				for (i = 0; i < len; i++)
					sv[i] = stm_sv_from_elem(aTHX_ ary[key + i].elemp);

				av = av_make(len, sv);
				crt_free(sv);
			}
			else {
				retsv = stm_sv_from_elem(aTHX_ ary[key + len-1].elemp);
			}
		}
		else {
/*
 *	return empty list or undef if no removal
 */
 			if (GIMME_V == G_ARRAY)
				av = newAV();
			else
				retsv = &PL_sv_undef;
		}
	}	/* end if PUSH else SPLICE */

	if (replace > len) {
/*
 *	more new elems than old:
 *	for existing elems:
 *		mark elems as written and copy replacements in place
 *	for remaining replacements:
 *		create new private elems and copy
 */
		replace -= len;
		ary = stm_extend_array(aTHX_ priv_structp, (priv_structp->ary_head + replace));
		for (i = priv_structp->ary_head - 1, j = i + replace; i >= key + len; i--) {
			slot_map[j] = slot_map[i];
			ary[j--].elemp = ary[i].elemp;
			slot_map[i] = SOC_STM_UNMAPPED;
			ary[i].elemp = NULL;
		}
		priv_structp->ary_head += replace;
		for (i = 0; i < len; i++)
			stm_elem_from_sv(aTHX_ ary[i + key].elemp, replacements[i]);
		for (; i < inserts; i++) {
			CRT_Newz(ary[i + key].elemp, 1, sociable_element_t);
			((sociable_element_t *)ary[i + key].elemp)->SOCHDR.soc_flags =
				SOC_INUSE | SOC_STM_PRIVATE;
			slot_map[i + key] = SOC_STM_UNMAPPED;
			stm_elem_from_sv(aTHX_ ary[i + key].elemp, replacements[i]);
		}
	}
	else { /* (replace <= len), including replace == 0 or len == 0 */
/*
 *	more old elems than new:
 *	for replacements:
 *		mark old as written and copy
 *	for remainder:
 *		move to delete list
 *
 *	copy replacements into existing slots
 */
		for (i = 0; i < replace; i++)
			stm_elem_from_sv(aTHX_ ary[i + key].elemp, replacements[i]);
/*
 *	delete remainder
 */
		for (; i < len; i++) {
			((sociable_element_t *)ary[i + key].elemp)->un.scalar.iv =
				priv_structp->ary_slot_map[i + key];
			stm_free_element(aTHX_ ary[i + key].elemp, priv_structp);
		}
/*
 *	pull in rest of array (maybe shrink storage in future ?)
 */
		len -= replace;
		if (len) {
			for (i = key + replace; (i + len) < priv_structp->ary_head; i++) {
				slot_map[i] = slot_map[i + len];
				ary[i].elemp = ary[i + len].elemp;
			}
			for (i = (priv_structp->ary_head - len - 1); i < priv_structp->ary_head; slot_map[i] = SOC_STM_UNMAPPED, ary[i++] = NULL);
			priv_structp->ary_head -= len;
		}
	}
	SOCIABLE_TRACE_1("stm_array_splice: returning %p\n", (av ? (SV *)av : retsv));
	return (av ? ((SV *)av) : retsv);
}

SV *
stm_pop(pTHX_ sociable_proxy_t *proxy)
{
	return stm_splice(aTHX_ proxy, 3, -1, 1, 0, NULL);
}

int
stm_push(pTHX_ sociable_proxy_t *proxy, int listlen, SV **ary)
{
	stm_splice(aTHX_ proxy, -1, -1, 0, listlen, ary);
	return 1;
}

SV *
stm_shift(pTHX_ sociable_proxy_t *proxy)
{
	return stm_splice(aTHX_ proxy, 3, 0, 1, 0, NULL);
}

int
stm_unshift(pTHX_ sociable_proxy_t *proxy, int listlen, SV **ary)
{
	stm_splice(aTHX_ proxy, -1, 0, 0, listlen, ary);
	return 1;
}

int
stm_extend(pTHX_ sociable_proxy_t *proxy, int listlen)
{
	sociable_structure_t *priv_structp = (sociable_structure_t *)stm_save_referent(aTHX_ proxy, 1);
	return priv_structp
		? (stm_extend_array(aTHX_ priv_structp, listlen) != NULL)
		: 0;
}

int
stm_storesize(pTHX_ sociable_proxy_t *proxy, int count)
{
	sociable_structure_t *priv_structp = (sociable_structure_t *)stm_save_referent(aTHX_ proxy, 1);

	if (!priv_structp)
		return 0;
	if (count > priv_structp->ary_head)
		return (stm_extend_array(aTHX_ priv_structp, count) != NULL);
	if (count == priv_structp->ary_head)
		return 1;
 	count -= priv_structp->ary_head;
	return stm_splice(aTHX_ proxy, 3, count, -count, 0, NULL);
}

/*
 *	acquire written elements of array for commit
 *	(assumes array spinlocked and acquireable)
 *		1) scan deleted list:
 *			if deleted element has changed or cannot be acquired
 *				restart
 *		2) scan the slot_map
 *			if has an original slot and was accessed
 *				if (1st access was read AND element has changed) OR
 * 					(1st access was write AND cannot acquire)
 *					restart
 *		3) scan deleted bitmap
 *			if bit set
 *				if cannot acquire associated element
 *					restart
 */
STATIC int
stm_acquire_array_elems(pTHX_ sociable_structure_t *priv_structp)
{
	sociable_locator_t *sociable = &proxy->sociable;
	int i, j, k;
	sociable_element_t **ary = proxy->ary_ptr;
	int arylen = proxy->ary_head;
	I32 *slot_map = proxy->ary_slot_map;
	sociable_element_t *deleted = proxy->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = proxy->ary_delete_map;
	dMY_CXT;
/*
 *	check for accessed deletes
 */
	while (deleted) {
		if ((SOC_STM_WAS_READ(deleted) && (!SOC_STM_VALID_ELEM(deleted, elemp->ary_ptr[deleted->un.scalar.iv]))) ||
			(SOC_STM_WAS_WRITTEN(deleted) &&
				(!SOC_STM_CAN_ACQUIRE(elemp->ary_ptr[deleted->un.scalar.iv]))))
			return 0;
		deleted = deleted->header.next;
	}
/*
 *	check for updates
 */
	for (i = 0; i < arylen; i++) {
		telemp = ary[i];
		if ((slot_map[i] == SOC_STM_UNMAPPED) ||
			(telemp == NULL) ||
			(telemp == SOC_STM_UNACCESSED))
			continue;
		if ((SOC_STM_WAS_READ(telemp) && (!SOC_STM_VALID_ELEM(telemp, elemp->ary_ptr[slot_map[i]]))) ||
			(SOC_STM_WAS_WRITTEN(telemp) &&
				(!SOC_STM_CAN_ACQUIRE(elemp->ary_ptr[slot_map[i]]))))
			return 0;
	}
/*
 *	check for unaccessed deletes
 */
	arylen = ((elemp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			if ((delete_map[i] & j) && (!SOC_STM_CAN_ACQUIRE(elemp->ary_ptr[(i << 3) + k])))
				return 0;
		}
	}
	return 1;
}

/*
 * Create a sociable element from a private SV.
 * Assumes lock is held.
 *
 *	NOTE: no STM processing here. A referenced variable isn't logged until
 *	its state is actually read or written.
 */
STATIC sociable_element_t *
stm_new_sociable(pTHX_ sociable_element_t *telemp)
{
    sociable_element_t *elemp = sociable_allocate(aTHX);

	SOCIABLE_TRACE_1("stm_new_sociable: got an element %p\n", elemp);
	telemp->header.cycleno = elemp->header.cycleno;
	memcpy(elemp, telemp, sizeof(sociable_element_t));
/*
 *	force seqno to nonzero
 */
	elemp->header.updcntlo = 1;
	elemp->header.updcnthi = 0;
	SOCIABLE_TRACE("stm_new_sociable: returning\n");
    return (elemp);
}

/*
 *	merge (aka commit) a private array into the sociable version
 *	(assumes array spinlocked and acquireable)
 *	create new array pointer
 *	scan slot_map
 *		if mapped
 *			move ptr from original slot to new slot
 *			if accessed
 *				update the original element
 *			else
 *				create new element in new slot from private element
 *
 *		scan deleted bitmap
 *			if bit set
 *				free the assoc. original element
 */
STATIC void
stm_merge_array(pTHX_ sociable_structure_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->header.sociable;
	int i,j,k;
	sociable_element_t **ary = proxy->ary_ptr;
	int arylen = proxy->ary_head;
	int *slot_map = proxy->ary_slot_map;
	sociable_element_t *deleted = proxy->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = proxy->ary_delete_map;
/*
 *	we should probably scan for and create new unmapped elements
 *	in a group to optimize
 */
	for (i = 0; i < arylen; i++) {
		telemp = ary[i];
		if (slot_map[i] == SOC_STM_UNMAPPED) {
			/* create sociable from private element */
			ary[i] = stm_new_sociable(aTHX_ telemp);
		}
		else {
			ary[i] = elemp->ary_ptr[slot_map[i]];
			if (SOC_STM_WAS_WRITTEN(ary[i]))
				stm_update_element(aTHX_ telemp, ary[i]);
		}
		stm_free_element(aTHX_ telemp, NULL);
	}
/*
 *	remove deletes: we should cluster the removed elements
 *	and return to the pool (or extent) in a single locked operation
 */
	arylen = ((elemp->ary_head >> 3) & 0x1FFFFFFF) + 1;
	for (i = 0; i < arylen; i++) {
		if (!delete_map[i])
			continue;
		for (j = 1, k = 0; j < 256; j <<= 1, k++) {
			if (delete_map[i] & j)
				sociable_destroy(aTHX_ elemp->ary_ptr[(i << 3) + k]);
		}
	}
/*
 *	move everything to the sociable array
 */
	proxy->ary_ptr = NULL;
	crt_free(elemp->ary_ptr);
	elemp->ary_ptr = ary;
	elemp->ary_size = proxy->ary_size;
	elemp->ary_head = proxy->ary_head;
}

/*
 *	destroy private version of array
 */
STATIC void *
stm_free_array(pTHX_ sociable_structure_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->header.sociable;
	int i;
	sociable_element_t **ary = proxy->ary_ptr;
	int arylen = proxy->ary_head;
	int *slot_map = proxy->ary_slot_map;
	sociable_element_t *deleted = proxy->ary_deletes;
	sociable_element_t *telemp;
	char *delete_map = proxy->ary_delete_map;

	while (deleted) {
		telemp = deleted;
		deleted = deleted->header.next;
		stm_free_element(aTHX_ telemp, NULL);
	}
	crt_free(slot_map);
	crt_free(delete_map);
	if (ary) {	/* merge may have borrowed this */
		for (i = 0; i < arylen; i++) {
			if (ary[i] && (ary[i] != SOC_STM_UNACCESSED))
				stm_free_element(aTHX_ ary[i], NULL);
		}
	}
	crt_free(proxy->header.pkg_name);
	crt_free(proxy);
	return NULL;
}

/******************************************************************
 ******************************************************************
 *
 *	STM HASH ACCESS METHODS
 *
 ******************************************************************
 ******************************************************************/
/*
 *	add a private HV to the WAL, and update its proxy structure
 *
 *	create empty proxy structure
 *	spinlock sociable version
 *	copy sociable ptr/seqno/cycleno to proxy
 *	copy structure metadata to proxy
 *	copy sociable slot ptrs to private version
 *	release spinlock
 *	log it
 *
 * Further explication seems in order:
 *	When an element is deleted:
 *		acquire for write
 *		mark element as deleted
 *		if for delete() AND element was not accessed, mark as read 1st
 *
 *	NOTE: elements deleted due to delete() are considered READ 1st
 *		(unless the element is already marked as write 1st), since subsequent
 *		operations in the xaction may be dependent on the returned value
 *	Also note that, if a later vivification with the same key occurs,
 *	the delete mark is removed and the element is reused
 *
 *	At commit time, we traverse the elements of the private hash:
 *	if element marked deleted or for write
 *		if cannot acquire
 *			restart
 *	else if element marked read 1st and has changed
 *		restart
 *
 *	When all elements acquired:
 *	if an element was not accessed (ie, its a new element)
 *		add it
 *	else if an element is marked deleted
 *		delete original
 *	else if element was written
 *		update
 *
 */
STATIC void
stm_clone_hash(sociable_structure_t *proxy, sociable_structure_t *elemp)
{
	sociable_hash_t *thash = &proxy->un.hash;

	proxy->header.updcntlo = elemp->header.updcntlo;
	proxy->header.updcnthi = elemp->header.updcnthi;
	proxy->header.cycleno = elemp->header.cycleno;
	proxy->header.sv_flags = elemp->header.sv_flags;
	proxy->hash_bucket_count = elemp->hash_bucket_count;
	proxy->extent.extent = elemp->extent.extent;
	proxy->extent.himark = elemp->extent.himark;
	SOC_SPIN_RELEASE(elemp);
/*
 *	elements are not added to the proxy until accessed
 */
	proxy->header.sociable = (sociable_element_t *)elemp;
	CRT_Newz(proxy->hash_buckets, proxy->hash_bucket_count, sociable_element_t*);
}

#ifdef SOC_STM_EAGER_COPY
/*
 *	clone a hash (with elements) for STM
 *	NOTE: incomplete/unsupported for now, due to
 *	possible spinlock starvation
 */
STATIC sociable_structure_t *
stm_copy_hash(pTHX_ sociable_structure_t *elemp)
{
/*
 *	allocate private element
 *	spinlock the sociable array
 *	get the sociable array metadata
 *  allocate memory for array
 *  foreach sociable element
 *		if defined
 *			private[element] = SOC_STM_UNACCESSED;
 *	release spinlock
 *	NOTE: perhaps we should release/re-acquire spinlock
 *	every N elements ???
 */
	sociable_structure_t *proxy;
	sociable_hash_t *hash = &elemp->un.hash;
	sociable_hash_t *thash = &telemp->un.hash;

	CRT_Newz(proxy, 1, sociable_structure_t);
	SOC_SPIN_LOCK(elemp);
	proxy->header.updcntlo = elemp->header.updcntlo;
	proxy->header.updcnthi = elemp->header.updcnthi;
	proxy->header.cycleno = elemp->header.cycleno;
	proxy->header.sv_flags = elemp->header.sv_flags;
	proxy->hash_bucket_count = elemp->hash_bucket_count;
	proxy->extent.extent = elemp->extent.extent;
	proxy->extent.himark = elemp->extent.himark;
	/* do a complete hash clone... */

	SOC_SPIN_RELEASE(elemp);
	return telemp;
}
#endif

STATIC sociable_element_t *
stm_discard_element(pTHX_ sociable_element_t *h, sociable_hash_t *hash, int bucket)
{
	sociable_element_t *next;
	if (h->PREV_PTR) {
		((sociable_element_t *)h->PREV_PTR)->NEXT_PTR = h->NEXT_PTR;
		if (h->NEXT_PTR)
			((sociable_element_t *)h->NEXT_PTR)->PREV_PTR = h->PREV_PTR;
	}
	else {
		hash->buckets[bucket].elemp = h->NEXT_PTR;
		if (h->NEXT_PTR)
			((sociable_element_t *)h->NEXT_PTR)->PREV_PTR = NULL;
	}
	next = (sociable_element_t *)h->NEXT_PTR;
	hash->keycount--;
	stm_free_element(aTHX_ h, NULL);
	return next;
}

/*
 *	Transactionally remove an element from a hash;
 *	the element is not truly removed, but just marked as deleted
 *	except for private-only elements, which are discarded
 */
I32
native_stm_remove_hash_element(pTHX_ sociable_proxy_t *proxy, char *key, I32 len)
{
	sociable_stm_proxy_t *stm_proxy = stm_lookup_stm_proxy(aTHX_ proxy);
	sociable_hash_t *hash = &stm_proxy->priv.structp->un.hash;
	sociable_structure_t *sociable;
	UV hashval = 0;
	I32 bucket;
	sociable_element_t *h = NULL;
	sociable_element_t *telemp = NULL;
	sociable_element_t *trail = NULL;
	sociable_element_t *free = NULL;

	SOCIABLE_TRACE("in native_stm_remove_hash_element\n");
/*
 *	don't acquire unless needed
 */
	PERL_HASH(hashval, key, len);
	bucket = hashval % hash->bucket_count;
/*
 *	first, scan proxy for existance
 */
	h = hash->buckets[bucket].elemp;
	while ((h != NULL) &&
		((hashval != h->hash_tuple.hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr.elemp, len)))
		h = h->NEXT_PTR;
/*
 *	if found, check if it has a sociable mate that has not already been removed
 */
	if (h) {
		if (!SOC_STM_DELETED(h)) {
			if (!SOC_STM_WAS_ACCESSED(h)) {
/*
 *	not committed, so just discard it
 */
				stm_discard_element(aTHX_ h, hash, bucket);
			}
			else {
/*
 *	no need to acquire the element, just mark as deleted
 */
		 		h->SOCHDR.soc_flags |= SOC_STM_ELEM_DELETED;
		 		stm_proxy->priv.structp->SOCHDR.soc_flags |= SOC_STM_WRITE;
		 	}
		}
		return 1;
	}
/*
 *	1st access: try to find the sociable version
 */
	if (detector->has_conflict &&
		(*detector->has_conflict)(aTHX_ stm_proxy, 1))
		return -1;

	sociable = stm_proxy->priv.structp->header.sociable;
	CRT_Newz(telemp, 1, sociable_element_t);
	telemp->hash_tuple.hashval = hashval;
	telemp->hash_tuple.keylen = len;
	CRT_Dup(telemp->hash_tuple.keyptr.elemp, len + 1, char, key, len);

	SOC_SPIN_LOCK(sociable);
	h = sociable->hash_buckets[bucket].elemp;
	while ((h != NULL) &&
		((hashval != h->hash_tuple.hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr.elemp, len)))
		h = h->NEXT_PTR;
/*
 *	if not found, release and return
 */
 	if (!h) {
		SOC_SPIN_RELEASE(sociable);
		crt_free(telemp->hash_tuple.keyptr.elemp);
		crt_free(telemp);
		return 0;
 	}
/*
 *	else clone into private hash, and mark deleted
 *	Note we do not need to acquire the element
 */
	stm_clone_element(telemp, h);
	SOC_SPIN_RELEASE(sociable);

	stm_proxy->priv.structp->SOCHDR.soc_flags |= SOC_STM_WRITE;

	telemp->NEXT_PTR = hash->buckets[bucket].elemp;
	telemp->SOCHDR.soc_flags |= SOC_STM_ELEM_DELETED;
	if (hash->buckets[bucket].elemp)
		((sociable_element_t *)hash->buckets[bucket].elemp)->PREV_PTR = telemp;
	hash->buckets[bucket].elemp = telemp;
	hash->keycount--;
	return 1;
}

/*
 *	Transactionally remove an element from a hash;
 *	the element is not truly removed, but just marked as deleted
 *	except for private-only elements, which are discarded
 */
I32
stm_remove_hash_element(pTHX_ sociable_proxy_t *proxy, char *key, I32 len)
{
	sociable_stm_proxy_t *stm_proxy = stm_lookup_stm_proxy(aTHX_ proxy);
	sociable_hash_t *hash = &stm_proxy->priv.structp->un.hash;
	sociable_structure_t *sociable;
	UV hashval = 0;
	I32 bucket;
	sociable_element_t *h = NULL;
	sociable_element_t *telemp = NULL;
	sociable_element_t *trail = NULL;
	sociable_element_t *free = NULL;

	SOCIABLE_TRACE("in native_stm_remove_hash_element\n");
/*
 *	don't acquire unless needed
 */
	PERL_HASH(hashval, key, len);
	bucket = hashval % hash->bucket_count;
/*
 *	first, scan proxy for existance
 */
	h = hash->buckets[bucket].elemp;
	while ((h != NULL) &&
		((hashval != h->hash_tuple.hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr.elemp, len)))
		h = h->NEXT_PTR;
/*
 *	if found, check if it has a sociable mate that has not already been removed
 */
	if (h) {
		if (!SOC_STM_DELETED(h)) {
			if (!SOC_STM_WAS_ACCESSED(h)) {
/*
 *	not committed, so just discard it
 */
				stm_discard_element(aTHX_ h, hash, bucket);
			}
			else {
/*
 *	no need to acquire the element, just mark as deleted
 */
		 		h->SOCHDR.soc_flags |= SOC_STM_ELEM_DELETED;
		 		stm_proxy->priv.structp->SOCHDR.soc_flags |= SOC_STM_WRITE;
		 	}
		}
		return 1;
	}
/*
 *	1st access: try to find in the sociable version
 */
	if (detector->has_conflict &&
		(*detector->has_conflict)(aTHX_ stm_proxy, 1))
		return -1;

	sociable = stm_proxy->priv.structp->header.sociable;
	CRT_Newz(telemp, 1, sociable_element_t);
	telemp->hash_tuple.hashval = hashval;
	telemp->hash_tuple.keylen = len;
	CRT_Dup(telemp->hash_tuple.keyptr.elemp, len + 1, char, key, len);

	SOC_SPIN_LOCK(sociable);
	h = sociable->hash_buckets[bucket].elemp;
	while ((h != NULL) &&
		((hashval != h->hash_tuple.hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr.elemp, len)))
		h = h->NEXT_PTR;
/*
 *	if not found, release and return
 */
 	if (!h) {
		SOC_SPIN_RELEASE(sociable);
		crt_free(telemp->hash_tuple.keyptr.elemp);
		crt_free(telemp);
		return 0;
 	}
/*
 *	else clone into private hash, and mark deleted
 *	Note we do not need to acquire the element
 */
	stm_clone_element(telemp, h);
	SOC_SPIN_RELEASE(sociable);

	stm_proxy->priv.structp->SOCHDR.soc_flags |= SOC_STM_WRITE;

	telemp->NEXT_PTR = hash->buckets[bucket].elemp;
	telemp->SOCHDR.soc_flags |= SOC_STM_ELEM_DELETED;
	if (hash->buckets[bucket].elemp)
		((sociable_element_t *)hash->buckets[bucket].elemp)->PREV_PTR = telemp;
	hash->buckets[bucket].elemp = telemp;
	hash->keycount--;
	return 1;
}

/*
 *	lookup value for the specified key; returns the element for the value
 */
sociable_element_t *
stm_hash_lookup(pTHX_ sociable_proxy_t *proxy, char *key, I32 len, I32 for_write, sociable_proxy_t *valproxy)
{
	sociable_structure_t *priv_structp = (sociable_structure_t *)stm_save_referent(aTHX_ proxy, for_write);
	sociable_hash_t *hash;
	sociable_addr_t *sociable = (sociable_structure_t *)proxy->elemp;
	UV hashval = 0;
	I32 bucket = 0;
	sociable_element_t *h = NULL;
	sociable_element_t *sh = NULL;
	sociable_element_t *telemp = NULL;

	SOCIABLE_TRACE_1("in stm_hash_lookup: key %s\n", key);
	memzero(valproxy, sizeof(sociable_proxy_t));
/*
 *	check for conflicts, or log if 1st access;
 *	but don't check/log for write until we know
 *	we're adding a new key
 */
	if (!priv_structp)
		return NULL;

	hash = &priv_structp->un.hash.elemp;
	PERL_HASH(hashval, key, len);
	bucket = hashval %  hash->bucket_count;
/*
 *	scan private version first
 */
	h = hash->buckets[bucket];
	while ((h != NULL) &&
		((h->hash_tuple.hashval != hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr, len)))
		h = h->header.next;

	SOC_SPIN_LOCK(sociable);
	sh = sociable->hash_buckets[bucket];
	while ((sh != NULL) &&
		((hashval != sh->hash_tuple.hashval) ||
		(len != sh->hash_tuple.keylen) ||
		memcmp(key, sh->hash_tuple.keyptr, len)))
		sh = sh->header.next;
/*
 *	if no sociable, release ASAP
 */
	if (!sh)
		SOC_SPIN_RELEASE(sociable);
/*
 *	if private not found
 *		if sociable not found
 *			if not for write
 *				return
 *			create empty element
 *		else
 *			create private clone of sociable
 *			mark private version w/ proper access (read or write)
 *		insert and return private version
 */
 	if (!h) {
 		if (!sh) {
			if (!for_write)
				return NULL;

			CRT_Newz(telemp, 1, sociable_element_t);
			telemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;
		}
		else {
/*
 *	create private clone
 *	implicitly its 1st access: try to find in the sociable version
 */
			if (detector->hash_elem_acquire &&
				(*detector->hash_elem_acquire)(aTHX_ sh, sociable, for_write))
				return NULL;

			CRT_Newz(telemp, 1, sociable_element_t);
			stm_clone_element(telemp, sh);
			SOC_SPIN_RELEASE(sociable);
			telemp->header.soc_flags = for_write ?
				SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_WRITE :
				SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_READ;
		}
/*
 *	insert into hash
 */
		telemp->hash_tuple.hashval = hashval;
		telemp->hash_tuple.keylen = len;
		CRT_Dup(telemp->hash_tuple.keyptr, len + 1, char, key, len);
		telemp->header.next = hash->buckets[hashval];
		if (hash->buckets[hashval])
			hash->buckets[hashval]->header.prev = telemp;
		hash->buckets[hashval] = telemp;
		return telemp;
 	}
/*
 *	private exists:
 *	if marked deleted
 *		undelete it
 *		set as an undef
 *	if no sociable
 *		return private
 *
 *	check for conflicts
 */
	if (sh && detector->hash_elem_conflict &&
		(*detector->hash_elem_conflict)(aTHX_ h, sh, for_write)) {
		SOC_SPIN_RELEASE(sociable);
		return NULL;
	}
	SOC_SPIN_RELEASE(sociable);

	if (SOC_STM_DELETED(h)) {
		h->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_WRITE;
		stm_clear_element(h);
	}

	SOCIABLE_TRACE_1("stm_hash_lookup: returning %p\n", h);
	return h;
}
/*
 * clear the hash, including destroying all contents
 */
int
stm_hash_clear(pTHX_ sociable_proxy_t *proxy)
{
/*
 *	simply marks all existing elements as deleted;
 *	note that private only elements are fully discarded
 *	since we must have write ownership of the hash to proceed, any xaction
 *	owning an element is SOL, and we don't attempt to acquire elements
 *
 *	as we're implicitly accessing all elements,
 *	should we karma-up the full amount to improve our chances
 *	of grabbing everything ? For now, no.
 *
 */
	sociable_structure_t *elemp = (sociable_structure_t *)stm_save_referent(aTHX_ proxy, 1);
	sociable_hash_t *hash = &elemp->un.hash;
	sociable_element_t *h = NULL;
 	I32 i;

	SOCIABLE_TRACE("in stm_hash_clear\n");
	if (!elemp)
		return 0;

	hash = &elemp->un.hash;
	for (i = 0; i <  hash->bucket_count; i++) {
		if (hash->buckets[i] == NULL)
			continue;
		h = hash->buckets[i];
		while (h != NULL) {
			if (!SOC_STM_WAS_ACCESSED(h)) {
				h = stm_discard_element(aTHX_ h, hash, i);
			}
			else {
				h->header.soc_flags |= SOC_STM_WRITE | SOC_STM_ELEM_DELETED;
				h = h->header.next;
			}
		}
	}
	hash->keycount = 0;
	SOCIABLE_TRACE("stm__hash_clear: returning\n");
	return 1;
}

/*
 *	acquire written elements of hash
 *	(assumes hash spinlocked and acquireable)
 */
STATIC int
stm_acquire_hash_elems(pTHX_ sociable_structure_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->header.sociable;
	int bucket;
	sociable_element_t *h;
	int bucketlen = proxy->hash_bucket_count;
	sociable_element_t **buckets = proxy->hash_buckets;
	dMY_CXT;

	for (bucket = 0; bucket < bucketlen; bucket++) {
		h = buckets[bucket];
		while (h) {
			if ((SOC_STM_WAS_READ(h) && (!SOC_STM_VALID_READ(h))) ||
				(SOC_STM_WAS_WRITTEN(h) && (!SOC_STM_CAN_ACQUIRE(h->header.sociable))))
				return 0;
			h = h->header.next;
		}
	}
	return 1;
}

/*
 *	merge a private hash into the sociable version
 *	(assumes hash spinlocked and acquireable)
 *
 *	if an element is marked deleted
 *		delete original
 *	else if element was written or new
 *		update or add
 */
STATIC void
stm_merge_hash(pTHX_ sociable_structure_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->header.sociable;
	int bucket;
	sociable_element_t *h;
	int bucketlen = proxy->hash_bucket_count;
	sociable_element_t **buckets = proxy->hash_buckets;
	sociable_element_t *telemp;

	for (bucket = 0; bucket < bucketlen; bucket++) {
		h = buckets[bucket];
		while (h) {
			if (SOC_STM_DELETED(h)) {
				sociable_remove_hash_element(aTHX_ &elemp->un.hash, h->hash_tuple.keyptr, h->hash_tuple.keylen, &elemp->extent);
			}
			else if (!SOC_STM_WAS_READ(h)) {
			/*
			 *	either write to existing element, or create a new one
			 */
				telemp = sociable_hash_lookup(aTHX_ &elemp->un.hash, h->hash_tuple.keyptr, h->hash_tuple.keylen, 1, &elemp->extent);
				stm_update_element(aTHX_ h, telemp);
			}
			h = h->header.next;
		}
	}
}
/*
 *	clean up a private hash
 */
STATIC void *
stm_free_hash(pTHX_ sociable_structure_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->header.sociable;
	int bucket;
	sociable_element_t *h, *next;
	int bucketlen = proxy->hash_bucket_count;
	sociable_element_t **buckets = proxy->hash_buckets;

	for (bucket = 0; bucket < bucketlen; bucket++) {
		h = buckets[bucket];
		while (h) {
			next = h->header.next;
			stm_free_element(aTHX_ h, NULL);
			h = next;
		}
	}
	crt_free(buckets);
	crt_free(proxy->header.pkg_name);
	crt_free(proxy);
	return NULL;
}

/*
 *	hash iteration/enumeration methods
 */
STATIC int
stm_hash_elem_deleted(sociable_structure_t *proxy, char *key, int len, int bucket)
{
	sociable_element_t *elemp = proxy->un.hash.buckets[bucket];
	while (elemp &&
		((elemp->hash_tuple.keylen != len) ||
		memcmp(elemp->hash_tuple.keyptr, key, len)))
		elemp = elemp->header.next;
	return (elemp && SOC_STM_DELETED(elemp));
}

STATIC int
stm_hash_elem_accessed(sociable_structure_t *proxy, char *key, int len, int bucket)
{
	sociable_element_t *elemp = proxy->un.hash.buckets[bucket];

	while (elemp &&
		((elemp->hash_tuple.keylen != len) ||
		memcmp(elemp->hash_tuple.keyptr, key, len)))
		elemp = elemp->header.next;
	return (elemp != NULL);
}

/*
 *	locate specified key in either private or sociable
 *	versions, returning the element, its bucket index, and
 *	sociable/private indicator
 *	assumes spinlock is held
 */
STATIC sociable_element_t *
stm_locate_hash_elem(sociable_structure_t *proxy, char *key, int len, int *bucket, int *is_sociable)
{
	U32 hashval;
	sociable_element_t *elemp;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->header.sociable;

	PERL_HASH(hashval, key, len);
	*bucket = hashval %  proxy->un.hash.bucket_count;
	elemp = sociable->un.hash.buckets[*bucket];
	*is_sociable = 1;
	while (elemp &&
		((elemp->hash_tuple.keylen != len) ||
		memcmp(elemp->hash_tuple.keyptr, key, len)))
		elemp = elemp->header.next;

	if (elemp)
		return stm_hash_elem_deleted(proxy, key, len, *bucket) ? NULL : elemp;

	elemp = proxy->un.hash.buckets[*bucket];
	*is_sociable = 0;
	while (elemp &&
		((elemp->hash_tuple.keylen != len) ||
		memcmp(elemp->hash_tuple.keyptr, key, len)))
		elemp = elemp->header.next;

	return ((elemp == NULL) || SOC_STM_WAS_ACCESSED(elemp)) ? NULL : elemp;
}

STATIC void
stm_hash_add_elem(pTHX_ sociable_structure_t *proxy, sociable_element_t *elemp, int bucket)
{
	sociable_element_t *telemp;
	CRT_Newz(telemp, 1, sociable_element_t);
	stm_clone_element(telemp, elemp);

	telemp->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE | SOC_STM_READ;
	telemp->header.next = proxy->un.hash.buckets[bucket];
	if (proxy->un.hash.buckets[bucket])
		proxy->un.hash.buckets[bucket]->header.prev = telemp;
	proxy->un.hash.buckets[bucket] = telemp;
}

int
stm_hash_elem_exists(pTHX_ sociable_proxy_t *proxy, char *key, int len)
{
	sociable_structure_t *privelemp = (sociable_structure_t *)stm_save_referent(aTHX_ proxy, 0);
	int bucket;
	int is_sociable;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->sociable;
	sociable_element_t *elemp;

	SOCIABLE_TRACE("in stm_hash_elem_exists\n");

	if (!privelemp)
		return NULL;

	elemp = stm_locate_hash_elem(privelemp, key, len, &bucket, &is_sociable);
	return (elemp != NULL);
/*
 *	NOTE we don't make a private clone...or should we ?
 */
}

SV *
stm_firstkey(pTHX_ sociable_proxy_t *proxy)
{
	sociable_structure_t *privelemp = (sociable_structure_t *)stm_save_referent(aTHX_ proxy, 0);
	sociable_element_t *elemp;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->sociable;
	int bucket;

	SOCIABLE_TRACE_1("in stm_firstkey for proxy %p\n", proxy);

	if (!privelemp)
		return NULL;

	for (bucket = 0; bucket < privelemp->un.hash.bucket_count; bucket++) {
		elemp = sociable->un.hash.buckets[bucket];
		while (elemp) {
			if (!stm_hash_elem_deleted(privelemp, elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen, bucket)) {
				if (!stm_hash_elem_accessed(privelemp, elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen, bucket))
					stm_hash_add_elem(aTHX_ privelemp, elemp, bucket);
				return newSVpv(elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen);
			}
			elemp = elemp->header.next;
		}
		elemp = privelemp->un.hash.buckets[bucket];
		while (elemp) {
/*
 *	only return new elements
 */
			if (!SOC_STM_WAS_ACCESSED(elemp)) {
				return newSVpv(elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen);
			}
			elemp = elemp->header.next;
		}
	}
	return &PL_sv_undef;
}

SV *
stm_nextkey(pTHX_ sociable_proxy_t *proxy, char *key, int len)
{
	sociable_structure_t *privelemp = (sociable_structure_t *)stm_save_referent(aTHX_ proxy, 0);
	sociable_element_t *elemp;
	sociable_structure_t *sociable = (sociable_structure_t *)proxy->sociable;
	int bucket;
	int is_sociable;

	SOCIABLE_TRACE_2("in stm_nextkey for proxy %p key %s\n", proxy, key);

	if (!privelemp)
		return NULL;

	elemp = stm_locate_hash_elem(privelemp, key, len, &bucket, &is_sociable);
	if (elemp == NULL) {
		SOC_SPIN_RELEASE(sociable);
		return NULL;
	}

	elemp = elemp->header.next;
	while (bucket < privelemp->un.hash.bucket_count) {
		if (is_sociable) {
			while (elemp) {
				if (!stm_hash_elem_deleted(privelemp, elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen, bucket)) {
					if (!stm_hash_elem_accessed(privelemp, elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen, bucket))
						stm_hash_add_elem(aTHX_ privelemp, elemp, bucket);
					SOC_SPIN_RELEASE(sociable);
					return newSVpv(elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen);
				}
				elemp = elemp->header.next;
			}
/*
 *	none in current sociable bucket, check private for new elements
 */
			is_sociable = 0;
			elemp = privelemp->un.hash.buckets[bucket];
		}
/*
 *	scan for 1st private in same bucket
 */
		while (elemp) {
			if (!stm_hash_elem_accessed(privelemp, elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen, bucket)) {
				SOC_SPIN_RELEASE(sociable);
				return newSVpv(elemp->hash_tuple.keyptr, elemp->hash_tuple.keylen);
			}
			elemp = elemp->header.next;
		}
/*
 *	nothing new in this bucket, move to next
 */
		bucket++;
		is_sociable = 1;
		if (bucket < privelemp->un.hash.bucket_count)
			elemp = sociable->un.hash.buckets[bucket];
	}
	SOC_SPIN_RELEASE(sociable);
	return &PL_sv_undef;
}
