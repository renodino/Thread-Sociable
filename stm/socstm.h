#ifndef _SOCSTM_H_

#define _SOCSTM_H_ 1

/*
 *	Sociable STM closure kinds
 */
#define SOC_STM_ROLLBACK (1)
#define SOC_STM_RESTART (2)
#define SOC_STM_COMMIT (4)

#define SOC_STM_RESTART_MSG "Thread::Sociable STM RESTART"

#define SOC_STM_LIMIT (100)
/*
 *	pseudo ptr value to indicate an unaccessed array element
 *	for STM
 */
#define SOC_STM_UNACCESSED "\377\377\377\377\377\377\377\377"
/*
 *	default extent for STM WAL and xaction array
 */
#define SOC_STM_EXTENT (10)

#define SOC_STM_VALID_READ(proxy) \
	((proxy->sociable.level == SOC_FOREIGN_MECH) \
		? (*proxy->mech->stm_valid_read(proxy)) \
		: ((proxy->elemp->header.header.soc_flags & SOC_INUSE) && \
			(proxy->elemp->header.header.cycleno == proxy->header.cycleno) && \
			(proxy->elemp->header.header.updcntlo == proxy->header.updcntlo) && \
			(proxy->elemp->header.header.updcnthi == proxy->header.updcnthi)))

#define SOC_STM_VALID_WRITE(proxy) \
	((proxy->sociable.level == SOC_FOREIGN_MECH) \
		? (*proxy->mech->stm_valid_write)(proxy) \
		: ((proxy->elemp->header.header.soc_flags & SOC_INUSE) && \
		(proxy->elemp->header.header.cycleno == proxy->header.cycleno) && \
		((!memcmp(&proxy->elemp->header.owner, &MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t))))

#define SOC_STM_VALID_ELEM(proxy) \
	((proxy->sociable.level == SOC_FOREIGN_MECH) \
		? (*proxy->mech->stm_valid_elem)(proxy) \
		: ((proxy->elemp->header.header.soc_flags & SOC_INUSE) && \
		(proxy->elemp->header.header.cycleno == proxy->header.cycleno) && \
		(proxy->elemp->header.header.updcntlo == proxy->header.updcntlo) && \
		(proxy->elemp->header.header.updcnthi == proxy->header.updcnthi))

#define SOC_STM_CAN_ACQUIRE(proxy) \
	((proxy->sociable.level == SOC_FOREIGN_MECH) \
		? (*proxy->mech->stm_can_acquire)(proxy, &MY_CXT.stm_ctxts[proxy->sociable.mechid]) \
		: ((!memcmp(&proxy->elemp->header.owner, &MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t))) || \
			(MY_CXT.stm_karma > ((*proxy->mech->loc_to_ptr)(&proxy->elemp->header.owner))->karma)))

#define NATIVE_CAN_ACQUIRE(proxy, owner) \
	((owner == NULL) || \
		(!memcmp(&proxy->shared.elemp->header.owner, &MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t))) || \
		(MY_CXT.stm_karma > owner->karma))

#define NATIVE_ELEM_CAN_ACQUIRE(elemp, owner) \
	((owner == NULL) || \
		(!memcmp(&(elemp)->header.owner, &MY_CXT.stm_ctxts[(elemp)->header.mechid].ctxt, sizeof(sociable_addr_t))) || \
		(MY_CXT.stm_karma > owner->karma))

#define NATIVE_VALID_READ(proxy) \
	((proxy->shared.elemp->header.header.soc_flags & SOC_INUSE) && \
		(proxy->priv.elemp->header.header.cycleno == proxy->shared.elemp->header.header.cycleno) && \
		(proxy->priv.elemp->header.header.updcntlo == proxy->shared.elemp->header.header.updcntlo) && \
		(proxy->priv.elemp->header.header.updcnthi == proxy->shared.elemp->header.header.updcnthi))

/*
	((proxy->elemp->header.header.soc_flags & SOC_INUSE) && \
		(proxy->elemp->header.header.cycleno == proxy->header.cycleno) && \
		(proxy->elemp->header.header.updcntlo == proxy->header.updcntlo) && \
		(proxy->elemp->header.header.updcnthi == proxy->header.updcnthi))
*/

#define NATIVE_VALID_ELEM_READ(priv, shared) \
	((shared->header.header.soc_flags & SOC_INUSE) && \
		(priv->header.header.cycleno == shared->header.header.cycleno) && \
		(priv->header.header.updcntlo == shared->header.header.updcntlo) && \
		(priv->header.header.updcnthi == shared->header.header.updcnthi))

#define NATIVE_VALID_WRITE(proxy) \
	((proxy->shared.elemp->header.header.soc_flags & SOC_INUSE) && \
		(proxy->shared.elemp->header.header.cycleno == proxy->priv.elemp->header.header.cycleno) && \
		(!memcmp(&proxy->shared.elemp->header.owner, &MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t))))
/*
	((proxy->elemp->header.header.soc_flags & SOC_INUSE) && \
		(proxy->elemp->header.header.cycleno == proxy->header.cycleno) && \
		(!memcmp(&proxy->elemp->header.owner, &MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t))))
*/

#define NATIVE_VALID_ELEM(proxy) \
	((proxy->elemp->header.header.soc_flags & SOC_INUSE) && \
		(proxy->elemp->header.header.cycleno == proxy->header.cycleno) && \
		(proxy->elemp->header.header.updcntlo == proxy->header.updcntlo) && \
		(proxy->elemp->header.header.updcnthi == proxy->header.updcnthi))

#define NATIVE_VALID_STRUCT_ELEM(privelemp, elemp) \
	((elemp->header.header.soc_flags & SOC_INUSE) && \
		(privelemp->header.header.cycleno == elemp->header.header.cycleno) && \
		(privelemp->header.header.updcntlo == elemp->header.header.updcntlo) && \
		(privelemp->header.header.updcnthi == elemp->header.header.updcnthi))

#define NATIVE_MAKE_OWNER(proxy) \
	memcpy(&proxy->shared.elemp->header.owner, &MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t))

#define NATIVE_AM_OWNER(proxy) \
	(!memcmp(&proxy->shared.elemp->header.owner, MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t)))

#define NATIVE_CLEAR_OWNER(proxy) \
	memzero(&proxy->shared.elemp->header.owner, sizeof(sociable_addr_t))

#define NATIVE_GET_OWNER(proxy) \
	((proxy->sociable.level == SOC_PROCESS_PRIVATE_MECH) \
		? proxy->elemp->header.owner.elemp \
		: (*proxy->mech->addr_to_ptr)(&proxy->elemp->header.owner))

#define NATIVE_ELEM_AM_OWNER(elemp) \
	(!memcmp(&elemp->header.owner, &MY_CXT.stm_ctxts[elemp->header.mechid].ctxt, sizeof(sociable_addr_t)))

#define NATIVE_ELEM_GET_OWNER(elem) \
	((elem->header.level == SOC_PROCESS_PRIVATE_MECH) \
		? elem->header.owner.elemp \
		: (*sociable_mechs[elem->header.mechid]->addr_to_ptr)(&elem->header.owner))

#define NATIVE_MAKE_ELEM_OWNER(elemp) \
	memcpy(&elemp->header.owner, &MY_CXT.stm_ctxts[elemp->header.mechid].ctxt, sizeof(sociable_addr_t))

#define NATIVE_GET_ARRAY_ELEM(elemp, proxy, structp, ary, i) \
 	if (structp->header.level == SOC_PROCESS_PRIVATE_MECH) { \
 		ary = (sociable_array_t *)structp->ary_ptr; \
	 	elemp = (sociable_element_t *)ary[i].elemp; \
	} \
	else { \
 		ary = (sociable_array_t *)(*proxy->mech->addr_to_ptr)(&structp->un.array.aryptr); \
 		elemp = (sociable_element_t *)(*proxy->mech->addr_to_ptr)(&ary[i]); \
	}

#define NATIVE_ARRAY_ELEM_PTR(proxy, elemp, i) \
	((proxy->sociable.level == SOC_PROCESS_PRIVATE_MECH) \
 		? elemp->ary_ptr[i].elemp \
 		: (*proxy->mech->addr_to_ptr)(&elemp->ary_ptr[i])

#define SOC_STM_IN_XACT (MY_CXT.stm_xact_count && (!MY_CXT.stm_reconciled))

#define SOC_IS_UNACCESSED(ary, i) \
	(!memcmp(&ary[i], SOC_STM_UNACCESSED, sizeof(sociable_addr_t)))

/*
 *	hierarchical karma mgmt:
 *		each transaction has a thread private context
 *		with a xaction log and karma value. Each
 *		initial access to sociable value causes
 *		thread-private karma increment, which is then
 *		transfered to each mech-specific xaction context,
 *		using either direct assignment (if a native mech)
 *		or by invoking a callback (e.g., for distributed
 *		xaction mgr)
 *
 *		When a xaction tests karma of another xaction,
 *		it only tests the xaction context for the same
 *		mech, again using either direct test (for native
 *		mechs) or a callback.
 *
 *		Note that karma updates/test for distributed
 *		elements will be expensive.
 *
 *		Also note that it may be desirable to implement
 *		a "push" scheme where the local xaction bumps
 *		the karma of an element owner if the local
 *		xaction is unable to acquire the element.
 *
 *		NOTE: should probably use ordered_mechs to order the updates
 */
#define SOC_BUMP_KARMA \
	{ \
		int i,j; \
		MY_CXT.stm_karma++; \
		for (i = 0, j = ordered_mechs[0]; i < sociable_next_mech; j = ordered_mechs[++i]) { \
			if (MY_CXT.stm_ctxts[j].WAL_count == 0) continue; \
			if (sociable_mechs[j]->level != SOC_FOREIGN_MECH) \
				MY_CXT.stm_ctxts[j].ctxtp->karma = MY_CXT.stm_karma; \
			else \
				(*sociable_mechs[j]->stm_update_karma)(); \
		} \
	}

#define SOC_TEST_KARMA(proxy) \
	(MY_CXT.stm_karma <= \
		((proxy->sociable.level != SOC_FOREIGN_MECH) \
			? MY_CXT.stm_ctxts[proxy->sociable.mechid]->karma \
			: (*proxy->mech->stm_get_karma)(MY_CXT.stm_ctxts[proxy->sociable.mechid])))

#define SOC_MAKE_OWNER(proxy) \
	if ((proxy->sociable.level != SOC_FOREIGN_MECH)) \
		memcpy(&proxy->elemp->header.owner, MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt, sizeof(sociable_addr_t)); \
	else \
		(*proxy->mech->stm_make_owner)(proxy, MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt)

#define SOC_CLEAR_OWNER(proxy) \
	if ((proxy->sociable.level != SOC_FOREIGN_MECH)) \
		memzero(&proxy->elemp->header.owner, sizeof(sociable_addr_t)); \
	else \
		(*proxy->mech->stm_clear_owner)(proxy)

#define SOC_AM_OWNER(proxy) \
	((proxy->sociable.level != SOC_FOREIGN_MECH) \
		? (!memcmp(&proxy->elemp->header.owner, MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt)) \
		: (*proxy->mech->stm_am_owner)(proxy, MY_CXT.stm_ctxts[proxy->sociable.mechid].ctxt))
/*
 *	indicator for unmapped array slots
 */
#define SOC_STM_UNMAPPED (-1)

typedef int (*STM_LOGGER_FUNC)(pTHX_ sociable_structure_t *proxy, int for_write) ;
/*
 *	conflict detector dispatcher
 */
typedef struct sociable_stm_detector {
	int (*acquire)(pTHX_ sociable_proxy_t *proxy, int for_write);
	int (*has_conflict)(pTHX_ sociable_stm_proxy_t *proxy, int for_write);
	int (*ary_elem_conflict)(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *structp, int key, int tkey, int for_write);
	int (*ary_splice_conflict)(pTHX_ sociable_proxy_t *proxy, sociable_structure_t *structp, int key, int len);
	int (*hash_elem_acquire)(pTHX_ sociable_proxy_t *sh, sociable_structure_t *sociable, int for_write);
	int (*hash_elem_conflict)(pTHX_ sociable_proxy_t *h, sociable_element_t *sh, int for_write);
} sociable_stm_detector_t;

sociable_element_t *stm_save_referent(pTHX_ sociable_proxy_t *proxy, int for_write);
int stm_array_fetch(pTHX_ sociable_proxy_t *proxy, I32 key, I32 for_write, sociable_proxy_t *valproxy);
sociable_element_t *native_stm_hash_lookup(pTHX_ sociable_proxy_t *proxy, char *key, I32 len, I32 for_write, sociable_proxy_t *valproxy);
int stm_array_delete(pTHX_ sociable_proxy_t *proxy, I32 key);
I32 native_stm_remove_hash_element(pTHX_ sociable_proxy_t *proxy, char *key, I32 len);
sociable_element_t *stm_hash_lookup(pTHX_ sociable_proxy_t *proxy, char *key, I32 len, I32 for_write, sociable_proxy_t *valproxy);
I32 stm_remove_hash_element(pTHX_ sociable_proxy_t *proxy, char *key, I32 len);
int stm_array_get_size(pTHX_ sociable_proxy_t *proxy);
int stm_array_clear(pTHX_ sociable_proxy_t *proxy);
int stm_hash_clear(pTHX_ sociable_proxy_t *proxy);
int stm_push(pTHX_ sociable_proxy_t *proxy, int listlen, SV **ary);
int stm_unshift(pTHX_ sociable_proxy_t *proxy, int listlen, SV **ary);
SV *stm_pop(pTHX_ sociable_proxy_t *proxy);
SV *stm_shift(pTHX_ sociable_proxy_t *proxy);
int stm_extend(pTHX_ sociable_proxy_t *proxy, int listlen);
int stm_storesize(pTHX_ sociable_proxy_t *proxy, int count);
int stm_array_elem_exists(pTHX_ sociable_proxy_t *proxy, int key);
int stm_hash_elem_exists(pTHX_ sociable_proxy_t *proxy, char *key, int len);
SV *stm_firstkey(pTHX_ sociable_proxy_t *proxy);
SV *stm_nextkey(pTHX_ sociable_proxy_t *proxy, char *key, int len);
SV *stm_splice(pTHX_ sociable_proxy_t *proxy, I32 items, I32 key, I32 len, I32 replace, SV **replacements);
void stm_collect_WALs(pTHX);
void stm_sort_WAL(pTHX_ pMY_CXT_ sociable_stm_log_t *wal, int level);
void stm_clear_all_logs(pTHX_ int reset_karma);
int stm_call_closures(pTHX_ pMY_CXT_ int kind);

extern int stm_commit_with_lock;
extern int stm_eager_detect;
extern int stm_timeout;


#endif