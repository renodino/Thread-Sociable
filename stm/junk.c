/*
 *	Transactionally add an (empty) element to a hash
 */
STATIC sociable_element_t *
stm_new_hash_element(pTHX_ sociable_structure_t *structp, char *key, I32 len)
{
	sociable_structure_t *sociable = (sociable_structure_t *)structp->sociable;
	sociable_hash_t *hash = &sociable->un.hash;
	sociable_element_t *hproxy = NULL;
	sociable_element_t *h = NULL;
	UV hashval = 0;
	I32 bucket;
	dMY_CXT;

	SOCIABLE_TRACE_1("in stm_new_hash_element for key %s\n", key);

	if (detector->has_conflict &&
		(*detector->has_conflict)(aTHX_ (sociable_element_t *)proxy, 1))
		return NULL;

	CRT_Newz(hproxy, 1, sociable_element_t);
	hproxy->header.soc_flags = SOC_INUSE | SOC_STM_PRIVATE;

	SOCIABLE_TRACE_1("stm__new_hash_element: got element %p\n", hproxy);

	proxy->header.soc_flags |= SOC_STM_WRITE;
	PERL_HASH(hashval, key, len);
	bucket = hashval % hash->bucket_count;
	hproxy->hash_tuple.hashval = hashval;

	CRT_Dup(hproxy->hash_tuple.keyptr, len + 1, char, key, len);
	hproxy->hash_tuple.keylen = len;
	if (hash->buckets[bucket])
		hash->buckets[bucket]->header.prev = hproxy;
	hproxy->header.next = hash->buckets[bucket];
	hproxy->header.prev = NULL;
	hash->buckets[bucket] = hproxy;
	hash->keycount++;
	SOCIABLE_TRACE_1("stm__new_hash_element: returning with key %s\n", h->hash_tuple.keyptr);
	return hproxy;
}

