#ifdef SOCIABLE_TIE
/*
 *	add a tie to a sociable
 */
void
sociable_add_tie(pTHX_ sociable_element_t *elemp, void * tsq, int tid, int proxyid)
{
	sociable_tie_t *tie = NULL;
	CRT_Newz(tie, 1, sociable_tie_t);
	tie->tsq = tsq;
	tie->tid = tid;
	tie->proxy_id = proxyid;
	ENTER_ELEM_LOCK(elemp);
	if (elemp->header.ties != NULL)
		tie->next = elemp->header.ties;
	elemp->header.ties = tie;
	LEAVE_ELEM_LOCK(elemp);
}

/*
 *	remove a tie from a sociable
 */
STATIC void
sociable_remove_tie(pTHX_ sociable_element_t *elemp, int tid)
{
	sociable_tie_t *prev = NULL;
	sociable_tie_t *tie = NULL;

	ENTER_ELEM_LOCK(elemp);
	prev = elemp->header.ties;
	if (prev && (prev->tid == tid)) {
		elemp->header.ties = prev->next;
		LEAVE_ELEM_LOCK(elemp);
		crt_free(prev);
		return;
	}
	while ((prev->next != NULL) && (prev->next->tid != tid))
		prev = prev->next;
	if ((prev->next != NULL) && (prev->next->tid == tid)) {
		tie = prev->next;
		prev->next = tie->next;
		crt_free(tie);
	}
	LEAVE_ELEM_LOCK(elemp);
}
#endif /* SOCIABLE_TIE */

#ifdef SOCIABLE_TIE

void
_sociable_tie(SV *ref, SV *tsq, int tid, int proxyid)
    PROTOTYPE: \[$@%]\[$@%]$$
    CODE:
    	sociable_element_t *elemp;
        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to _sociable_tie must be passed as ref");
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);

		elemp = sociable_find(aTHX_ ref);
		if (elemp == NULL)
            Perl_croak(aTHX_ "Argument to _sociable_tie must be sociable.");
	    SOCIABLE_TRACE_3("_sociable_tie: tying sociable %p to tid %d for proxy %d\n", elemp, tid, proxyid);
		/*
		 * remove any existing tie for the tid
		 */
	    sociable_remove_tie(aTHX_ elemp, tid);
	    sociable_add_tie(aTHX_ elemp, tsq, tid, proxyid);
        ST(0) = &PL_sv_yes;


void
_sociable_untie(SV *ref, int tid)
    PROTOTYPE: \[$@%]$
    CODE:
    	sociable_element_t *elemp;
        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to _sociable_untie must be passed as ref");
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);

		elemp = sociable_find(aTHX_ ref);
		if (elemp == NULL)
            Perl_croak(aTHX_ "Argument to _sociable_untie must be sociable.");
	    SOCIABLE_TRACE_2("_sociable_tie: untying sociable %p from tid %d\n", elemp, tid);
	    sociable_remove_tie(aTHX_ elemp, tid);
        ST(0) = &PL_sv_yes;

#endif /* SOCIABLE_TIE */

