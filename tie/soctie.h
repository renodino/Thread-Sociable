#ifndef _SOCTIE_H_

#define _SOCTIE_H_ 1

#ifdef SOCIABLE_TIE
/*
 * associates a TSQ, TID, and proxy id
 * with a sociable variable to implement
 * inter-thread ties; note this requires
 * both Thread::Sociable::Queue and
 * Thread::Sociable::Apartment
 */
typedef struct sociable_tie_t sociable_tie_t;
typedef struct sociable_tie_t {
	sociable_tie_t *next;				/* we permit multiple threads to tie, and execute each */
	void *tsq;
	int tid;
	int proxy_id;
} sociable_tie_t;
#endif

#endif