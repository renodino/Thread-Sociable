#define PERL_NO_GET_CONTEXT
#include "crt_memmgt.h"
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#ifdef HAS_PPPORT_H
#include "ppport.h"
#endif
#include "sociable.h"
#include "socstoremech.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "socqueue.h"

/*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!
 *!
 *!	BEGIN QUEUE METHODS
 *!
 *!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*! */
/*
 * Recover an SV from the sociable element into the supplied SV
 */
STATIC void
_recover_queue_element(pTHX_ AV *av, sociable_element_t *elemp)
{
	SV *csv = newSV(0);
	SV *vsv = newSV(0);
	SvUPGRADE(vsv, SvTYPE(&elemp->header.header));

    switch (SvTYPE(&elemp->header.header)) {
	    case SVt_NULL:
			sv_setsv(vsv, &PL_sv_undef);
			break;

	    case SVt_IV:
    		if (SvIsUV(&elemp->header.header)) {
    			sv_setuv(vsv, elemp->un.scalar.uv);
    		}
    		else {
    			sv_setiv(vsv, elemp->un.scalar.iv);
    		}
    		break;

	    case SVt_NV:
			sv_setnv(vsv, elemp->un.scalar.nv);
			break;

	    case SVt_PV:
			sv_setpvn(vsv, elemp->un.scalar.bufptr, elemp->un.scalar.buflen);
			break;

	    case SVt_RV:
/*
 *	what to do w/ refs ?
 */
//			SvRV_set(vsv, (SV*)pv);
			break;

		default:
			Perl_croak(aTHX_ "Unrecognized element type\n");

    }
/*
 *	if the element has a pkg_name and is transient, then
 *	recover via Storable (but how to call from here ?)
 */
 	if (SOC_IS_TRANSIENT(elemp)) {
 		if (elemp->header.pkg_name) {
			sv_setpv(csv,
				((strcmp(elemp->header.pkg_name, "SCALAR") &&
					strcmp(elemp->header.pkg_name, "ARRAY") &&
					strcmp(elemp->header.pkg_name, "HASH")) ?
					elemp->header.pkg_name :
					"Storable")
			);
		}
	}
	av_push(av, csv);
	av_push(av, vsv);
}
/*
 *	convert request/response packet into a private AV of SVs; each
 *	element is a 2 tuple of [ class, instance ], where class is undef
 *	for transient scalars and Sociables, "Storable" for other base transients,
 *	and a classname for transient objects
 */
STATIC SV *
_recover_packet(pTHX_ sociable_array_t *request)
{
	sociable_element_t *elemp = NULL;
	sociable_element_t **ary = request->aryptr;
	AV *av = newAV();
	int i;
/*
 *	make room for the id, plus [class, instance] tuples
 */
	av_extend(av, 1 + (request->aryhead << 1));
	av_push(av, (request->qid ? newSVuv(request->qid) : &PL_sv_undef));

	for (i = 0; i < request->aryhead; i++)
		_recover_queue_element(aTHX_ av, ary[i]);

	return newRV_inc((SV *)av);
}

STATIC void
_add_resp_packet(pTHX_ int i, sociable_array_t *qmap, HV *resps, sociable_struct_extent_t *extent)
{
	char idstr[40];
	sociable_structure_t **result = (sociable_structure_t **)SOC_SPLICE_RETURN(qmap, i, extent);
	sociable_structure_t *pkt = (sociable_structure_t *)qmap->aryptr[i];

	sprintf(idstr, "%d", pkt->ary_qid);
	hv_store(resps, idstr, strlen(idstr), _recover_packet(aTHX_ &result[0]->un.array), 0);
	crt_free(result);
}

STATIC HV *
_recover_packet_list(pTHX_ int *ready, int count, sociable_array_t *qmap, sociable_struct_extent_t *extent)
{
	int i, j;
	char idstr[40];
	sociable_structure_t **result;
	sociable_structure_t *pkt;
	HV *resps = newHV();

	for (i = 0; i < count; i++) {
		j = ready[i];
		pkt = (sociable_structure_t *)qmap->aryptr[j];
		sprintf(idstr, "%d", pkt->ary_qid);
		result = (sociable_structure_t **)SOC_SPLICE_RETURN(qmap, j, extent);
		hv_store(resps, idstr, strlen(idstr), _recover_packet(aTHX_ &result[0]->un.array), 0);
    	crt_free(result);
    }
    return resps;
}

/*
 *	create an empty queue packet with specified ID and flags,
 *	and pre-alloc qelems slots for packet contents
 */
STATIC sociable_structure_t *
_make_packet(pTHX_ int id, int flags, int qelems)
{
   	sociable_structure_t *pkt = NULL;
/*
 * create sociable array to hold ID, simplex/urgent flag, and the
 * packets contents; caller provides any special marshalling
 * before calling
 */
	pkt = sociable_struct_allocate(aTHX);
	pkt->header.sv_flags = SVt_PVAV;
	pkt->header.soc_flags |= SOC_INUSE;
	sociable_new_extent_array(aTHX_ pkt, qelems, qelems);

	if (qelems)
		sociable_extend_array(aTHX_ &pkt->un.array, qelems, NULL);
	pkt->ary_qid = id;
	pkt->ary_qflags = flags;
	return pkt;
}

/*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!
 *!
 *!	END QUEUE METHODS
 *!
 *!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*! */


MODULE = Thread::Sociable        PACKAGE = Thread::Sociable::DuplexQueue

PROTOTYPES: DISABLE

#/*
# *	constructor; uses sociable scalar as the object, and
# *	stores the queue context in the scalar PV pointer
# *	!!!NOTE: need to limit resource usage by providing queue type:
# *	simplex, duplex, or multiplex
# */
void
new_queue(SV *self, int listeners, int maxpending, int debug)
	CODE:
		sociable_queue_t *q;
		sociable_element_t *elemp;
		SV *qsv = NULL;

		if (!SvROK(self))
			Perl_croak(aTHX_ "Invalid object. Lockable scalar reference required.");

		qsv = SvRV(self);
		if (!SvROK(qsv))
			Perl_croak(aTHX_ "Invalid object. Lockable scalar reference required.");
		qsv = SvRV(qsv);
		elemp = sociable_find(aTHX_ qsv);
		if (!elemp)
			Perl_croak(aTHX_ "Invalid object. Lockable scalar reference required.");

		CRT_Newz(q, 1, sociable_queue_t);
		q->idgen = 1;
	    q->need_listener = listeners;
	    q->max_pending = maxpending;
	    if (!maxpending)
	    	maxpending = sociable_dflt_extent;
	    q->debug = debug;

		CRT_Newz(q->queue.aryptr, SOCIABLE_DFLT_ARRAYSZ, sociable_element_t*);
		q->queue.arysz = SOCIABLE_DFLT_ARRAYSZ;
		q->queue.aryhead = 0;

		CRT_Newz(q->map.aryptr, SOCIABLE_DFLT_ARRAYSZ, sociable_element_t*);
		q->map.arysz = SOCIABLE_DFLT_ARRAYSZ;
		q->map.aryhead = 0;
		/*
		 *	eventually provide tunable extents for the following
		 */
		CRT_Newz(q->marks.aryptr, SOCIABLE_DFLT_ARRAYSZ, sociable_element_t*);
		q->marks.arysz = SOCIABLE_DFLT_ARRAYSZ;
		q->marks.aryhead = 0;
#ifdef SOCIABLE_MULTIQ
		CRT_Newz(q->subscribers.buckets, sociable_hash_buckets, sociable_element_t*);
		q->subscribers.bucket_count = sociable_hash_buckets;

		CRT_Newz(q->sub_map.buckets, sociable_hash_buckets, sociable_element_t*);
		q->sub_map.bucket_count = sociable_hash_buckets;

		CRT_Newz(q->pending_map.buckets, sociable_hash_buckets, sociable_element_t*);
		q->pending_map.bucket_count = sociable_hash_buckets;

		CRT_Newz(q->sub_id_map.buckets, sociable_hash_buckets, sociable_element_t*);
		q->sub_id_map.bucket_count = sociable_hash_buckets;
		q->extent.extent = maxpending * 6;
#else
		q->extent.extent = maxpending * 3;
#endif
		q->extent.himark = q->extent.extent + 30;
		q->extent.pool = NULL;
		q->extent.pool_cnt = 0;

		elemp->un.queue = q;
		XSRETURN_YES;


void
free_queue(SV *self)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
		if (elemp)
			q = elemp->un.queue;
    	SOCIABLE_TRACE("sociable_free_queue: find queue\n");
		if (q) {
    		SOCIABLE_TRACE_1("sociable_free_queue: found queue %p, freeing\n", q);
			ENTER_ELEM_LOCK(elemp);
			q->inuse = 0;
			if (q->debug)
				printf("Freeing TSQ %p\n", q);
			sociable_array_clear(aTHX_ &q->queue);
			sociable_array_clear(aTHX_ &q->map);
			sociable_array_clear(aTHX_ &q->marks);
#ifdef SOCIABLE_MULTIQ
			sociable_hash_clear(aTHX_ &q->subscribers);
			sociable_hash_clear(aTHX_ &q->sub_map);
			sociable_hash_clear(aTHX_ &q->pending_map);
			sociable_hash_clear(aTHX_ &q->sub_id_map);
#endif
			LEAVE_ELEM_LOCK(elemp);
			crt_free(q);
        	self = SvRV(self);
        	if (SvROK(self))
        	    self = SvRV(self);
	    	SOCIABLE_TRACE("sociable_free_queue: destroying sociable\n");
        	sociable_destroy(aTHX_ elemp);
        	sociable_dissociate(aTHX_ self);
			XSRETURN_YES;
		}
		else {
    		SOCIABLE_TRACE("sociable_free_queue: queue not found\n");
			XSRETURN_UNDEF;
		}


void
_has_listener(SV *self)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
		if (elemp)
			q = elemp->un.queue;
		ST(0) = (q && q->listeners) ? sv_2mortal(newSViv(q->listeners)) : &PL_sv_undef;


void
listen(SV *self)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}
		ENTER_ELEM_LOCK(elemp);
		if (q->debug)
			printf("Listener registered on TSQ %p\n", q);
		q->listeners++;
		COND_BROADCAST(&elemp->header.lock.user_cond);	/* may need multiple locks per queue ? */
		LEAVE_ELEM_LOCK(elemp);
		XSRETURN_YES;


void
ignore(SV *self)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}
		ENTER_ELEM_LOCK(elemp);
		if (q->debug)
			printf("Listener registered on TSQ %p\n", q);
		if (q->listeners)
			q->listeners--;
		LEAVE_ELEM_LOCK(elemp);
		XSRETURN_YES;


void
_make_queue_pkt(SV *self, SV *svav, SV *svflags, SV *svqelems)
	CODE:
    	int id = 0;
    	int flags = SvIV(svflags);
    	int qelems = SvIV(svqelems);
    	sociable_element_t *elemp = NULL;
		sociable_queue_t *q = NULL;
    	sociable_structure_t *pkt = NULL;
    	AV *av = NULL;

        if ((! SvROK(self)) || (! SvROK(svav)))
            Perl_croak(aTHX_ "Argument to _make_queue_pkt needs to be passed as ref");
        self = SvRV(self);
        av = (AV *)SvRV(svav);

    	elemp = sociable_find(aTHX_ self);
		if ((!elemp) || (!elemp->header.pkg_name) ||
			strcmp(elemp->header.pkg_name, "Thread::Sociable::DuplexQueue"))
            Perl_croak(aTHX_ "Not a sociable queue.");

		q = elemp->un.queue;

		if (q) {
			/*
			 * create sociable array to hold ID, simplex/urgent flag, and the
			 * packets contents; caller provides any special marshalling
			 * before calling
			 */
			ENTER_ELEM_LOCK(elemp);
			id = q->idgen++;
			LEAVE_ELEM_LOCK(elemp);
		    pkt = _make_packet(aTHX_ id, flags, qelems);
		    sociable_associate(aTHX_ (SV *)av, (sociable_element_t *)pkt);
		}


void
_add_to_pkt(SV *svav, int i, SV *kind, SV *val)
	CODE:
    	sociable_structure_t *pkt = NULL;
    	sociable_element_t *valelemp;
    	AV *av = NULL;

        if (! SvROK(svav))
            Perl_croak(aTHX_ "Argument to _add_to_pkt needs to be passed as ref");
        av = (AV *)SvRV(av);

    	pkt = (sociable_structure_t *)sociable_find(aTHX_ (SV *)av);
		if (!pkt)
            Perl_croak(aTHX_ "Not a sociable packet.");

		valelemp = sociable_array_fetch(aTHX_ &pkt->un.array, pkt->ary_head, 1, &pkt->extent);
		valelemp->header.soc_flags = (sociable_find(aTHX_ val) == NULL) ?
			(valelemp->header.soc_flags | SOC_TRANSIENT) :
			(valelemp->header.soc_flags & (~SOC_TRANSIENT));

		if (SvOK(kind)) {
			CRT_Dup(valelemp->header.pkg_name, SvCUR(kind) + 1, char, SvPV_nolen(kind), SvCUR(kind));
		}
		sociable_scalar_store(aTHX_ val, valelemp);


void
_enqueue(SV *self, SV *pkt)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_element_t *valelemp = NULL;
    	sociable_structure_t *pktelemp = (sociable_structure_t *)sociable_find(aTHX_ pkt);
    	I32 id;
    	I32 urgent = 0;
    	I32 n;

		if (elemp)
			q = elemp->un.queue;

		if (q && ((!q->need_listener) || q->listeners)) {
	        id = pktelemp->ary_qid;

			ENTER_ELEM_LOCK(elemp);
			/*
			 *	check current length if we have a limit
			 */
			while (q->max_pending && (q->max_pending < q->queue.aryhead)) {
				COND_WAIT(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex);
			}

			urgent = pktelemp->ary_qflags & TSDQ_URGENT;
			n = urgent ? 0 : q->queue.aryhead;
			if (urgent)
				sociable_unshift_array(aTHX_ &q->queue, 1);

			valelemp = sociable_array_fetch(aTHX_ &q->queue, n, 1, &q->extent);

			sociable_scalar_store(aTHX_ pkt, valelemp);
    		COND_SIGNAL(&elemp->header.lock.lock.cond);
			LEAVE_ELEM_LOCK(elemp);
			/*
			 *	notify any global waiters
			 */
			ENTER_TSDQ_LOCK;
			COND_BROADCAST(&tsdq_global_lock.cond);
			LEAVE_TSDQ_LOCK;

			ST(0) = sv_2mortal(newSViv(id));
		}
		else {
			ST(0) = &PL_sv_undef;
		}



void
_dequeue(SV *self, double timeout, int urgent)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_structure_t *request = NULL;
    	sociable_element_t *id = NULL;
    	sociable_element_t **qary = NULL;
    	sociable_array_t *qmap = NULL;
    	int i;

		if (elemp)
			q = elemp->un.queue;

		if (!q) {
			XSRETURN_UNDEF;
		}

		qary = q->queue.aryptr;
		qmap = &q->map;
	    while (1) {
			ENTER_ELEM_LOCK(elemp);
			if ((!(timeout || q->queue.aryhead)) ||
				(urgent && (!q->urgent_count))) {
				LEAVE_ELEM_LOCK(elemp);
				XSRETURN_UNDEF;
			}
			/*
			 *	NOTE: we need to do timedwait if we have a timeout
			 *	but we need a way to read time()
			 */
			while (!q->queue.aryhead)
   				COND_WAIT(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex);

	    	request = (sociable_structure_t *)qary[0];
	    	SOCIABLE_TRACE_3("Thread::Sociable::DuplexQueue::dequeue: request is %p ary_head is %d ary is %p\n", request, q->queue.aryhead, q->queue.aryptr);
        	q->queue.aryhead--;
        	for (i = 0; i < q->queue.aryhead; qary[i] = qary[i+1], i++);
        	qary[q->queue.aryhead] = NULL;

			/*
			 * cancelled request ?
			 */
			if (request == NULL) {
				LEAVE_ELEM_LOCK(elemp);
				continue;
			}

   			if (q->urgent_count)
   				q->urgent_count--;
			LEAVE_ELEM_LOCK(elemp);
			/*
			 *	signal any waiters
			 */
			ENTER_ELEM_LOCK(elemp);
   			COND_BROADCAST(&elemp->header.lock.user_cond);
			LEAVE_ELEM_LOCK(elemp);
   			break;
   		}	/* end while forever */
   		/*
   		 *	now process the request packet back into a private AV of SVs
   		 */
   		ST(0) = _recover_packet(aTHX_ &request->un.array);
   		/*
		 *	destroy the entire packet
		 *	NOTE: may not do this for multiplex!
		 *	NOTE2: need to make sure any sociable refs in the array
		 *	don't get destroyed
		 */
		sociable_destroy(aTHX_ (sociable_element_t *)request);


void
_respond(SV *self, SV *response, int id)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_element_t *resp = sociable_find(aTHX_ response);
    	sociable_element_t *valelemp = NULL;
    	sociable_structure_t *pkt = NULL;
    	sociable_array_t *qary = NULL;
    	sociable_array_t *qmap = NULL;
    	sociable_array_t *qmarks = NULL;
    	int i;

		if (elemp)
			q = elemp->un.queue;

		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		qmarks = &q->marks;
		SOC_FIND_Q_PKT(qmarks, i, pkt, id);
		if (i < qmarks->aryhead)
			SOC_SPLICE_REMOVE(qmarks, i, &q->extent);
		/*
		 *	check if its been canceled
		 */
		qmap = &q->map;
		SOC_FIND_Q_PKT(qmap, i, pkt, id);
		if (i < qmarks->aryhead) {
			SOC_SPLICE_REMOVE(qmap, i, &q->extent);
	 	    COND_BROADCAST(&elemp->header.lock.user_cond);
			LEAVE_ELEM_LOCK(elemp);
			XSRETURN_UNDEF;
		}

		valelemp = sociable_array_fetch(aTHX_ qmap, qmap->aryhead, 1, &q->extent);
		sociable_scalar_store(aTHX_ response, valelemp);
 	    COND_BROADCAST(&elemp->header.lock.user_cond);
		LEAVE_ELEM_LOCK(elemp);

		ENTER_TSDQ_LOCK;
		COND_BROADCAST(&tsdq_global_lock.cond);
		LEAVE_TSDQ_LOCK;
		ST(0) = &PL_sv_yes;


void
queue_extent(SV *self, int extent, int himark)
    CODE:
    	I32 old_extent = 0;
    	I32 old_himark = 0;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
		sociable_queue_t *q = NULL;

		if (elemp)
			q = elemp->un.queue;

		if (!q) {
			XSRETURN_UNDEF;
		}
	    SOCIABLE_TRACE_1("sociable_extent: returning extents for elemp %p\n", elemp);
		old_extent = q->extent.extent;
		old_himark = q->extent.himark;
		EXTEND(SP, 2);
	 	PUSHs(sv_2mortal(newSViv(old_extent)));
	 	PUSHs(sv_2mortal(newSViv(old_himark)));
	 	if (extent < 0) {
			if ((extent != -1) && sociable_warn)
				Perl_warn(aTHX_ "Invalid extent specified.");
			XSRETURN(2);
		}

		if ((!extent) && himark)
			himark = 0;

		if (extent && (himark <= extent)) {
			if ((himark != -1) && sociable_warn)
				Perl_warn(aTHX_ "Invalid himark specified.");
			himark = (I32)(extent * 1.10);
	   		if (himark - extent < 10)
	   			himark = extent + 10;
		}
		q->extent.extent = extent;
		q->extent.himark = himark;
		if (himark < q->extent.pool_cnt)
			sociable_trim_extent(aTHX_ &q->extent);
		XSRETURN(2);


void
pending(SV *self)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_element_t **ary = NULL;
    	sociable_array_t *qmap = NULL;
    	sociable_structure_t *pkt = NULL;
    	int i;
    	int count = 0;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}
		ENTER_ELEM_LOCK(elemp);
		/*
		 *	only report uncancelled requests
		 */
		ary = q->queue.aryptr;
		count = q->queue.aryhead;
		for (i = 0; i < q->queue.aryhead; i++) {
			if (ary[i] == NULL) {
				count--;
				continue;
			}
			qmap = &q->map;
			for (i = 0; i < qmap->aryhead; i++) {
				pkt = (sociable_structure_t *)qmap->aryptr[i];
				if (!pkt->ary_head)
					count--;
			}
		}
		LEAVE_ELEM_LOCK(elemp);
		ST(0) = sv_2mortal(newSViv(count));



void
max_pending(SV *self, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	I32 prev = 0;
		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}
		ENTER_ELEM_LOCK(elemp);
		prev = q->max_pending;
		if (items > 1) {
			if (SvIOK(ST(1))) {
				q->max_pending = SvIV(ST(1));
				/*
				 *	wake up anyone whos been waiting for queue to change
				 */
				COND_BROADCAST(&elemp->header.lock.user_cond);
			}
		}
		LEAVE_ELEM_LOCK(elemp);
		ST(0) = sv_2mortal(newSViv(prev));


void
available(SV *self, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *ary = NULL;
    	sociable_structure_t *pkt;
    	I32 id = 0;
    	int i, j;
    	AV *ids = newAV();
    	I32 *pktids = NULL;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		ary = &q->map;
		if (items == 1) {
			for (i = 0; i < ary->aryhead; i++) {
				pkt = (sociable_structure_t *)ary->aryptr[i];
				if (pkt->ary_qid <= 0)
					continue;
 				av_push(ids, sv_2mortal(newSViv(pkt->ary_qid)));
 			}
		}
		else {
			CRT_Newz(pktids, items, I32);
			for (j = 1; j < items; j++) {
				if (SvIOK(ST(j)))
					pktids[j-1] = SvIV(ST(j));
			}
			for (i = 0; i < ary->aryhead; i++) {
				for (j = 1; j < items; j++) {
					pkt = (sociable_structure_t *)ary->aryptr[i];
					if (pktids[j-1] == pkt->ary_qid)
 						av_push(ids, sv_2mortal(newSViv(pktids[j-1])));
	 			}
	 		}
	 		crt_free(pktids);
		}
		if (av_len(ids)) {
			if (GIMME_V == G_ARRAY) {
				ST(0) = newRV((SV *)ids);
			}
			else {
				ST(0) = newSViv(id);
			}
		}
		else
			ST(0) = &PL_sv_undef;
		LEAVE_ELEM_LOCK(elemp);


void
ready(SV *self, int id)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *ary = NULL;
    	sociable_structure_t *pkt;
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		ary = &q->map;
		SOC_FIND_Q_PKT(ary, i, pkt, id);
 		if (i < ary->aryhead)
 			ST(0) = &PL_sv_yes;
		else
			ST(0) = &PL_sv_undef;
		LEAVE_ELEM_LOCK(elemp);


void
_wait(SV *self, int id)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *ary = NULL;
    	sociable_structure_t **result = NULL;
    	sociable_structure_t *pkt;
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		ary = &q->map;
		while (1) {
			SOC_FIND_Q_PKT(ary, i, pkt, id);
	 		if (i < ary->aryhead)
	 			break;
 		    COND_WAIT(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex);
		}
		result = (sociable_structure_t **)SOC_SPLICE_RETURN(ary, i, &q->extent);
		if (ary->aryhead)
     		COND_SIGNAL(&elemp->header.lock.lock.cond);

		LEAVE_ELEM_LOCK(elemp);
		/*
		 *	now filter and return the response
		 */
    	ST(0) = _recover_packet(aTHX_ &result[0]->un.array);
   		/*
		 *	destroy the entire packet
		 *	NOTE: may not do this for multiplex!
		 *	NOTE2: need to make sure any sociable refs in the array
		 *	don't get destroyed
		 */
		sociable_destroy(aTHX_ (sociable_element_t *)result[0]);
    	crt_free(result);


void
_wait_until(SV *self, int id, double timeout, int tid)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *qmap = NULL;
    	sociable_structure_t **result = NULL;
    	sociable_structure_t *pkt;
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		qmap = &q->map;
		timeout += _hires_time();

 		while (timeout > _hires_time()) {
 			ENTER_ELEM_LOCK(elemp);

			SOC_FIND_Q_PKT(qmap, i, pkt, id);
			if (i < qmap->aryhead) {
				result = (sociable_structure_t **)SOC_SPLICE_RETURN(qmap, i, &q->extent);
				if (qmap->aryhead)
		     		COND_SIGNAL(&elemp->header.lock.lock.cond);

				LEAVE_ELEM_LOCK(elemp);
			/*
			 *	now filter and return the response
			 */
		    	ST(0) = _recover_packet(aTHX_ &result[0]->un.array);
		    	crt_free(result);
				XSRETURN(1);
			}
			sociable_cond_timedwait(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex, timeout);
	 	}
	    ST(0) = &PL_sv_undef;


void
_wait_any(SV *self, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *qmap = NULL;
    	sociable_structure_t *pkt;
    	int i, j;
    	int count = 0;
    	HV *resps = newHV();
		int *ids = NULL;
		int idcnt = items - 1;

		if (elemp)
			q = elemp->un.queue;
		if ((!q) || (items == 1)) {
			XSRETURN_UNDEF;
		}

		qmap = &q->map;

		CRT_Newz(ids, idcnt, int);
		for (i = 1; i < items; i++)
			ids[i-1] = SvIV(ST(i));

		while (!count) {
			ENTER_ELEM_LOCK(elemp);
			while (!qmap->aryhead)
				COND_WAIT(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex);

			for (count = 0, i = 0; (i < qmap->aryhead); i++) {
			/*
			 *	only collect actual response packets, leaving cancel
			 *	packets in place so that listeners can cancel them
			 */
				pkt = (sociable_structure_t *)qmap->aryptr[i];
				if (pkt && pkt->ary_head) {
					for (j = 0; (j < idcnt) && (ids[j] != pkt->ary_qid); j++);
					if (j < idcnt) {
						_add_resp_packet(aTHX_ i, qmap, resps, &q->extent);
						count++;
						/*
						 * trim the id list
						 */
						ids[j] = ids[idcnt - 1];
						idcnt--;
					}
				}
			}
			if (count)
				COND_SIGNAL(&elemp->header.lock.lock.cond);
			LEAVE_ELEM_LOCK(elemp);
		}
		crt_free(ids);
		ST(0) = (SV *)resps;


void
_wait_any_until(SV *self, double timeout, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *qmap = NULL;
    	sociable_structure_t *pkt;
    	int i, j;
    	int count = 0;
    	HV *resps = newHV();
		int *ids = NULL;
		int idcnt = items - 2;

		if (elemp)
			q = elemp->un.queue;
		if ((!q) || (items == 2)) {
			XSRETURN_UNDEF;
		}

		qmap = &q->map;

		CRT_Newz(ids, idcnt, int);
		for (i = 2; i < items; i++)
			ids[i-2] = SvIV(ST(i));

		while ((!count) && (timeout > _hires_time())) {
			ENTER_ELEM_LOCK(elemp);
			while ((!qmap->aryhead) && (timeout > _hires_time()))
				sociable_cond_timedwait(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex, timeout);

			if (timeout <= _hires_time()) {
				LEAVE_ELEM_LOCK(elemp);
				crt_free(ids);
				XSRETURN_UNDEF;
			}


			for (count = 0, i = 0; (i < qmap->aryhead); i++) {
			/*
			 *	only collect actual response packets, leaving cancel
			 *	packets in place so that listeners can cancel them
			 */
				pkt = (sociable_structure_t *)qmap->aryptr[i];
				if (pkt && pkt->ary_head) {
					for (j = 0; (j < idcnt) && (ids[j] != pkt->ary_qid); j++);
					if (j < idcnt) {
						_add_resp_packet(aTHX_ i, qmap, resps, &q->extent);
						count++;
						/*
						 * trim the id list
						 */
						ids[j] = ids[idcnt - 1];
						idcnt--;
					}
				}
			}
			if (count)
				COND_SIGNAL(&elemp->header.lock.lock.cond);
			LEAVE_ELEM_LOCK(elemp);
		}
		crt_free(ids);
		ST(0) = (SV *)resps;


void
_wait_all(SV *self, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *qmap = NULL;
    	sociable_structure_t *pkt;
    	int i, j;
    	int count = items - 1;
    	HV *resps;
		int *ids = NULL;
		int *ready = NULL;
		int done = 0;

		if (elemp)
			q = elemp->un.queue;
		if ((!q) || (items == 1)) {
			XSRETURN_UNDEF;
		}

		qmap = &q->map;

		CRT_Newz(ready, count, int);
		CRT_Newz(ids, count, int);
		for (i = 1; i < items; i++)
			ids[i-1] = SvIV(ST(i));
		/*
		 *	possible issue: if someone either cancels a request,
		 *	or removes a response from our ID list, we will hang forever here
		 */
		ENTER_ELEM_LOCK(elemp);
		while (done < count) {
			while (!qmap->aryhead)
				COND_WAIT(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex);
			/*
			 *	reset ready list
			 */
			for (i = 0; i < count; ready[i++] = 0);

			for (done = 0, i = 0; (i < qmap->aryhead) && (done < count); i++) {
			/*
			 *	only collect actual response packets, leaving cancel
			 *	packets in place so that listeners can cancel them
			 */
				pkt = (sociable_structure_t *)qmap->aryptr[i];
				if (pkt && pkt->ary_head) {
					for (j = 0; (j < count) && (ids[j] != pkt->ary_qid); j++);
					if (j < count)
						ready[done++] = i;
				}
			}
		}
		/*
		 * now recover the packets
		 */
		resps = _recover_packet_list(aTHX_ ready, count, qmap, &q->extent);
		COND_SIGNAL(&elemp->header.lock.lock.cond);
		LEAVE_ELEM_LOCK(elemp);
		crt_free(ids);
		crt_free(ready);
		ST(0) = (SV *)resps;


void
_wait_all_until(SV *self, double timeout, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *qmap = NULL;
    	sociable_structure_t *pkt;
    	int i, j;
    	int count = items - 2;
    	HV *resps;
		int *ids = NULL;
		int *ready = NULL;
		int done = 0;

		if (elemp)
			q = elemp->un.queue;
		if ((!q) || (items == 2)) {
			XSRETURN_UNDEF;
		}

		qmap = &q->map;

		CRT_Newz(ids, count, int);
		CRT_Newz(ready, count, int);
		for (i = 2; i < items; i++)
			ids[i-2] = SvIV(ST(i));

		ENTER_ELEM_LOCK(elemp);
		while ((done < count) && (timeout > _hires_time())) {
			while ((!qmap->aryhead) && (timeout > _hires_time()))
				sociable_cond_timedwait(&elemp->header.lock.user_cond, &elemp->header.lock.lock.mutex, timeout);

			if (timeout <= _hires_time()) {
				LEAVE_ELEM_LOCK(elemp);
				crt_free(ids);
				crt_free(ready);
				XSRETURN_UNDEF;
			}
			/*
			 *	reset ready list
			 */
			for (i = 0; i < count; ready[i++] = 0);

			for (done = 0, i = 0; (i < qmap->aryhead) && (done < count); i++) {
			/*
			 *	only collect actual response packets, leaving cancel
			 *	packets in place so that listeners can cancel them
			 */
				pkt = (sociable_structure_t *)qmap->aryptr[i];
				if (pkt && pkt->ary_head) {
					for (j = 0; (j < count) && (ids[j] != pkt->ary_qid); j++);
					/*
					 * don't remove packets until we've collected all
					 */
					if (j < count)
						ready[done++] = i;
				}
			}
			if (done)
				COND_SIGNAL(&elemp->header.lock.lock.cond);
		}
		/*
		 * now recover the packets
		 */
		resps = _recover_packet_list(aTHX_ ready, count, qmap, &q->extent);
		COND_SIGNAL(&elemp->header.lock.lock.cond);
		LEAVE_ELEM_LOCK(elemp);
		crt_free(ids);
		crt_free(ready);
		ST(0) = (SV *)resps;


void
mark(SV *self, int id, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_element_t *valelemp = NULL;
    	sociable_array_t *qmap = NULL;
    	sociable_array_t *qmarks = NULL;
    	sociable_structure_t *pkt;
    	SV *value = (items > 2) ? ST(2) : newSViv(1);
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		/*
		 *	already responded or cancelled
		 */
		qmarks = &q->marks;
		qmap = &q->map;
		SOC_FIND_Q_PKT(qmap, i, pkt, id);
		if (i < qmap->aryhead) {
		/*
		 *	already cancelled or responded to
		 */
			LEAVE_ELEM_LOCK(elemp);
			XSRETURN_UNDEF;
		}
		SOC_FIND_Q_PKT(qmarks, i, pkt, id);
		/*
		 *	already marked, update element
		 */
		valelemp = (i < qmarks->aryhead) ?
			sociable_array_fetch(aTHX_ qmarks, i, 0, &q->extent) :
			sociable_array_fetch(aTHX_ qmarks, qmarks->aryhead, 1, &q->extent);

		sociable_scalar_store(aTHX_ value, valelemp);
		LEAVE_ELEM_LOCK(elemp);


void
unmark(SV *self, int id)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *qmarks = NULL;
    	sociable_structure_t *pkt;
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		qmarks = &q->marks;
		SOC_FIND_Q_PKT(qmarks, i, pkt, id);
		if (i < qmarks->aryhead)
			SOC_SPLICE_REMOVE(qmarks, i, &q->extent);
		LEAVE_ELEM_LOCK(elemp);


void
get_mark(SV *self, int id)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_structure_t **valelemp = NULL;
    	sociable_array_t *qmarks = NULL;
    	sociable_structure_t *pkt;
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		qmarks = &q->marks;
		SOC_FIND_Q_PKT(qmarks, i, pkt, id);
		if (i >= qmarks->aryhead) {
			LEAVE_ELEM_LOCK(elemp);
			XSRETURN_UNDEF;
		}
		valelemp = (sociable_structure_t **)SOC_SPLICE_RETURN(qmarks, i, &q->extent);
		LEAVE_ELEM_LOCK(elemp);
		ST(0) = _recover_packet(aTHX_ &valelemp[0]->un.array);
		crt_free(valelemp);


void
marked(SV *self, int id, ... )
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_element_t *valelemp = NULL;
    	sociable_array_t *qmarks = NULL;
    	sociable_structure_t *pkt;
    	SV *value = (items > 2) ? ST(2) : NULL;
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		qmarks = &q->marks;
		SOC_FIND_Q_PKT(qmarks, i, pkt, id);
		if (i >= qmarks->aryhead) {
			LEAVE_ELEM_LOCK(elemp);
			XSRETURN_UNDEF;
		}
		if (!value) {
			LEAVE_ELEM_LOCK(elemp);
			XSRETURN_YES;
		}
		valelemp = sociable_array_fetch(aTHX_ qmarks, i, 0, &q->extent);
		LEAVE_ELEM_LOCK(elemp);
		ST(0) =
			(
/* !!! order is important, unsigned first */
			(SvUOK(value) && SvUOK(&valelemp->header) && (SvUV(value) == valelemp->un.scalar.uv)) ||
			(SvIOK(value) && SvIOK(&valelemp->header) && (SvIV(value) == valelemp->un.scalar.iv)) ||
			(SvNOK(value) && SvNOK(&valelemp->header) && (SvNV(value) == valelemp->un.scalar.nv)) ||
			((SvCUR(value) == valelemp->un.scalar.buflen) &&
				(!strcmp(SvPV_nolen(value), valelemp->un.scalar.bufptr)))) ?
			&PL_sv_yes :
			&PL_sv_undef;


void
_cancel(SV *self, ...)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *queue = NULL;
    	sociable_array_t *qmap = NULL;
    	sociable_array_t *qmarks = NULL;
    	sociable_structure_t *pkt;
    	SV *value = (items > 1) ? ST(2) : newSViv(1);
    	AV *resps = newAV();
    	int i, j;
    	int id;

		if (items == 1) {
			XSRETURN_UNDEF;
		}

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}

		ENTER_ELEM_LOCK(elemp);
		queue = &q->queue;
		qmarks = &q->marks;
		qmap = &q->map;
		for (i = 1; i < items; i++) {
			id = SvIV(ST(i));
			/*
			 *	remove its marks (if any)
			 */
			SOC_FIND_Q_PKT(qmarks, j, pkt, id);
			if (j < qmarks->aryhead)
				SOC_SPLICE_REMOVE(qmarks, j, &q->extent);
			/*
			 *	check if still pending
			 */
			SOC_FIND_Q_PKT(queue, j, pkt, id);
			if (j < queue->aryhead) {
			/*
			 *	still pending, replace slot with NULL and free the request
			 *	NOTE: this is NOT a splice!
			 */
				pkt = (sociable_structure_t *)sociable_array_fetch(aTHX_ queue, j, 0, &q->extent);
				queue->aryptr[j] = NULL;
				sociable_destroy(aTHX_ (sociable_element_t *)pkt);
				continue;
			}
			/*
			 *	check if already responded to
			 */
			SOC_FIND_Q_PKT(qmap, j, pkt, id);
			if (j == qmap->aryhead) {
			/*
			 *	in progress, add empty packet to map
			 */
				pkt = sociable_struct_allocate(aTHX);
				pkt->header.sv_flags = SVt_PVAV;
				pkt->header.soc_flags = SOC_INUSE;
				pkt->ary_qid = id;
				pkt->ary_qflags = 0;
				pkt->ary_head = 0;
				sociable_extend_array(aTHX_ qmap, qmap->aryhead + 1, NULL);
				qmap->aryptr[qmap->aryhead - 1] = (sociable_element_t *)pkt;
				continue;
			}
			/*
			 * already responded to, add id to response list
			 */
			av_push(resps, sv_2mortal(newSViv(id)));
		}
		LEAVE_ELEM_LOCK(elemp);
		ST(0) = (SV *)resps;


void
_cancel_all(SV *self)
	CODE:
		sociable_queue_t *q = NULL;
    	sociable_element_t *elemp = sociable_find(aTHX_ self);
    	sociable_array_t *qmap = NULL;
    	sociable_structure_t *pkt = NULL;
    	AV *resps = newAV();
    	int i;

		if (elemp)
			q = elemp->un.queue;
		if (!q) {
			XSRETURN_UNDEF;
		}
		ENTER_ELEM_LOCK(elemp);
		qmap = &q->map;
		/*
		 *	remove all marks
		 */
		sociable_array_clear(aTHX_ &q->marks);
		/*
		 *	remove all pending entries
		 */
		sociable_array_clear(aTHX_ &q->queue);
		/*
		 *	collect IDs of all pending responses;
		 *	NOTE: we go ahead and include the cancelled entries too,
		 *	let the Perl part handle them
		 */
		for (i = 0; (i < qmap->aryhead); i++) {
			pkt = (sociable_structure_t *)qmap->aryptr[i];
			av_push(resps, sv_2mortal(newSViv(pkt->ary_qid)));
		}
		LEAVE_ELEM_LOCK(elemp);
		ST(0) = (SV *)resps;


#/*
# *	method to permit global lock manipulation from Perl
# *	(clumsy, yes, but effective)
# */
void
_enter_tsdq_lock()
	CODE:
	ENTER_TSDQ_LOCK;
	ST(0) = &PL_sv_yes;


void
_leave_tsdq_lock()
	CODE:
	LEAVE_TSDQ_LOCK;
	ST(0) = &PL_sv_yes;

