#ifndef _SOCQUEUE_H_

#define _SOCQUEUE_H_ 1

/*
 *	structures for Thread::Sociable::DuplexQueue
 */

typedef struct sociable_queue {
	U32 				idgen;			/* msg id generator */
	I32					listeners;		/* number of listeners */
	bool				need_listener;	/* true => listener required to enqueue */
	bool				debug;			/* true => emit copious noise! */
	bool				inuse;			/* true => queue is valid */
	I32					max_pending;	/* max number of pending messages */
	I32					urgent_count;	/* number of urgent msgs */
	sociable_array_t	queue;			/* array to hold queue elements */
	sociable_array_t	map;			/* map to manage duplex/multiplex resps */
	sociable_array_t	marks;			/* hash of marks on q'd msgs */
#ifdef SOCIABLE_MULTIQ
	sociable_hash_t		subscribers;	/* map of subscribers */
	sociable_hash_t		sub_map;		/* map of pending msgs to subscribers */
	sociable_hash_t		pending_map;	/* map of pending responses from subscribers */
	sociable_hash_t		sub_id_map;		/* map of subscriber TID's to id's */
#endif
	sociable_struct_extent_t extent;	/* global extent covers all the embedded arrays/hashes */
} sociable_queue_t;
/*
 *	flags for special msgs
 */
#define TSDQ_URGENT (1)
#define TSDQ_SIMPLEX (2)
#define TSDQ_URGENT_SIMPLEX (3)
#define TSDQ_FIRST_ONLY (-1)

#define q_head un.queue.aryhead
#define q_tail un.queue.tail
#define q_size un.queue.arysz
#define q_ptr un.queue.aryptr
#define q_qid un.queue.qid
#define q_flags un.queue.qflags
#define q_extent extent.extent
#define q_himark extent.himark
#define q_pool extent.pool
#define q_pool_cnt extent.pool_cnt

#define SOC_FIND_Q_PKT(list, idx, pkt, id) \
	for (idx = 0; (idx < list->aryhead); idx++) { \
		pkt = (sociable_structure_t *)list->aryptr[idx]; \
		if (pkt->un.array.qid == id) break; \
	}

#endif