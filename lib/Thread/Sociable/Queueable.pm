#/**
# Abstract base class defining the interfaces, and providing
# simple marshalling methods, for complex object to be passed
# across a <a href='./Queue.html'>Thread::Sociable::Queue</a>
# queue.
# <p>
# Licensed under the Academic Free License version 2.1, as specified in the
# License.txt file included in this software package, or at
# <a href="http://www.opensource.org/licenses/afl-2.1.php">OpenSource.org</a>.
#
# @author D. Arnold
# @since 2005-12-01
# @self	$obj
#*/
package Thread::Sociable::Queueable;
#
#	abstract class to permit an object to be
#	marshalled in some way before pushing onto
#	a Thread::Sociable::Queue queue
#
require 5.008;

use threads;
use Thread::Sociable;

use strict;
use warnings;

our $VERSION = '1.01';

#/**
# Marshal an object for queueing to a <A href='./Queue.html'>Thread::Sociable::Queue</a>
# queue. Called by any of TSQ's <A href='./Queue.html#enqueue'>enqueue()</a> methods,
# as well as <A href='./Queue.html#respond'>respond()</a> method.
# <p>
# The default implementation <A href='#curse>curse()'s</a> the input
# object into either a sociable array or sociable hash (depending on the base structure
# of the object), and returns a list consisting of the object's class name, and the cursed object.
#
# @returnlist	list of (object's class, object's marshalled representation)
#*/
sub onEnqueue {
	my $obj = shift;
#
#	capture class name, and create cursed
#	version of object
#
	return (ref $obj, $obj->curse());
}

#/**
# Unmarshall an object after being dequeued. Called by any of TSQ's
# <a href='./Queue.html#dequeue'>dequeue()</a> methods,
# as well as the various request side dequeueing
# methods (e.g., <a href='./Queue.html#wait'>wait()</a>).
# <p>
# The default implementation <a href='#redeem'>redeem()'s</a> the input object
# to copy the input sociable arrayref or hashref into a nonsociable equivalent, then
# blessing it into the specified class, returning the redeemed object.
#
# @param $object the marshalled representation of the object
# @return		the unmarshalled <i>aka</i> "redeemed" object
#*/
sub onDequeue {
	my ($class, $obj) = @_;
#
#	reconstruct as non-sociable by redeeming
#
	return $class->redeem($obj);
}

#/**
# Pure virtual function to apply any object-specific cancel processing. Called by TSQ's
# <a href='./Queue.html#cancel>cancel()</a> methods,
# as well as the <a href='./Queue.html#respond>respond()</a> method
# when a cancelled operation is detected.
#
# @return		1
#*/
sub onCancel {
	my $obj = shift;
	return 1;
}
#/**
# Marshal an object into a value that can be passed via
# a <a href='./Queue.html'>Thread::Sociable::Queue</a> object.
# <p>
# Called by TSQ's various <a href='./Queue.html#enqueue'>enqueue()</a> and
# <a href='./Queue.html#respond'>respond()</a> methods
# when the TQQ object is being enqueue'd. Should return an unblessed,
# sociable version of the input object.
# <p>
# Default returns a sociable
# arrayref or hashref, depending on the object's base structure, with
# copies of all scalar members.
# <p>
# <b>Note</b> that objects with more complex members will need to
# implement an object specific <code>curse()</code> to do any deepcopying,
# including curse()ing any subordinate objects.
#
# @return		marshalled version of the object
#*/
sub curse {
	my $obj = shift;
#
#	if we're already sociable, don't socialize again
#
	return $obj if is_sociable($obj);

	if ($obj->isa('HASH')) {
		my %cursed : Sociable = ();
		$cursed{$_} = $obj->{$_}
			foreach (keys %$obj);
		return \%cursed;
	}

	my @cursed : Sociable = ();
	$cursed[$_] = $obj->[$_]
		foreach (0..$#$obj);
	return \@cursed;
}
#/**
# Unmarshall an object back into its blessed form.
# <p>
# Called by TSQ's various <a href='./Queue.html#dequeue'>dequeue()</a> and
# <a href='./Queue.html#wait'>wait</a> methods to
# "redeem" (i.e., rebless) the object into its original class.
# <p>
# Default creates non-sociable copy of the input object structure,
# copying its scalar contents, and blessing it into the specified class.
# <p>
# <b>Note</b> that objects with complex members need to implement
# an object specific <code>redeem()</code>, possibly recursively
# redeem()ing subordinate objects <i>(be careful
# of circular references!)</i>
#
# @param $object	marshalled <i>aka</i> "cursed" version of the object
#
# @return		unmarshalled, blessed version of the object
#*/
sub redeem {
	my ($class, $obj) = @_;
#
#	if object is already sociable, just rebless it
#
	return bless $obj, $class
		if is_sociable($obj);
#
#	we *could* just return the blessed object,
#	which would be sociable...but that might
#	not be the expected behavior...
#
	if (ref $obj eq 'HASH') {
		my $redeemed = {};
		$redeemed->{$_} = $obj->{$_}
			foreach (keys %$obj);
		return bless $redeemed, $class;
	}

	my $redeemed = [];
	$redeemed->[$_] = $obj->[$_]
		foreach (0..$#$obj);
	return bless $redeemed, $class;
}

1;