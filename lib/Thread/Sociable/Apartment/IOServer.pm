package Thread::Sociable::Apartment::IOServer;

use Thread::Sociable::Apartment::Server;
use base qw(Thread::Sociable::Apartment::Server);

our $VERSION = '0.10';

#
#	abstract class to be overridden by
#	actual implemented classes
#
#	extends T::A::Server to provide a poll()
#	method for I/O server objects (i.e., to permit
#	interleaving of I/O and TQD polling
#
sub poll {
	my $obj = shift;
	return $obj;
}

1;


__END__

=pod

=head1 NAME

Thread::Sociable::Apartment::IOServer - Subclass of L<Thread::Sociable::Apartment::Server> for I/O objects

Refer to L<Thread::Sociable::Apartment> for usage instructions.

=head1 AUTHOR & COPYRIGHT

Copyright(C) 2005, Dean Arnold, Presicient Corp., USA

Permission to use this module is granted under the
L<Perl Artistic License|perlartistic>.
