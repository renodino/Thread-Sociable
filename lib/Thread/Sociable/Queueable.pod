=pod

=head1 NAME

Thread::Sociable::Queueable - abstract class for marshalling elements for a Thread::Sociable::Queue queue

=head1 SYNOPSIS

	use Thread::Sociable::Queueable;
	use base qw(Thread::Sociable::Queueable);
	#
	#	implement onEnqueue method
	#	(default implementation shown)
	#
	sub onEnqueue {
		my $obj = shift;
	#
	#	capture class name, and create shared
	#	version of object
	#
		return $obj->isa('ARRAY') ?
			(ref $obj, socialize([ @$obj ])) :
			(ref $obj, socialize({ %$obj }));
	}
	#
	#	implement onDequeue method
	#	(default implementation shown)
	#
	sub onDequeue {
		my ($class, $obj) = @_;
	#
	#	reconstruct as non-sociable
	#
		$obj = (ref $obj eq 'ARRAY') ? [ @$obj ] : { %$obj };
		bless $obj, $class;
		return $obj;
	}
	#
	#	permit the object to be reconstructed on dequeueing
	#
	sub onCancel {
		my $obj = shift;
		return 1;
	}
	#
	#	curse (ie, unbless) the object into a sociable structure
	#
	sub curse {
		my $obj = shift;

		if ($obj->isa('HASH')) {
			my %cursed : sociable = ();
			$cursed{$_} = $obj->{$_}
				foreach (keys %$obj);
			return \%cursed;
		}

		my @cursed : sociable = ();
		$cursed[$_] = $obj->[$_]
			foreach (0..$#$obj);
		return \@cursed;
	}
	#
	#	redeem (ie, rebless) the object into
	#	the class
	#
	sub redeem {
		my ($class, $obj) = @_;

		if (ref $obj eq 'HASH') {
			my $redeemed = {};
			$redeemed->{$_} = $obj->{$_}
				foreach (keys %$obj);
			return bless $redeemed, $class;
		}

		my $redeemed = [];
		$redeemed->[$_] = $obj->[$_]
			foreach (0..$#$obj);
		return bless $redeemed, $class;
	}

=head1 DESCRIPTION

Thread::Sociable::Queueable (I<aka TSQbl>) provides abstract methods to be invoked
whenever an object is enqueued or dequeued, in either the request
or response direction, on a L<Thread::Sociable::Queue> (I<TSQ>) queue.

The primary purpose is to simplify application logic so that
marshalling/unmarhsalling of objects between threads is performed
automatically. In addition, when subclassed, the application class
can modify or add logic (e.g., notifying a server thread object
to update its reference count when a wrapped object is passed between
threads - see L<DBIx::Threaded> for an example).

=head1 METHODS

Refer to the included classdocs for summary and detailed method descriptions.

=head1 SEE ALSO

L<Thread::Sociable::Queue>

L<threads>

L<Thread::Sociable>

L<Thread::Queue::Duplex>

=head1 AUTHOR, COPYRIGHT, & LICENSE

Dean Arnold, Presicient Corp. L<darnold@presicient.com>

Copyright(C) 2005, Presicient Corp., USA

Permission is granted to use this software under the same terms
as Perl itself. Refer to the Perl Artistic License for details.

=cut
