#/**
# Thread-safe "publish and subscribe" queue with identifiable elements
# for sociable applications.
# <p>
# Copyright&copy; 2007, Dean Arnold, Presicient Corp., USA.
# All rights reserved.<p>
# Permission is granted to use this software according to the
# terms of the <a href='http://perldoc.perl.org/perlartistic.html'>Perl Artistic License</a>, as
# specified in the Perl README file.
#
# @author D. Arnold
# @since 2005-12-01
# @self	$obj
# @see		<a href='../Sociable.html'>Thread::Sociable</a>
# @see		<a href='./Queueable.html'>Thread::Sociable::Queueable</a>
# @see		<a href='./DuplexQueue.html'>Thread::Sociable::DuplexQueue</a>
#*/
package Thread::Sociable::MultiQueue;
require 5.008;

use threads;
use Thread::Sociable;
use Thread::Sociable::Queueable;
use Thread::Sociable::DuplexQueue;

use Thread::Sociable::DuplexQueue qw($tqd_global_lock);
use base qw(Thread::Sociable::Queueable);

use strict;
use warnings;

our $VERSION = '1.01';

#
#	global semaphore used for class-level wait()
#	notification
#
#	borrowed the following member names from TSMQ:
#
use constant TSMQ_Q => 0;
use constant TSMQ_MAP => 1;
use constant TSMQ_IDGEN => 2;
use constant TSMQ_LISTENERS => 3;
use constant TSMQ_REQUIRE_LISTENER => 4;
use constant TSMQ_MAX_PENDING => 5;
use constant TSMQ_URGENT_COUNT => 6;
use constant TSMQ_MARKS => 7;
#
#	then add our own
#
use constant TSMQ_SUBSCRIBERS => 8;
use constant TSMQ_SUB_MAP => 9;
use constant TSMQ_PENDING_MAP => 10;
use constant TSMQ_SUB_ID_MAP => 11;
#
#	flags for special msgs
#
use constant TSMQ_URGENT => 1;
use constant TSMQ_SIMPLEX => 2;
use constant TSMQ_URGENT_SIMPLEX => 3;
#
#	when used for subid, indicates only the
#	first sub to see it should process the msg
#	(ie reverts to TSMQ behavior)
#
use constant TSMQ_FIRST_ONLY => -1;

sub _get_id {
	my $obj = shift;

	lock(${$obj->[TSMQ_IDGEN]});
	my $id = ${$obj->[TSMQ_IDGEN]}++;
#
#	rollover, just in case...not perfect,
#	but good enough
#
	${$obj->[TSMQ_IDGEN]} = 1
		if (${$obj->[TSMQ_IDGEN]} > 2147483647);
	return $id;
}

#
#	common function for build enqueue list
#
sub _filter_nq {
	my $id = shift;
	my @params : Sociable = ($id, (undef) x (scalar @_ << 1));
#
#	marshall params, checking for Queueable objects
#
	my $i = 1;
	foreach (@_) {
		@params[$i..$i+1] =
			(ref $_ &&
			(ref $_ ne 'ARRAY') &&
			(ref $_ ne 'HASH') &&
			(ref $_ ne 'SCALAR') &&
			$_->isa('Thread::Queue::Queueable')) ?
			$_->onEnqueue() : (undef, $_);
#
#	invoke onEnqueue method
#
#			$params[$i] = ref $_;
#			$params[$i+1] = $_->onEnqueue();
#		}
#		else {
#			@params[$i..$i+1] = (undef, $_);
#		}
		$i += 2;
	}
	return \@params;
}

#/**
# Wait until a listener has registered.
#
# @param $timeout	(optional) number of seconds to wait for a listener.
#
# @return		Thread::Queue::Duplex if a listener is registered; undef otherwise.
#*/
sub wait_for_listener {
	my ($obj, $timeout) = shift;
	my $listeners = $obj->[TSMQ_LISTENERS];
	lock($$listeners);

	return undef
		if ($timeout && ($timeout < 0));

	if ($timeout) {
		$timeout += time();
		cond_timedwait($$listeners, $timeout)
			while (!$$listeners) && ($timeout > time());

		return $$listeners ? $obj : undef;
	}
	cond_wait($$listeners)
		while (!$$listeners);

	return $$listeners ? $obj : undef;
}

#/**
# Set maximum number of pending requests permitted. Signals any
# currently threads which may be blocked waiting for the number
# of pending requests to drop below the maximum permitted.
#
# @param $limit		positive integer maximum number of pending requests permitted.
#					A value of zero indicates no limit.
#
# @return		Thread::Queue::Duplex object
#*/
sub set_max_pending {
    my ($obj, $limit) = @_;
    $@ = 'Invalid limit.',
    return undef
    	unless (defined($limit) && ($limit=~/^\d+/) && ($limit >= 0));

	lock(@{$obj->[TSMQ_Q]});
    ${$obj->[TSMQ_MAX_PENDING]} = $limit;
#
#	wake up anyone whos been waiting for queue to change
#
	cond_broadcast(@{$obj->[TSMQ_Q]});
    return $obj;
}

#/**
# Wait for a response to a request. Also available as <code>dequeue_response()</code>
# alias.
#
# @param $id		the request ID of the response for which to wait.
#
# @return 			the response as an arrayref.
#*/
sub wait {
	my $obj = shift;
	my $id = shift;

 	my $reqmap = $obj->[TSMQ_MAP];
	my $result;
	{
 		lock(%$reqmap);
 		unless ($$reqmap{$id}) {
 		    cond_wait %$reqmap
 		    	until $$reqmap{$id};
 		}
     	$result = delete $$reqmap{$id};
     	cond_signal %$reqmap
     		if keys %$reqmap;
    }
    return $obj->_filter_resp($result);
}
*dequeue_response = \&wait;

#/**
# Test if a response is available for a specific request.
#
# @param $id		the request ID of the response for which to test.
#
# @return 			Thread::Queue::Duplex object if response is available; undef otherwise.
#*/
sub ready {
	my $obj = shift;
	my $id = shift;
#
#	no lock really needed here...
#
    return defined($obj->[TSMQ_MAP]{$id}) ? $obj : undef;
}

#/**
# Test if a response is available for a either any request,
# or for any of a set of requests.
#
# @param @ids		(optional) list of request IDs of responses for which to test.
#
# @return 			first request ID of available responses, or undef if none available
# @returnlist 		list of request IDs of available responses, or undef if none available
#*/
sub available {
	my $obj = shift;

	my @ids = ();
 	my $reqmap = $obj->[TSMQ_MAP];
 	lock(%$reqmap);
	if (scalar @_) {
#		print STDERR "available with a list\n";
 		map { push @ids, $_ if $$reqmap{$_}; } @_;
	}
	else {
#		print STDERR "available without list\n";
 		@ids = keys %$reqmap;
	}
    return scalar @ids ? wantarray ? @ids : $ids[0] : undef;
}

#/**
# Wait up to $timeout seconds for a response to a request.
#
# @param $id		the request ID of the response for which to wait.
# @param $timeout	number of seconds to wait
#
# @return 			the response as an arrayref, or undef if none available within the timeout
#*/
sub wait_until {
	my ($obj, $id, $timeout) = @_;

    return undef
    	unless $timeout && ($timeout > 0);
	$timeout += time();
	my $result;
 	my $reqmap = $obj->[TSMQ_MAP];
 	my $tid = threads->self()->tid();

 	while ($timeout > time()) {
 		lock(%$reqmap);

		print STDERR "wait_until in $tid for $id at ", time(), "\n"
			if $tqd_debug;

   		cond_timedwait(%$reqmap, $timeout)
   			unless $$reqmap{$id};

		print STDERR "wait_until in $tid for $id signaled at ", time(), "\n"
			if $tqd_debug;

		next unless $$reqmap{$id};

		print STDERR "wait_until in $tid for $id done ", time(), "\n"
			if $tqd_debug;

		print STDERR "avail keys in $tid ", join(', ', keys %$reqmap), "\n"
		 	if $tqd_debug;

 	   	$result = delete $$reqmap{$id};

 	    cond_broadcast %$reqmap;
 		last;
	}
    return $result ? $obj->_filter_resp($result) : undef;
}
#
#	some grouped waits
#	wait indefinitely for *any* of the
#	supplied ids
#
#/**
# Wait for a response to any specified request. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on any of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Queue::TSQContainer
# objects (<i>TSMQ is itself a TSQContainer</i>),
# or arrayrefs with a Thread::Queue::TSQContainer object, and zero or more request
# IDs. For Thread::Queue::TSQContainer object arguments, and arrayref arguments
# with no identifiers, waits for any enqueue event on the contained queue.
# For arrayref arguments with IDS, waits for a response event for any
# of the specified IDs.
#
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Queue::TSQContainer objects,
#						or arrayrefs containing a Thread::Queue::TSQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of TSMQ containers which have events pending.
#*/
sub wait_any {
	my $obj = shift;
	return _tqd_wait(undef, undef, @_)
		unless ref $obj;
	my $reqmap = $obj->[TSMQ_MAP];
	my %responses = ();
	{
		lock(%$reqmap);
#
#	cond_wait isn't behaving as expected, so we need to
#	test first, then wait if needed
#
   		map {
   			$responses{$_} = delete $$reqmap{$_}
   				if $$reqmap{$_};
   		} @_;

		until (keys %responses) {
			cond_wait %$reqmap;
		   	map {
		   		$responses{$_} = delete $$reqmap{$_}
		   			if $$reqmap{$_};
		   	} @_;
#
#	go ahead and signal...if no one's waiting, no harm
#
		    cond_signal %$reqmap;
		}
	}
	$responses{$_} = $obj->_filter_resp($responses{$_})
		foreach (keys %responses);
    return \%responses;
}
#
#	wait up to timeout for any
#
#/**
# Wait up to $timeout seconds for a response to any specified request. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on any of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Queue::TSQContainer objects,
# or arrayrefs with a Thread::Queue::TSQContainer object, and zero or more request
# IDs. For Thread::Queue::TSQContainer object arguments, and arrayref arguments
# with no identifiers, waits for any enqueue event on the queue.
# For arrayref arguments with IDS, waits for a response event for any
# of the specified IDs.
#
# @param $timeout		number of seconds to wait for a response event
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Queue::TSQContainer objects,
#						or arrayrefs containing a Thread::Queue::TSQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		undef if no response events occured within $timeout seconds; otherwise,
#				as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of queues which have events pending.
#*/
sub wait_any_until {
	my $obj = shift;
	return _tqd_wait(shift, undef, @_)
		unless ref $obj;

	my $timeout = shift;

	return undef unless $timeout && ($timeout > 0);
	$timeout += time();
	my $reqmap = $obj->[TSMQ_MAP];
	my %responses = ();
	{
		lock(%$reqmap);
#
#	cond_wait isn't behaving as expected, so we need to
#	test first, then wait if needed
#
	   	map {
	   		$responses{$_} = delete $$reqmap{$_}
	   			if $$reqmap{$_};
	   	} @_;

		while ((! keys %responses) && ($timeout > time())) {
			cond_timedwait(%$reqmap, $timeout);

	   		map {
		   		$responses{$_} = delete $$reqmap{$_}
	   				if $$reqmap{$_};
	   		} @_;
#
#	go ahead and signal...if no one's waiting, no harm
#
		    cond_signal %$reqmap;
		}
	}
	$responses{$_} = $obj->_filter_resp($responses{$_})
		foreach (keys %responses);
    return keys %responses ? \%responses : undef;
}
#/**
# Wait for a response to all specified requests. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on all of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Queue::TSQContainer objects,
# or arrayrefs with a Thread::Queue::TSQContainer object, and zero or more request
# IDs. For Thread::Queue::TSQContainer object arguments, and arrayref arguments
# with no identifiers, waits for responses to all current requests on the queue.
# For arrayref arguments with IDS, waits for a response to all
# of the specified IDs.
#
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Queue::TSQContainer objects,
#						or arrayrefs containing a Thread::Queue::TSQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of queues which have events pending.
#*/
sub wait_all {
	my $obj = shift;
	return _tqd_wait(undef, 1, @_)
		unless ref $obj;
	my $reqmap = $obj->[TSMQ_MAP];
	my %responses = ();
	{
		lock(%$reqmap);
	   	map {
	   		$responses{$_} = delete $$reqmap{$_}
	   			if $$reqmap{$_};
	   	} @_;
		until (scalar keys %responses == scalar @_) {
			cond_wait %$reqmap;

		   	map {
		   		$responses{$_} = delete $$reqmap{$_}
		   			if $$reqmap{$_};
		   	} @_;
#
#	go ahead and signal...if no one's waiting, no harm
#
		    cond_signal %$reqmap;
		}
	}
	$responses{$_} = $obj->_filter_resp($responses{$_})
		foreach (keys %responses);
    return \%responses;
}

#/**
# Wait up to $timeout seconds for a response to all specified requests. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on all of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Queue::TSQContainer objects,
# or arrayrefs with a Thread::Queue::TSQContainer object, and zero or more request
# IDs. For Thread::Queue::TSQContainer object arguments, and arrayref arguments
# with no identifiers, waits for responses to all current requests on the queue.
# For arrayref arguments with IDS, waits for a response to all
# of the specified IDs.
#
# @param $timeout		number of seconds to wait for all response
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Queue::TSQContainer objects,
#						or arrayrefs containing a Thread::Queue::TSQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		undef unless all response events occured within $timeout seconds; otherwise,
#				as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of queues which have events pending.
#*/
sub wait_all_until {
	my $obj = shift;

	return _tqd_wait(shift, 1, @_)
		unless ref $obj;

	my $timeout = shift;

	return undef unless $timeout && ($timeout > 0);
	$timeout += time();
	my $reqmap = $obj->[TSMQ_MAP];
	my %responses = ();
	{
		lock(%$reqmap);
   		map {
   			$responses{$_} = delete $$reqmap{$_}
   				if $$reqmap{$_};
   		} @_;
		while ((scalar keys %responses != scalar @_) &&
			($timeout > time())) {
			cond_timedwait(%$reqmap, $timeout);

   			map {
		   		$responses{$_} = $$reqmap{$_}
   					if $$reqmap{$_};
   			} @_;
#
#	go ahead and signal...if no one's waiting, no harm
#
		    cond_signal %$reqmap;
		}
#
#	if we got all our responses, then remove from map
#
		map { delete $$reqmap{$_} } @_
			if (scalar keys %responses == scalar @_);
	}
	$responses{$_} = $obj->_filter_resp($responses{$_})
		foreach (keys %responses);
#print 'list has ', scalar @_, ' we got ', scalar keys %responses, "\n";
    return (scalar keys %responses == scalar @_) ? \%responses : undef;
}

#/**
# Mark a request with a value. Provides a means to
# associate properties to a request after it has been
# queued, but before the response has been posted. The
# responder may test for marks via the <a href='#marked'>marked()</a>
# method, or retrieve the mark value via <a href='#get_mark'>get_mark()</a>.
#
# @param $id	ID of request to be marked
# @param $value	(optional) value to be added as a mark; if not specified,
#				a default value of 1 is used.
#
# @return		Thread::Queue::Duplex object
#*/
sub mark {
	my ($obj, $id, $value) = @_;

	$value = 1 unless defined($value);
	lock(%{$obj->[TSMQ_MAP]});
	lock(%{$obj->[TSMQ_MARKS]});
#
#	already responded or cancelled
#
	return undef
		if (exists $obj->[TSMQ_MAP]{$id});

	$obj->[TSMQ_MARKS]{$id} = $value;
	return $obj;
}

#/**
# Remove any marks from a request.
#
# @param $id	ID of request to be unmarked.
#
# @return		Thread::Queue::Duplex object
#*/
sub unmark {
	my ($obj, $id) = @_;

	lock(%{$obj->[TSMQ_MARKS]});
	delete $obj->[TSMQ_MARKS]{$id};
	return $obj;
}

#/**
# Returns any current mark on a specified request.
#
# @param $id	ID of request whose mark is to be returned.
#
# @return		the mark value; undef if not marked
#*/
sub get_mark {
	my ($obj, $id) = @_;
	lock(%{$obj->[TSMQ_MARKS]});
	return $obj->[TSMQ_MARKS]{$id};
}

#/**
# Test if a request is marked, or if the mark is a specified value.
#
# @param $id	ID of request to test for a mark
# @param $value	(optional) value to test for
#
# @return		1 if the request is marked and either no $value was specified,
#				or, if a $value was specified, the mark value equals $value; undef
#				otherwise.
#*/
sub marked {
	my ($obj, $id, $value) = @_;
	lock(%{$obj->[TSMQ_MARKS]});
	return (defined($obj->[TSMQ_MARKS]{$id}) &&
		((defined($value) && ($obj->[TSMQ_MARKS]{$id} eq $value)) ||
		(!defined($value))));
}

##########################################################
#
#	BEGIN CLASS LEVEL METHODS
#
##########################################################

sub _tqd_wait {
	my $timeout = shift;
	my $wait_all = shift;
#
#	validate params
#
	map {
		return undef
			unless ($_ &&
				ref $_ &&
				(
					((ref $_ eq 'ARRAY') &&
						($#$_ >= 0) &&
						ref $_->[0] &&
						$_->[0]->isa('Thread::Queue::TSQContainer')
					) ||
					$_->isa('Thread::Queue::TSQContainer')
				));
	} @_;

	my @avail = ();

	my @qs = ();
	my @containers = ();
	push(@containers, ((ref $_ eq 'ARRAY') ? $_->[0] : $_)),
	push(@qs, $containers[-1]->get_queue())
		foreach (@_);

#print join(', ', @qs), "\n";

	my ($q, $container, $ids);
	my $count = scalar @qs;
	my @ids;
	$timeout += time() if $timeout;
	while ($count) {
		lock($tqd_global_lock);
		foreach (0..$#_) {
			last unless $count;
			next unless $qs[$_];
			$q = $qs[$_];
			$container = $containers[$_];
			$ids = $_[$_];
#
#	if we've got ids, check for responses
#
			push(@avail, $container),
			$qs[$_] = undef,
			$count--
	    		if (((ref $ids eq 'ARRAY') && (scalar @$ids > 1)) ?
	    			$q->available(@{$ids}[1..$#$ids]) :
	    			$q->pending());

		}	# end foreach queue
		last
			unless (($wait_all && $count) || (! scalar @avail));

 		unless ($timeout) {

			print STDERR "TSMQ: locking...\n"
			 	if $tqd_debug;

	 		cond_wait($tqd_global_lock);
			print STDERR "TSMQ: locked\n"
			 	if $tqd_debug;

	 		next;
 		}
#		print STDERR "timed out and avail has ", scalar @avail, "\n" and
		cond_timedwait($tqd_global_lock, $timeout);
		return ()
			unless ($timeout > time());
	}

#print STDERR "avail has ", scalar @avail, "\n";
	return @avail;
}
##########################################################
#
#	END CLASS LEVEL METHODS
#
##########################################################

#################################################
#
#	The following methods must be borrowed from TSMQ:
#	TQQ curse()/redeem()
#
##################################################
#################################################
#
# !!!NOTE!!!NOTE!!!NOTE!!!NOTE!!!NOTE!!!NOTE!!!NOTE
#
#	always retain this lock order:
#	TSMQ_Q before TSMQ_PENDING_MAP
#	TSMQ_PENDING_MAP before TSMQ_MAP
#	TSMQ_MAP before TSMQ_MARKS
#
#################################################

sub new {
    my $class = shift;
#
#	!!!BORROW CODE FROM TSMQ!!!
#
	my $obj = $class->SUPER::new(@_);
	return undef unless $obj;
#
#	map of subids to their msgid queues
#
	my %subscribers : Lockable = ();
#
#	map of msgids to their pending msg info
#
	my %pendingmsgs : Lockable = ();
#
#	map of msgids to their pending response info
#
	my %pendingresps : Lockable = ();
#
#	map of subscriber TIDs to their sub ID
#
	my %subids : Lockable = ();

	$obj->[TSMQ_SUBSCRIBERS] = \%subscribers;
	$obj->[TSMQ_SUB_MAP] = \%pendingmsgs;
	$obj->[TSMQ_PENDING_MAP] = \%pendingresps;
	$obj->[TSMQ_SUB_ID_MAP] = \%subids;
    return $obj;
}

sub subscribe {
	my $obj = shift;
	my $tid = threads->self()->tid() + 1;
	my $id = shift || $tid;
	lock(${$obj->[TSMQ_LISTENERS]});
#
#	we only permit a single subscriber per thread
#
	$@ = 'Thread $tid already subscribed as ' .
		$obj->[TSMQ_SUB_ID_MAP]{$tid},
	return undef
		if $obj->[TSMQ_SUB_ID_MAP]{$tid};

	${$obj->[TSMQ_LISTENERS]}++;
	my @pending_msgs : Lockable = ();
	$obj->[TSMQ_SUBSCRIBERS]{$id} = \@pending_msgs;
	$obj->[TSMQ_SUB_ID_MAP]{$tid} = $id;
	cond_broadcast(${$obj->[TSMQ_LISTENERS]});
	return $obj;
}
*listen = \&subscribe;

sub unsubscribe {
	my $obj = shift;
	my $id = threads->self()->tid() + 1;
	my $pending;
	{
		lock(${$obj->[TSMQ_LISTENERS]});
		$@ = "No subscriber in thread $id.",
		return undef
			unless $obj->[TSMQ_SUB_ID_MAP]{$id};
		$id = delete $obj->[TSMQ_SUB_ID_MAP]{$id};
		$pending = delete $obj->[TSMQ_SUBSCRIBERS]{$id};
		${$obj->[TSMQ_LISTENERS]}--
			if ${$obj->[TSMQ_LISTENERS]};
	}
	return $obj unless scalar @$pending;

	my @completed = ();
#
#	walk pending requests and remove any reference to ourselves,
#	possibly signalling request completion if we're the only/last
#	subscriber
#	Note we leave pending responses intact
#
	{
		lock(@{$obj->[TSMQ_Q]});
		foreach (@$pending) {
#
#	need to be careful here due to inconsistent deref behavior of
#	Thread::Sociable
#
			if ($obj->[TSMQ_SUB_MAP]{$_}) {
				my $entry = $obj->[TSMQ_SUB_MAP]{$_};
				if ($entry->{_refcnt} && ($entry->{_refcnt} != TSMQ_FIRST_ONLY)) {
#
#	what should we do about FIRST_ONLY here ?
#	if we're the only sub, then the publisher may get
#	hung...for now, we'll ignore them
#
					$entry->{_refcnt}--;
					push @completed, $_
						unless $entry->{_refcnt};
				}
			}
		}
	}
#
#	we postpone posting completions so we don't constantly
#	lock/unlock ..and possibly deadlock...; this way we
#	can just lock, post all, unlock
#
	$obj->_post_complete(@completed)
		if scalar @completed;

	return $obj;
}
*ignore = \&unsubscribe;

sub get_subscribers {
	my $obj = shift;
	lock(${$obj->[TSMQ_LISTENERS]});
	return keys %{$obj->[TSMQ_SUBSCRIBERS]};
}
#
#	wait until we've got subscribers
#
sub wait_for_subscribers {
	my ($obj, $count, $timeout) = @_;
	my $listeners = $obj->[TSMQ_LISTENERS];
	lock($$listeners);

	return undef
		if ($timeout && ($timeout < 0));

	if ($timeout) {
		$timeout += time();
		cond_timedwait($$listeners, $timeout)
			while ($$listeners != $count) && ($timeout > time());

		return ($$listeners eq $count) ? $obj : undef;
	}

	cond_wait($$listeners)
		while ($$listeners != $count);
	return ($$listeners == $count) ? $obj : undef;
}
#
#	override lock & load to include subid list
#
sub _lock_load {
	my $obj = shift;
	my $flags = shift || 0;
	my $subs = shift;
#
#	create Sociable array of params here, leaving
#	the open spot for the subids list
#
	my $msgid = $obj->_get_id;	# always assign ID, even for simplex
#
#	NOTE: we trick _filter_nq into opening up a couple extra
#	slots for us, which we later fill with an Underdog
#	Super Energy Pill, namely, the simplex flag and the refcnt
#
	my $params = Thread::Queue::Duplex::_filter_nq($msgid, undef, @_);
	$params->[1] = $flags;
#
#	Note that sequence is important here;
#	a sub may unsubscribe after we've
#	published to it, but before msg delivery...but
#	the unsubscribe process will purge the sub
#	from any pending msgs. We don't publish
#	to subs that aren't subscribed; since we have
#	to cond_wait until the queue length is under the max,
#	we don't do subs assignment until then (in case
#	some subs unsubscribe while we're cond_waiting)
#
	my $q = $obj->[TSMQ_Q];
	lock(@$q);
#
#	check current length if we have a limit
#
	while (${$obj->[TSMQ_MAX_PENDING]} &&
		(${$obj->[TSMQ_MAX_PENDING]} <= scalar keys %{$obj->[TSMQ_SUB_MAP]})) {
#		print "pending before: ", scalar @$q, "\n";
		cond_wait(@$q);
#		print "pending after: ", scalar @$q, "\n";
	}

	my $firstonly = (defined($subs) && (! ref $subs) && ($subs == TSMQ_FIRST_ONLY));
	$subs = (defined($subs) && (! $firstonly)) ?
		[ $subs ] :
		[ keys %{$obj->[TSMQ_SUBSCRIBERS]} ]
		unless defined($subs) && (ref $subs);

	$params->[2] = $firstonly ? TSMQ_FIRST_ONLY : scalar @$subs;
	$obj->[TSMQ_SUB_MAP]{$msgid} = $params;
	unless ($params->[1] & TSMQ_SIMPLEX) {
#
#	if we need a response, post the prelim
#
		my %pending : Lockable = (_refcnt => $params->[2]);
		lock(%{$obj->[TSMQ_PENDING_MAP]});
		$obj->[TSMQ_PENDING_MAP]{$msgid} = \%pending;
	}
#
#	now post the msgid to every assigned sub's queue
#	at present, we'll use the TSMQ_Q lock to lock these as well
#
	foreach (@$subs) {
		my $s = $obj->[TSMQ_SUBSCRIBERS]{$_};
		($flags & TSMQ_URGENT) ?
		    unshift @$s, $msgid :
		    push @$s, $msgid;
	}
    cond_broadcast @$q;
    return $msgid;
}


###########################
#
#	publish methods:
#	Note the parlor trick: we just alias to the
#	enqueue() methods, but with a funky
#	signature to indicate that all subs
#	should be queued
#	FYI: subscribers registering after we publish,
#	but before the msg is fully consumed, won't get
#	the message
#
sub publish {
    return enqueue(undef, @_);
}

sub publish_urgent {
    return enqueue_urgent(undef, @_);
}
#
#	blocking versions of publish()
#
sub publish_and_wait {
    return enqueue_and_wait(undef, @_);
}

sub publish_and_wait_until {
    return enqueue_and_wait_until(undef, @_);
}

sub publish_urgent_and_wait {
    return enqueue_urgent_and_wait(undef, @_);
}

sub publish_urgent_and_wait_until {
    return enqueue_urgent_and_wait_until(undef, @_);
}
#
#	Simplex versions
#
sub publish_simplex {
    return enqueue_simplex(undef, @_);
}

sub publish_simplex_urgent {
    return enqueue_simplex_urgent(undef, @_);
}
#
#	override of base enqueue methods
#
sub enqueue {
    my $obj = shift;
    my $subs = shift;
#
#	check if its a publish...
#
	($obj, $subs) = ($subs, undef)
		unless defined($obj);

	return undef
		if ($obj->[TSMQ_REQUIRE_LISTENER] &&
			(! ${$obj->[TSMQ_LISTENERS]}));
	my $id = _lock_load($obj, undef, $subs, @_);
	lock($tqd_global_lock);
	cond_broadcast($tqd_global_lock);
    return $id;
}

sub enqueue_urgent {
    my $obj = shift;
    my $subs = shift;
#
#	check if its a publish...
#
	($obj, $subs) = ($subs, undef)
		unless defined($obj);

	return undef
		if ($obj->[TSMQ_REQUIRE_LISTENER] &&
			(! ${$obj->[TSMQ_LISTENERS]}));
	my $id = _lock_load($obj, TSMQ_URGENT, $subs, @_);

	lock($tqd_global_lock);
	cond_broadcast($tqd_global_lock);
    return $id;
}
#
#	blocking versions of enqueue()
#
sub enqueue_and_wait {
    my $obj = shift;
#
#	check if its a publish...
#
	$obj = shift,
	unshift @_, undef
		unless defined($obj);

	my $id = $obj->enqueue(@_);
	return defined($id) ? $obj->wait($id) : undef;
}

sub enqueue_and_wait_until {
    my $obj = shift;
    my $subs = shift;
	my $timeout = shift;
#
#	check if its a publish...
#
	($obj, $subs) = ($subs, undef)
		unless defined($obj);

	my $id = $obj->enqueue($subs, @_);
	return defined($id) ? $obj->wait_until($id, $timeout) : undef;
}

sub enqueue_urgent_and_wait {
    my $obj = shift;
#
#	check if its a publish...
#
	$obj = shift,
	unshift @_, undef
		unless defined($obj);

	my $id = $obj->enqueue_urgent(@_);
	return defined($id) ? $obj->wait($id) : undef;
}

sub enqueue_urgent_and_wait_until {
    my $obj = shift;
    my $subs = shift;
	my $timeout = shift;
#
#	check if its a publish...
#
	($obj, $subs) = ($subs, undef)
		unless defined($obj);

	my $id = $obj->enqueue_urgent($subs, @_);
	return defined($id) ? $obj->wait_until($id, $timeout) : undef;
}
#
#	Simplex versions
#
sub enqueue_simplex {
    my $obj = shift;
    my $subs = shift;
#
#	check if its a publish...
#
	($obj, $subs) = ($subs, undef)
		unless defined($obj);

	return undef
		if ($obj->[TSMQ_REQUIRE_LISTENER] &&
			(! ${$obj->[TSMQ_LISTENERS]}));

	_lock_load($obj, TSMQ_SIMPLEX, $subs, @_);

	lock($tqd_global_lock);
	cond_broadcast($tqd_global_lock);
    return $obj;
}

sub enqueue_simplex_urgent {
    my $obj = shift;
    my $subs = shift;
#
#	check if its a publish...
#
	($obj, $subs) = ($subs, undef)
		unless defined($obj);

	return undef
		if ($obj->[TSMQ_REQUIRE_LISTENER] &&
			(! ${$obj->[TSMQ_LISTENERS]}));

	_lock_load($obj, TSMQ_URGENT_SIMPLEX, $subs, @_);

	lock($tqd_global_lock);
	cond_broadcast($tqd_global_lock);
    return $obj;
}

#
#	recover original param list, including reblessing Queueables
#	NOTE: we need to maintain the list until all the subs have
#	consumed it
#
sub _filter_dq {
	my $result = shift;
#
#	keep ID; collapse the rest
#
	my @results = (($result->[1] & TSMQ_SIMPLEX) ? undef : $result->[0]);
	my $class;
	my $i = 3;

	$class = $result->[$i++],
	push (@results,
		$class ?
		${class}->onDequeue($result->[$i++]) :
		$result->[$i++])
		while ($i < scalar @$result);

    return \@results;
}
#
#	common method for getting the request out of the queue
#
sub _get_request {
	my ($obj, $msgid, $need_urgent) = @_;
#
#	cancelled request (ie map entry has been deleted) ?
#
   	return undef
   		unless exists $obj->[TSMQ_SUB_MAP]{$msgid};

   	my $request = $obj->[TSMQ_SUB_MAP]{$msgid};
   	return undef
   		if ($need_urgent && (!($request->[1] & TSMQ_URGENT)));
   	$request->[2]--
   		unless ($request->[2] == TSMQ_FIRST_ONLY);
   	delete $obj->[TSMQ_SUB_MAP]{$msgid}
   		if ($request->[2] <= 0);
   	return $request;
}

sub dequeue  {
    my $obj = shift;
    my $request;
    my $id = $obj->[TSMQ_SUB_ID_MAP]{threads->self()->tid() + 1};
    $@ = 'No subscriber for thread ' . threads->self()->tid(),
    return undef
    	unless $id;

	my $q = $obj->[TSMQ_Q];
    while (1) {

		lock(@$q);
		my $s = $obj->[TSMQ_SUBSCRIBERS]{$id};

    	cond_wait @$q
    		while (! scalar @$s);

#   print $id, " dequeue\n";

		$request = $obj->_get_request(shift @$s);
		next unless $request;
#
#	signal any waiters
#
    	cond_broadcast @$q;
    	last;
    }
    return _filter_dq($request);
}

sub dequeue_until {
    my ($obj, $timeout) = @_;

    return undef
    	unless $timeout && ($timeout > 0);

	$timeout += time();
	my $request;
    my $id = $obj->[TSMQ_SUB_ID_MAP]{threads->self()->tid() + 1};
    $@ = 'No subscriber for thread ' . threads->self()->tid(),
    return undef
    	unless $id;

	my $q = $obj->[TSMQ_Q];
	while (1)
	{
		lock(@$q);
		my $s = $obj->[TSMQ_SUBSCRIBERS]{$id};

   		cond_timedwait(@$q, $timeout)
	   		while (! scalar @$s) && ($timeout > time());
#
#	if none, then we must've timed out
#
		return undef
			unless scalar @$s;

#   print $id, " dequeue_until\n";

		$request = $obj->_get_request(shift @$s);
		next unless $request;
#
#	signal any waiters
#
    	cond_broadcast @$q;
    	last;
	}
    return _filter_dq($request);
}

sub dequeue_nb {
    my $obj = shift;
    my $request;
    my $id = $obj->[TSMQ_SUB_ID_MAP]{threads->self()->tid() + 1};
    $@ = 'No subscriber for thread ' . threads->self()->tid(),
    return undef
    	unless $id;
    my $q = $obj->[TSMQ_Q];
    while (1)
    {
		lock(@$q);
		my $s = $obj->[TSMQ_SUBSCRIBERS]{$id};
		return undef
			unless scalar @$s;
#   print $id, " dequeue_nb\n";
#
		$request = $obj->_get_request(shift @$s);
		return undef unless $request;
#
#	signal any waiters
#
    	cond_broadcast @$q;
		last;
	}
    return _filter_dq($request);
}

sub dequeue_urgent {
    my $obj = shift;
    my $request;
    my $id = $obj->[TSMQ_SUB_ID_MAP]{threads->self()->tid() + 1};
    $@ = 'No subscriber for thread ' . threads->self()->tid(),
    return undef
    	unless $id;
    my $q = $obj->[TSMQ_Q];
    while (1)
    {
		lock(@$q);
		my $s = $obj->[TSMQ_SUBSCRIBERS]{$id};
		return undef
			unless scalar @$s;

		$request = $obj->_get_request(shift @$s, 1);
		return undef unless $request;
#
#	signal any waiters
#
    	cond_broadcast @$q;
    	last;
	}
    return _filter_dq($request);
}

sub pending {
    my $obj = shift;
#
#	returned value depends on context: if its a
#	sub, return its queue; else return the
#	number of keys in the request map
#
    my $id = $obj->[TSMQ_SUB_ID_MAP]{threads->self()->tid() + 1};
	lock(@{$obj->[TSMQ_Q]});
	return scalar keys %{$obj->[TSMQ_SUB_MAP]}
		unless defined($id) && exists $obj->[TSMQ_SUBSCRIBERS]{$id};
#
#	Texas 2 step to avoid issues w/ Thread::Sociable
#
	my $q = $obj->[TSMQ_SUBSCRIBERS]{$id};
	return scalar @$q;
}

#
#	common function for building response list
#
sub _create_resp {
	my @params : Lockable = ((undef) x (scalar @_ << 1));
#
#	marshall params, checking for Queueable objects
#
	my $i = 0;
	foreach (@_) {
		if (ref $_ &&
			(ref $_ ne 'ARRAY') &&
			(ref $_ ne 'HASH') &&
			(ref $_ ne 'SCALAR') &&
			$_->isa('Thread::Queue::Queueable')) {
#
#	invoke onEnqueue method
#
			$params[$i] = ref $_;
			$params[$i+1] = $_->onEnqueue();
		}
		else {
			@params[$i..$i+1] = (undef, $_);
		}
		$i += 2;
	}
	return \@params;
}

sub respond {
	my $obj = shift;
	my $msgid = shift;
    my $id = $obj->[TSMQ_SUB_ID_MAP]{threads->self()->tid() + 1};
    $@ = 'No subscriber for thread ' . threads->self()->tid(),
    return undef
    	unless $id;
#
#	silently ignore response to a simplex request
#
	return $obj unless defined($msgid);

	my $result = _create_resp(@_);
	my $pending;
	{
		lock(%{$obj->[TSMQ_PENDING_MAP]});
#
#	check if its been canceled
#
		_cancel_resp({ $id => $result }),
		return $obj
			unless exists $obj->[TSMQ_PENDING_MAP]{$msgid};
		$pending = $obj->[TSMQ_PENDING_MAP]{$msgid};
#
#	else post result, update refcount, and if done,
#	post completion; make sure we use same lock order
#	everywhere
#
		$pending->{$id} = $result;
		$pending->{_refcnt}--
			if $pending->{_refcnt} && ($pending->{_refcnt} > 0);

		if ($pending->{_refcnt} <= 0) {
			delete $pending->{_refcnt};
			lock(%{$obj->[TSMQ_MAP]});
			$obj->[TSMQ_MAP]{$msgid} = delete $obj->[TSMQ_PENDING_MAP]{$msgid};
		    cond_broadcast %{$obj->[TSMQ_MAP]};
#
#	order is important; we always lock MAP before MARK
#
			lock(%{$obj->[TSMQ_MARKS]});
			delete $obj->[TSMQ_MARKS]{$msgid};
		}
	}

	lock($tqd_global_lock);
	cond_broadcast($tqd_global_lock);

	return $obj;
}
#
#	common function for filtering response list
#
sub _filter_resp {
	my ($obj, $result) = @_;
#
#	collapse the response elements
#
	delete $result->{_refcnt};
	my %results = ();
	my $class;
	my $persub;
	my $subresp;
	foreach (keys %$result) {
		$results{$_} = $subresp = [];
		$persub = $result->{$_};
		$class = shift @$persub,
		push (@$subresp,
			$class ?
			${class}->onDequeue(shift @$persub) :
			shift @$persub)
			while (@$persub);
	}
    return \%results;
}
#
#	called to post completion when a sub unsubscribes
#
sub _post_complete {
	my $obj = shift;

	my $count = 0;
	{
#
#	note: lock order is important!
#
		lock(%{$obj->[TSMQ_PENDING_MAP]});
		lock(%{$obj->[TSMQ_MAP]});
		lock(%{$obj->[TSMQ_MARKS]});
		foreach (@_) {
			delete $obj->[TSMQ_MARKS]{$_};
#
#	check if its been canceled
#
			next
				unless exists $obj->[TSMQ_PENDING_MAP]{$_};
#
#	Texas 2 step to avoid Thread::Sociable misbehavior
#
			my $pending = delete $obj->[TSMQ_PENDING_MAP]{$_};
			delete $pending->{_refcnt};
			$obj->[TSMQ_MAP]{$_} = $pending;
			$count++;
		}

	    cond_broadcast %{$obj->[TSMQ_MAP]}
	    	if $count;
	}

	if ($count) {
		lock($tqd_global_lock);
		cond_broadcast($tqd_global_lock);
	}
	return $obj;
}

sub _cancel_resp {
	my $resp = shift;
#
#	collapse the response elements,
#	and call onCancel to any that are Queueables
#
	my $class;
	delete $resp->{_refcnt};
	foreach (values %$resp) {
		$class = shift @$_,
		$class ?
			${class}->onCancel(shift @$_) :
			shift @$_
			while (@$_);
	}
    return 1;
}

sub cancel {
	my $obj = shift;
#
#	*always* lock in this order to avoid deadlock
#
	lock(@{$obj->[TSMQ_Q]});
	lock(%{$obj->[TSMQ_PENDING_MAP]});
	lock(%{$obj->[TSMQ_MAP]});
	lock(%{$obj->[TSMQ_MARKS]});
	foreach (@_) {
		delete $obj->[TSMQ_MARKS]{$_};
#
#	delete submap entry,
#	and purge any pending responses
#
		delete $obj->[TSMQ_SUB_MAP]{$_}
			if exists $obj->[TSMQ_SUB_MAP]{$_};
#
#	if already responded to, call onCancel for any Queueuables
#
		my $resp =
			(exists $obj->[TSMQ_PENDING_MAP]{$_}) ?
				delete $obj->[TSMQ_PENDING_MAP]{$_} :
			(exists $obj->[TSMQ_MAP]{$_}) ?
				delete $obj->[TSMQ_MAP]{$_} :
				undef;
		_cancel_resp($resp) if $resp;
	}
	return $obj;
}

sub cancel_all {
	my $obj = shift;
#
#	when we lock both, *always* lock in this order to avoid
#	deadlock
#
	lock(@{$obj->[TSMQ_Q]});
	lock(%{$obj->[TSMQ_PENDING_MAP]});
	lock(%{$obj->[TSMQ_MAP]});
	lock(%{$obj->[TSMQ_MARKS]});
#
#	first cancel all pending responses
#
	_cancel_resp(delete $obj->[TSMQ_PENDING_MAP]{$_})
		foreach (keys %{$obj->[TSMQ_PENDING_MAP]});

	_cancel_resp(delete $obj->[TSMQ_MAP]{$_})
		foreach (keys %{$obj->[TSMQ_MAP]});
#
#	then cancel all the pending requests by
#	setting their IDs to -1
#
	delete $obj->[TSMQ_MARKS]{$_},
	delete $obj->[TSMQ_SUB_MAP]{$_}
		foreach (keys %{$obj->[TSMQ_SUB_MAP]});
#
#	how will we cancel inprogress requests ??
#	need a map value, or alternate map...
#
	return $obj;
}

1;
