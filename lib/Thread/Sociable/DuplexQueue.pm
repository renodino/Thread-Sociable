#/**
# Thread-safe request/response queue with identifiable elements.
# Provides methods for N threads to queue items to other threads, and
# then wait only for responses to specific queued items.
# <p>
# Note: this object is derived from an Thread::Sociable arrayref
# to optimize performance.
# <p>
# Licensed under the Academic Free License version 3.0, as specified in the
# License.txt file included in this software package, or at
# <a href="http://www.opensource.org/licenses/afl-3.0.php">OpenSource.org</a>.
#
# @author D. Arnold
# @since 2005-12-01
# @self	$self
# @see		<a href='./Queueable.html'>Thread::Sociable::Queueable</a>
#*/
package Thread::Sociable::DuplexQueue;
#
#	Copyright (C) 2007, Presicient Corp., USA
#
require 5.008;

use threads;
use Thread::Sociable;
use Thread::Sociable::Queueable;
use Thread::Sociable::TSDQContainer;
use Storable qw(freeze thaw);
use Exporter;
use base qw(Exporter Thread::Sociable::Queueable Thread::Sociable::TSDQContainer);

use strict;
use warnings;

our $VERSION = '1.01';

use constant TSDQ_URGENT => 1;
use constant TSDQ_SIMPLEX => 2;
use constant TSDQ_URGENT_SIMPLEX => 3;
use constant TSDQ_FIRST_ONLY => -1;

#/**
# Constructor. Creates a new empty queue, and associated mapping hash.
#
# @param ListenerRequired	boolean value indicating if registered listener
#							required before enqueue is permitted.
# @param MaxPending			positive integer maximum number of pending requests;
#							enqueue attempts will block until the pending count
# 							drops below this value. The limit may be applied or modified later
# 							via the <a href='#set_max_pending'>set_max_pending()</a> method.
#							A value of zero indicates no limit.
#
# @return		Thread::Sociable::DuplexQueue object
#*/
sub new {
    my $class = shift;

    $@ = 'Invalid argument list',
    return undef
    	if (scalar @_ && (scalar @_ & 1));

    my %args = @_;
    foreach (keys %args) {
	    $@ = 'Invalid argument list',
	    return undef
	    	unless ($_ eq 'ListenerRequired') ||
	    		($_ eq 'MaxPending');

	    $@ = 'Invalid argument list',
	    return undef
	    	if (($_ eq 'MaxPending') &&
	    		defined($args{$_}) &&
	    		(($args{$_}!~/^\d+/) || ($args{$_} < 0)));
    }
#
#	use sociable scalar; addr of queue context is stored in the scalar
#	(yes, very dangerous...but its the first release...)
#
	my $q : Lockable = 1;
	my $self = bless \$q, $class;

	$self->new_queue($args{ListenerRequired}, $args{MaxPending});
	return $self;
}

#/**
# Register as a queue listener. Permits "ListenerRequired"
# queues to accept requests when at least one listener
# has registered.
#
# @return		Thread::Sociable::DuplexQueue object
#*/
# @xs listen

#/**
# Deregister as a queue listener. When all listeners
# deregister, a "ListenerRequired" queue will no longer
# accept new requests until a listener registers via
# <a href='#listen'>listen()</a>
#
# @return		Thread::Sociable::DuplexQueue object
#*/
# @xs ignore

#/**
# Wait until a listener has registered.
#
# @param $timeout	(optional) number of seconds to wait for a listener.
#
# @return		Thread::Sociable::DuplexQueue if a listener is registered; undef otherwise.
#*/
sub wait_for_listener {
	my ($self, $timeout) = shift;
	lock($self);

	return undef
		if ($timeout && ($timeout < 0));

	if ($timeout) {
		$timeout += time();
		cond_timedwait($self, $timeout)
			while (!$self->_has_listeners) && ($timeout > time());

		return $self->_has_listeners ? $self : undef;
	}

	cond_wait($self)
		while (!$self->_has_listeners);

	return $self;
}


sub _marshall {
	my $flags = shift;
#
#	create a queue packet of appropriate size, generating
#	the packet id and setting simplex flag
#
	my @params = ();
	$self->_make_queue_pkt(@params, $flags, scalar @_);
#
#	marshall params, checking for Queueable objects
#
	((!ref $_) || is_sociable($_)) ?
		_add_to_pkt(@params, undef, $_) :
	((ref $_ eq 'ARRAY') ||
		(ref $_ eq 'HASH') || (ref $_ eq 'SCALAR') ||
		!$_->isa('Thread::Sociable::Queueable')) ?
		_add_to_pkt(@params, ref $_, freeze($_)) :
		_add_to_pkt(@params, $_->onEnqueue());
		foreach (@_);
	return \@params;
}

#/**
# Enqueue a request to the tail of the queue.
#
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Request ID if successful; undef if ListenerRequired and no listeners
#				are registered
#*/
sub enqueue {
	my $self = shift;
	return $self->_enqueue($self->_marshall(0, @_));
}

#/**
# Enqueue a request to the head of the queue.
#
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Request ID if successful; undef if ListenerRequired and no listeners
#				are registered
#*/
sub enqueue_urgent {
	my $self = shift;
	return $self->_enqueue($self->_marshall(TSDQ_URGENT_SIMPLEX, @_));
}
#
#	blocking versions of enqueue()
#
#/**
# Enqueue a request to the tail of the queue, and wait for the response.
#
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Response structure if successful; undef if ListenerRequired and no listeners
#				are registered
#*/
sub enqueue_and_wait {
	my $self = shift;
	my $id = $self->_enqueue($self->_marshall(0, @_))
		or return undef;

	return $self->wait($id);
}

#/**
# Enqueue a request to the tail of the queue, and wait up to $timeout seconds
# for the response.
#
# @param $timeout number of seconds to wait for a response
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Response structure if successful; undef if ListenerRequired and no listeners
#				are registered, or if no response is received within the specified $timeout
#*/
sub enqueue_and_wait_until {
    my $self = shift;
	my $timeout = shift;

	my $id = $self->_enqueue($self->_marshall(0, @_))
		or return undef;
	return $self->wait_until($id, $timeout);
}

#/**
# Enqueue a request to the head of the queue, and wait up to $timeout seconds
# for the response.
#
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Response structure if successful; undef if ListenerRequired and no listeners
#				are registered
#*/
sub enqueue_urgent_and_wait {
    my $self = shift;

	my $id = $self->_enqueue($self->_marshall(TSDQ_URGENT, @_))
		or return undef;

	return $self->wait($id);
}

#/**
# Enqueue a request to the head of the queue, and wait up to $timeout seconds
# for the response.
#
# @param $timeout number of seconds to wait for a response
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Response structure if successful; undef if ListenerRequired and no listeners
#				are registered, or if no response is received within the specified $timeout
#*/
sub enqueue_urgent_and_wait_until {
    my $self = shift;
	my $timeout = shift;

	my $id = $self->_enqueue($self->_marshall(TSDQ_URGENT, @_))
		or return undef;
	return $self->wait_until($id, $timeout);
}
#
#	Simplex versions
#
#/**
# Enqueue a simplex request to the tail of the queue. Simplex requests
# do not generate responses.
#
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Thread::Sociable::DuplexQueue object if successful; undef if ListenerRequired and
#				no listeners are registered
#*/
sub enqueue_simplex {
	my $self = shift;
	return $self->_enqueue($self->_marshall(TSDQ_SIMPLEX, @_)) ? $self : undef;
}

#/**
# Enqueue a simplex request to the head of the queue. Simplex requests
# do not generate responses.
#
# @param @args	the request. Request values must be either scalars,
#				references to Thread::Sociable variables, or Thread::Sociable::Queueable
#				objects
#
# @return		Thread::Sociable::DuplexQueue object if successful; undef if ListenerRequired and
#				no listeners are registered
#*/
sub enqueue_simplex_urgent {
	my $self = shift;
	return $self->_enqueue($self->_marshall(TSDQ_URGENT_SIMPLEX, @_)) ? $self : undef;
}

sub _filter_packet {
	my $params = shift;
	my @results = ();
	my $i = 1;
	my $j = 0;
	my $class = '';

	$class = $params->[$i++],
	$results[$j++] =
		$class ?
			($class eq 'Storable') ?
				thaw($params->[$i++]) :
				${class}->onDequeue($params->[$i++]) :
			$params->[$i++]
		while ($i <= $#$params);

	return \@results;
}

#/**
# Dequeue the next request. Waits until a request is available before
# returning.
#
# @return		arrayref of request values. The request ID is the first element
#				in the returned array.
#*/
sub dequeue  {
    return _filter_packet($_[0]->_dequeue(-1, 0));
}

#/**
# Dequeue the next request. Waits until a request is available, or up to
# $timeout seconds, before returning.
#
# @param $timeout number of seconds to wait for a request
#
# @return		undef if no request available within $timeout seconds. Otherwise,
#				arrayref of request values. The request ID is the first element
#				in the returned array.
#*/
sub dequeue_until {
    my ($self, $timeout) = @_;

    return undef
    	unless $timeout && ($timeout > 0);

	$timeout += time();
	my $request = $self->_dequeue($timeout, 0)
#
#	if none, then we must've timed out
#
		or return undef;

    return _filter_packet($request);
}

#/**
# Dequeue the next request. Returns immediately if no request is available.
#
# @return		undef if no request available; otherwise,
#				arrayref of request values. The request ID is the first element
#				in the returned array.
#*/
sub dequeue_nb {
	my $request = $_[0]->_dequeue(0, 0)
		or return undef;

    return _filter_packet($request);
}

#/**
# Dequeue any available urgent request. Returns immediately if an urgent request
# is not available.
#
# @return		undef if an urgent request is not available; otherwise, an
#				arrayref of request values. The request ID is the first element
#				in the returned array.
#*/
sub dequeue_urgent {
	my $request = $_[0]->_dequeue(0, 1)
		or return undef;

    return _filter_packet($request);
}

#/**
# Report the number of pending requests.
#
# @return		number of requests remaining in the queue.
#*/
# @xs pending

#/**
# Get or set maximum number of pending requests permitted. If setting, signals any
# threads which may be blocked waiting for the number
# of pending requests to drop below the maximum permitted.
#
# @optional $limit		positive integer maximum number of pending requests permitted.
#					A value of zero indicates no limit.
#
# @return		if setting, the prior limit; else the current limit
#*/
# @xs max_pending

sub _cancel_resp {
	my ($self, $resp) = @_;
#
#	collapse the response elements,
#	and call onCancel to any that are Queueables
#
	my $class;
	$class = shift @$resp,
	($class && ${class}->isa('Thread::Sociable::Queueable')) ?
		${class}->onCancel(shift @$resp) :
		shift @$resp
		while (@$resp);
    return 1;
}

#/**
# Post a response to a request. If the request has been cancelled,
# the response is discarded; otherwise, all threads blocked on the
# queue are signalled that a new response is available.
#
# @param $id		the ID of the request being responded to.
# @param @response	the response. Response values must be either scalars,
#					references to Thread::Sociable variables, or Thread::Sociable::Queueable
#					objects
#
# @return		Thread::Sociable::DuplexQueue object
#*/
sub respond {
	my $self = shift;
	my $id = shift;
#
#	silently ignore response to a simplex request
#

	return $self unless defined($id);

	my $resp = $self->_marshall(0, @_)

	return $self->_respond($resp, id) || $self->_cancel_resp($resp);
}

#/**
# Wait for a response to a request. Also available as <code>dequeue_response()</code>
# alias.
#
# @param $id		the request ID of the response for which to wait.
#
# @return 			the response as an arrayref.
#*/
sub wait {
	my ($self, $id) = @_;

	my $result = $self->_wait($id)
		or return undef;

	return _filter_packet($result);
}
*dequeue_response = \&wait;

#/**
# Test if a response is available for a specific request.
#
# @param $id		the request ID of the response for which to test.
#
# @return 			Thread::Sociable::DuplexQueue object if response is available; undef otherwise.
#*/
# @xs ready

#/**
# Test if a response is available for a either any request,
# or for any of a set of requests.
#
# @param @ids		(optional) list of request IDs of responses for which to test.
#
# @return 			first request ID of available responses, or undef if none available
# @returnlist 		list of request IDs of available responses, or undef if none available
#*/
# @xs available

#/**
# Wait up to $timeout seconds for a response to a request.
#
# @param $id		the request ID of the response for which to wait.
# @param $timeout	number of seconds to wait
#
# @return 			the response as an arrayref, or undef if none available within the timeout
#*/
sub wait_until {
	my ($self, $id, $timeout) = @_;
	return undef
		unless $timeout && ($timeout > 0);

	my $result = $self->_wait_until($id, $timeout, threads->tid())
		or return undef;

	return _filter_packet($result);
}
#
#	some grouped waits
#	wait indefinitely for *any* of the
#	supplied ids
#
#/**
# Wait for a response to any specified request. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on any of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Sociable::TSDQContainer
# objects (<i>TSDQ is itself a TSDQContainer</i>),
# or arrayrefs with a Thread::Sociable::TSDQContainer object, and zero or more request
# IDs. For Thread::Sociable::TSDQContainer object arguments, and arrayref arguments
# with no identifiers, waits for any enqueue event on the contained queue.
# For arrayref arguments with IDS, waits for a response event for any
# of the specified IDs.
#
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Sociable::TSDQContainer objects,
#						or arrayrefs containing a Thread::Sociable::TSDQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of TSDQ containers which have events pending.
#*/
sub wait_any {
	my $self = shift;
	return _tsdq_wait(undef, undef, @_)
		unless ref $self;

	my $responses = $self->_wait_any(@_);

	my ($id, $pkt);
	$responses{$id} = _filter_packet($pkt)
		while (($id, $pkt) = each %$responses);

    return $responses;
}
#
#	wait up to timeout for any
#
#/**
# Wait up to $timeout seconds for a response to any specified request. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on any of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Sociable::TSDQContainer objects,
# or arrayrefs with a Thread::Sociable::TSDQContainer object, and zero or more request
# IDs. For Thread::Sociable::TSDQContainer object arguments, and arrayref arguments
# with no identifiers, waits for any enqueue event on the queue.
# For arrayref arguments with IDS, waits for a response event for any
# of the specified IDs.
#
# @param $timeout		number of seconds to wait for a response event
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Sociable::TSDQContainer objects,
#						or arrayrefs containing a Thread::Sociable::TSDQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		undef if no response events occured within $timeout seconds; otherwise,
#				as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of queues which have events pending.
#*/
sub wait_any_until {
	my $self = shift;
	return _tsdq_wait(shift, undef, @_)
		unless ref $self;

	my $timeout = shift;

	return undef unless $timeout && ($timeout > 0);

	my $responses = $self->_wait_any_until($timeout, @_);

	my ($id, $pkt);
	$responses{$id} = _filter_packet($pkt)
		while (($id, $pkt) = each %$responses);

    return $responses;
}
#/**
# Wait for a response to all specified requests. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on all of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Sociable::TSDQContainer objects,
# or arrayrefs with a Thread::Sociable::TSDQContainer object, and zero or more request
# IDs. For Thread::Sociable::TSDQContainer object arguments, and arrayref arguments
# with no identifiers, waits for responses to all current requests on the queue.
# For arrayref arguments with IDS, waits for a response to all
# of the specified IDs.
#
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Sociable::TSDQContainer objects,
#						or arrayrefs containing a Thread::Sociable::TSDQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of queues which have events pending.
#*/
sub wait_all {
	my $self = shift;
	return _tsdq_wait(undef, 1, @_)
		unless ref $self;

	my $responses = $self->_wait_all(@_);

	my ($id, $pkt);
	$responses{$id} = _filter_packet($pkt)
		while (($id, $pkt) = each %$responses);

    return $responses;
}

#/**
# Wait up to $timeout seconds for a response to all specified requests. May be called as either
# an instance or class method.
# <p>
# As an instance method, a list of request IDs is provided, and the method waits for
# a response event on all of the specified requests.
# <p>
# As a class method, the caller provides a list of either Thread::Sociable::TSDQContainer objects,
# or arrayrefs with a Thread::Sociable::TSDQContainer object, and zero or more request
# IDs. For Thread::Sociable::TSDQContainer object arguments, and arrayref arguments
# with no identifiers, waits for responses to all current requests on the queue.
# For arrayref arguments with IDS, waits for a response to all
# of the specified IDs.
#
# @param $timeout		number of seconds to wait for all response
# @param @IDs_or_container_refs	as instance method, a list of request IDs to wait for;
#						as class method, a list of either of Thread::Sociable::TSDQContainer objects,
#						or arrayrefs containing a Thread::Sociable::TSDQContainer object, followed by
#						zero or more request IDs for the queue object.
#
# @return 		undef unless all response events occured within $timeout seconds; otherwise,
#				as an instance method, returns a hashref of request IDs mapped to their response;
#				as a class method, returns a list of queues which have events pending.
#*/
sub wait_all_until {
	my $self = shift;

	return _tsdq_wait(shift, 1, @_)
		unless ref $self;

	my $timeout = shift;

	return undef unless $timeout && ($timeout > 0);

	my $responses = $self->_wait_all_until($timeout, @_);

	my ($id, $pkt);
	$responses{$id} = _filter_packet($pkt)
		while (($id, $pkt) = each %$responses);

    return $responses;
}

#/**
# Mark a request with a value. Provides a means to
# associate properties to a request after it has been
# queued, but before the response has been posted. The
# responder may test for marks via the <a href='#marked'>marked()</a>
# method, or retrieve the mark value via <a href='#get_mark'>get_mark()</a>.
#
# @param $id	ID of request to be marked
# @param $value	(optional) value to be added as a mark; if not specified,
#				a default value of 1 is used.
#
# @return		Thread::Sociable::DuplexQueue object
#*/
# @xs mark

#/**
# Remove any marks from a request.
#
# @param $id	ID of request to be unmarked.
#
# @return		Thread::Sociable::DuplexQueue object
#*/
# @xs unmark

#/**
# Returns any current mark on a specified request.
#
# @param $id	ID of request whose mark is to be returned.
#
# @return		the mark value; undef if not marked
#*/
# @xs get_mark

#/**
# Test if a request is marked, or if the mark is a specified value.
#
# @param $id	ID of request to test for a mark
# @param $value	(optional) value to test for
#
# @return		1 if the request is marked and either no $value was specified,
#				or, if a $value was specified, the mark value equals $value; undef
#				otherwise.
#*/
# @xs marked


#/**
# Cancel one or more pending requests.
# <p>
# If a response to a cancelled request has already been
# posted to the queue response map (i.e., the request has already
# been serviced), the response is removed from the map,
# the <a href='./Queueable.html#onCancel>onCancel()</a> method is
# invoked on each <a href='./Queueable.html>Thread::Sociable::Queueable</a>
# object in the response, and the response is discarded.
# <p>
# If a response to a cancelled request has <b>not</b> yet been posted to
# the queue response map, an empty entry is added to the queue response map.
# (<b>Note:</b> Thread::Sociable doesn't permit splicing sociable arrays yet,
# so we can't remove the request from the queue).
# <p>
# When a server thread attempts to <code>dequeue[_nb|_until]()</code> a cancelled
# request, the request is discarded and the dequeue operation is retried.
# If the cancelled request is already dequeued, the server thread will
# detect the cancellation when it attempts to <a href='#respond'>respond</a> to the request,
# and will invoke the <a href='./Queueable.html#onCancel>onCancel()</a>
# method on any <a href='./Queueable.html>Thread::Sociable::Queueable</a>
# objects in the response, and then discards the response.
# <p>
# <b>Note</b> that, as simplex requests do not have an identifier, there
# is no way to explicitly cancel a specific simplex request.
#
# @param @ids	list of request IDs to be cancelled.
#
# @return		Thread::Sociable::DuplexQueue object
#*/
sub cancel {
	my $self = shift;
#
#	XS does heavy lifting, but returns list of any
#	already responded to so we can cleanup
#
	my @resps = $self->_cancel(@_);
	foreach (@resps) {
#
#	already responded to, call onCancel for any Queueuables
#
		my $resp = $self->get_response($_);
		$self->_cancel_resp($resp)
			if $resp;
	}
	return $self;
}

#/**
# Cancel <b>all</b> current requests and responses, using the
# <a href='#cancel'>cancel()</a> algorithm above, plus cancels
# all simplex requests still in the queue.
# <p>
# <b>Note:</b> In-progress requests (i.e.,
# request which have been removed from the queue, but do not yet
# have an entry in the response map)  will <b>not</b> be cancelled.
#
# @return		Thread::Sociable::DuplexQueue object
#*/
sub cancel_all {
	my $self = shift;
	my @resps = $self->_cancel_all();
	foreach (@resps) {
#
#	already responded to, call onCancel for any Queueuables
#
		my $resp = $self->get_response($_);
		$self->_cancel_resp($resp)
			if $resp;
	}
	return $self;
}
##########################################################
#
#	BEGIN CLASS LEVEL METHODS
#
##########################################################

sub _tsdq_wait {
	my $timeout = shift;
	my $wait_all = shift;
#
#	validate params
#
	foreach (@_) {
		return undef
			unless ($_ &&
				ref $_ &&
				(
					((ref $_ eq 'ARRAY') &&
						($#$_ >= 0) &&
						ref $_->[0] &&
						$_->[0]->isa('Thread::Sociable::TSDQContainer')
					) ||
					$_->isa('Thread::Sociable::TSDQContainer')
				));
	}

	my @avail = ();

	my @qs = ();
	my @containers = ();
	push(@containers, ((ref $_ eq 'ARRAY') ? $_->[0] : $_)),
	push(@qs, $containers[-1]->get_queue())
		foreach (@_);

#print join(', ', @qs), "\n";

	my ($q, $container, $ids);
	my $count = scalar @qs;
	my @ids;
	$timeout += time() if $timeout;
	while ($count) {
		_enter_tsdq_lock();
		foreach (0..$#_) {
			last unless $count;
			next unless $qs[$_];
			$q = $qs[$_];
			$container = $containers[$_];
			$ids = $_[$_];
#
#	if we've got ids, check for responses
#
			push(@avail, $container),
			$qs[$_] = undef,
			$count--
	    		if (((ref $ids eq 'ARRAY') && (scalar @$ids > 1)) ?
	    			$q->available(@{$ids}[1..$#$ids]) :
	    			$q->pending());

		}	# end foreach queue
		_leave_tsdq_lock(),
		last
			unless (($wait_all && $count) || (! scalar @avail));

 		unless ($timeout) {

			print STDERR "TSDQ: locking...\n"
			 	if $tsq_debug;

	 		cond_wait($tsq_global_lock);
			print STDERR "TSDQ: locked\n"
			 	if $tsq_debug;

	 		next;
 		}
#		print STDERR "timed out and avail has ", scalar @avail, "\n" and
		cond_timedwait($tsq_global_lock, $timeout);
		_leave_tsdq_lock();
		return ()
			unless ($timeout > time());
	}

#print STDERR "avail has ", scalar @avail, "\n";
	return @avail;
}
##########################################################
#
#	END CLASS LEVEL METHODS
#
##########################################################
###############################################
#
#	All TSDQbl default methods can be used as is
#
###############################################
###############################################
#
#	TSDQContainer overrides
#
###############################################
sub set_queue { return $_[0]; }

sub get_queue { return $_[0]; }

1;
