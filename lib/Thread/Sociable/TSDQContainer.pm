#/**
# Abstract class for Thread::Sociable::DuplexQueue container object.
# Used by TSDQ's wait() class methods, to simplify waiting
# for an object with a pending queue event (e.g.,
# Thread::Sociable::Apartment::Client objects).
# <p>
# Licensed under the Academic Free License version 2.1, as specified in the
# License.txt file included in this software package, or at
# <a href="http://www.opensource.org/licenses/afl-2.1.php">OpenSource.org</a>.
#
# @author D. Arnold
# @since 2005-12-01
# @self	$self
# @see		<a href='./Queueable.html'>Thread::Sociable::Queueable</a>
#*/
package Thread::Sociable::TSDQContainer;
#
#	Copyright (C) 2005,2006, Presicient Corp., USA
#
#/**
# Returns the contained TSDQ object.
# Abstract method that assumes the object is hash based, and
# the contained TSDQ is in a member named <b>_tsdq</b>.
#
# @return		the contained TSDQ object
#*/
sub get_queue { return $_[0]->{_tsdq}; }

#/**
# Set the contained TSDQ object.
# Abstract method that assumes the object is hash based, and
# the contained TSDQ is in a member named <b>_tqd</b>.
#
# @param $tsdq	the TSDQ to be contained
#
# @return		the TSDQContainer object
#*/
sub set_queue { $_[0]->{_tsdq} = $_[1]; return $_[0]; }

1;