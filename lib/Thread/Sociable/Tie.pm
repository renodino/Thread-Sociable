#/**
# Generic package for sociable ties. Invoked by the Thread::Sociable::sociably_tie
# function to provide a sociable routing wrapper for multithreaded tie's.
# <p>
# Sociably tying is only supported within apartment threaded environments
# (specifically, from within a Thread::Sociable::Apartment::Server object
# hierarchy), and is applied to <b>existing</b> sociable variables.
# <p>
# <i><b>NOTE:</b> This documentation is provided for informational
# purposes only; applications are <b>not</b> expected to directly reference
# the methods provided.</i>
# <p>
# Sociable tie contexts are maintained in a thread-local
# Thread::Sociable::Apartment::Server class variable hash, keyed by the
# stringified version of the tied variable reference.
# An application specifies sociable tying via
# <pre>
#	sociably_tie $sociable, 'TieClass' [, @tie_params] ;
# </pre>
# which results in the following processing:
# <ol>
# <li>The appropriate Thread::Sociable::Tie::TIE<i>XXX</i> constructor is
# invoked to create a proxy context
#
# <li>a reference to <code>$sociable</code>'s underlying sociable element
# is stored in the proxy context
#
# <li>a new thread-private <i>shadow</i> variable is created and <code>tie</code>'d
# to the supplied 'TieClass', using any provided <code>@tie_params</code>; the
# returned ref to the shadow is stored in the proxy context.
#
# <li><code>$sociable</code> is dissociated from it's sociable element
# (making it a thread-private variable), and <code>tie</code>'d to this class.
#
# <li><code>Thread::Sociable::sociable_tie()</code> is then called with
# the sociable element reference, the stringified ref of <code>$sociable</code>,
# and the apartment thread's sociable queue. <code>sociable_tie()</code>
# associates the provided information with the sociable element.
#
# <li>the reference to <code>$sociable</code> is then returned.
# </ol>
# <p>
# Once $sociable is tie'd through Thread::Sociable::Tie, all subsequent references
# from within the thread will be routed through the shadow variable. References from
# external threads are routed as follows:
# <ol>
# <li>An external thread references a sociable element with one or more
#	sociable ties registered to it.
#
# <li>Thread::Sociable posts an equivalent tie() method call, with the stringified
# ref to the thread's proxy variable, and any associated
# parameter values, to each registered tie's apartment thread TSDQ, and then waits for
# responses to all the posted methods.
#
# <li>Each tie'd thread uses the stringified ref to look up the proxy context for
# the tied variable, and then invokes the equivalent tied operation on the shadow
# variable of the context.
#
# <li>if the tied operation returns a value, it is returned as a return value
# to Thread::Sociable, which will store it in the sociable element.
#
# <li>When all registered threads have posted their response, Thread::Sociable
# will apply the final value of the sociable element and return to the original
# referent.
# </ol>
#
# <b>Note</b> that the tie method posting and waiting for responses is executed
# within the referent's thread.
# <p>
# To untie, a thread calls
# <pre>
#	sociably_untie $sociable;
# </pre>
# which results in the following processing:
# <ol>
# <li>Thread::Sociable::Tie::UNTIE is invoked
# <li>untie() is called on the shadow variable.
# <li>the original $sociable variable is re-associated with the
# underlying sociable element
# <li>the thread is deregistered from the sociable element, causing
# the TSDQ to be removed from sociable element's context.
# <li>the proxy context is removed from the Thread::Sociable::Apartment's
# tie proxy list.
# </ol>
#
# <b>Note</b> that a DESTROY operation will perform the same deregistration
# operation.
# <p>
# <code>sociably_tied()</code> (the equivalent of tied()) performs the
# following processing:
# <ol>
# <li>the stringified ref of the variable is used to lookup its proxy context
# <li>if not found, undef is returned immediately
# <il>otherwise, returns the result of <code>tied()</code> on the shadow
# variable
# </ol>
# Copyright&copy; 2007, Presicient Corp., USA
# <p>
# Licensed under the Academic Free License version 3.0, as specified in the
# License.txt file included in this software package, or at
# <a href="http://www.opensource.org/licenses/afl-3.0.php">OpenSource.org</a>.
#
# @author D. Arnold
# @since 2007-03-31
# @self	$self
# @see		<a href='../Sociable.html'>Thread::Sociable</a>
#*/
package Thread::Sociable::Tie;

#/*
# Sociably tie a scalar.
#
# @param $class		Thread::Sociable::SociableTie class
# @param $sociable	Thread::Sociable variable to be tied
# @param $realclass	Name of class originally specified by application
# @optional @args	Any additional arguments for the original tie class.
#*/
sub TIESCALAR {
	my $class = shift;
	my $sociable = shift;
	my $realclass = shift;
	my $privscalar;

	my $proxyid = Thread::Sociable::Apartment::nextProxyId();
#
#	create a local private scalar and tie it to the
#	specified class
#
	return bless \$privscalar, $class;
}


sub TIEHANDLE {
	if (1 == 0) {
	my $class = shift;
	my $sociable = shift;
	my $privfd;

	my $proxyid = Thread::Sociable::Apartment::nextProxyId();
#
#	create a local private filehandle (?) and tie it to the
#	specified class
#
	return bless \$privfd, $class;
	}
}


sub TIEARRAY {
	my $class = shift;
	my $sociable = shift;
	my @privary;

	my $proxyid = Thread::Sociable::Apartment::nextProxyId();
#
#	create a local private array and tie it to the
#	specified class
#
	return bless \@privary, $class;
}


sub TIEHASH {
	my $class = shift;
	my $sociable = shift;
	my %privhash;

	my $proxyid = Thread::Sociable::Apartment::nextProxyId();
#
#	create a local private hash and tie it to the
#	specified class
#
	return bless \%privhash, $class;
}


################################################
#
#	BEGIN GENERIC METHODS
#
################################################

sub FETCH {
}

sub STORE {
}

sub DESTROY {
}

sub UNTIE {
}

################################################
#
#	BEGIN HASH METHODS
#
################################################
sub DELETE {
}

sub CLEAR {
}

sub EXISTS {
}

sub FIRSTKEY {
}

sub NEXTKEY {
}

sub SCALAR {
}
################################################
#
#	BEGIN ARRAY METHODS
#
################################################
sub FETCHSIZE {
}

sub STORESIZE {
}

sub PUSH {
}

sub POP {
}

sub SHIFT {
}

sub UNSHIFT {
}

sub SPLICE {
}

sub EXTEND {
}

################################################
#
#	BEGIN FILEHANDLE METHODS
#
################################################
sub READ {
}

sub READLINE {
}

sub GETC {
}

sub WRITE {
}

sub PRINT {
}

sub PRINTF {
}

sub BINMODE {
}

sub EOF {
}

sub FILENO {
}

sub SEEK {
}

sub TELL {
}

sub OPEN {
}

sub CLOSE {
}


1;