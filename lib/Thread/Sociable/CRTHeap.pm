=pod

=begin classdoc

Provides the default C runtime heap storage mechanism.

@author D. Arnold
@since 2007-10-08
@see Thread::Sociable

=end classdoc

=cut
package Thread::Sociable::CRTHeap;

use 5.008;

use Thread::Sociable;

use strict;
use warnings;

our $VERSION = '1.01';

require XSLoader;
XSLoader::load('Thread::Sociable::CRTHeap', $VERSION);

sub new {
	return init(@_)
}

1;