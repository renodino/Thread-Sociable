=pod

=begin classdoc

Enhanced replacement for <cpan>threads::shared</cpan>. Provides
<ul>
<li>Classic lock-based concurrency control
<li>Software transactional memory
<li>Integrated thread queueing
<li>support for configurable storage mechanisms
<li>inter-thread tie() support
</ul>

=end classdoc

=cut

package Thread::Sociable;
#
#	derived (in part) from threads::shared 1.01
#
use 5.008;

#use threads;
use Thread::Sociable::Tie;
use Data::Dumper;
use Config;

use strict;
use warnings;
no warnings 'redefine';

our $VERSION = '1.01';

BEGIN {
    # Declare that we have been loaded
    $Thread::Sociable::threads_shared = 1;
}

require XSLoader;
XSLoader::load('Thread::Sociable', $VERSION);

*is_sociable = \&_id;

sub import
{
    my ($package, %config) = @_;

    sociable_enable_debug() if $config{debug};

    sociable_enable_warnings() if $config{warnings};

	sociable_implicit_lockable()
    	if exists $config{always_lockable};

    sociable_default_extent($config{default_extent}, $config{default_himark})
    	if exists $config{default_extent} ||
    		exists $config{default_himark};

    if (exists $config{storage_mechanisms}) {
#
#	scan mechanisms for CRTHeap; if not found AND usethreads defined,
#	install CRTHeap first and make it the default
#	(eventually install appleseeds if useithreads NOT defined?)
#
    	my $mechs = $config{storage_mechanisms};
    	if ((!exists $config{default_mechanism}) && $Config{useithreads}) {
    		while (my ($name, $properties) = each %$mechs) {
    			$properties = [ $properties ] unless ref $properties;
    			next
    				unless ($properties->[0] eq 'Thread::Sociable::CRTHeap');
   				delete $mechs->{$name};
				die $@
					unless sociable_install_mechanism($name, @$properties) &&
		    			sociable_default_mechanism($name);
    		}
		}
    	while (my ($name, $properties) = each %$mechs) {
			$properties = [ $properties ] unless ref $properties;
    		die $@
    			unless sociable_install_mechanism($name, @$properties);
		    die $@
		    	if exists $config{default_mechanism} &&
		    		($config{default_mechanism} eq $name) &&
		    		!sociable_default_mechanism($config{default_mechanism});
    	}
    }
	elsif ($Config{useithreads}) {
		die $@
			unless sociable_install_mechanism('CRTHeap', 'Thread::Sociable::CRTHeap') &&
				   sociable_default_mechanism('CRTHeap');
	}
	

    # Exported subroutines
    my @EXPORT = qw(
    	bless 
    	cond_broadcast
    	cond_signal 
    	cond_timedwait 
    	cond_wait 
    	is_lockable
    	is_sociable 
    	sociable_begin_work
    	sociable_default_extent 
    	sociable_disable_debug
    	sociable_disable_warnings
    	sociable_discard 
    	sociable_enable_debug 
    	sociable_enable_warnings 
    	sociable_explicit_lockable
    	sociable_extent
    	sociable_implicit_lockable
    	sociable_install_mechanism
    	sociable_restart
		sociable_stm_clone
		sociable_stm_dump_log
		sociable_stm_eager
		sociable_stm_get_log
		sociable_stm_lazy
		sociable_stm_limit
		sociable_stm_locked
		sociable_stm_lockfree
		sociable_stm_protocol
		sociable_stm_timeout
    	sociable_tie 
    	sociable_tied 
    	sociable_untie
    	socialize 
    	socialize_with_lock 
    	MODIFY_SCALAR_ATTRIBUTES 
    	MODIFY_ARRAY_ATTRIBUTES 
    	MODIFY_HASH_ATTRIBUTES
    );

    # Export subroutine names
    my $caller = caller();
    foreach my $sym (@EXPORT) {
        no strict 'refs';
        *{$caller.'::'.$sym} = \&{$sym};
    }
}

#
#	chain our version of these methods into UNIVERSAL;
#	after we process our attr's, we'll pass the rest through
#	to the old versions
#
#	add support for parameterized attributes!
#
#our $old_scalar_attr;
#our $old_array_attr;
#our $old_hash_attr;

#BEGIN {
#	$old_scalar_attr = \&UNIVERSAL::MODIFY_SCALAR_ATTRIBUTES;
#	$old_array_attr = \&UNIVERSAL::MODIFY_ARRAY_ATTRIBUTES,
#	$old_hash_attr = \&UNIVERSAL::MODIFY_HASH_ATTRIBUTES,
#	*UNIVERSAL::MODIFY_SCALAR_ATTRIBUTES = \&SociableAttr,
#	*UNIVERSAL::MODIFY_ARRAY_ATTRIBUTES = \&SociableAttr,
#	*UNIVERSAL::MODIFY_HASH_ATTRIBUTES = \&SociableAttr
#		unless $old_scalar_attr;
#}

# /* sub SociableAttr {
sub MODIFY_SCALAR_ATTRIBUTES {
	my $package = shift;
	my $ref = shift;

	my $i = 0;
	my $attr;
	$i++
		while ($i <= $#_) && ($_[$i] ne 'Sociable') && ($_[$i] ne 'Lockable');

	$attr = splice(@_, $i, 1),
	($attr eq 'Sociable') ? socialize($ref) : socialize_with_lock($ref)
		if ($i <= $#_);

	return @_;
}

sub MODIFY_ARRAY_ATTRIBUTES {
	my $package = shift;
	my $ref = shift;

	my $i = 0;
	my $attr;
	$i++
		while ($i <= $#_) && ($_[$i] ne 'Sociable') && ($_[$i] ne 'Lockable');

	$attr = splice(@_, $i, 1),
	($attr eq 'Sociable') ? socialize($ref) : socialize_with_lock($ref)
		if ($i <= $#_);

	return @_;
}

sub MODIFY_HASH_ATTRIBUTES {
	my $package = shift;
	my $ref = shift;

	my $i = 0;
	my $attr;
	$i++
		while ($i <= $#_) && ($_[$i] ne 'Sociable') && ($_[$i] ne 'Lockable');

	$attr = splice(@_, $i, 1),
	($attr eq 'Sociable') ? socialize($ref) : socialize_with_lock($ref)
		if ($i <= $#_);

	return @_;
}

=pod

=begin classdoc

Install a storage mechanism.

@param $name	name of the mechanism
@param @properties	list of mechanism properties; first element must be the name of the 
	package name implementing the mechanism; the remainder of the list is implementation
	dependent, and is passed to the package constructor as is, after the mechanism name,
	e.g.,
<pre>
	appleseeds => [ 'Thread::Sociable::Appleseeds', AllocSize => 1_000_000, ID => 'APPLESEEDS' ],
	crtheap    => 'Thread::Sociable::CRTHeap'
</pre>

@return		1 on success, undef on failure with an error
				message in $@ indicating the install failure.

=end classdoc

=cut

sub sociable_install_mechanism {
	my $name = shift;
	my $class = shift;
#
#	verify the name doesn't exist;
#	is so, and uses same class, silently accept it
#
	eval {
		require $class;
	};
	$@ = "Cannot load mechanism $class: $@",
	return undef
		if $@;
#
#	NOTE: the class constructor doesn't return a typical Perl object;
#	instead, it returns a pointer to a sociable_storage_mech_t
#	structure to be installed in the Thread::Sociable's
#	process-private mechanisms list
#
	return _install_mechanism($name, $class, sub { ${class}->new($name, @_); });
}

=pod

=begin classdoc

Sociably tie's a supplied sociable variable to the
specified class.

=end classdoc

=cut

sub sociable_tie {
	my $socref = _sociable_tie($Thread::Sociable::Apartment::tsdq, @_);
	my $sociable = shift;
	my $class = shift;
	my @proxy = [ $socref ];
	if (ref $socref eq 'SCALAR') {
		my $privscalar;
		push @proxy, tie $privscalar, $class, @_;
	}
	elsif (ref $socref eq 'ARRAY') {
		my @privary;
		push @proxy, tie @privary, $class, @_;
	}
	elsif (ref $socref eq 'HASH') {
		my %privhash;
		push @proxy, tie %privhash, $class, @_;
	}
	elsif (ref $socref eq 'GLOB') {
#		my *privfd;
#		push @proxy, tie *privfd, $class, @_;
	}
	return Thread::Sociable::Apartment::add_sociable_proxy("$socref", \@proxy);
}

=pod

=begin classdoc

Is this variable sociably tied ?

=end classdoc

=cut

sub sociable_tied {

}

=pod

=begin classdoc

Untie's the sociably tied variable from this thread.

=end classdoc

=cut

sub sociable_untie {

}

=pod

=begin classdoc

Start an STM transaction. The thread may already be inside
a transaction, in which case the transactions will nest.
Causes all subsequent sociable variable accesses to be transactionally
logged, using the current conflict detection and commit behavior settings.
Refer to <cpan>Thread::Sociable</cpan> manual for details on using
STM.

@param	&xact_code	a closure to be executed within the transaction
@optional onCommit	a closure to be executed when the transaction commits
@optional onRestart a closure to be executed when the transaction is restarted
@optional onRollback a closure to be executed when the transaction is rolled back

=end classdoc

=cut

sub sociable_begin_work {
	my ($code, %xact) = @_;

	foreach (qw(onCommit onRestart onRollback)) {
		die "$_ argument is not a code reference."
			if exists $xact{$_} && ((!ref $xact{$_}) || (ref $xact{$_} ne 'CODE'));
	}

	my $pending = _sociable_add_xact($xact{onCommit}, $xact{onRestart}, $xact{onRollback});
	while (1) {
		eval {
			$code->();
			sociable_commit()
				if ($pending == 1);
		};
		last
			unless $@;
#
#	cascade the error to outermost xaction
#
		die $@ if ($pending > 1);

		return sociable_rollback()
			if (substr($@, 0, 28) ne 'Thread::Sociable STM RESTART');
#
#	restart automatically invokes rollbacks and sets $@
#
		return
			unless sociable_restart();
#
#	inject a (pseudo)random backoff on restart
#
		threads->yield();
	}
}

=pod

=begin classdoc

Restart the current STM transaction.

@optional $msg	optional message appended to the standard STM restart message

=end classdoc

=cut

sub sociable_restart {
	my $msg = $_[0] ? ": $_[0]" : '';
	die "Thread::Sociable: STM RESTART$msg"; 
}

=pod

=begin classdoc

Dump a pretty-printed version of the current thread's STM log.

=end classdoc

=cut

sub sociable_stm_dump_log {
	my ($xactid, $levels, $karma, $xactcnt, $stmlog) = sociable_stm_get_log();
	print Dumper($xactid, $levels, $karma, $xactcnt, $stmlog);
}

#
#	classdocs for XS methods
#
=pod

=begin classdoc

Transactionally clone a complete sociable array or hash. Creates a complete
thread-private copy of the structure for use during the transaction.
Useful for operations which may iterate over an entire array or hash
inside a transaction, in order to avoid the overhead of incremental
transactional acquisition.

@param $sociable	the sociable array or hash

@xs sociable_stm_clone

=end classdoc

=begin classdoc

Get or set the maximum number of concurrent transactions.

@optional $limit	the desired maximum number of concurrent transactions

@return 	for get operation, the current transaction limit; otherwise, the
			prior transaction limit

@xs sociable_stm_limit

=end classdoc

=begin classdoc

Get or set the maximum lifetime of a transaction. If a transaction
exceeds this time limit, it will be rolled back. <i>Note: transaction
timeout is not yet implemented.</i>

@optional $timeout	the desired maximum transaction lifetime in seconds

@return 	for get operation, the current transaction timeout; otherwise, the
			prior transaction timeout

@xs sociable_stm_timeout

=end classdoc

=begin classdoc

Set the STM commit protocol to locked.

@return the prior commit protocol as a string, either "locked" or "lockfree"

@xs sociable_stm_locked

=end classdoc

=begin classdoc

Set the STM commit protocol to lockfree

@return the prior commit protocol as a string, either "locked" or "lockfree"

@xs sociable_stm_lockfree

=end classdoc

=begin classdoc

Get the current STM commit and conflict detection protocols as a string.

@return a string combining the commit protocol and conflict detection protocol:
	one of "locked eager", "locked lazy", "lockfree eager", "lockfree lazy"

@xs sociable_stm_protocol

=end classdoc

=begin classdoc

Set the STM conflict detection protocol to eager.

@return the prior conflict detection protocol as a string, either "eager" or "lazy"

@xs sociable_stm_eager()

=end classdoc

=begin classdoc

Set the STM conflict detection protocol to lazy.

@return the prior conflict detection protocol as a string, either "eager" or "lazy"

@xs sociable_stm_lazy()

=end classdoc

=begin classdoc

Get the current thread's STM log as a Perl structure.

@return a hash reference containing references to 

@xs sociable_get_stm_log()

=end classdoc

=begin classdoc

Enable debug mode. Causes lots of diagnostic info to be
printed during sociable operations.

@xs sociable_enable_debug()

=end classdoc

=begin classdoc

Disable debug mode. Suppresses diagnostic info
printed during sociable operations.

@xs sociable_disable_debug()

=end classdoc

=begin classdoc

Enable warnings. Causes Perl warnings to be emitted
whenever a reference to a sociable variable determines
the variable has been discarded.

@xs sociable_enable_warnings()

=end classdoc

=begin classdoc

Disable warnings. Suppresses Perl warnings emitted
whenever a reference to a sociable variable determines
the variable has been discarded. The variable is silently
converted to a private version.

@xs sociable_disable_warnings()

=end classdoc

=begin classdoc

Implicitly make all new sociable variables lockable on creation.

@xs sociable_implicit_lockable()

=end classdoc

=begin classdoc

Only make a variable lockable if it is explicitly created
via the Lockable attribute, or the socialize_with_lock()
method.

@xs sociable_explicit_lockable()

=end classdoc

=begin classdoc

Convert the specified variable to a sociable. If the
implicit lockable flag has been set, the variable
will also have a lock context associated with it.

@param	*variable	the variable to be socialized. May be a scalar, array, or hash.

@return a reference to the socialized variable.

@xs socialize()

=end classdoc

=begin classdoc

Convert the specified variable to a sociable with
a lock context associated with it.

@param	*variable	the variable to be socialized. May be a scalar, array, or hash.

@return a reference to the socialized variable.

@xs socialize_with_lock()

=end classdoc

=begin classdoc

Discard the sociable version of the specified variable.
Any future reference to the variable by any thread
will revert to a private version (optionally emitting
a Perl warning if warnings have been enabled)

@param	*variable	the variable to be socialized. May be a scalar, array, or hash.

@return true

@xs sociable_discard()

=end classdoc

=begin classdoc

Get or set the default extents applied to sociable arrays and hashes.
If neither extent nor himark is specified, then a get operation
is performed. If only extent is specified, himark is set to
int(1.1 * extent).<p>
The initial default extent is 10, and default himark is 20.

@optional $extent	the default extent to be applied to structures.
@optional $himark	the default high water mark to be applied to structures.

@returnlist the prior default extent and himark values.

@xs sociable_default_extent()

=end classdoc

=begin classdoc

Get or set the extents applied to a sociable array or hash.
If neither extent nor himark is specified, then a get operation
is performed. If only extent is specified, himark is set to
int(1.1 * extent).

@param	*variable	the variable to be socialized. May be an array or hash.
@optional $extent	the extent to be applied to the variable.
@optional $himark	the high water mark to be applied to the variable.

@returnlist the prior extent and himark values for the variable.

@xs sociable_default_extent()

=end classdoc

=begin classdoc

Is this variable sociable ?

@param	*variable	the variable to be tested for sociability. May be a scalar, array or hash.

@return undef if not sociable; otherwise, the ID of the variable's backing sociable storage

@xs is_sociable

=end classdoc

=begin classdoc

Is this variable lockable ?

@param	*variable	the variable to be tested for lockability. May be a scalar, array or hash.

@return undef if not lockable; otherwise, the ID of the variable's backing sociable storage

@xs is_lockable

=cut

1;

