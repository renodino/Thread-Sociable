package SocBenchmark;

use threads;
use Thread::Sociable;
use Time::HiRes qw(time);

use strict;
use warnings;

sub _groupRun {
	my $sub = shift;
	my $threads = shift;

	my @thrds = ();
	push @thrds, threads->create($sub, @_)
		for (1..$threads);
	my $ttltime = 0;

	$ttltime += $_->join()
		foreach (@thrds);
	return $ttltime;
}

######################################################
#
#	BEGIN SCALAR TEST
#
######################################################

sub scalarRead {
	my ($threads, $cycles, $str) = @_;

	$str ||= 'some string';
	return ($threads > 1) ?
		_groupRun(\&scalarReadThread, $threads, $cycles, $str) :
		scalarReadThread($cycles, $str);
}

sub scalarWrite {
	my ($threads, $cycles, $str) = @_;

	return ($threads > 1) ?
		_groupRun(\&scalarWriteThread, $threads, $cycles, $str) :
		scalarWriteThread($cycles);
}

sub scalarReadThread {
	my ($cycles, $str) = @_;

	my $test : Sociable = $str;

	my $t;
	sleep 2;
	my $started = time();
	$t = $test for (1..$cycles);
	sociable_discard($test);

	return time() - $started;
}

sub scalarWriteThread {
	my ($cycles) = @_;

	my $test : Sociable = 'some string';

	sleep 2;
	my $started = time();
	$test = $_ for (1..$cycles);
	sociable_discard($test);
	return time() - $started;
}

######################################################
#
#	END SCALAR TEST
#
######################################################

######################################################
#
#	BEGIN LOCKABLE SCALAR TEST
#
######################################################
sub scalarLockedRead {
	my ($threads, $cycles, $str, $opcnt) = @_;

	$str ||= 'some string';
	return ($threads > 1) ?
		_groupRun(\&scalarLockedReadThread, $threads, $cycles, $str, $opcnt) :
		scalarLockedReadThread($cycles, $str, $opcnt);
}

sub scalarLockedWrite {
	my ($threads, $cycles, $str, $opcnt) = @_;

	return ($threads > 1) ?
		_groupRun(\&scalarLockedWriteThread, $threads, $cycles, $str, $opcnt) :
		scalarLockedWriteThread($cycles, $str, $opcnt);
}

sub scalarLockedReadThread {
	my ($cycles, $str, $opcnt) = @_;

	my $test : Lockable = $str;
	my @ops = (0..$opcnt);

	sleep 2;
	my $started = time();
	for my $i (1..$cycles) {
		lock($test);
		$ops[$_] = $test
			for (1..$opcnt);
	}
	sociable_discard($test);
	return time() - $started;
}

sub scalarLockedWriteThread {
	my ($cycles, $str, $opcnt) = @_;

	my $test : Lockable = 'some string';
	my @ops = (($str) x $opcnt);

	sleep 2;
	my $started = time();
	for my $i (1..$cycles) {
		lock($test);
		$test = $ops[$_]
			for (0..$#ops);
	}
	sociable_discard($test);
	return time() - $started;
}
######################################################
#
#	END LOCKABLE SCALAR TEST
#
######################################################
######################################################
#
#	BEGIN ARRAY TEST
#
######################################################

sub _initArray {
	my ($test, $pos, $cycles, $len) = @_;
	@$pos = map int(rand($len)), 1..100;
	$test->[$_] = $_ for (0..$len-1);
	sleep 2;
	return time();
}

sub arrayRead {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&arrayReadThread, $threads, $cycles, $len) :
		arrayReadThread($cycles, $len);
}

sub arrayWrite {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&arrayWriteThread, $threads, $cycles, $len) :
		arrayWriteThread($cycles, $len);
}

sub arrayReadThread {
	my ($cycles, $len) = @_;

	my @test : Sociable = ();

	@test = (1..$len);

	my $t;
#
#	precompute a random access pattern
#
	my @pos = ();
	my $started = _initArray(\@test, \@pos, $cycles, $len);
	$t = $test[$pos[$_%100]]
		for (1..$cycles);
	sociable_discard(@test);
	return time() - $started;
}

sub arrayWriteThread {
	my ($cycles, $len) = @_;

	my @test : Sociable = (1..$len);
#
#	precompute a random access pattern
#
	my @pos = ();
	my $started = _initArray(\@test, \@pos, $cycles, $len);
	$test[$pos[$_%100]] = $_
		for (1..$cycles);
	sociable_discard(@test);
	return time() - $started;
}

######################################################
#
#	END ARRAY TEST
#
######################################################
######################################################
#
#	BEGIN ARRAY EXTENT TEST
#
######################################################
sub arrayExtentWrite {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&arrayExtentWriteThread, $threads, $cycles, $len) :
		arrayExtentWriteThread($cycles, $len);
}

sub arrayExtentWriteThread {
	my ($cycles, $len) = @_;

	my @test : Sociable = ();
	sociable_extent(@test, $len + 5, $len + 10);

	my $started = time();
	for (1..$cycles) {
		{
			@test = 1..$len;
		}
		{
			@test = ();
		}
	}
	sociable_discard(@test);
	return time() - $started;
}

######################################################
#
#	END ARRAY EXTENT TEST
#
######################################################
######################################################
#
#	BEGIN ARRAY OPS TEST
#
######################################################
sub arrayOps {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&arrayOpsThread, $threads, $cycles, $len) :
		arrayOpsThread($cycles, $len);
}

#
#	do push, pop, shift, unshift
#	can we do splice ???
#
sub arrayOpsThread {
	my ($cycles, $len) = @_;

	my @test : Sociable = ();

	@test = 1..$len;
	my $t;

	print "*** length is ", $#test, " scalar is ", (scalar @test), "\n";

	print "*** element 1 does not exist!\n"
		unless exists $test[1];

	print "*** element $len + 1 exists!\n"
		if exists $test[$len + 1];

	print "*** negative element not correct!\n"
		unless ($test[-1] == $test[$len - 1]);

	$t = delete $test[1];
	print "*** element 1 not deleted!\n"
		if (!$t) || ($t != 2) || defined($test[1]);
	$test[1] = $t;
#
#	need a test to delete off end of array
#

#
#	precompute a random access pattern
#
	my $started = time();

	for (1..$cycles) {
		$t = pop(@test);
		print "pop returns $t\n";
		unshift(@test, $t);
		$t = shift @test;
		print "shift returns $t\n";
		push(@test, $t);
	}

	sociable_discard(@test);

	return time() - $started;
}

######################################################
#
#	END ARRAY OPS TEST
#
######################################################
######################################################
#
#	BEGIN LOCKABLE ARRAY TEST
#
######################################################

sub arrayLockedRead {
	my ($threads, $cycles, $len, $opcnt) = @_;

	return ($threads > 1) ?
		_groupRun(\&arrayLockedReadThread, $threads, $cycles, $len, $opcnt) :
		arrayLockedReadThread($cycles, $len, $opcnt);
}

sub arrayLockedWrite {
	my ($threads, $cycles, $len, $opcnt) = @_;

	return ($threads > 1) ?
		_groupRun(\&arrayLockedWriteThread, $threads, $cycles, $len, $opcnt) :
		arrayLockedWriteThread($cycles, $len, $opcnt);
}

sub arrayLockedReadThread {
	my ($cycles, $len, $opcnt) = @_;

	sociable_disable_debug();
	my @test : Lockable = ();
	@test = 1..$len;
	my @ops = (0..$opcnt);

	my $t;
#
#	precompute a random access pattern
#
	my @pos = ();
	my $started = _initArray(\@test, \@pos, $cycles, $len);
	for my $i (1..$cycles) {
		lock(@test);
		$t = $pos[$i%100];
		$ops[$_] = $test[$t] for (1..$opcnt);
	}
	sociable_enable_debug();
	sociable_discard(@test);
	return time() - $started;
}

sub arrayLockedWriteThread {
	my ($cycles, $len, $opcnt) = @_;
	sociable_disable_debug();

	my @test : Lockable = ();
	@test = 1..$len;
	my @ops = (0..$opcnt);
	my $t;
#
#	precompute a random access pattern
#
	my @pos = ();
	my $started = _initArray(\@test, \@pos, $cycles, $len);
	for my $i (1..$cycles) {
		lock(@test);
		$t = $pos[$i%100];
		$test[$t] = $ops[$_] for (1..$opcnt);
	}
	sociable_enable_debug();
	sociable_discard(@test);
	return time() - $started;
}

######################################################
#
#	END LOCKABLE ARRAY TEST
#
######################################################
######################################################
#
#	BEGIN HASH TEST
#
######################################################

sub _initHash {
	my ($test, $pos, $cycles, $len) = @_;
	@$pos = map int(rand($len)), 1..100;
	$test->{$_} = $_ for (1..$len);
	sleep 2;
	return time();
}

sub hashRead {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&hashReadThread, $threads, $cycles, $len) :
		hashReadThread($cycles, $len);
}

sub hashWrite {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&hashWriteThread, $threads, $cycles, $len) :
		hashWriteThread($cycles, $len);
}

sub hashReadThread {
	my ($cycles, $len) = @_;
	my %test : Sociable = ();

	my $t;
#
#	precompute a random access pattern and some contents
#
	my @pos = ();
	my $started = _initHash(\%test, \@pos, $cycles, $len);
	$t = $test{$pos[$_%100]}
		for (1..$cycles);
	sociable_discard(%test);
	return time() - $started;
}

sub hashWriteThread {
	my ($cycles, $len) = @_;

	my %test : Sociable = ();
#
#	precompute a random access pattern
#
	my @pos = ();
	my $started = _initHash(\%test, \@pos, $cycles, $len);
	$test{$pos[$_%100]} = $_
		for (1..$cycles);
	sociable_discard(%test);
	return time() - $started;
}


######################################################
#
#	END HASH TEST
#
######################################################
######################################################
#
#	BEGIN HASH EXTENT TEST
#
######################################################
sub hashExtentWrite {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&hashExtentWriteThread, $threads, $cycles, $len) :
		hashExtentWriteThread($cycles, $len);
}

sub hashExtentWriteThread {
	my ($cycles, $len) = @_;

	my %test : Sociable = ();
	sociable_extent(%test, $len + 5, $len + 10);
#
#	precompute a random access pattern
#
	my @pos = ();
	my $started = _initHash(\%test, \@pos, $cycles, $len);
	$test{$_} = $_
		for (1..$cycles);
	sociable_discard(%test);
	return time() - $started;
}


######################################################
#
#	END HASH EXTENT TEST
#
######################################################

######################################################
#
#	BEGIN LOCKABLE HASH TEST
#
######################################################
sub hashLockedRead {
	my ($threads, $cycles, $len, $opcnt) = @_;

	return ($threads > 1) ?
		_groupRun(\&hashLockedReadThread, $threads, $cycles, $len, $opcnt) :
		hashLockedReadThread($cycles, $len, $opcnt);
}

sub hashLockedWrite {
	my ($threads, $cycles, $len, $opcnt) = @_;

	return ($threads > 1) ?
		_groupRun(\&hashLockedWriteThread, $threads, $cycles, $len, $opcnt) :
		hashLockedWriteThread($cycles, $len, $opcnt);
}

sub hashLockedReadThread {
	my ($cycles, $len, $opcnt) = @_;
	my %test : Lockable = ();
	my @ops = (0..$opcnt);
	my $t;
#
#	precompute a random access pattern and some contents
#
	my @pos = ();
	my $started = _initHash(\%test, \@pos, $cycles, $len);
	for my $i (1..$cycles) {
		lock(%test);
		$t = $pos[$i%100];
		$ops[$_] = $test{$t} for (1..$opcnt);
	}
	sociable_discard(%test);
	return time() - $started;
}

sub hashLockedWriteThread {
	my ($cycles, $len, $opcnt) = @_;

	my %test : Lockable = ();
	my @ops = (0..$opcnt);
	my $t;
#
#	precompute a random access pattern
#
	my @pos = ();
	my $started = _initHash(\%test, \@pos, $cycles, $len);
	for my $i (1..$cycles) {
		lock(%test);
		$t = $pos[$i%100];
		$test{$t} = $ops[$_] for (1..$opcnt);
	}
	sociable_discard(%test);
	return time() - $started;
}


######################################################
#
#	END LOCKABLE HASH TEST
#
######################################################

######################################################
#
#	BEGIN HASH OPS TEST
#
######################################################
sub hashOps {
	my ($threads, $cycles, $len) = @_;

	return ($threads > 1) ?
		_groupRun(\&hashOpsThread, $threads, $cycles, $len) :
		hashOpsThread($cycles, $len);
}

#
#	do delete and exists
#
sub hashOpsThread {
	my ($cycles, $len) = @_;

	my %test : Sociable = ();

	$test{$_} = $_
		for (1..$len);
	my ($t, $k, $v);

	$t = 0;
	while (($k, $v) = each %test) {
		print "\n*** bad key/value \n" and next
			unless $k && $v &&  ($k == $v);
		$t++;
	}

	print "\n*** missing some keys!\n"
		unless ($t == $len);
#
#	precompute a random access pattern
#
	my $started = time();

	my $i;
	for (1..$cycles) {
		$i = $_%$len;
		if (exists $test{$i}) {
			$t = delete $test{$i};
			print "\n*** deleted element not returned!\n"
				unless $t && ($t == $i);
		}
		else {
			print "\n*** element $i not exists!\n"
		}
		$test{$i} = $i;
	}
	sociable_discard(%test);

	return time() - $started;
}

######################################################
#
#	END HASH OPS TEST
#
######################################################

1;