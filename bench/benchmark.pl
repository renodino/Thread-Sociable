BEGIN {
	unshift @INC, './bench';
}
use SocBenchmark;
use ShrBenchmark;

use strict;
use warnings;

my @tests = ( (0) x 16 );
my @testnames = (
	'Scalar read',
	'Scalar write',
	'Scalar locked read',
	'Scalar locked write',
	'Array read',
	'Array write',
	'Array locked read',
	'Array locked write',
	'Array extent write',
	'Array ops',
	'Hash read',
	'Hash write',
	'Hash locked read',
	'Hash locked write',
	'Hash extent write',
	'Hash ops',
);

my @soctestsubs = (
	\&SocBenchmark::scalarRead,
	\&SocBenchmark::scalarWrite,
	\&SocBenchmark::scalarLockedRead,
	\&SocBenchmark::scalarLockedWrite,
	\&SocBenchmark::arrayRead,
	\&SocBenchmark::arrayWrite,
	\&SocBenchmark::arrayLockedRead,
	\&SocBenchmark::arrayLockedWrite,
	\&SocBenchmark::arrayExtentWrite,
	\&SocBenchmark::arrayOps,
	\&SocBenchmark::hashRead,
	\&SocBenchmark::hashWrite,
	\&SocBenchmark::hashLockedRead,
	\&SocBenchmark::hashLockedWrite,
	\&SocBenchmark::hashExtentWrite,
	\&SocBenchmark::hashOps,
);

my @shrtestsubs = (
	\&ShrBenchmark::scalarRead,
	\&ShrBenchmark::scalarWrite,
	\&ShrBenchmark::scalarLockedRead,
	\&ShrBenchmark::scalarLockedWrite,
	\&ShrBenchmark::arrayRead,
	\&ShrBenchmark::arrayWrite,
	\&ShrBenchmark::arrayLockedRead,
	\&ShrBenchmark::arrayLockedWrite,
	\&ShrBenchmark::arrayExtentWrite,
	\&ShrBenchmark::arrayOps,
	\&ShrBenchmark::hashRead,
	\&ShrBenchmark::hashWrite,
	\&ShrBenchmark::hashLockedRead,
	\&ShrBenchmark::hashLockedWrite,
	\&ShrBenchmark::hashExtentWrite,
	\&ShrBenchmark::hashOps,
);

my $cycles = 10000;
my $threads = 1;
my $len = 10;
my $size = 10;
my $skips = 3;
my $opcnt = 1;

my %opts = qw(
	-sr 0
	-sw 1
	-slr 2
	-slw 3
	-ar 4
	-aw 5
	-alr 6
	-alw 7
	-ae 8
	-ao 9
	-hr 10
	-hw 11
	-hlr 12
	-hlw 13
	-he 14
	-ho 15
);


while (@ARGV && substr($ARGV[0], 0, 1) eq '-') {
	my $opt = shift @ARGV;

	$cycles = shift @ARGV, next
		if ($opt eq '-c');

	$threads = shift @ARGV, next
		if ($opt eq '-t');

	$len = shift @ARGV, next
		if ($opt eq '-l');

	$skips &= 1, next
		if ($opt eq '-x');

	$skips &= 2, next
		if ($opt eq '-y');

	$size = shift @ARGV, next
		if ($opt eq '-z');

	$opcnt = shift @ARGV, next
		if ($opt eq '-o');

	@tests[0..3] = (1,1,1,1) and next
		if ($opt eq '-s');

	@tests[4..9] = (1,1,1,1,1,1) and next
		if ($opt eq '-a');

	@tests[10..15] = (1,1,1,1,1,1) and next
		if ($opt eq '-h');

	$tests[$opts{$opt}] = 1, next
		if exists $opts{$opt};

	print "Unknown options $opt

Valid options are

    -a           : all array tests
    -ar          : array read
    -aw          : array write
    -alr         : array locked read
    -alw         : array locked write
    -ae          : array extent write
    -ao          : array ops (push, pop, shift, unshift)
    -c <count>   : # of cycles per test, default 10000
    -h           : all hash tests
    -hr          : hash read
    -hw          : hash write
    -hlr         : hash locked read
    -hlw         : hash locked write
    -he          : hash extent write
    -ho          : hash ops (delete, exists)
    -l <count>   : # of array/hash elements for test, default 10
    -o <count>   : # of operations inside lock for lock tests, default 1
    -s           : all scalar tests
    -sr          : scalar read
    -sw          : scalar write
    -slr         : scalar locked read
    -slw         : scalar locked write
    -t <threads> : # of threads per test, default 1
    -x           : threads::shared tests only
    -y           : Thread::Sociable tests only
    -z <count>   : size of string used for test, default 10

    Default is all test types.
" and exit;
}

die "Skipped both shared and sociable tests, exitting. (idiot...)\n"
	unless $skips;

my $count = 0;

map $count += $_, @tests;

@tests = (1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)
	unless $count;

my $str = 'A' x $size;

foreach (0..$#tests) {
	next unless $tests[$_];
	$str = ($_ < 4) ? 'A' x $size : $len;
	print "\n**********************\n";
	print "Sociable $testnames[$_]: $cycles in ", $soctestsubs[$_]->($threads, $cycles, $str, $opcnt), " secs\n" and
	Thread::Sociable::sociable_checkmem()
		if $skips & 2;
	print "  Shared $testnames[$_]: $cycles in ", $shrtestsubs[$_]->($threads, $cycles, $str, $opcnt), " secs\n"
		if $skips & 1;
	print "Sociable $testnames[$_]: $cycles in ", $soctestsubs[$_]->($threads, $cycles, $str, $opcnt), " secs\n" and
	Thread::Sociable::sociable_checkmem()
		if $skips & 2;
	print "  Shared $testnames[$_]: $cycles in ", $shrtestsubs[$_]->($threads, $cycles, $str, $opcnt), " secs\n"
		if $skips & 1;
}

print "Tests compelete\n";