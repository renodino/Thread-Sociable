#define PERL_NO_GET_CONTEXT
#include "crt_memmgt.h"
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#ifdef HAS_PPPORT_H
#  include "ppport.h"
#endif
#include "sociable.h"
#include "socstoremech.h"
#include "crtmech.h"
#include "crt_memmgt.h"
#include "socqueue.h"

#ifdef SOCIABLE_STM
#include "socstm.h"
#endif

#ifdef SOCIABLE_TIE
#include "soctie.h"
#endif

/*
 * vtable for scalars
 * scalars use ext magic (but maybe should use uvar ?)
 * and use abbreviated form of proxy
 */
MGVTBL native_scalar_vtbl;
MGVTBL foreign_scalar_vtbl;

/*
 * vtable for hashes and arrays
 * uses tie magic
 * uses a full element proxy
 * which may have private version of the structure when
 * inside a transaction;
 * otherwise, all element references
 * get directed to the sociable version
 */
MGVTBL native_array_vtbl;
MGVTBL foreign_array_vtbl;
/*
 * Elements of hashes and arrays have this
 * _AS WELL AS_ the scalar magic:
 *  The sociable_elem_vtbl associates the element with the array/hash and
 *  the sociable_scalar_vtbl associates it with the value
 */
MGVTBL native_elem_vtbl;
MGVTBL foreign_elem_vtbl;

#ifdef MGf_LOCAL
#define SOC_MGFLAGS (MGf_DUP|MGf_LOCAL)
#else
#define SOC_MGFLAGS (MGf_DUP)
#endif

void sociable_dissociate(pTHX_ SV *sv);
STATIC sociable_locator_t *sociable_find(pTHX_ SV *sv);

STATIC I32 sociable_array_extent = SOCIABLE_DFLT_ARRAYSZ;	/* array extent/default size */
STATIC I32 sociable_hash_buckets = SOCIABLE_BUCKETS;		/* buckets in a hash */
/*
 *	set externally to force every created element to
 *	allocate mutex and condition variable structures
 */
STATIC bool sociable_always_lockable = FALSE;
#ifdef SOCIABLE_DEBUG
STATIC bool sociable_debug = TRUE;
#else
STATIC bool sociable_debug = FALSE;
#endif
STATIC bool sociable_warn = FALSE;
STATIC I32 sociable_dflt_extent = 10;
STATIC I32 sociable_dflt_himark = 20;

static char *svtype_strs[] = {
	"SVt_NULL",	/* 0 */
	"SVt_IV",	/* 1 */
	"SVt_NV",	/* 2 */
	"SVt_RV",	/* 3 */
	"SVt_PV",	/* 4 */
	"SVt_PVIV",	/* 5 */
	"SVt_PVNV",	/* 6 */
	"SVt_PVMG",	/* 7 */
	"SVt_PVBM",	/* 8 */
	"SVt_PVLV",	/* 9 */
	"SVt_PVAV",	/* 10 */
	"SVt_PVHV",	/* 11 */
	"SVt_PVCV",	/* 12 */
	"SVt_PVGV",	/* 13 */
	"SVt_PVFM",	/* 14 */
	"SVt_PVIO"	/* 15 */
};

/*
 *	storage mechanism dispatch tables
 */
sociable_storage_mech_t *sociable_mechs[SOCIABLE_MECHS];
int sociable_next_mech = 0;
STATIC crt_recursive_lock_t sociable_mech_lock;
STATIC sociable_storage_mech_t *default_mech = NULL;
/*
 *	maintain an indirection list of indexes into sociable_mechs
 *	ordered by level (descending), name (ascending)
 *	in order to assure that lockfree commit applies spinlocks
 *	in deadlock-free order, with distributed 1st, then
 *	process shared, then process private. Likewise, locked
 *	commit uses a similar order to avoid deadlock
 */
int ordered_mechs[SOCIABLE_MECHS];

/*
 *	our context is defined in Sociable.h
 */
START_MY_CXT

STATIC
void
sociable_trace(const char *pat, ...)
{
#ifdef SOCIABLE_DEBUG
    va_list args;
    va_start(args, pat);
	if (sociable_debug)
	    vprintf(pat, args);
    va_end(args);
#endif
}

/**************************************************************
 **************************************************************
 * MECHANISM MGMT METHODS
 **************************************************************
 **************************************************************/
/*
 *	find mechanism by name
 */
STATIC sociable_storage_mech_t *
sociable_find_mech(char *name)
{
	int i;
	for (i = 0; (i < sociable_next_mech); i++) {
		if (!strcmp(name, sociable_mechs[ordered_mechs[i]]->name))
			return sociable_mechs[ordered_mechs[i]];
	}
	return NULL;
}

/*
 *	find mechanism specified by 'Mechanism" key
 *	in a hash, returning default mechanism if 
 *	the key does not exist
 */
STATIC sociable_storage_mech_t *
sociable_lookup_mech(int items, SV *item[])
{
	char *mechname;
	sociable_storage_mech_t *mech;
	dTHX;
   	/*
   	 *	look for 'Mechanism' key; if found, lookup mechanism,
   	 *	else check for default mechanism
   	 */
	int i = 1;
	while (i < items) {
		if (!SvOK(item[i]))
			Perl_croak(aTHX_ "Undefined property for socialize");
		mechname = SvPV_nolen(item[i]);
		if (!strcmp(mechname, "Mechanism"))
			break;
		i += 2;
	}
	if (i < items) {
		mech = sociable_find_mech(mechname);
		if (!mech)
			Perl_croak(aTHX_ "Unknown storage mechanism %s", mechname);
	}
	else {
/*
 *	check for default mechanism
 */
		if (!default_mech)
			Perl_croak(aTHX_ "No mechanism specified");
		mech = default_mech;
	}
	return mech;
}
/*
 *	insert a mechanism into ordered list
 *	ordered by level descending, name ascending
 */
STATIC void
sociable_insert_mech(sociable_storage_mech_t *mech)
{
	int i, j;
	for (i = 0; i < sociable_next_mech - 1; i++) {
		if (mech->level < sociable_mechs[ordered_mechs[i]]->level)
			continue;
		if ((mech->level > sociable_mechs[ordered_mechs[i]]->level) ||
			(strcmp(mech->name, sociable_mechs[ordered_mechs[i]]->name) < 0))
			break;
	}
	for (j = sociable_next_mech - 1; j > i; j--)
		ordered_mechs[j] = ordered_mechs[j-1];
	sociable_mechs[ordered_mechs[i]] = mech;
}

/**************************************************************
 **************************************************************
 * SCALAR ELEMENT METHODS
 **************************************************************
 **************************************************************/

/*
 * Locate the proxy of an SV by looking for magic *and* 
 * verifying its cycle count
 */
STATIC sociable_proxy_t *
sociable_find_proxy(pTHX_ SV *sv)
{
    MAGIC *mg;
	sociable_proxy_t *proxy = NULL;
    SOCIABLE_TRACE_2(" *** finding the magic for %p, type is %d\n", sv, SvTYPE(sv));

    if (SvTYPE(sv) < SVt_PVMG)
    	return(NULL);

	mg = mg_find(sv, PERL_MAGIC_ext);
	if (mg && 
		((mg->mg_virtual == &native_scalar_vtbl) ||
		(mg->mg_virtual == &foreign_scalar_vtbl))) {
		SOCIABLE_TRACE(" *** found ext magic\n");
		proxy = (sociable_proxy_t *)(mg->mg_ptr);
	}
    else if ((mg = mg_find(sv, PERL_MAGIC_tied)) &&
		((mg->mg_virtual == &native_array_vtbl) ||
		(mg->mg_virtual == &foreign_array_vtbl))) {
		SOCIABLE_TRACE(" *** found tie magic\n");
		proxy = (sociable_proxy_t *)(mg->mg_ptr);
	}
 /*
  * Just for tidyness of API also handle tie objects
  *	NOTE: don't know what to do with this
  *
    else if (SvROK(sv) && sv_derived_from(sv, "Thread::Sociable::tie"))
        return (SOCIABLE_FROM_OBJ(sv));
 */
	if (proxy == NULL)
		return(NULL);

	if (SOC_IS_NATIVE(proxy)
		? ((proxy->elemp->header.header.soc_flags & SOC_INUSE) &&
			(proxy->header.cycleno == proxy->elemp->header.header.cycleno))
		: !(*proxy->mech->expired)(proxy, proxy->header.cycleno))
		return(proxy);
/*
 *	stale, so dissociate
 */
	SOCIABLE_TRACE_1("Attempted reference to stale sociable (proxy %p).", proxy);
	sociable_dissociate(aTHX_ sv);
	return(NULL);
}
/*
 * Locate and lock the sociable element for an SV
 * by looking for the magic *and* verifying its cycle count
 */
STATIC sociable_locator_t *
sociable_find_and_lock(pTHX_ SV *sv)
{
	sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ sv);

	if (!proxy)
		return(NULL);

	if (!SOC_IS_LOCKABLE(proxy)) {
		SOCIABLE_TRACE_1("Attempt to lock an unlockable sociable (elemp %p).", proxy);
		return(NULL);
	}
	SOCIABLE_TRACE_1(" *** locking elemp %s\n", proxy->loc_string);
    ENTER_ELEM_LOCK(proxy);
	SOCIABLE_TRACE_1(" *** elemp %s locked\n", proxy->loc_string);
	return(&proxy->sociable);
}
/*
 * Locate the sociable element for an SV
 * by looking for the magic *and* verifying its cycle count
 */
STATIC sociable_locator_t *
sociable_find(pTHX_ SV *sv)
{
	sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ sv);
	return (proxy ? &proxy->sociable : NULL);
}

/*
 * Associate a private SV with a sociable element by adding appropriate
 * magic. Assumes lock is held.
 *
 *	NOTE: no STM processing here. A referenced variable isn't logged until
 *	its state is actually read or written. Proxy structure is alloc'd
 *	from heap, rather than pools, as this is strictly
 *	thread-private
 */
STATIC void
sociable_associate(pTHX_ SV *sv, sociable_proxy_t *proxy)
{
    MAGIC *mg = 0;
	MGVTBL *mgvtbl = NULL;
	
	SOCIABLE_TRACE_2("in sociable_associate for SV %p type %s\n", sv, svtype_strs[SvTYPE(sv)] );

    switch(SvTYPE(sv)) {
    case SVt_PVAV:
    case SVt_PVHV:

		SOCIABLE_TRACE_1("sociable_associate: associate with AV/HV (%d)\n", SvTYPE(sv));
/*
 *	if associating an existing sociable with a new sociable, 
 *	dissociate it first...
 */
 		if (!(mg = mg_find(sv, PERL_MAGIC_tied))
            || ((mg->mg_virtual != &native_array_vtbl)
            	 && (mg->mg_virtual != &foreign_array_vtbl))
            || !(SOC_SAME_ELEM((sociable_proxy_t *)mg->mg_ptr, proxy)))
        {
            SV *obj = newSV(0);	/* used to bless into a tie */
			SOCIABLE_TRACE_1("sociable_associate: tying AV/HV (%d)\n", SvTYPE(sv));
            if (mg) {
/*
 *	(do we need more complex free here ?)
 */
	            if ((mg->mg_virtual == &foreign_array_vtbl) ||
	            	(mg->mg_virtual == &native_array_vtbl))
	            	mg->mg_ptr = crt_free(mg->mg_ptr);

                sv_unmagic(sv, PERL_MAGIC_tied);
            }
/*
 *	obj is provided during tied ref to an element of the structure
 */
           	sv_setref_iv(obj, "Thread::Sociable::tie", PTR2UV(proxy));
           	mgvtbl = (proxy->mech->level == SOC_FOREIGN_MECH)
           		? &foreign_array_vtbl
           		: &native_array_vtbl;
			SOCIABLE_TRACE("sociable_associate: adding magic\n");
            mg = sv_magicext(sv, obj, PERL_MAGIC_tied, mgvtbl, (char *)proxy, 0);
            mg->mg_flags |= (MGf_COPY|MGf_DUP);
            SvREFCNT_dec(obj);	/* ???? */
        }
        break;

    default:
		SOCIABLE_TRACE_1("sociable_associate: associate with PV (%d)\n", SvTYPE(sv));
        if ((SvTYPE(sv) < SVt_PVMG)
            || !(mg = mg_find(sv, PERL_MAGIC_ext))
            || ((mg->mg_virtual != &native_scalar_vtbl) &&
            	(mg->mg_virtual != &foreign_scalar_vtbl))
            /* || (SV*) mg->mg_ptr != ssv */)
        {
			SOCIABLE_TRACE("sociable_associate: replace PV magic\n");
            if (mg) {	/* remove existing magic */
/*
 * already associated with something else, so dissociate first
 *	NOTE: refer to threads::shared for chained magic!
 *	(do we need more complex free here ?)
 */
	            if ((mg->mg_virtual == &foreign_scalar_vtbl) ||
	            	(mg->mg_virtual == &native_scalar_vtbl))
	            	mg->mg_ptr = crt_free(mg->mg_ptr);

                sv_unmagic(sv, PERL_MAGIC_ext);
            }

           	mgvtbl = (proxy->mech->level == SOC_FOREIGN_MECH)
           		? &foreign_scalar_vtbl
           		: &native_scalar_vtbl;
			SOCIABLE_TRACE("sociable_associate: adding magic\n");
           	mg = sv_magicext(sv, Nullsv, PERL_MAGIC_ext, mgvtbl, (char *)proxy, 0);
            mg->mg_flags |= SOC_MGFLAGS;
/*
 *	make sure encodings match
 */
 			SOC_ENCODE_SV(sv, proxy);
        }
        break;
    }

	SOCIABLE_TRACE("sociable_associate: returning\n");
    assert ( sociable_find(aTHX_ sv) == &proxy->sociable);
}

/*
 * Create a private SV from a sociable element.
 * No locking is performed.
 *
 *	NOTE: no STM processing here. A referenced variable isn't logged until
 *	its state is actually read or written. HOWEVER...we need to add a
 *	proxy map keyed by element address so refs to SVs in the same thread
 *	don't log separately
 */
STATIC SV *
sociable_new_private(pTHX_ sociable_proxy_t *proxy)
{
    MAGIC *mg = 0;
    MGVTBL *mgvtbl = NULL;
    SV *sv = NULL;
	int elemtype = SOC_GET_SVTYPE(proxy);
/*
 * Requires caller to hold lock
 */
    /* sv_upgrade(sv, SvTYPE(&elemp->header)); */
	SOCIABLE_TRACE_2("in sociable_new_private for elemp %p type %s\n", 
		proxy->loc_string, svtype_strs[elemtype]);
	
    if ((elemtype == SVt_PVAV) || (elemtype == SVt_PVHV)) {
    	SV *avhv = (elemtype == SVt_PVAV) ? (SV*)newAV() : (SV*)newHV();
		SV *obj = newSV(0);	/* provided to strucuture element tie functions */
		sv_setref_iv(obj, "Thread::Sociable::tie", PTR2UV(proxy));
		mgvtbl = (proxy->mech->level == SOC_FOREIGN_MECH)
			? &foreign_array_vtbl
			: &native_array_vtbl;
		mg = sv_magicext(avhv, obj, PERL_MAGIC_tied, mgvtbl, (char *)proxy, 0);
		mg->mg_flags |= (MGf_COPY|MGf_DUP);
		/* SvREFCNT_dec(obj);	/* causes obj to be freed */
        return(avhv);
	}

	SOCIABLE_TRACE_1("sociable_new_private: associate with PV %s\n", svtype_strs[elemtype]);
	sv = newSV(0);
	sv_upgrade(sv, elemtype);
	mgvtbl = (proxy->mech->level == SOC_FOREIGN_MECH)
		? &foreign_scalar_vtbl
		: &native_scalar_vtbl;
	mg = sv_magicext(sv, Nullsv, PERL_MAGIC_ext, mgvtbl, (char *)proxy, 0);
	SOC_ENCODE_SV(sv, proxy);
	mg->mg_flags |= SOC_MGFLAGS;
    return (sv);
}

/*
 * Dissociate an SV from its current element.
 *
 *	NOTE: No STM processing here; let caller decide
 *	what to do w/ a stale element inside a transaction
 *
 *	NOTE2: for arrays, hashes, clear the private versions;
 *	scalars are set to undef
 */
STATIC void
sociable_dissociate(pTHX_ SV *sv)
{
	MAGIC *tmg = mg_find(sv, PERL_MAGIC_tied);
	MAGIC *emg = mg_find(sv, PERL_MAGIC_ext);
	sociable_proxy_t *proxy;

	if (tmg && 
		((tmg->mg_virtual == &native_array_vtbl) ||
		(tmg->mg_virtual == &foreign_array_vtbl))) {
		SOCIABLE_TRACE_1("sociable_dissociate: untying AV/HV (%d)\n", SvTYPE(sv));
/*
 *	need more free processing here ?
 */
 		proxy = (sociable_proxy_t *)tmg->mg_ptr;
 		if (proxy->pkg_name)
 			crt_free(proxy->pkg_name);
		tmg->mg_ptr = crt_free(tmg->mg_ptr);	/* free proxy structure */

		sv_unmagic(sv, PERL_MAGIC_tied);

/* ???????	how do we untie here ?
		SV *obj = newSV(0);

		sv_setref_iv(obj, "Thread::Sociable::tie", proxy);
		emg = sv_magicext(sv, obj, PERL_MAGIC_tied, &sociable_array_vtbl, (char *)elemp, 0);
		emg->mg_flags |= (MGf_COPY|MGf_DUP);
		SvREFCNT_dec(obj);
*/
		if (SvTYPE(sv) == SVt_PVAV) {
			av_clear((AV *)sv);
		}
		else {
			hv_clear((HV *)sv);
		}
		return;
	}

	if (emg && 
		((emg->mg_virtual == &native_scalar_vtbl) ||
		(emg->mg_virtual == &foreign_scalar_vtbl))) {
		SOCIABLE_TRACE("sociable_dissociate: removing magic\n");
/*
 *	need more free processing here ?
 */
 		proxy = (sociable_proxy_t *)emg->mg_ptr;
 		if (proxy->pkg_name)
 			crt_free(proxy->pkg_name);
		emg->mg_ptr = crt_free(emg->mg_ptr);	/* free proxy structure */

		sv_unmagic(sv, PERL_MAGIC_ext);
		sv_setsv(sv, &PL_sv_undef);
	}
}
/*
 *	is this RV's proxy compatible with this proxy ?
 */
STATIC int 
sociable_is_compatible(pTHX_ SV *sv, sociable_proxy_t *proxy)
{
	sociable_proxy_t *refproxy;
	SV *errsv;
/*
 *	get the referenced proxy from the magic
 */
	MAGIC *mg = mg_find(sv, PERL_MAGIC_ext);
	if (!mg || 
		((mg->mg_virtual != &native_scalar_vtbl) &&
		(mg->mg_virtual != &foreign_scalar_vtbl))) {
    	mg = mg_find(sv, PERL_MAGIC_tied);
    	if (!mg ||
			((mg->mg_virtual != &native_array_vtbl) &&
			(mg->mg_virtual != &foreign_array_vtbl))) {
			errsv = get_sv("@", TRUE);
			sv_setpv(errsv, "Attempt to assign non-sociable ref to sociable variable.");
			return 0;
		}
	}
	refproxy = (sociable_proxy_t *)(mg->mg_ptr);
	if (refproxy->mech->level < proxy->mech->level) {
		errsv = get_sv("@", TRUE);
		sv_setpvf(errsv, "Attempt to assign incompatible %s ref to %s variable.",
			refproxy->mech->name, proxy->mech->name);
		return 0;
	}
	return 1;
}

/***********************************************************
 ***********************************************************
 * SCALAR ELEMENT METHODS
 ***********************************************************
 ***********************************************************/
/*
 * Implements Perl-level socialize() and :sociable
 */
STATIC void
sociable_socialize(pTHX_ SV *sv, sociable_storage_mech_t *mech, I32 withlock)
{
	sociable_proxy_t *proxy = NULL;
	unsigned int stm_hash = 0;

	SOCIABLE_TRACE("in sociable_socialize\n");
	
	withlock |= (*mech->always_lockable_enabled)();

    if (SvTYPE(sv) == SVt_PVGV)
        Perl_croak(aTHX_ "Cannot socialize globs yet");

    if (SvTYPE(sv) == SVt_PVCV)
        Perl_croak(aTHX_ "Cannot socialize subs");

    proxy = (SvTYPE(sv) == SVt_PVAV) ? (*mech->new_array)(aTHX_ sv->sv_flags, withlock)
    	:	(SvTYPE(sv) == SVt_PVHV) ? (*mech->new_hash)(aTHX_ sv->sv_flags, withlock)
    	:	/* SCALAR TYPE */          (*mech->new_scalar)(aTHX_ sv, withlock);
/*
 *	backfill common stuff
 */
	proxy->mech = sociable_mechs[proxy->sociable.mechid];
	proxy->elemp = (proxy->sociable.level != SOC_FOREIGN_MECH)
		? (*proxy->mech->addr_to_ptr)(&proxy->sociable.un)
		: NULL;
	PRINTABLE_LOC(&proxy->sociable, proxy->loc_string);
#ifdef SOCIABLE_STM
/*
 *	precompute STM hash bucket from locator
 */
 	PERL_HASH(stm_hash, (char *)(&proxy->sociable), sizeof(sociable_locator_t));
	proxy->stm_hash = stm_hash % SOCIABLE_BUCKETS;
#endif
	SOCIABLE_TRACE("sociable_socialize: calling sociable_associate\n");
    sociable_associate(aTHX_ sv, proxy);
	SOCIABLE_TRACE("return from sociable_socialize\n");
}
/*
 * Create a private RV from a sociable RV element, also copying the
 * object status of the referent.
 * If the private side is already an appropriate RV->SV combination, keep
 * it if possible.
 *
 *	When app takes private ref of sociable element, it gets a private SV
 *  pointing to the private proxy SV tied to the element. When a deref
 *	occurs, Perl first accesses the private proxy SV, and then operates
 *	on it, which will cause the sociable magic to kick in.
 *
 *	When a ref of a sociable element is assigned to a sociable element,
 *	we recv a proxy SV
 *
 */
STATIC int
sociable_get_RV(pTHX_ SV *sv, sociable_locator_t *refloc, char *loc_string)
{
	sociable_locator_t *tp = NULL;
	sociable_proxy_t *refproxy;
    SV *obj = NULL;
    SOC_PRINTABLE_LOC(locstr);

	SOCIABLE_TRACE_4("in sociable_get_RV: for element %s SV %p type %s flags %08X\n", 
		loc_string, sv, svtype_strs[SvTYPE(sv)], SvFLAGS(sv));

	if (SvROK(sv)) {
		obj = SvRV(sv);
		if (tp = sociable_find(aTHX_ obj)) {
			PRINTABLE_LOC(tp, locstr);
			SOCIABLE_TRACE_2("sociable_get_RV: private is RV to SV %p for element %s\n", obj, locstr);
		}
	}

    if (tp && !memcmp(tp, refloc, sizeof(sociable_locator_t)))
    	return 0; /* already the same */
/*
 * Can't reuse obj
 */
	SOCIABLE_TRACE("sociable_get_RV: can't reuse object\n");
	if (SvROK(sv)) {
/*
 *	free up any existing reference; hopefully this will
 *	invoke free magic if its a sociable referent
 */
		SvREFCNT_dec(SvRV(sv));
		SOCIABLE_TRACE("sociable_get_RV: free existing ref\n");
	}
	else {
		assert(SvTYPE(sv) >= SVt_RV);
		SOCIABLE_TRACE("sociable_get_RV: setting SV to RV\n");
		sv_setsv_nomg(sv, &PL_sv_undef);
		SvROK_on(sv);
	}
	SOCIABLE_TRACE("sociable_get_RV: creating new object\n");
	refproxy = (*sociable_mechs[refloc->mechid]->create_proxy)(refloc);
	obj = sociable_new_private(aTHX_ refproxy);
	SOCIABLE_TRACE_1("sociable_get_RV: new object is %p\n", obj);
	SvRV_set(sv, obj);
/*
 * If it was blessed, remove the blessing
 */
    if (SvOBJECT(obj)) {
        SvREFCNT_dec(SvSTASH(obj));
        SvOBJECT_off(obj);
    }
/*
 * if the referent is blessed, bless the private instance
 */
    if (refproxy->pkg_name) {
        HV* stash = gv_stashpv(refproxy->pkg_name, TRUE);
        SvOBJECT_on(obj);
        SvSTASH_set(obj, (HV*)SvREFCNT_inc(stash));
    }
    return 0;
}
/***********************************************************
 ***********************************************************
 * PERL_MAGIC_ext(~) METHODS
 ***********************************************************
 ***********************************************************/
/***********************************************************
 * COMMON SCALAR METHODS
 ***********************************************************/
/*
 * Called during cloning of PERL_MAGIC_ext(n) magic in new thread
 *	we need to clone a new proxy for the associated scalar
 *	NOTE: we don't dup any STM transaction info here
 */
STATIC int
sociable_scalar_mg_dup(pTHX_ MAGIC *mg, CLONE_PARAMS *param)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	sociable_proxy_t *clone_proxy = NULL;

	SOCIABLE_TRACE_1(" *** CLONEing proxy %p\n", proxy);
/*
 *	NOTE: we proceed to clone even tho the element may have expired;
 *	any later access to said element will force unmagic
 */
	CRT_Dup(clone_proxy, 1, sociable_proxy_t, proxy, 1);
	if (proxy->pkg_name)
		clone_proxy->pkg_name = crt_strdup(proxy->pkg_name);
	mg->mg_ptr = (char *)clone_proxy;
    return (0);
}

/*
 *	suss out the proxy (if any)
 *
 *	what does this mean inside a xaction ???
 *	for now, nothing
 */
STATIC int
sociable_scalar_mg_free(pTHX_ SV *sv, MAGIC *mg)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;

	SOCIABLE_TRACE_1(" *** freeing proxy %p\n", proxy);
	if (proxy->pkg_name)
		crt_free(proxy->pkg_name);
	mg->mg_ptr = crt_free(proxy);
	return(0);
}

STATIC int
sociable_scalar_mg_copy(pTHX_ SV *sv, MAGIC *mg, SV *tosv, const char *buf, int len)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	SOCIABLE_TRACE_1(" *** UNEXPECTED: copying proxy %p\n", proxy);
	return(0);
}

#ifdef MGf_LOCAL
/*
 * Called during local $shared (????????)
 */
STATIC int
sociable_scalar_mg_local(pTHX_ SV* nsv, MAGIC *mg)
{
    MAGIC *nmg;
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
    nmg = sv_magicext(nsv, mg->mg_obj, mg->mg_type, mg->mg_virtual,
                            mg->mg_ptr, mg->mg_len);
    nmg->mg_flags   = mg->mg_flags;
    nmg->mg_private = mg->mg_private;

    return (0);
}
#endif

/***********************************************************
 * NATIVE SCALAR METHODS
 ***********************************************************/
/*
 * Recover an SV from the sociable element into the supplied SV
 */
SV *
native_sv_from_elem(pTHX_ SV *sv, sociable_element_t *elemp, char *loc_string)
{
	sociable_scalar_t *scalar = &elemp->un.scalar;
	sociable_storage_mech_t *elem_mech = sociable_mechs[elemp->header.mechid];
	char *bufptr;

   	SOCIABLE_TRACE_4("native_sv_from_elem: elemp %s type is %s\n\tsv %p type is %s\n", 
   		loc_string, svtype_strs[SvTYPE(&elemp->SOCHDR)], sv, svtype_strs[SvTYPE(sv)]);

	if (SvROK(&elemp->SOCHDR)) {
		sociable_get_RV(aTHX_ sv, &elemp->un.ref.elemloc, loc_string);
		return sv;
	}

	SvUPGRADE(sv, SvTYPE(&elemp->SOCHDR));

    switch (SvTYPE(&elemp->SOCHDR)) {
	    case SVt_NULL:
			sv_setsv(sv, &PL_sv_undef);
			break;

	    case SVt_IV:
    		if (SvIsUV(&elemp->SOCHDR)) {
			   	SOCIABLE_TRACE_2("native_sv_from_elem: scalar elemp %s has UV %08X\n", loc_string, scalar->uv);
    			sv_setuv(sv, scalar->uv);
    		}
    		else {
			   	SOCIABLE_TRACE_2("native_sv_from_elem: scalar elemp %s has IV %08X\n", loc_string, scalar->iv);
    			sv_setiv(sv, scalar->iv);
    		}
    		break;

	    case SVt_NV:
		   	SOCIABLE_TRACE_2("native_sv_from_elem: scalar elemp %s has NV %f\n", loc_string, scalar->nv);
			sv_setnv(sv, scalar->nv);
			break;

	    case SVt_PV:
	    	bufptr = (*elem_mech->addr_to_ptr)(&scalar->bufptr.un);
		   	SOCIABLE_TRACE_3("native_sv_from_elem: scalar elemp %s has buflen %d buffer %s\n", loc_string, scalar->buflen, bufptr);
			sv_setpvn(sv, bufptr, scalar->buflen);
			break;

/*
 *	!!!NEED TO IMPLEMENT THESE CASES
	    case SVt_PVIV:
			sv_setpvn(sv, elemp->un.bufptr, elemp->buflen);
			SvIV_set(sv, elemp->iv);
			break;

	    case SVt_PVNV:
			sv_setpvn(sv, elemp->un.bufptr, elemp->buflen);
			SvIV_set(sv, elemp->iv);
			SvNV_set(sv, elemp->nv);
			break;

	    case SVt_RV:
			SvRV_set(sv, (SV*)pv);
			break;

	    case SVt_PVAV:
			SvPV_set(sv, (char*)0);
			AvMAX(sv)	= -1;
			AvFILLp(sv)	= -1;
			SvIV_set(sv, 0);
			SvNV_set(sv, 0.0);
			SvMAGIC_set(sv, magic);
			SvSTASH_set(sv, stash);
			AvALLOC(sv)	= 0;
			AvARYLEN(sv)	= 0;
			AvFLAGS(sv)	= AVf_REAL;
			break;

	    case SVt_PVHV:
			SvPV_set(sv, (char*)0);
			HvFILL(sv)	= 0;
			HvMAX(sv)	= 0;
			HvTOTALKEYS(sv)	= 0;
			HvPLACEHOLDERS_set(sv, 0);
			SvMAGIC_set(sv, magic);
			SvSTASH_set(sv, stash);
			HvRITER(sv)	= 0;
			HvEITER(sv)	= 0;
			HvPMROOT(sv)	= 0;
			HvNAME(sv)	= 0;
			break;

*/
		default:
			Perl_croak(aTHX_ "Unrecognized element type\n");

    }
	NATIVE_ENCODE_SV_ELEM(sv, elemp);
	return sv;
}
/*
 * Recover an SV from the sociable element into the supplied SV
 */
int
native_sv_recover(pTHX_ SV *sv, sociable_proxy_t *proxy, sociable_element_t *elemp)
{
	bool need_seqno = TRUE;
	if (!elemp) {
		need_seqno = FALSE;
		elemp = proxy->elemp;
	}
	native_sv_from_elem(aTHX_ sv, elemp, proxy->loc_string);

	if (need_seqno) {
		proxy->header.updcntlo = elemp->header.header.updcntlo;
		proxy->header.updcnthi = elemp->header.header.updcnthi;
	}
	return 0;
}

/*
 * Get magic for PERL_MAGIC_ext(n)
 * NOTE: we don't lock here...leave that to the app to apply if desired
 *
 * Load the private SV with the current contents of the sociable element.
 * Subsequently, Perl retrieves the value out of the private SV (ie, the SV is a proxy)
 * This means that RV's should result in creating a new private
 * SV to be set in the supplied proxy SV. Note that (like threads::shared)
 * this means that no 2 RV retrievals will stringify with the same
 * result, and hence ref equivalance is not guaranteed.
 *
 * If in a transaction:
 * if SV has been logged then
 *		log it
 *		copy sociable value to the SV
 *		set proxy seqno
 * else if sociable and proxy seqno's not same then
 *		throw restart exception
 * mark proxy as read
 */
STATIC int
native_scalar_mg_get(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = (sociable_proxy_t *)(mg->mg_ptr);
	sociable_element_t *elemp = NULL;
	dMY_CXT;

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 * the sociable was discarded,
 * remove sociable magic from this SV
 */
		sociable_dissociate(aTHX_ sv);
		return (0);
	}

	elemp = proxy->elemp;
	SOCIABLE_TRACE_5("native_scalar_mg_get: get for element %s type %s for SV %p type %s flags %08X\n",
		proxy->loc_string, svtype_strs[SvTYPE(&elemp->header.header)], sv, 
		svtype_strs[SvTYPE(sv)], SvFLAGS(sv));

#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO !!!
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
/*
 * need to handle as a full roundtrip, and store the
 * result in the sociable variable before returning the
 * final value
 *
    		sociable_enqueue(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
 */
    }
#endif
#ifdef SOCIABLE_STM
/*
 * if in xaction then use STM to get private version;
 *	everything then behaves same, just using private element
 */
	if (SOC_USE_STM(proxy) && (!(elemp = native_save_referent(aTHX_ proxy, 0))))
		Perl_croak(aTHX_ Nullch);
#endif
/*
 * its a reference, create an anonymous SV as the referent
 */
	return (SvROK(&elemp->header.header))
		? sociable_get_RV(aTHX_ sv, &elemp->un.ref.elemloc, proxy->loc_string)
		: (NATIVE_ELEM_UPDATED(proxy, elemp))
			? native_sv_recover(aTHX_ sv, proxy, elemp)
	    	: 0;
}

int
native_sv_save(pTHX_ SV *sv, sociable_element_t *elemp)
{
	char * pv;
	SV *obj;
	sociable_locator_t *refloc;
	sociable_element_t *refelemp;
	sociable_scalar_t *scalar = &elemp->un.scalar;
 	sociable_storage_mech_t *elem_mech = sociable_mechs[elemp->header.mechid];
 	char *bufptr;
 	SOC_PRINTABLE_LOC(locstr);
 	SOC_PRINTABLE_LOC(elemstr);
 	
#ifdef SOCIABLE_DEBUG
	sprintf(elemstr, (elem_mech->level == SOC_PROCESS_PRIVATE_MECH) ? "P:%p" : "S:%p", elemp);
#endif
/* ????
    if (mt != SVt_PV && SvREADONLY(sv) && SvFAKE(sv)) {
		sv_force_normal(sv);
    }

    if (mt < SVt_PVIV)
		(void)SvOOK_off(sv);
*/

	SOCIABLE_TRACE_2("native_sv_save: entered for elemp %s\nsaving an sv type %s\n", 
		elemstr, ((SvTYPE(sv) <= 15) ? svtype_strs[SvTYPE(sv)] : "Unknown"));
/*
 *	clear the buffer if not to be used
 */
	if (SvOK(&elemp->SOCHDR)) {
		if (!SvROK(&elemp->SOCHDR)) {
			if (scalar->bufsz) {
				if ((!SvOK(sv)) || SvROK(sv)) {
					PRINTABLE_LOC(&scalar->bufptr, locstr);
					SOCIABLE_TRACE_2("native_sv_save: freeing buffer %s for elemp %s\n", locstr, elemstr);
					SOC_LOC_FREE(elem_mech, &scalar->bufptr);
					SOCIABLE_TRACE_2("native_sv_save: freed buffer %s for elemp %s\n", locstr, elemstr);
    				scalar->bufsz = 0;
    			}
    		}
    	}
    }

	SOCIABLE_TRACE_1("native_sv_save: SvTYPE is %04x\n", SvTYPE(sv));
	if (!SvOK(sv)) {
		SOCIABLE_TRACE("native_sv_save: save'ing undef SV\n");
		elemp->SOCHDR.sv_flags = 0;
	}
    else if (SvROK(sv)) {
/*
 * if its a ref, then get its referent and verify
 * its sociable
 */
        obj = SvRV(sv);
		SOCIABLE_TRACE_2("native_sv_save: assigning a ref to elemp %s\nnative_sv_save: ref is %p\n",
			elemstr, obj);

        if (!(refloc = sociable_find(aTHX_ obj)))
	        Perl_croak(aTHX_ "Invalid value for sociable scalar");

        if (refloc->level < elemp->header.level)
	        Perl_croak(aTHX_ "Attempted assignment of incompatible reference (%s to %s)",
	        	sociable_mechs[refloc->mechid]->name, elem_mech->name);

		PRINTABLE_LOC(refloc, locstr);
		refelemp = (*sociable_mechs[refloc->mechid]->addr_to_ptr)(&refloc->un);
		SOCIABLE_TRACE_1("native_sv_save: storing for a ref for refelemp %s\n", locstr);
		memcpy(&elemp->un.ref.elemloc, &refloc, sizeof(sociable_locator_t));
		elemp->un.ref.cycleno = refelemp->header.header.cycleno;
		SvROK_on(&elemp->SOCHDR);
		NATIVE_INCR_ELEM_SEQNO(elemp);
/*
 * if the contents were blessed after
 * the ref was created, we need to copy the package
 *	...BUT HOW CAN THIS EVER HAPPEN ???? We only permit
 */
		if (SvOBJECT(obj)) {
			SOC_MEM_FREE(elem_mech, &elemp->header.pkg_name);
			SOC_MEM_STRDUP(elem_mech, &elemp->header.pkg_name, HvNAME(SvSTASH(obj)));
		}
    }
/*
 *	!!! Order is important here! Must check for unsigned before signed
 */
	else if (SvUOK(sv)) {
		SOCIABLE_TRACE("native_sv_save: save'ing UV SV\n");
		scalar->uv = SvUVX(sv);
		elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK|SVf_IVisUV;
	}
	else if (SvIOK(sv)) {
		SOCIABLE_TRACE("native_sv_save: save'ing IV SV\n");
		scalar->iv = SvIVX(sv);
		elemp->SOCHDR.sv_flags = SVt_IV|SVf_IOK;
	}
	else if (SvNOK(sv)) {
		SOCIABLE_TRACE("native_sv_save: save'ing NV SV\n");
		scalar->nv = SvNVX(sv);
		elemp->SOCHDR.sv_flags = SVt_NV|SVf_NOK;
	}
	else if (SvPOK(sv) || (SvTYPE(sv) <= SVt_PVLV)) {
		pv = SvPV_nomg(sv, scalar->buflen);
		SOCIABLE_TRACE_1("native_sv_save: save'ing PV SV length %d\n", scalar->buflen);
		if (!scalar->bufsz) {
			scalar->bufsz = SvLEN(sv) + 10;
			SOC_LOC_ALLOC(elem_mech, &scalar->bufptr, scalar->bufsz);
		}
		else if (scalar->bufsz && (scalar->bufsz <= SvLEN(sv))) {
			PRINTABLE_LOC(&scalar->bufptr, locstr);
			SOCIABLE_TRACE_2("native_sv_save: changing buffers: freeing buffer %s for elemp %s\n", 
				locstr, elemstr);
			SOC_LOC_FREE(elem_mech, &scalar->bufptr);
			scalar->bufsz = SvLEN(sv) + 10;
			SOC_LOC_ALLOC(elem_mech, &scalar->bufptr, scalar->bufsz);
		}
		bufptr = (*elem_mech->addr_to_ptr)(&scalar->bufptr.un);
		scalar->buflen = SvCUR(sv);
		memcpy(bufptr, pv, scalar->buflen);
		elemp->SOCHDR.sv_flags = SVt_PV|SVf_POK;
	}
	else {
		SOCIABLE_TRACE_1("native_sv_save: unknown SV type %d!!!\n", SvTYPE(sv));
		/* Perl_warn(aTHX_ "Can't socialize that kind of scalar %d", SvTYPE(sv)); */
	}
/*************************************************
 *
 * don't know what to do with these as yet
 *
	    case SVt_PVIV:
			elemp->iv = SvIVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;

	    case SVt_PVNV:
			elemp->iv = SvIVX(sv);
			elemp->nv = SvNVX(sv);
			pv = SvPV(sv, elemp->buflen);
			elemp->bufsz = SvLEN(sv);
			CRT_Newz( elemp->un.bufptr, elemp->bufsz, char);
			memcpy(elemp->un.bufptr, pv, elemp->bufsz);
			break;
*************************************************/
	NATIVE_ENCODE_ELEM(sv, elemp);
	NATIVE_INCR_ELEM_SEQNO(elemp);
	SOCIABLE_TRACE("native_sv_save: returning\n");
	return 0;
}

/*
 * Set magic for PERL_MAGIC_ext(n)
 *
 * If in a transaction:
 * if SV has not been logged then
 *		log it
 *		set proxy seqno
 * else if sociable and proxy not same then
 *		throw restart exception
 * mark proxy as written
 */
STATIC int
native_scalar_mg_set(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = (sociable_proxy_t *)(mg->mg_ptr);
	sociable_element_t *elemp = NULL;
	dMY_CXT;

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 * the sociable was discarded,
 * remove sociable magic from this SV
 */
		sociable_dissociate(aTHX_ sv);
		return (0);
	}

	elemp = proxy->elemp;
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "STORE", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
/*
 * if in xaction, get (and maybe log and acquire) private element;
 *	rest of processing is same for sociable and private element
 */
#ifdef SOCIABLE_STM
	if (SOC_USE_STM(proxy) && (!(elemp = native_save_referent(aTHX_ proxy, 1))))
		Perl_croak(aTHX_ Nullch);
#endif

   	return native_sv_save(aTHX_ sv, elemp);
}

STATIC U32
native_get_scalar_len(sociable_element_t *elemp)
{
/*
 *	optimize: if we've already taken length of a numeric,
 *	save its length value; any subsequent store will
 *	clear the buffer len
 */
	sociable_scalar_t *scalar = &elemp->un.scalar;
	dTHX;

	if ((scalar->buflen == 0) && (SvTYPE(&elemp->header.header) != SVt_PV)) {
/*
 *	returning content length
 *  !!! THERE HAS GOT TO BE A FASTER/BETTER WAY TO DO THIS!!!
 */
 		sociable_storage_mech_t *mech = sociable_mechs[elemp->header.mechid];
		SV *tsv = (SvTYPE(&elemp->header.header) == SVt_IV)
			? (SvIsUV(&elemp->header.header)
				? newSVuv(scalar->iv)
				: newSViv(scalar->iv))
			: newSVnv(scalar->nv);
		char *ptr = SvPV(tsv, scalar->buflen);
		if (scalar->buflen >= scalar->bufsz)
			SOC_MEM_REALLOC(mech, &scalar->bufptr.un, scalar->bufsz, sizeof(char), scalar->buflen + 1);
		memcpy((*mech->addr_to_ptr)(&scalar->bufptr.un), ptr, scalar->buflen);
		SvREFCNT_dec(tsv);	/* to free it ...does this free the pointer we referenced ? */
		SOCIABLE_TRACE_1(" *** length of temp sv is %d\n", scalar->buflen);
	}
	return scalar->buflen;
}
/*
 *	Return length of the sociable element
 *
 *	if in transaction
 *		treat same as a read operation
 */
STATIC U32
native_scalar_mg_len(pTHX_ SV *sv, MAGIC *mg)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	sociable_element_t *elemp = proxy->elemp;
	dMY_CXT;

	SOCIABLE_TRACE_1(" *** taking length of proxy %p\n", proxy);
	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 * remove the magic and return private length
 */
 		SOC_DESOCIALIZE(proxy, sv);
		return SvCUR(sv);
	}
/*
 * if in xaction then use STM to get ( and maybe log ) private version;
 *	everything then behaves same, just using private element
 *	NOTE: we may actually do a write here, but its not important
 *	if we don't copy it back on commit
 */
#ifdef SOCIABLE_STM
	if (SOC_USE_STM(proxy) && (!(elemp = native_save_referent(aTHX_ proxy, 0))))
		Perl_croak(aTHX_ Nullch);
#endif
	return native_get_scalar_len(elemp);
}

STATIC int
native_clear_scalar(sociable_element_t *elemp)
{
	SOC_PRINTABLE_LOC(locstr);
	sociable_scalar_t *scalar;

	if (!SvOK(&elemp->header.header))
		return 0;

	if (SvROK(&elemp->header.header)) {
		memset(&elemp->un.ref.elemloc, 0, sizeof(sociable_locator_t));
		elemp->un.ref.cycleno = 0;
		return 0;
	}

	scalar = &elemp->un.scalar;
	if (scalar->bufsz) {
		PRINTABLE_LOC(&scalar->bufptr, locstr);
		SOCIABLE_TRACE_2("native_scalar_mg_clear: freeing buffer %p for elemp %p\n", locstr, elemp);
		SOC_LOC_FREE(sociable_mechs[scalar->bufptr.mechid], &scalar->bufptr);
		SOCIABLE_TRACE_1("native_scalar_mg_clear: freed buffer for elemp %p\n", elemp);
	}
    memset(&scalar->bufptr, 0, sizeof(sociable_locator_t));
    scalar->bufsz = 0;
    scalar->iv = 0;
    scalar->nv = 0.0;
	scalar->buflen = 0;
	elemp->header.header.sv_flags = 0;
	return 0;
}
/*
 *	clear the sociable value (???)
 */
STATIC int
native_scalar_mg_clear(pTHX_ SV *sv, MAGIC *mg)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	sociable_element_t *elemp = proxy->elemp;
	dMY_CXT;

	SOCIABLE_TRACE_1(" *** clearing for proxy %p\n", proxy);
	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 * remove the magic and clear private value
 */
		SOC_DESOCIALIZE(proxy, sv);
		sv_setpvn(sv, "", 0);
		return SvLEN(sv);
	}
/*
 * if in xaction, get (and maybe log and acquire) private element;
 *	rest of processing is same for sociable and private element
 */
#ifdef SOCIABLE_STM
	if (SOC_USE_STM(proxy) && (!(elemp = native_save_referent(aTHX_ proxy, 1))))
		Perl_croak(aTHX_ Nullch);
#endif
	return native_clear_scalar(elemp);
}

MGVTBL native_scalar_vtbl = {
    native_scalar_mg_get,     /* get */
    native_scalar_mg_set,     /* set */
	native_scalar_mg_len,		/* len */
	native_scalar_mg_clear,	/* clear */
	sociable_scalar_mg_free,	/* free */
	sociable_scalar_mg_copy,	/* copy */
    sociable_scalar_mg_dup,		/* dup */
#ifdef MGf_LOCAL
    sociable_scalar_mg_local,   /* local */
#endif
};

/***********************************************************
 * FOREIGN SCALAR METHODS
 ***********************************************************/
/*
 * Get magic for PERL_MAGIC_ext(n)
 * NOTE: we don't lock here...leave that to the app to apply if desired
 *
 * Load the private SV with the current contents of the sociable element.
 * Subsequently, Perl retrieves the value out of the private SV (ie, the SV is a proxy)
 * This means that RV's should result in creating a new private
 * SV to be set in the supplied proxy SV. Note that (like threads::shared)
 * this means that no 2 RV retrievals will stringify with the same
 * result, and hence ref equivalance is not guaranteed.
 *
 * If in a transaction:
 * if SV has been logged then
 *		log it
 *		copy sociable value to the SV
 *		set proxy seqno
 * else if sociable and proxy seqno's not same then
 *		throw restart exception
 * mark proxy as read
 */
STATIC int
foreign_scalar_mg_get(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = (sociable_proxy_t *)(mg->mg_ptr);
    sociable_storage_mech_t *mech = NULL;
    sociable_element_t *elemp;
	dMY_CXT;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 * the sociable was discarded,
 * remove sociable magic from this SV
 */
		sociable_dissociate(aTHX_ sv);
		return (0);
	}

	mech = proxy->mech;
	SOCIABLE_TRACE_4("foreign_scalar_mg_get: get for element %s for SV %p type %s flags %08X\n",
		proxy->loc_string, sv, svtype_strs[SvTYPE(sv)], SvFLAGS(sv));

#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*mech->is_tied)(proxy)) {
/* !!! TO DO !!!
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
/*
 * need to handle as a full roundtrip, and store the
 * result in the sociable variable before returning the
 * final value
 *
    		sociable_enqueue(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
 */
    }
#endif
	SOCIABLE_TRACE_1("foreign_scalar_mg_get: recovering type %s\n",
		svtype_strs[(*mech->get_sv_type)(proxy)]);
#ifdef SOCIABLE_STM
/*
 * if in xaction then use STM to get ( and maybe log ) private version;
 *	everything then behaves same, just using private element
 */
	if (SOC_USE_STM(proxy)) {
		if (!(elemp = stm_save_referent(aTHX_ proxy, 0)))
			Perl_croak(aTHX_ Nullch);
/*
 * its a reference, create an anonymous SV as the referent
 */
		return (SvROK(&elemp->header.header))
			? sociable_get_RV(aTHX_ sv, &elemp->un.ref.elemloc, proxy->loc_string)
			: (NATIVE_ELEM_UPDATED(proxy, elemp))
				? native_sv_recover(aTHX_ sv, proxy, elemp)
		    	: 0;
    }
#endif
    if ((*mech->is_rv)(proxy)) {
/*
 * its a reference, create an anonymous SV as the referent
 */
    	SOCIABLE_TRACE_1("foreign_scalar_mg_get: recovering RV value for %s\n", proxy->loc_string);
		FOREIGN_GET_RV(sv, proxy);
        return 0;
    }

   	SOCIABLE_TRACE_3("foreign_scalar_mg_get: recovering value for %s updhi %u updlo %u\n", 
   		proxy->loc_string, FOREIGN_GET_SEQNOHI(proxy), FOREIGN_GET_SEQNOLO(proxy));
   	SOCIABLE_TRACE_3("foreign_scalar_mg_get: proxy %p updhi %u updlo %u\n", 
   		proxy, proxy->header.updcnthi, proxy->header.updcntlo);
/*
 * only update if private older than sociable version
 */
	if (FOREIGN_ELEM_UPDATED(proxy)) {
		(*proxy->mech->sv_recover)(aTHX_ sv, proxy);
		FOREIGN_GET_ELEM_SEQNO(proxy);
	}
/*	else
		printf(" *** not updated\n");
*/
    return (0);
}

/*
 * Copy the contents of a private SV to a sociable element.
 * Used by various mg_set() functions.
 * NOTE: we don't lock here...leave that to the app to apply if desired
 */
STATIC int
foreign_scalar_store(pTHX_ SV *sv, sociable_proxy_t *proxy)
{
	SV *obj = sv;
	sociable_locator_t *refloc = NULL;
	SOC_PRINTABLE_LOC(locstr);

	SOCIABLE_TRACE_1("foreign_scalar_store: storing for elemp %s\n", proxy->loc_string);
    if (SvROK(sv)) {
/*
 * if its a ref, then get its referent and verify its sociable
 */
		SOCIABLE_TRACE_1("foreign_scalar_store: assigning a ref to elemp %s\n", proxy->loc_string);
        obj = SvRV(sv);

		SOCIABLE_TRACE_1("foreign_scalar_store: ref is %p\n", obj);
        refloc = sociable_find(aTHX_ obj);

        if (refloc == NULL) {
			SOCIABLE_TRACE("foreign_scalar_store: not a sociable ref!!!\n");
	        Perl_croak(aTHX_ "Invalid value for sociable scalar");
		}

		PRINTABLE_LOC(refloc, locstr);
		SOCIABLE_TRACE_1("foreign_scalar_store: storing for a ref for refelemp %s\n", locstr);
/*
 *	if already a scalar, but not ref, clear it
 */
		if ((*proxy->mech->sv_ok)(proxy) && (!(*proxy->mech->is_rv)(proxy)))
			(*proxy->mech->clear_scalar)(aTHX_ proxy);

		(*proxy->mech->set_ref)(proxy, refloc);
    }
    else {

		SOCIABLE_TRACE_1("foreign_scalar_store: calling SvTEMP_off for elemp %s\n", proxy->loc_string);
		SvTEMP_off(sv);
		SOCIABLE_TRACE_1("foreign_scalar_store: calling sv_save for elemp %s\n", proxy->loc_string);
/*
 * extract SV's value buffer and print it...
 */
		SOCIABLE_TRACE_2("foreign_scalar_store: input sv len is %d cur is %d\n", SvLEN(sv), SvCUR(sv));
		(*proxy->mech->sv_save)(aTHX_ sv, proxy);
	}
/*
 *	if being set to a blessed element, keep the package name for reference
 */
	FOREIGN_INCR_ELEM_SEQNO(proxy);
/*
 * if the contents were blessed after
 * the ref was created, we need to copy the package
 */
	if (SvOBJECT(obj))
		(*proxy->mech->bless)(proxy, HvNAME(SvSTASH(obj)));
	return 0;
}

/*
 * Set magic for PERL_MAGIC_ext(n)
 *
 * If in a transaction:
 * if SV has not been logged then
 *		log it
 *		set proxy seqno
 * else if sociable and proxy not same then
 *		throw restart exception
 * mark proxy as written
 */
STATIC int
foreign_scalar_mg_set(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = (sociable_proxy_t *)(mg->mg_ptr);
	sociable_element_t *elemp = NULL;
	dMY_CXT;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 * the sociable was discarded,
 * remove sociable magic from this SV
 */
		sociable_dissociate(aTHX_ sv);
		return (0);
	}

#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "STORE", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
/*
 * if in xaction, get (and maybe log and acquire) private element;
 *	rest of processing is same for sociable and private element
 */
#ifdef SOCIABLE_STM
	if (SOC_USE_STM(proxy)) {
		if (!(elemp = stm_save_referent(aTHX_ proxy, 1)))
			Perl_croak(aTHX_ Nullch);
		return native_sv_save(aTHX_ sv, elemp);
	}
#endif

   	return foreign_scalar_store(aTHX_ sv, proxy);
}

/*
 *	Return length of the sociable element
 *
 *	if in transaction
 *		treat same as a read operation
 */
STATIC U32
foreign_scalar_mg_len(pTHX_ SV *sv, MAGIC *mg)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	sociable_element_t *elemp = NULL;
	dMY_CXT;

	SOCIABLE_TRACE_1(" *** taking length of elem %s\n", proxy->loc_string);
	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 * remove the magic and return private length
 */
		SOC_DESOCIALIZE(proxy, sv);
		return SvCUR(sv);
	}
/*
 * if in xaction then use STM to get ( and maybe log ) private version;
 *	everything then behaves same, just using private element
 */
#ifdef SOCIABLE_STM
	if (SOC_USE_STM(proxy)) {
		if (!(elemp = stm_save_referent(aTHX_ proxy, 0)))
			Perl_croak(aTHX_ Nullch);
		return native_get_scalar_len(elemp);
	}
#endif
	return (*proxy->mech->get_scalar_len)(aTHX_ proxy);
}

/*
 *	clear the sociable value (???)
 */
STATIC int
foreign_scalar_mg_clear(pTHX_ SV *sv, MAGIC *mg)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	sociable_element_t *elemp = NULL;
	dMY_CXT;

	SOCIABLE_TRACE_1(" *** clearing for proxy %p\n", proxy);
	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 * remove the magic and clear private value
 */
		SOC_DESOCIALIZE(proxy, sv);
		sv_setpvn(sv, "", 0);
		return SvLEN(sv);
	}
/*
 * if in xaction, get (and maybe log and acquire) private element;
 *	rest of processing is same for sociable and private element
 */
#ifdef SOCIABLE_STM
	if (SOC_USE_STM(proxy)) {
		if (!(elemp = stm_save_referent(aTHX_ proxy, 1)))
			Perl_croak(aTHX_ Nullch);
		native_clear_scalar(elemp);
		return 0;
	}
#endif
	(*proxy->mech->clear_scalar)(aTHX_ proxy);
	return 0;
}

MGVTBL foreign_scalar_vtbl = {
    foreign_scalar_mg_get,     /* get */
    foreign_scalar_mg_set,     /* set */
	foreign_scalar_mg_len,		/* len */
	foreign_scalar_mg_clear,	/* clear */
	sociable_scalar_mg_free,	/* free */
	sociable_scalar_mg_copy,	/* copy */
    sociable_scalar_mg_dup,		/* dup */
#ifdef MGf_LOCAL
    sociable_scalar_mg_local,   /* local */
#endif
};

/***********************************************************
 ***********************************************************
 * PERL_MAGIC_tiedelem(p) METHODS
 ***********************************************************
 ***********************************************************/
/***********************************************************
 * NATIVE tiedelem METHODS
 ***********************************************************/
/*
 * Get magic for PERL_MAGIC_tiedelem(p)
 * Note that responsibility for locking is delegated to the app
 */
STATIC int
native_elem_mg_FETCH(pTHX_ SV *sv, MAGIC *mg)
{
/*
 *	mg_obj is a ref to the parent array/hash
 */
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
    sociable_proxy_t valproxy;
	dMY_CXT;

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 *	structure no longer sociable, so return undef
 *	can't privatize the structure, we don't have a SV for it
 */
		SOCIABLE_TRACE("native_elem_mg_FETCH: attempt to reference stale structure\n");
        sv_setsv(sv, &PL_sv_undef);

		SOCIABLE_TRACE("native_elem_mg_FETCH: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("in native_elem_mg_FETCH: array/hash elemp %s\n", proxy->loc_string);
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
    if (SvTYPE(&elemp->header.header) == SVt_PVAV) {
        assert ( mg->mg_ptr == 0 );
/*
 * make sure we've got room; note we don't vivify
 */
		SOCIABLE_TRACE_1("native_elem_mg_FETCH: its an array %d\n", SvTYPE(&elemp->header.header));
#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy)) {
	        if (!stm_array_fetch(aTHX_ proxy, mg->mg_len, 0, &valproxy))
        		Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
        }
 		else
#endif
	        (*proxy->mech->array_fetch)(aTHX_ proxy, mg->mg_len, 0, &valproxy);
    }
    else {	/* its a hash */
        char *key = mg->mg_ptr;
        STRLEN len = mg->mg_len;
        assert ( mg->mg_ptr != 0 );
		SOCIABLE_TRACE_1("native_elem_mg_FETCH: its an hash %d\n", SvTYPE(&elemp->header.header));
        if (mg->mg_len == HEf_SVKEY)
           key = SvPV((SV *) mg->mg_ptr, len);

#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy)) {
	        if (!native_stm_hash_lookup(aTHX_ proxy, key, len, 0, &valproxy))
        		Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
        }
 		else
#endif
	        (*proxy->mech->hash_lookup)(aTHX_ proxy, key, len, 0, &valproxy);
    }

	SOCIABLE_TRACE_1("native_elem_mg_FETCH: valelemp %s\n", valproxy.loc_string);
    if (SvOK(&valproxy.header)) {
        /* Exists in the structure */
        if (valproxy.sociable.level == SOC_FOREIGN_MECH) {
	        if ((*valproxy.mech->is_rv)(&valproxy))
	            FOREIGN_GET_RV(sv, &valproxy);
	        else
	            (*valproxy.mech->sv_recover)(aTHX_ sv, &valproxy);
        }
        else {
	        if (SvROK(&valproxy.elemp->header.header))
	            NATIVE_GET_RV(sv, &valproxy);
	        else
	            native_sv_recover(aTHX_ sv, &valproxy, NULL);
		}
/*
 *	if we were blessed, free up the class string
 */
		if (valproxy.pkg_name)
			crt_free(valproxy.pkg_name);
	}
    else /* Not in the structure */
        sv_setsv(sv, &PL_sv_undef);

	SOCIABLE_TRACE("native_elem_mg_FETCH: returning\n");
    return (0);
}

/*
 * Set magic for PERL_MAGIC_tiedelem(p)
 * *NOTE* appears to assume the array has already
 * been extended as needed to fit the element;
 * hash will create the slot if needed
 */
STATIC int
native_elem_mg_STORE(pTHX_ SV *sv, MAGIC *mg)
{
/*
 *	mg_obj is a ref to the parent array/hash
 */
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	sociable_proxy_t valproxy;
	dMY_CXT;

	SOCIABLE_TRACE_1("native_elem_mg_STORE: array elemp %s\n", proxy->loc_string);

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 *	structure no longer sociable, so return undef
 *	can't privatize the structure, we don't have a SV for it
 */
		SOCIABLE_TRACE("native_elem_mg_STORE: attempt to reference stale structure\n");
        sv_setsv(sv, &PL_sv_undef);

		SOCIABLE_TRACE("native_elem_mg_STORE: returning\n");
	    return (0);
	}
/*
 * Theory - SV itself is magically sociable - and we have ordered the
 * magic such that by the time we get here it has been stored
 * to its sociable counterpart
 */
    assert(proxy != NULL);
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
    if (SvTYPE(&elemp->header.header) == SVt_PVAV) {
/*
 * its an array, so get the current contents at its
 * index (as stored in mg_len); extend the array if needed
 */
        assert ( mg->mg_ptr == 0 );
	 	SOCIABLE_TRACE_2("native_elem_mg_STORE: its an array %d mg_len is %d\n", SvTYPE(&proxy->header), mg->mg_len);

#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy)) {
	        if (!stm_array_fetch(aTHX_ proxy, mg->mg_len, 1, &valproxy))
        		Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
        }
 		else
#endif
	        (*proxy->mech->array_fetch)(aTHX_ proxy, mg->mg_len, 1, &valproxy);
    }
    else {
/*
 * its a hash, get the key from mg_ptr
 * if the mg_len indicates its the key value, get the key
 * finally, fetch the value for the key and, if not exists,
 * create the hash entry for the key
 */
        char *key = mg->mg_ptr;
        STRLEN len = mg->mg_len;
        assert ( mg->mg_ptr != 0 );
	 	SOCIABLE_TRACE_1("native_elem_mg_STORE: its an hash %d\n", SvTYPE(&elemp->header.header));
/*
 * the key was an SV, get its string buffer and length
 */
        if (mg->mg_len == HEf_SVKEY)
           key = SvPV((SV *) mg->mg_ptr, len);

#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy)) {
	        if (!native_stm_hash_lookup(aTHX_ proxy, key, len, 1, &valproxy))
        		Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
        }
 		else
#endif
	        (*proxy->mech->hash_lookup)(aTHX_ proxy, key, len, 1, &valproxy);
    }
/*
 *	we now have an SV from the slot in the array/hash,
 *	so associate it with the input SV value
 */
	SOCIABLE_TRACE_1("native_elem_mg_STORE: valelemp is %s\n", valproxy.loc_string);
    native_sv_save(aTHX_ sv, valproxy.elemp);
    if (valproxy.pkg_name)
    	crt_free(valproxy.pkg_name);
	SOCIABLE_TRACE("native_elem_mg_STORE: returning\n");
    return (0);
}

/*
 * Delete magic for PERL_MAGIC_tiedelem(p)
 */
STATIC int
native_elem_mg_DELETE(pTHX_ SV *sv, MAGIC *mg)
{
    MAGIC *shmg;
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	char *key;
	STRLEN len;
	dMY_CXT;

	SOCIABLE_TRACE_1("in native_elem_mg_DELETE: array elemp %s\n", proxy->loc_string);
    native_elem_mg_FETCH(aTHX_ sv, mg);

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, so return undef
 *	can't privatize the structure, we don't have a SV for it
 */
		SOCIABLE_TRACE("native_elem_mg_DELETE: attempt to reference stale structure\n");
        sv_setsv(sv, &PL_sv_undef);

		SOCIABLE_TRACE("native_elem_mg_DELETE: returning\n");
	    return (0);
	}
/*
 * do a magic get here...but why ? it can't exist in the array
 * unless its magic can it ?
 */
    if ((shmg = mg_find(sv, PERL_MAGIC_ext)))
        native_scalar_mg_get(aTHX_ sv, shmg);

#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
    if (SvTYPE(&elemp->header.header) == SVt_PVAV) {
		SOCIABLE_TRACE_1("native_elem_mg_DELETE: deleting array mg_len %d\n", mg->mg_len);

#ifdef SOCIABLE_STM
		if (SOC_USE_STM(proxy)) {
	        if (!stm_array_delete(aTHX_ proxy, mg->mg_len))
	        	Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
		}
		else
#endif
	    	(*proxy->mech->array_delete)(aTHX_ proxy, mg->mg_len);
	    return (0);
    }

	assert ( mg->mg_ptr != 0 );

	key = mg->mg_ptr;
	len = mg->mg_len;
	if (mg->mg_len == HEf_SVKEY)
		key = SvPV((SV *) mg->mg_ptr, len);

	SOCIABLE_TRACE_1("native_elem_mg_DELETE: deleting hash element %s\n", key);
#ifdef SOCIABLE_STM
	if (SOC_USE_STM(proxy)) {
		if (native_stm_remove_hash_element(aTHX_ proxy, key, len) < 0)
			Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
	    return 0;
	}
#endif
	(*proxy->mech->remove_hash_element)(aTHX_ proxy, key, len);
	return 0;
}

/*
 * Called during cloning of PERL_MAGIC_tiedelem(p) magic in new
 * thread
 * NOTE: we don't need to dup, since we don't refcount!!
 *
 *	transaction context not cloned between threads
 */
/*
STATIC int
native_elem_mg_dup(pTHX_ MAGIC *mg, CLONE_PARAMS *param)
{
    SvREFCNT_inc(SOCIABLE_FROM_OBJ(mg->mg_obj));
    assert(mg->mg_flags & MGf_DUP);
    return (0);
}
*/

MGVTBL native_elem_vtbl = {
    native_elem_mg_FETCH,     /* get */
    native_elem_mg_STORE,     /* set */
    0,                          /* len */
    native_elem_mg_DELETE,    /* clear */
    0,                          /* free */
    0,                          /* copy */
    0,       					/* dup */
#ifdef MGf_LOCAL
    0,                          /* local */
#endif
};

/***********************************************************
 * FOREIGN tiedelem METHODS
 ***********************************************************/
/*
 * Get magic for PERL_MAGIC_tiedelem(p)
 * Note that responsibility for locking is delegated to the app
 */
STATIC int
foreign_elem_mg_FETCH(pTHX_ SV *sv, MAGIC *mg)
{
/*
 *	mg_obj is a ref to the parent array/hash
 */
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    sociable_proxy_t valproxy;
    int elem_type = (*proxy->mech->get_sv_type)(proxy);
	dMY_CXT;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 *	structure no longer sociable, so return undef
 *	can't privatize the structure, we don't have a SV for it
 */
		SOCIABLE_TRACE("foreign_elem_mg_FETCH: attempt to reference stale structure\n");
        sv_setsv(sv, &PL_sv_undef);

		SOCIABLE_TRACE("foreign_elem_mg_FETCH: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("in foreign_elem_mg_FETCH: array elemp %s\n", proxy->loc_string);
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
    if (elem_type == SVt_PVAV) {
        assert ( mg->mg_ptr == 0 );
/*
 * make sure we've got room; note we don't vivify
 */
		SOCIABLE_TRACE_1("foreign_elem_mg_FETCH: its an array %d\n", elem_type);
#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy) &&
	        !stm_array_fetch(aTHX_ proxy, mg->mg_len, 0, &valproxy))
	        Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
 		else
#endif
	        (*proxy->mech->array_fetch)(aTHX_ proxy, mg->mg_len, 0, &valproxy);
    }
    else {	/* its a hash */
        char *key = mg->mg_ptr;
        STRLEN len = mg->mg_len;
        assert ( mg->mg_ptr != 0 );
		SOCIABLE_TRACE_1("foreign_elem_mg_FETCH: its an hash %d\n", elem_type);
        if (mg->mg_len == HEf_SVKEY)
           key = SvPV((SV *) mg->mg_ptr, len);

#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy) &&
	        !stm_hash_lookup(aTHX_ proxy, key, len, 0, &valproxy))
        	Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
 		else
#endif
	        (*proxy->mech->hash_lookup)(aTHX_ proxy, key, len, 0, &valproxy);
    }

	SOCIABLE_TRACE_1("foreign_elem_mg_FETCH: valelemp %s\n", valproxy.loc_string);
    if (valproxy.mech) {
        /* Exists in the structure */
        if (valproxy.sociable.level == SOC_FOREIGN_MECH) {
        	if ((*valproxy.mech->is_rv)(&valproxy))
	            FOREIGN_GET_RV(sv, &valproxy);
        	else
        	    (*valproxy.mech->sv_recover)(aTHX_ sv, &valproxy);
        }
        else {
        	if (SvROK(&valproxy.elemp->header.header))
	            NATIVE_GET_RV(sv, &valproxy);
        	else
        	    native_sv_recover(aTHX_ sv, &valproxy, NULL);
		}
		if (valproxy.pkg_name)
			crt_free(valproxy.pkg_name);
    }
    else /* Not in the structure */
        sv_setsv(sv, &PL_sv_undef);

	SOCIABLE_TRACE("foreign_elem_mg_FETCH: returning\n");
    return (0);
}

/*
 * Set magic for PERL_MAGIC_tiedelem(p)
 * *NOTE* appears to assume the array has already
 * been extended as needed to fit the element;
 * hash will create the slot if needed
 */
STATIC int
foreign_elem_mg_STORE(pTHX_ SV *sv, MAGIC *mg)
{
/*
 *	mg_obj is a ref to the parent array/hash
 */
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    sociable_proxy_t valproxy;
    int elem_type = (*proxy->mech->get_sv_type)(proxy);
	dMY_CXT;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 *	structure no longer sociable, so return undef
 *	can't privatize the structure, we don't have a SV for it
 */
		SOCIABLE_TRACE("foreign_elem_mg_STORE: attempt to reference stale structure\n");
        sv_setsv(sv, &PL_sv_undef);

		SOCIABLE_TRACE("foreign_elem_mg_STORE: returning\n");
	    return (0);
	}
/*
 * Theory - SV itself is magically sociable - and we have ordered the
 * magic such that by the time we get here it has been stored
 * to its sociable counterpart
 */
    assert(proxy != NULL);
	SOCIABLE_TRACE_1("foreign_elem_mg_STORE: array elemp %s\n", proxy->loc_string);

#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
    if (elem_type == SVt_PVAV) {
/*
 * its an array, so get the current contents at its
 * index (as stored in mg_len); extend the array if needed
 */
        assert ( mg->mg_ptr == 0 );
	 	SOCIABLE_TRACE_2("foreign_elem_mg_STORE: its an array %d mg_len is %d\n", elem_type, mg->mg_len);

#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy)) {
	        if (!stm_array_fetch(aTHX_ proxy, mg->mg_len, 1, &valproxy))
	        	Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
 		}
 		else
#endif
	        (*proxy->mech->array_fetch)(aTHX_ proxy, mg->mg_len, 1, &valproxy);
    }
    else {
/*
 * its a hash, get the key from mg_ptr
 * if the mg_len indicates its the key value, get the key
 * finally, fetch the value for the key and, if not exists,
 * create the hash entry for the key
 */
        char *key = mg->mg_ptr;
        STRLEN len = mg->mg_len;
        assert ( mg->mg_ptr != 0 );
	 	SOCIABLE_TRACE_1("foreign_elem_mg_STORE: its an hash %d\n", elem_type);
/*
 * the key was an SV, get its string buffer and length
 */
        if (mg->mg_len == HEf_SVKEY)
           key = SvPV((SV *) mg->mg_ptr, len);

#ifdef SOCIABLE_STM
 		if (SOC_USE_STM(proxy)) {
	        if (!stm_hash_lookup(aTHX_ proxy, key, len, 1, &valproxy))
	        	Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
 		}
 		else
#endif
	        (*proxy->mech->hash_lookup)(aTHX_ proxy, key, len, 1, &valproxy);
    }
/*
 *	we now have an SV from the slot in the array/hash,
 *	so associate it with the input SV value
 */
	SOCIABLE_TRACE_1("foreign_elem_mg_STORE: valelemp is %s\n", valproxy.loc_string);
    foreign_scalar_store(aTHX_ sv, &valproxy);
    if (valproxy.pkg_name)
    	crt_free(valproxy.pkg_name);
	SOCIABLE_TRACE("foreign_elem_mg_STORE: returning\n");
    return (0);
}

/*
 * Delete magic for PERL_MAGIC_tiedelem(p)
 */
STATIC int
foreign_elem_mg_DELETE(pTHX_ SV *sv, MAGIC *mg)
{
    MAGIC *shmg;
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    int elem_type = (*proxy->mech->get_sv_type)(proxy);
	dMY_CXT;

	SOCIABLE_TRACE_1("in sociable_elem_mg_DELETE: array elemp %s\n", proxy->loc_string);
    foreign_elem_mg_FETCH(aTHX_ sv, mg);

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, so return undef
 *	can't privatize the structure, we don't have a SV for it
 */
		SOCIABLE_TRACE("foreign_elem_mg_DELETE: attempt to reference stale structure\n");
        sv_setsv(sv, &PL_sv_undef);

		SOCIABLE_TRACE("foreign_elem_mg_DELETE: returning\n");
	    return (0);
	}
/*
 * do a magic scalar get here...but why ?
 * it can't be both a scalar AND a list !
 */
    if ((shmg = mg_find(sv, PERL_MAGIC_ext)))
        foreign_scalar_mg_get(aTHX_ sv, shmg);

#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
    if (elem_type == SVt_PVAV) {
		SOCIABLE_TRACE_1("foreign_elem_mg_DELETE: deleting array mg_len %d\n", mg->mg_len);

#ifdef SOCIABLE_STM
		if (SOC_USE_STM(proxy)) {
	        if (!stm_array_delete(aTHX_ proxy, mg->mg_len))
	        	Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
		}
		else
#endif
	        (*proxy->mech->array_delete)(aTHX_ proxy, mg->mg_len);
    }
    else {
        char *key = mg->mg_ptr;
        STRLEN len = mg->mg_len;
        assert ( mg->mg_ptr != 0 );
        if (mg->mg_len == HEf_SVKEY)
           key = SvPV((SV *) mg->mg_ptr, len);

		SOCIABLE_TRACE_1("foreign_elem_mg_DELETE: deleting hash element %s\n", key);
#ifdef SOCIABLE_STM
		if (SOC_USE_STM(proxy)) {
	        if (stm_remove_hash_element(aTHX_ proxy, key, len) < 0)
	        	Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
		}
		else
#endif
			(*proxy->mech->remove_hash_element)(aTHX_ proxy, key, len);
    }

    return (0);
}

/*
 * Called during cloning of PERL_MAGIC_tiedelem(p) magic in new
 * thread
 * NOTE: we don't need to dup, since we don't refcount!!
 *
 *	transaction context not cloned between threads
 */
/*
STATIC int
foreign_elem_mg_dup(pTHX_ MAGIC *mg, CLONE_PARAMS *param)
{
    SvREFCNT_inc(SOCIABLE_FROM_OBJ(mg->mg_obj));
    assert(mg->mg_flags & MGf_DUP);
    return (0);
}
*/

MGVTBL foreign_elem_vtbl = {
    foreign_elem_mg_FETCH,     /* get */
    foreign_elem_mg_STORE,     /* set */
    0,                          /* len */
    foreign_elem_mg_DELETE,    /* clear */
    0,                          /* free */
    0,                          /* copy */
    0,       					/* dup */
#ifdef MGf_LOCAL
    0,                          /* local */
#endif
};

/***********************************************************
 ***********************************************************
 * PERL_MAGIC_tied(P) METHODS
 ***********************************************************
 ***********************************************************/
/***********************************************************
 * NATIVE tied METHODS
 ***********************************************************/
/*
 * Len magic for PERL_MAGIC_tied(P)
 * again, we delegate locking to the app
 *
 * if in transaction
 *		if array/hash not logged
 *			log it
 *			spinlock sociable version
 *			copy sociable seqno to proxy
 *			copy structure metadata to proxy
 *			release spinlock
 *			create empty proxy structure
 *		else if sociable and proxy not sync'd
 *			throw restart exception
 *		endif
 *		fetch proxy structure length
 *		mark proxy structure as read if not previsouly written
 * endif
 */
STATIC I32
native_array_mg_FETCHSIZE(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	dMY_CXT;

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, privatize it
 */
		SOCIABLE_TRACE("native_array_mg_FETCHSIZE: attempt to reference stale structure\n");
        sociable_dissociate(aTHX_ sv);

		SOCIABLE_TRACE("native_array_mg_FETCHSIZE: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("in native_array_mg_FETCHSIZE for elemp %p\n", elemp);
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
	return SOC_USE_STM(proxy)
		? stm_array_get_size(aTHX_ proxy)
		: (*proxy->mech->array_get_size)(aTHX_ proxy);
}

/*
 * Clear magic for PERL_MAGIC_tied(P)
 *
 * if in transaction
 *		if array/hash not logged
 *			log it
 *			spinlock sociable version
 *			copy sociable seqno to proxy
 *			copy structure metadata to proxy
 *			release spinlock
 *			create empty proxy structure
 *		else if sociable and proxy not sync'd
 *			throw restart exception
 *		endif
 *		mark proxy structure as cleared
 *		free all proxy elements
 *		update proxy metadata
 *		mark proxy structure as written
 * endif
 */
STATIC int
native_array_mg_CLEAR(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
    sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	int rc;
	dMY_CXT;

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, privatize it
 */
		SOCIABLE_TRACE("native_array_mg_CLEAR: attempt to reference stale structure\n");
        sociable_dissociate(aTHX_ sv);

		SOCIABLE_TRACE("native_array_mg_CLEAR: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("native_array_mg_CLEAR: clearing array elemp %s\n", proxy->loc_string);
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
	rc = SOC_USE_STM(proxy)
		? ((SvTYPE(&elemp->header.header) == SVt_PVAV) 
			? stm_array_clear(aTHX_ proxy) 
			: stm_hash_clear(aTHX_ proxy)) 
		: ((SvTYPE(&elemp->header.header) == SVt_PVAV) 
			? (*proxy->mech->array_clear)(aTHX_ proxy) 
			: (*proxy->mech->hash_clear)(aTHX_ proxy));
    if (!rc)
       	Perl_croak(aTHX_ Nullch);
    return (0);
}

STATIC int
native_array_mg_set(pTHX_ SV *sv, MAGIC *mg)
{
	SOCIABLE_TRACE("in native_array_mg_set\n");
    return (0);
}

STATIC int
native_array_mg_get(pTHX_ SV *sv, MAGIC *mg)
{
	SOCIABLE_TRACE("in native_array_mg_get\n");
    return (0);
}

/*
 * Free magic for PERL_MAGIC_tied(P)
 * Note: we require explicit discard(), so remove this
 */
STATIC int
native_array_mg_free(pTHX_ SV *sv, MAGIC *mg)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	SOCIABLE_TRACE_2("native_array_mg_free: freeing array sv %p proxy %p\n", sv, mg->mg_ptr);
	if (proxy->pkg_name)
		crt_free(proxy->pkg_name);
	crt_free(proxy);
	mg->mg_ptr = NULL;
    return (0);
}
/*
 * Copy magic for PERL_MAGIC_tied(P)
 * This is called when perl is about to access an element of
 * the array -
 *
 *	Not certain how this relates to STM, if at all...
 */
STATIC int
native_array_mg_copy(pTHX_ SV *sv, MAGIC* mg, SV *nsv, const char *name, int namlen)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
    MAGIC *nmg = NULL;

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, don't copy
 */
		SOCIABLE_TRACE("native_array_mg_copy: attempt to reference stale structure\n");
        sociable_dissociate(aTHX_ sv);

		SOCIABLE_TRACE("native_array_mg_copy: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_3("native_array_mg_copy: sv %p src element mg is %p mg_obj %p\n", sv, mg, mg->mg_obj);
    nmg = sv_magicext(
    	nsv,
    	mg->mg_obj,
    	toLOWER(mg->mg_type),
    	&native_elem_vtbl,
    	name,
    	namlen);
	SOCIABLE_TRACE_3("native_array_mg_copy: nsv %p copy element nmg is %p mg_obj %p\n", nsv, nmg, nmg->mg_obj);
    nmg->mg_flags |= MGf_DUP;
    return (1);
}

/*
 * Called during cloning of PERL_MAGIC_tied(P) magic in new thread
 *	No transaction context is cloned between threads
 */
STATIC int
native_array_mg_dup(pTHX_ MAGIC *mg, CLONE_PARAMS *param)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	sociable_proxy_t *clone_proxy = NULL;

	if (NATIVE_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, don't copy
 */
		SOCIABLE_TRACE("native_array_mg_dup: attempt to reference stale structure\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("native_array_mg_dup: dup'ing proxy %p\n", proxy);
/*
 *	NOTE: we proceed to clone even tho the element may have expired;
 *	any later access to said element will force unmagic
 */
	CRT_Dup(clone_proxy, 1, sociable_proxy_t, proxy, 1);
	if (proxy->pkg_name)
		clone_proxy->pkg_name = crt_strdup(proxy->pkg_name);
	mg->mg_ptr = (char *)clone_proxy;
    return (0);
}

MGVTBL native_array_vtbl = {
    native_array_mg_get,		/* get */
    native_array_mg_set,		/* set */
    native_array_mg_FETCHSIZE,/* len */
    native_array_mg_CLEAR,    /* clear */
    native_array_mg_free,		/* free */
    native_array_mg_copy,     /* copy */
    native_array_mg_dup,      /* dup */
#ifdef MGf_LOCAL
    0,                          /* local */
#endif
};

/***********************************************************
 * FOREIGN tied METHODS
 ***********************************************************/
/*
 * Len magic for PERL_MAGIC_tied(P)
 * again, we delegate locking to the app
 *
 * if in transaction
 *		if array/hash not logged
 *			log it
 *			spinlock sociable version
 *			copy sociable seqno to proxy
 *			copy structure metadata to proxy
 *			release spinlock
 *			create empty proxy structure
 *		else if sociable and proxy not sync'd
 *			throw restart exception
 *		endif
 *		fetch proxy structure length
 *		mark proxy structure as read if not previsouly written
 * endif
 */
STATIC I32
foreign_array_mg_FETCHSIZE(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
	dMY_CXT;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, privatize it
 */
		SOCIABLE_TRACE("foreign_array_mg_FETCHSIZE: attempt to reference stale structure\n");
        sociable_dissociate(aTHX_ sv);

		SOCIABLE_TRACE("foreign_array_mg_FETCHSIZE: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("in sociable_array_mg_FETCHSIZE for elemp %p\n", proxy);
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
	return SOC_USE_STM(proxy)
		? stm_array_get_size(aTHX_ proxy)
		: (*proxy->mech->array_get_size)(aTHX_ proxy);
}

/*
 * Clear magic for PERL_MAGIC_tied(P)
 *
 * if in transaction
 *		if array/hash not logged
 *			log it
 *			spinlock sociable version
 *			copy sociable seqno to proxy
 *			copy structure metadata to proxy
 *			release spinlock
 *			create empty proxy structure
 *		else if sociable and proxy not sync'd
 *			throw restart exception
 *		endif
 *		mark proxy structure as cleared
 *		free all proxy elements
 *		update proxy metadata
 *		mark proxy structure as written
 * endif
 */
STATIC int
foreign_array_mg_CLEAR(pTHX_ SV *sv, MAGIC *mg)
{
    sociable_proxy_t *proxy = SOC_PROXY_FROM_OBJ(mg->mg_obj);
	int rc;
	int elem_type = (*proxy->mech->get_sv_type)(proxy);
	dMY_CXT;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, privatize it
 */
		SOCIABLE_TRACE("foreign_array_mg_CLEAR: attempt to reference stale structure\n");
        sociable_dissociate(aTHX_ sv);

		SOCIABLE_TRACE("foreign_array_mg_CLEAR: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("foreign_array_mg_CLEAR: clearing array elemp %s\n", proxy->loc_string);
#ifdef SOCIABLE_TIE
/*
 *	if it has ties, execute them instead
 */
    if ((*proxy->mech->is_tied)(proxy)) {
/* !!! TO DO
    	sociable_tie_t *tie = elemp->header.ties;
    	while (tie != NULL) {
    		sociable_enqueue_simplex(tie->tsq, tie->proxy_id, "FETCH", sv);
    		tie = tie->header.next;
    	}
    	return(0);
*/
    }
#endif
	rc = SOC_USE_STM(proxy)
		? ((elem_type == SVt_PVAV) 
			? stm_array_clear(aTHX_ proxy) 
			: stm_hash_clear(aTHX_ proxy)) 
		: ((elem_type == SVt_PVAV) 
			? (*proxy->mech->array_clear)(aTHX_ proxy) 
			: (*proxy->mech->hash_clear)(aTHX_ proxy));
    if (!rc)
       	Perl_croak(aTHX_ Nullch);
    return (0);
}

STATIC int
foreign_array_mg_set(pTHX_ SV *sv, MAGIC *mg)
{
	SOCIABLE_TRACE("in sociable_array_mg_set\n");
    return (0);
}

STATIC int
foreign_array_mg_get(pTHX_ SV *sv, MAGIC *mg)
{
	SOCIABLE_TRACE("in sociable_array_mg_get\n");
    return (0);
}

/*
 * Free magic for PERL_MAGIC_tied(P)
 * Note: we require explicit discard(), so remove this
 */
STATIC int
foreign_array_mg_free(pTHX_ SV *sv, MAGIC *mg)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	SOCIABLE_TRACE_2("foreign_array_mg_free: freeing array sv %p proxy %p\n", sv, mg->mg_ptr);
	if (proxy->pkg_name)
		crt_free(proxy->pkg_name);
	crt_free(proxy);
	mg->mg_ptr = NULL;
    return (0);
}
/*
 * Copy magic for PERL_MAGIC_tied(P)
 * This is called when perl is about to access an element of
 * the array -
 *
 *	Not certain how this relates to STM, if at all...
 */
STATIC int
foreign_array_mg_copy(pTHX_ SV *sv, MAGIC* mg, SV *nsv, const char *name, int namlen)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
    MAGIC *nmg = NULL;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, don't copy
 */
		SOCIABLE_TRACE("foreign_array_mg_copy: attempt to reference stale structure\n");
        sociable_dissociate(aTHX_ sv);

		SOCIABLE_TRACE("foreign_array_mg_copy: returning\n");
	    return (0);
	}

	SOCIABLE_TRACE_3("foreign_array_mg_copy: sv %p src element mg is %p mg_obj %p\n", sv, mg, mg->mg_obj);
    nmg = sv_magicext(
    	nsv,
    	mg->mg_obj,
    	toLOWER(mg->mg_type),
    	&foreign_elem_vtbl,
    	name,
    	namlen);
	SOCIABLE_TRACE_3("foreign_array_mg_copy: nsv %p copy element nmg is %p mg_obj %p\n", nsv, nmg, nmg->mg_obj);
    nmg->mg_flags |= MGf_DUP;
    return (1);
}

/*
 * Called during cloning of PERL_MAGIC_tied(P) magic in new thread
 * NOTE: we don't need to do anything for cloning, since we don't refcount
 *
 *	No transaction context is cloned between threads
 */
STATIC int
foreign_array_mg_dup(pTHX_ MAGIC *mg, CLONE_PARAMS *param)
{
	sociable_proxy_t *proxy = (sociable_proxy_t *)mg->mg_ptr;
	sociable_proxy_t *clone_proxy = NULL;

	if (FOREIGN_NOT_SOCIABLE(proxy)) {
/*
 *	array/hash no longer sociable, don't copy
 */
		SOCIABLE_TRACE("foreign_array_mg_dup: attempt to reference stale structure\n");
	    return (0);
	}

	SOCIABLE_TRACE_1("foreign_array_mg_dup: dup'ing proxy %p\n", proxy);
/*
 *	NOTE: we proceed to clone even tho the element may have expired;
 *	any later access to said element will force unmagic
 */
	CRT_Dup(clone_proxy, 1, sociable_proxy_t, proxy, 1);
	if (proxy->pkg_name)
		clone_proxy->pkg_name = crt_strdup(proxy->pkg_name);
	mg->mg_ptr = (char *)clone_proxy;
    return (0);
}

MGVTBL foreign_array_vtbl = {
    foreign_array_mg_get,		/* get */
    foreign_array_mg_set,		/* set */
    foreign_array_mg_FETCHSIZE,/* len */
    foreign_array_mg_CLEAR,    /* clear */
    foreign_array_mg_free,		/* free */
    foreign_array_mg_copy,     /* copy */
    foreign_array_mg_dup,      /* dup */
#ifdef MGf_LOCAL
    0,                          /* local */
#endif
};

/*
 * Recursive locks on a sociablesv.
 * Locks are dynamically scoped at the level of the first lock.
 */
STATIC void
sociable_lock_intern(pTHX_ SV *sv)
{
	dMY_CXT;

    if (SvROK(sv))
        sv = SvRV(sv);

    if (!sociable_find_and_lock(aTHX_ sv)) {
/*
 *	maybe its a ref, try again for good measure
 */
    	if (SvROK(sv))
    	    sv = SvRV(sv);
	    if (!sociable_find_and_lock(aTHX_ sv)) {
/*
 * assume its for shared
 * I think we need to test for sociable magic 1st, and then
 * start chaining if its not...
 */
	    	(*MY_CXT.lockchain)(aTHX_ sv);
	       /* Perl_croak(aTHX_ "lock can only be used on Lockable variables"); */
		}
	}
	SOCIABLE_TRACE("sociable_lock_intern: return\n");
}
/*
 * Creates the sociable global element maps and locks.
 * Called at BOOT time. Note the element maps are
 * created from process heap, *not* Perl heap.
 *
 *	Scalar elements are stored in a simple map, and SV's
 *	use ext magic to store a element ptr
 *	in the SV. set/get for the SV then simply returns the values
 *	currently in the element
 */
STATIC void
sociable_init(pTHX)
{
	int i;
	dMY_CXT;
/*
 * init storage mechanisms
 */
	for (i = 0; i < SOCIABLE_MECHS; sociable_mechs[i++] = NULL);
	sociable_next_mech = 0;
/*
 *	DANGER DANGER WILL ROBINSON!!!
 *	This is waaayy too incestuous w/ Perl CORE for my tastes, but
 *	is the only way to emulate shared's scoped locking.
 *	And we probably need to verify if lockhook already has something
 *	in it...
 */
 	if ((PL_lockhook != NULL) && (PL_lockhook != &sociable_lock_intern)) {
 		/* printf("\n*** lockhook populated w/ %p!!!\n", PL_lockhook); */
 		MY_CXT.lockchain = PL_lockhook;
 	}
    PL_lockhook = &sociable_lock_intern;
#ifdef SOCIABLE_STM
/*
 *	create STM transaction contexts
 */
/*  	stm_init(); */
#endif
}


MODULE = Thread::Sociable        PACKAGE = Thread::Sociable::tie

PROTOTYPES: DISABLE

void
PUSH(SV *obj, ...)
    CODE:
        dTHX;
		/*
		 *	get the sociable version of the array
		 */
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
        int i;
        int rc;
        SV **svary;
		dMY_CXT;

	    SOCIABLE_TRACE("in Thread::Sociable::tie::PUSH\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior.
	     */
	    	if (SvTYPE(obj) != SVt_PVAV)
	    		Perl_croak(aTHX_ "Thread::Sociable::tie::PUSH: not a sociable array.");
	    	if (sociable_warn)
	    		Perl_warn(aTHX_ "Thread::Sociable::tie::PUSH: not a sociable array.");
	        for (i = 1; i < items; i++)
	        	av_push((AV *)obj, ST(i));
	        XSRETURN_YES;
	    }

		/*
		 *	create an SV list for the replacements
		 */
		SOC_ALLOC_SV_TMPS(svary, items - 1);
        for (i = 1; i < items; i++) {
        	/*
        	 *	if a ref, verify that its a sociable and compatible
        	 */
        	if (SvROK(ST(i)) &&
        	 	!sociable_is_compatible(aTHX_ ST(i), proxy)) {
				SOC_FREE_SV_TMPS(svary);
        		Perl_croak(aTHX_ Nullch);
        	}
            svary[i - 1] = ST(i);
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		rc = SOC_USE_STM(proxy)
			? stm_push(aTHX_ proxy, items - 1, svary) 
			: (*proxy->mech->push)(aTHX_ proxy, items - 1, svary);
		SOC_FREE_SV_TMPS(svary);
		if (!rc)
			Perl_croak(aTHX_ Nullch);
		XSRETURN_YES;


void
UNSHIFT(SV *obj, ...)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
        int i;
        int rc;
        SV **svary;
		dMY_CXT;
		/*
		 * extend the array if needed
		 */
        SOCIABLE_TRACE("in Thread::Sociable::tie::UNSHIFT\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior.
	     */
	    	if (SvTYPE(obj) != SVt_PVAV)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::UNSHIFT: not a sociable array.");
	    	if (sociable_warn)
		    	Perl_warn(aTHX_ "Thread::Sociable::tie::UNSHIFT: not a sociable array.");
		    av_unshift((AV *)obj, items - 1);
	        for (i = 1; i < items; i++)
	        	av_store((AV *)obj, i - i, ST(i));
	        XSRETURN_YES;
		}
		/*
		 *	use SPLICE with replacement list, and items == -1
		 */
		SOC_ALLOC_SV_TMPS(svary, items - 1);
        for (i = 1; i < items; i++) {
        	/*
        	 *	if a ref, verify that its a sociable and compatible
        	 */
        	if (SvROK(ST(i)) &&
        	 	!sociable_is_compatible(aTHX_ ST(i), proxy)) {
				SOC_FREE_SV_TMPS(svary);
        		Perl_croak(aTHX_ Nullch);
        	}
            svary[i - 1] = ST(i);
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		rc = SOC_USE_STM(proxy) 
			? stm_unshift(aTHX_ proxy, items - 1, svary) 
			: (*proxy->mech->unshift)(aTHX_ proxy, items - 1, svary);
		SOC_FREE_SV_TMPS(svary);
        if (!rc)
			Perl_croak(aTHX_ Nullch);
		XSRETURN_YES;


void
POP(SV *obj)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
		SV *sv;
		dMY_CXT;

	    SOCIABLE_TRACE("in Thread::Sociable::tie::POP\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior.
	     */
	    	if (SvTYPE(obj) != SVt_PVAV)
	    		Perl_croak(aTHX_ "Thread::Sociable::tie::POP: not a sociable array.");
	    	if (sociable_warn)
		    	Perl_warn(aTHX_ "Thread::Sociable::tie::POP: not a sociable array.");
	    	XSRETURN_UNDEF;
	    }
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		sv = SOC_USE_STM(proxy)
			? stm_pop(aTHX_ proxy) 
			: (*proxy->mech->pop)(aTHX_ proxy);
		if (sv == NULL)
			Perl_croak(aTHX_ Nullch);
        ST(0) = sv;
		XSRETURN(1);


void
SHIFT(SV *obj)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
		SV *sv;
		dMY_CXT;

	    SOCIABLE_TRACE("in Thread::Sociable::tie::SHIFT\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior.
	     */
	    	if (SvTYPE(obj) != SVt_PVAV)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::SHIFT: not a sociable array.");
	    	if (sociable_warn)
		    	Perl_warn(aTHX_ "Thread::Sociable::tie::SHIFT: not a sociable array.");
	    	XSRETURN_UNDEF;
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		sv = SOC_USE_STM(proxy)
			? stm_shift(aTHX_ proxy) 
			: (*proxy->mech->shift)(aTHX_ proxy);
		if (sv == NULL)
			Perl_croak(aTHX_ Nullch);
        ST(0) = sv;
		XSRETURN(1);


void
SPLICE(SV *obj, ...)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
        SV **svary = NULL;
        int i;
        int offset = 0;
        int len = 0;
        int listlen = (items > 3) ? items - 3 : 0;
		SV *svav;
		dMY_CXT;

        SOCIABLE_TRACE("in Thread::Sociable::tie::SPLICE\n");
        /*
         *	check for optional params
         */
        if (items > 1) {
        	if (! SvIOK(ST(1)))
        	    Perl_croak(aTHX_ "Offset must be an integer");
        	offset = SvIV(ST(1));
        }
        if (items > 2) {
        	if (! SvIOK(ST(2)))
        	    Perl_croak(aTHX_ "Length must be an integer");
        	len = SvIV(ST(2));
        }

	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior.
	     */
	    	if (SvTYPE(obj) != SVt_PVAV)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::SPLICE: not a sociable array.");
	    	if (sociable_warn)
		    	Perl_warn(aTHX_ "Thread::Sociable::tie::SPLICE: not a sociable array.");
		    if (offset != 0)
		    	Perl_warn(aTHX_ "Splice past end of array.");
			if (items > 3) {
		    	av_extend((AV *)obj, items - 4);
		    	for (i = 3; i < items; i++)
		    		av_store((AV *)obj, i - 3, ST(i));
			}
			/*
			 *	since we assume it was empty, return empty list or undef
			 */
			if (GIMME_V == G_ARRAY) {
				ST(0) = sv_2mortal((SV*)newAV());
				XSRETURN(1);
			}
			XSRETURN_UNDEF;
		}
		if (items > 3) {
			SOC_ALLOC_SV_TMPS(svary, listlen);
	        for (i = 3; i < items; i++) {
        	/*
        	 *	if a ref, verify that its a sociable and compatible
        	 */
	        	if (SvROK(ST(i)) &&
	        	 	!sociable_is_compatible(aTHX_ ST(i), proxy)) {
					SOC_FREE_SV_TMPS(svary);
	        		Perl_croak(aTHX_ Nullch);
	        	}
	            svary[i - 3] = ST(i);
			}
		}
#ifdef SOCIABLE_TIE
		/*
		 *	need to marshall parameters, but *only* supplied params!
		 */
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		svav = SOC_USE_STM(proxy)
			? stm_splice(aTHX_ proxy, items, offset, len, listlen, svary) 
			: (*proxy->mech->splice)(aTHX_ proxy, items, offset, len, listlen, svary);
		SOC_FREE_SV_TMPS(svary);
		if (svav == NULL)
			Perl_croak(aTHX_ Nullch);
		/*
		 *	return list of removed values OR just the last value (scalar/void context)
		 */
		ST(0) = svav;
		XSRETURN(1);


void
EXTEND(SV *obj, IV count)
    CODE:
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
		dMY_CXT;

	    SOCIABLE_TRACE("in Thread::Sociable::tie::EXTEND\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior.
	     */
	    	if (SvTYPE(obj) != SVt_PVAV)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::EXTEND: not a sociable array.");
	    	if (sociable_warn)
		    	Perl_warn(aTHX_ "Thread::Sociable::tie::EXTEND: not a sociable array.");
		    av_extend((AV *)obj, count - 1);
		    XSRETURN_YES;
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		if (SOC_USE_STM(proxy)
			? stm_extend(aTHX_ proxy, count) 
			: (*proxy->mech->extend)(aTHX_ proxy, count))
        	XSRETURN_YES;
        else
        	XSRETURN_UNDEF;


void
STORESIZE(SV *obj,IV count)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
        int i;
		dMY_CXT;
        /*
         * check if count is greater or less than current head;
         * if less, trim; if more, extend
         */
	    SOCIABLE_TRACE("in Thread::Sociable::tie::STORESIZE\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior.
	     */
	    	if (SvTYPE(obj) != SVt_PVAV)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::STORESIZE: not a sociable array.");
	    	if (sociable_warn)
		    	Perl_warn(aTHX_ "Thread::Sociable::tie::STORESIZE: not a sociable array.");
		    if (av_len((AV *)obj) < count) {
		    	av_extend((AV *)obj, count - 1);
		    	for (i = av_len((AV *)obj); i < count; i++)
		    		av_store((AV *)obj, i, &PL_sv_undef);
		    }
		    ST(0) = sv_2mortal(newSViv(av_len((AV *)obj)));
		    XSRETURN(1);
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		ST(0) = sv_2mortal(newSViv(
			SOC_USE_STM(proxy)
			? stm_storesize(aTHX_ proxy, count) 
			: (*proxy->mech->storesize)(aTHX_ proxy, count)
	        ));
	    XSRETURN(1);


void
EXISTS(SV *obj, SV *index)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
		STRLEN len;
		char *key;
		dMY_CXT;

	    SOCIABLE_TRACE("in Thread::Sociable::tie::EXISTS\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior. Since array/hash is empty, return undef
	     */
	    	if ((SvTYPE(obj) != SVt_PVAV) || (SvTYPE(obj) != SVt_PVHV))
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::EXISTS: not a sociable array or hash.");
	    	if (sociable_warn)
		    	Perl_warn(aTHX_ "Thread::Sociable::tie::EXISTS: not a sociable array or hash.");
			XSRETURN_UNDEF;
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
        if (SvTYPE(&proxy->header) == SVt_PVAV) {
	        IV i = SvIV(index);
			/*
			 *	for expedience, no conflict detection here
			 *	NOTE: do we need to touch the sociable to test for existance
			 *	of the element ? Probably, but not for now
			 */
			ST(0) = (SOC_USE_STM(proxy)
				? stm_array_elem_exists(aTHX_ proxy, i) 
				: (*proxy->mech->array_elem_exists)(aTHX_ proxy, i)) 
					? &PL_sv_yes 
					: &PL_sv_no;
        }
        else {
			key = SvPV(index,len);
			ST(0) = (SOC_USE_STM(proxy)
				? stm_hash_elem_exists(aTHX_ proxy, key, len) 
				: (*proxy->mech->hash_elem_exists)(aTHX_ proxy, key, len)) 
					? &PL_sv_yes 
					: &PL_sv_no;
		}
        XSRETURN(1);


void
FIRSTKEY(SV *obj)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
		dMY_CXT;
		/*
		 * scan the hash buckets until we find one thats
		 * occupied, then return the first entry in the bucket
		 */
	    SOCIABLE_TRACE("in Thread::Sociable::tie::FIRSTKEY\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior. Since array/hash is empty, return undef
	     */
	    	if (SvTYPE(obj) != SVt_PVHV)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::FIRSTKEY: not a sociable hash.");
	    	if (sociable_warn)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::FIRSTKEY: not a sociable hash.");
		    hv_iterinit((HV *)obj);
		    XSRETURN_UNDEF;
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
		ST(0) = sv_2mortal(
			SOC_USE_STM(proxy)
				? stm_firstkey(aTHX_ proxy) 
				: (*proxy->mech->firstkey)(aTHX_ proxy)
			);
		XSRETURN(1);


void
NEXTKEY(SV *obj, SV *oldkey)
    CODE:
        dTHX;
	    sociable_proxy_t *proxy = sociable_find_proxy(aTHX_ obj);
        char *oldkeyp;
        I32 oldkeylen;
		dMY_CXT;
		/*
		 * compute the hash of the provided key, then scan its bucket
		 * until its entry is found, and return its next
		 * if its not found, return undef; if its last in
		 * the bucket, scan to next occupied bucket
		 */
	    SOCIABLE_TRACE("in Thread::Sociable::tie::NEXTKEY\n");
	    if (!proxy) {
	    /*
	     *	should only got here if sociable was discarded,
	     *  Ergo, the private array is empty.
	     *	Revert to normal behavior. Since array/hash is empty, return undef
	     */
	    	if (SvTYPE(obj) != SVt_PVHV)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::NEXTKEY: not a sociable hash.");
	    	if (sociable_warn)
		    	Perl_croak(aTHX_ "Thread::Sociable::tie::NEXTKEY: not a sociable hash.");
		    XSRETURN_UNDEF;
		}
#ifdef SOCIABLE_TIE
	    if ((*proxy->mech->is_tied)(proxy)) {
	    }
#endif
        oldkeyp = SvPV(oldkey, oldkeylen);
		ST(0) = sv_2mortal(
			SOC_USE_STM(proxy)
				? stm_nextkey(aTHX_ proxy, oldkeyp, oldkeylen) 
				: (*proxy->mech->nextkey)(aTHX_ proxy, oldkeyp, oldkeylen)
			);
		XSRETURN(1);


MODULE = Thread::Sociable        PACKAGE = Thread::Sociable::CRTHeap
PROTOTYPES: DISABLE

void
init(SV *pkgname, SV *name, int mechindex, ...)
	CODE:
		/*
		 *	probably need a spinlock here
		 */
		if (!crt_inited) {
			crt_inited = TRUE;
			crt_init();
		/*
		 *	perform any other intialization
		 */
		}
		ST(0) = sv_2mortal(newSVuv(PTR2UV(&crt_mech)));
		XSRETURN(1);


MODULE = Thread::Sociable        PACKAGE = Thread::Sociable

PROTOTYPES: DISABLE

void
_id(SV *ref)
    PROTOTYPE: \[$@%]
    PREINIT:
        sociable_proxy_t *proxy;
    CODE:
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);
	    SOCIABLE_TRACE_1(" *** _id: go find it for %p!\n", ref);
        proxy = sociable_find_proxy(aTHX_ ref);
	    SOCIABLE_TRACE_1(" *** _id: proxy is %p\n", proxy);
        if (proxy == NULL) {
            XSRETURN_UNDEF;
		}
		/*
		 *	provide complete locator
		 */
        ST(0) = sv_2mortal(newSVpv((char *)&proxy->sociable, sizeof(sociable_locator_t)));


void
is_lockable(SV *ref)
    PROTOTYPE: \[$@%]
    PREINIT:
        sociable_proxy_t *proxy;
    CODE:
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);
	    SOCIABLE_TRACE_1(" *** _id: go find it for %p!\n", ref);
        proxy = sociable_find_proxy(aTHX_ ref);
	    SOCIABLE_TRACE_1(" *** _id: elemp is %s\n", proxy->loc_string);
        if ((proxy == NULL) || (!(*proxy->mech->is_lockable)(proxy))) {
            XSRETURN_UNDEF;
		}
        ST(0) = sv_2mortal(newSVuv(PTR2UV(proxy)));


void
socialize(SV *ref, ...)
    PROTOTYPE: \[$@%]
    CODE:
    	SV **item;
    	sociable_storage_mech_t *mech = default_mech;
    	int i;
    	
    	if (items > 1) {
    		item = crt_malloc((items - 1) * sizeof(SV *));
    		for (i = 1; i < items - 1; item[i++] = ST(i));
   			mech = sociable_lookup_mech(items, item);
   			crt_free(item);
    	}
		if (!mech)
			Perl_croak(aTHX_ "No mechanism specified");
        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to socialize needs to be passed as ref");
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);

	    SOCIABLE_TRACE_2("socialize: calling sociable_socialize for %p mechanism %s\n", ref, mech->name);
        sociable_socialize(aTHX_ ref, mech, 0);
	    SOCIABLE_TRACE("socialize: return from sociable_socialize\n");
        ST(0) = sv_2mortal(newRV_inc(ref));


void
socialize_with_lock(SV *ref, ...)
    PROTOTYPE: \[$@%]
    CODE:
    	SV **item;
    	sociable_storage_mech_t *mech = default_mech;
    	int i;
    	
    	if (items > 1) {
    		item = crt_malloc((items - 1) * sizeof(SV *));
    		for (i = 1; i < items - 1; item[i++] = ST(i));
   			mech = sociable_lookup_mech(items, item);
   			crt_free(item);
    	}
		if (!mech)
			Perl_croak(aTHX_ "No mechanism specified");
        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to socialize_with_lock needs to be passed as ref");
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);

	    SOCIABLE_TRACE_2("socialize_with_lock: calling sociable_socialize for %p mechanism %s\n", ref, mech->name);
        sociable_socialize(aTHX_ ref, mech, 1);
	    SOCIABLE_TRACE("socialize_with_lock: return from sociable_socialize\n");
        ST(0) = sv_2mortal(newRV_inc(ref));


void
sociable_discard(SV *ref)
    PROTOTYPE: \[$@%]
    CODE:
    	sociable_proxy_t *proxy;
        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to sociable_discard needs to be passed as ref");
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);

	    SOCIABLE_TRACE("sociable_discard: destroying sociable\n");
		proxy = sociable_find_proxy(aTHX_ ref);
		if (proxy == NULL)
            Perl_croak(aTHX_ "Argument to sociable_discard needs to be sociable.");

        (*proxy->mech->destroy)(aTHX_ proxy);
        sociable_dissociate(aTHX_ ref);
        ST(0) = &PL_sv_yes;


void
sociable_default_extent(int extent, int himark, ...)
    PROTOTYPE: $$
    CODE:
    	IV old_extent;
    	IV old_himark;
    	sociable_storage_mech_t *mech;
    	char *mechname;
    	STRLEN mechlen;
    	int i;

    	old_extent = sociable_dflt_extent;
    	old_himark = sociable_dflt_himark;
		EXTEND(SP, 2);
	 	PUSHs(sv_2mortal(newSViv(old_extent)));
	 	PUSHs(sv_2mortal(newSViv(old_himark)));
	 	if (extent < 0) {
			if ((extent != -1) && sociable_warn)
				Perl_warn(aTHX_ "Invalid extent specified.");
			XSRETURN(2);
		}

		if ((!extent) && himark)
			himark = 0;

		if (extent && (himark <= extent)) {
			if ((himark != -1) && sociable_warn)
				Perl_warn(aTHX_ "Invalid himark specified.");
			himark = (I32)(extent * 1.10);
	   		if (himark - extent < 10)
	   			himark = extent + 10;
		}
		if (items > 2) {
		/*
		 *	setting default for a mechanism
		 */
		 	for (i = 2; i < items; i++) {
		 		mechname = SvPV(ST(i), mechlen);
		 		mech = sociable_find_mech(mechname);
		 		if (mech == NULL)
		 			Perl_croak(aTHX_ "Unknown mechanism %s", mechname);
		 		(*mech->default_extent)(aTHX_ extent, himark);
		 	}
		}
		else {
		 	for (i = 0; i < sociable_next_mech; i++)
		 		(*sociable_mechs[ordered_mechs[i]]->default_extent)(aTHX_ extent, himark);
			/*
			 *	how do we set global default ? We need the highest level lock
			 *	available...so we'll just skip this for now...
			 */
			 /*
			ENTER_ELEM_POOL_LOCK;
			sociable_dflt_extent = extent;
			sociable_dflt_himark = himark;
			LEAVE_ELEM_POOL_LOCK;
			*/
		}
		XSRETURN(2);


void
sociable_extent(SV *ref, int extent, int himark)
    PROTOTYPE: \[@%];$$
    CODE:
    	IV old_extent = 0;
    	IV old_himark = 0;
        sociable_proxy_t *proxy;
        int elem_type;

        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to sociable_extent needs to be passed as ref");

        ref = SvRV(ref);

		proxy = sociable_find_proxy(aTHX_ ref);
		if ((proxy == NULL) || (!SOC_IS_INUSE(proxy->elemp)) ||
			((elem_type = (*proxy->mech->get_sv_type)(proxy)) &&
				(elem_type != SVt_PVAV) && (elem_type != SVt_PVHV)))
            Perl_croak(aTHX_ "First argument to sociable_extent must to be sociable array or hash");

	    SOCIABLE_TRACE_1("sociable_extent: returning extents for elemp %s\n", proxy->loc_string);
		old_extent = (*proxy->mech->get_extent)(proxy);
		old_himark = (*proxy->mech->get_himark)(proxy);
		EXTEND(SP, 2);
	 	PUSHs(sv_2mortal(newSViv(old_extent)));
	 	PUSHs(sv_2mortal(newSViv(old_himark)));
	 	if (extent < 0) {
			if ((extent != -1) && sociable_warn)
				Perl_warn(aTHX_ "Invalid extent specified.");
			XSRETURN(2);
		}

		if ((!extent) && himark)
			himark = 0;

		if (extent && (himark <= extent)) {
			if ((himark != -1) && sociable_warn)
				Perl_warn(aTHX_ "Invalid himark specified.");
			himark = (I32)(extent * 1.10);
	   		if (himark - extent < 10)
	   			himark = extent + 10;
		}
		(*proxy->mech->set_extent)(proxy, extent, himark);
		XSRETURN(2);


void
sociable_enable_debug(...)
    CODE:
    	int i = 0;
    	sociable_storage_mech_t *mech;
    	if (items == 0) {
			sociable_debug = TRUE;
    		while (i < sociable_next_mech)
    			(*sociable_mechs[ordered_mechs[i++]]->set_debug)(TRUE);
    		XSRETURN_YES;
    	}
    	for (; i < items; i++) {
    		mech = sociable_find_mech(SvPV_nolen(ST(i)));
    		if (mech)
    			(*mech->set_debug)(TRUE);
    	}
   		XSRETURN_YES;

void
sociable_disable_debug(...)
    CODE:
    	int i = 0;
    	sociable_storage_mech_t *mech;
    	if (items == 0) {
			sociable_debug = FALSE;
    		while (i < sociable_next_mech)
    			(*sociable_mechs[ordered_mechs[i++]]->set_debug)(FALSE);
    		XSRETURN_YES;
    	}
    	for (; i < items; i++) {
    		mech = sociable_find_mech(SvPV_nolen(ST(i)));
    		if (mech)
    			(*mech->set_debug)(FALSE);
    	}
   		XSRETURN_YES;


void
sociable_enable_warnings(...)
    CODE:
    	int i = 0;
    	sociable_storage_mech_t *mech;
    	if (items == 0) {
			sociable_warn = TRUE;
    		while (i < sociable_next_mech)
    			(*sociable_mechs[ordered_mechs[i++]]->set_warn)(TRUE);
    		XSRETURN_YES;
    	}
    	for (; i < items; i++) {
    		mech = sociable_find_mech(SvPV_nolen(ST(i)));
    		if (mech)
    			(*mech->set_warn)(TRUE);
    	}
   		XSRETURN_YES;


void
sociable_disable_warnings(...)
    CODE:
    	int i = 0;
    	sociable_storage_mech_t *mech;
    	if (items == 0) {
			sociable_warn = FALSE;
    		while (i < sociable_next_mech)
    			(*sociable_mechs[ordered_mechs[i++]]->set_warn)(FALSE);
    		XSRETURN_YES;
    	}
    	for (; i < items; i++) {
    		mech = sociable_find_mech(SvPV_nolen(ST(i)));
    		if (mech)
    			(*mech->set_warn)(FALSE);
    	}
   		XSRETURN_YES;


void
sociable_implicit_lockable(...)
    CODE:
    	int i = 0;
    	sociable_storage_mech_t *mech;
    	if (items == 0) {
			sociable_always_lockable = TRUE;
    		while (i < sociable_next_mech)
    			(*sociable_mechs[ordered_mechs[i++]]->set_always_lockable)(TRUE);
    		XSRETURN_YES;
    	}
    	for (; i < items; i++) {
    		mech = sociable_find_mech(SvPV_nolen(ST(i)));
    		if (mech)
    			(*mech->set_always_lockable)(TRUE);
    	}
   		XSRETURN_YES;


void
sociable_explicit_lockable(...)
    CODE:
    	int i = 0;
    	sociable_storage_mech_t *mech;
    	if (items == 0) {
			sociable_always_lockable = FALSE;
    		while (i < sociable_next_mech)
    			(*sociable_mechs[ordered_mechs[i++]]->set_always_lockable)(FALSE);
    		XSRETURN_YES;
    	}
    	for (; i < items; i++) {
    		mech = sociable_find_mech(SvPV_nolen(ST(i)));
    		if (mech)
    			(*mech->set_always_lockable)(FALSE);
    	}
   		XSRETURN_YES;


void
cond_wait(SV *ref_cond, SV *ref_lock = 0)
    PROTOTYPE: \[$@%];\[$@%]
    PREINIT:
        sociable_proxy_t *proxy;
        sociable_proxy_t *cond_proxy;
    CODE:
        if (!SvROK(ref_cond))
            Perl_croak(aTHX_ "Argument to cond_wait needs to be passed as ref");
        ref_cond = SvRV(ref_cond);
        if (SvROK(ref_cond))
            ref_cond = SvRV(ref_cond);
        proxy = sociable_find_proxy(aTHX_ ref_cond);
        if (proxy == NULL)
            Perl_croak(aTHX_ "cond_wait can only be used on sociable values");

		SOCIABLE_TRACE_1("*** attempting cond_wait on %s\n", proxy->loc_string);

		if (ref_lock && (ref_cond != ref_lock)) {
			if (!SvROK(ref_lock))
				Perl_croak(aTHX_ "cond_wait lock needs to be passed as ref");
			ref_lock = SvRV(ref_lock);
			if (SvROK(ref_lock)) ref_lock = SvRV(ref_lock);
			cond_proxy = sociable_find_proxy(aTHX_ ref_lock);
			if (cond_proxy == NULL)
				Perl_croak(aTHX_ "cond_wait lock must be a sociable value");
		}
		else
			cond_proxy = NULL;
		(*proxy->mech->cond_wait)(aTHX_ proxy, cond_proxy);


int
cond_timedwait(SV *ref_cond, double abs, SV *ref_lock = 0)
    PROTOTYPE: \[$@%]$;\[$@%]
    PREINIT:
        sociable_proxy_t *proxy;
        sociable_proxy_t *cond_proxy;
    CODE:
        if (! SvROK(ref_cond))
            Perl_croak(aTHX_ "Argument to cond_timedwait needs to be passed as ref");
        ref_cond = SvRV(ref_cond);
        if (SvROK(ref_cond))
            ref_cond = SvRV(ref_cond);
        proxy = sociable_find_proxy(aTHX_ ref_cond);
        if (proxy == NULL)
            Perl_croak(aTHX_ "cond_timedwait can only be used on sociable values");

        if (ref_lock && (ref_cond != ref_lock)) {
            if (! SvROK(ref_lock))
                Perl_croak(aTHX_ "cond_timedwait lock needs to be passed as ref");
            ref_lock = SvRV(ref_lock);
            if (SvROK(ref_lock)) ref_lock = SvRV(ref_lock);
            cond_proxy = sociable_find_proxy(aTHX_ ref_lock);
            if (cond_proxy == NULL)
                Perl_croak(aTHX_ "cond_timedwait lock must be a sociable value");
        }
        else 
        	cond_proxy = NULL;
		RETVAL = (*proxy->mech->cond_timedwait)(aTHX_ proxy, abs, cond_proxy);
        if (RETVAL == 0)
            XSRETURN_UNDEF;
    OUTPUT:
        RETVAL


void
cond_signal(SV *ref)
    PROTOTYPE: \[$@%]
    PREINIT:
        sociable_proxy_t *proxy;
    CODE:
        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to cond_signal needs to be passed as ref");
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);
        proxy = sociable_find_proxy(aTHX_ ref);
        if (proxy == NULL)
            Perl_croak(aTHX_ "cond_signal can only be used on sociable values");
		(*proxy->mech->cond_signal)(aTHX_ proxy);


void
cond_broadcast(SV *ref)
    PROTOTYPE: \[$@%]
    PREINIT:
        sociable_proxy_t *proxy;
    CODE:
        if (! SvROK(ref))
            Perl_croak(aTHX_ "Argument to cond_broadcast needs to be passed as ref");
        ref = SvRV(ref);
        if (SvROK(ref))
            ref = SvRV(ref);
        proxy = sociable_find_proxy(aTHX_ ref);
        if (proxy == NULL)
            Perl_croak(aTHX_ "cond_broadcast can only be used on sociable values");
		(*proxy->mech->cond_broadcast)(aTHX_ proxy);


void
bless(SV* ref, ...);
    PROTOTYPE: $;$
    PREINIT:
        HV* stash;
        sociable_proxy_t *proxy;
    CODE:
        if (items == 1) {	/* a rebless ??? */
            stash = CopSTASH(PL_curcop);
        }
        else {
            SV* classname = ST(1);
            STRLEN len;
            char *ptr;

            if (classname &&
                ! SvGMAGICAL(classname) &&
                ! SvAMAGIC(classname) &&
                SvROK(classname))
                Perl_croak(aTHX_ "Attempt to bless into a reference");
            ptr = SvPV(classname, len);
            if (ckWARN(WARN_MISC) && len == 0) {
                Perl_warner(aTHX_ packWARN(WARN_MISC),
					"Explicit blessing to '' (assuming package main)");
            }
            stash = gv_stashpvn(ptr, len, TRUE);
        }
        /*
         * we have a package name, save it with
         * our sociable element so we can rebless
         * when its revived in another thread
         */
        SvREFCNT_inc(ref);
        (void)sv_bless(ref, stash);
        ST(0) = sv_2mortal(ref);
        proxy = sociable_find_proxy(aTHX_ ref);
        if (proxy != NULL) {
            dTHX;
            (*proxy->mech->bless)(proxy, HvNAME(stash));
        }


void
sociable_checkmem()
    CODE:
    	int i;
    	for (i = 0; i < sociable_next_mech; i++)
    		(*sociable_mechs[ordered_mechs[i]]->mem_check)();
    	XSRETURN_YES;


void
sociable_mech_installed(SV *name)
	CODE:
		if (sociable_find_mech(SvPV_nolen(name)))
			XSRETURN_YES;
		XSRETURN_UNDEF;


void
_install_mechanism(SV *name, SV *pkgname, SV *constructor)
	CODE:
		sociable_storage_mech_t *mechp;
		char *mechname = SvPV_nolen(name);
		char *mechpkg = SvPV_nolen(pkgname);
		ENTER_MECH_LOCK;
		if (sociable_next_mech >= SOCIABLE_MECHS) {
			LEAVE_MECH_LOCK;
			Perl_croak_nocontext("Maximum number of mechanisms already installed.");
		}
		mechp = sociable_find_mech(mechname);
		if (mechp) {
			LEAVE_MECH_LOCK;
			if (strcmp(mechpkg, mechp->classname)) {
				Perl_croak_nocontext("%s is already installed as a %s mechanism.", 
					mechname, mechp->classname);
			}
			XSRETURN_YES;
		}
		/*
		 *	invoke the constructor callback within the lock
		 */
       	PUSHMARK(SP);
       	call_sv(constructor, G_SCALAR|G_NOARGS|G_EVAL|G_KEEPERR);
		if (SvTRUE(ERRSV)) {
			LEAVE_MECH_LOCK;
			XSRETURN_UNDEF;
		}
		mechp = (sociable_storage_mech_t *)(POPpx);
		/*
		 *	yes this is bad, but its GOWI
		 */
		mechp->mechid = sociable_next_mech;
		sociable_mechs[sociable_next_mech++] = mechp;
		LEAVE_MECH_LOCK;
		/*
		 *	establish defaults
		 */
		(*mechp->set_debug)(sociable_debug);
		(*mechp->set_warn)(sociable_warn);
		(*mechp->set_always_lockable)(sociable_always_lockable);
		/*
		 *	insert into the ordered list (level descending, name ascending)
		 */
		sociable_insert_mech(mechp);
		XSRETURN_YES;


void
sociable_default_mechanism(...)
	CODE:
		sociable_storage_mech_t *mechp;
		char *mechname;
		char *olddflt = default_mech ? default_mech->name : NULL;
		
		if (items > 0) {
			mechname = SvPV_nolen(ST(0));
			ENTER_MECH_LOCK;
			mechp = sociable_find_mech(mechname);
			if (!mechp) {
				LEAVE_MECH_LOCK;
				Perl_croak_nocontext("Mechanism %s not found.", mechname);
			}
			default_mech = mechp;
			LEAVE_MECH_LOCK;
		}
		ST(0) = olddflt ? sv_2mortal(newSVpv(olddflt, 0)) : sv_2mortal(newSVpv("", 0));
		XSRETURN(1);

#
#	returns the list of mechanisms ordered
#	by their mechids
#
void
sociable_get_mechanisms()
	CODE:
		int i;
		EXTEND(SP, SOCIABLE_MECHS);
		ENTER_MECH_LOCK;
		for (i = 0; i < SOCIABLE_MECHS; i++) {
			ST(i) = sociable_mechs[i]
				? newSVpv(sociable_mechs[i]->name, strlen(sociable_mechs[i]->name))
				: &PL_sv_undef;
		}
		LEAVE_MECH_LOCK;
		XSRETURN(SOCIABLE_MECHS);


#
#	need a way to get individual mechanism details
#
void
sociable_get_mechanism(SV *name)
	CODE:
		char *namep = SvPV_nolen(name);
		HV *mechhv = newHV();
		sociable_storage_mech_t *mech = sociable_find_mech(namep);
		SV *errsv;
		if (!mech) {
		 	errsv = get_sv("@", TRUE);
			sv_setpv(errsv, "Mechanism not found.");
			XSRETURN_UNDEF;
		}
		/*
		 *	so what do we return here ?
		 *	Probably a hash of properties,
		 *	with Name, Class, Level, Debug, Warn, AlwaysLock std.
		 */
		hv_store(mechhv, "Name", 4, newSVpv(mech->name, 0), 0);
		hv_store(mechhv, "Class", 5, newSVpv(mech->classname, 0), 0);
		hv_store(mechhv, "Level", 5, newSViv(mech->level), 0);
		hv_store(mechhv, "Debug", 5, newSViv(mech->debug), 0);
		hv_store(mechhv, "Warn", 4, newSViv(mech->warn), 0);
		hv_store(mechhv, "AlwaysLock", 10, newSViv(mech->always_lockable), 0);
		(*mech->get_properties)(mechhv);
		ST(0) = (SV *)mechhv;
		XSRETURN_YES;
		

#
#	get a specific mechanism property
#
void
sociable_get_property(SV *name, SV *property)
	CODE:
		char *namep = SvPV_nolen(name);
		char *prop = SvPV_nolen(property);
		sociable_storage_mech_t *mech = sociable_find_mech(namep);
		SV *errsv;
		if (!mech) {
		 	errsv = get_sv("@", TRUE);
			sv_setpv(errsv, "Mechanism not found.");
			XSRETURN_UNDEF;
		}

		if (!strcmp(prop, "Class")) {
			ST(0) = newSVpv(mech->classname, 0);
		}
		else if (!strcmp(prop, "Level")) {
			ST(0) = newSVpv(
				(mech->level == SOC_PROCESS_PRIVATE_MECH) 
				? "Private"
				: (mech->level == SOC_PROCESS_PRIVATE_MECH)
					? "Shared"
					: "Foreign", 0);
		}
		else if (!strcmp(prop, "Debug")) {
			ST(0) = newSViv(mech->debug);
		}
		else if (!strcmp(prop, "Warn")) {
			ST(0) = newSViv(mech->warn);
		}
		else if (!strcmp(prop, "AlwaysLock")) {
			ST(0) = newSViv(mech->always_lockable);
		}
		else {
			ST(0) = (*mech->get_property)(prop);
		}
		XSRETURN(1);


#
#	set a specific mechanism property
#
void
sociable_set_property(SV *name, SV *property, SV *val)
	CODE:
		char *namep = SvPV_nolen(name);
		char *prop = SvPV_nolen(property);
		sociable_storage_mech_t *mech = sociable_find_mech(namep);
		SV *errsv;
		if (!mech) {
		 	errsv = get_sv("@", TRUE);
			sv_setpv(errsv, "Mechanism not found.");
			XSRETURN_UNDEF;
		}
		ST(0) = (*mech->set_property)(prop, val);
		XSRETURN(1);


#/*
#*	begin an STM transaction:
#*		if not in transaction
#*			set in_stm flag
#*			if commit CV
#*				register it
#*			if restart CV
#*				register it
#*			if abort CV
#*				register it
#*
#*	onCommit, onRestart and onRollback closures are kept in a stack,
#*	with the most recent methods on the top of the stack
#*	(which may be NULL). On commit, restart, or rollback, the
#*	most recent closures are executed first.
#*
#*	NOTE: this provides only limited support for nested transactions,
#*	We do need to be careful about restart and rollback coderefs in
#*	nested xactions
#*	NOTE2: transaction context and logs are not acquired until
#*	first reference to an STM-able variable.
#*/
void
_sociable_add_xact(SV *commit, SV *restart, SV *rollback)
    CODE:
        dTHX;

		dMY_CXT;
		if (MY_CXT.stm_reconciled)
			Perl_croak(aTHX_ "Thread::Sociable STM: New transaction cannot be started in onCommit/onRestart/onRollback closure.");
		/*
		 *	use lazy allocation
		 */
		if (!MY_CXT.stm_xact_count) {
			MY_CXT.stm_karma = 0;
			memzero(&MY_CXT.stm_referents, sizeof(sociable_stm_bucket_t) * SOCIABLE_BUCKETS);
		}

		if (SvOK(commit)) {
			SvREFCNT_inc(commit);
			MY_CXT.stm_commits[MY_CXT.stm_xact_count] = commit;
		}
		if (SvOK(restart)) {
			SvREFCNT_inc(restart);
			MY_CXT.stm_restarts[MY_CXT.stm_xact_count] = restart;
		}
		if (SvOK(rollback)) {
			SvREFCNT_inc(restart);
			MY_CXT.stm_rollbacks[MY_CXT.stm_xact_count] = rollback;
		}
		MY_CXT.stm_xact_count++;
		ST(0) = sv_2mortal(newSViv(MY_CXT.stm_xact_count));


#/*
# *	commit the transaction
# */
void
sociable_commit()
	CODE:
		sociable_stm_log_t *wal;
		sociable_storage_mech_t *mech;
		int i, j, k, acquired;
		STRLEN n_a;

		dTHX;
		dMY_CXT;
		if (!MY_CXT.stm_xact_count)
			Perl_croak(aTHX_ "Thread::Sociable not in a transaction.");

		MY_CXT.stm_xact_count--;
		if (MY_CXT.stm_xact_count != 0) {
			XSRETURN_YES;
		}
		/*
		 *	collect WALs from the referent map
		 */
		stm_collect_WALs(aTHX);
		if (stm_commit_with_lock) {
		/*
		 *	grab global lock
		 */
			for (i = 0, j = ordered_mechs[i]; i < sociable_next_mech; j = ordered_mechs[++i]) {
				if (MY_CXT.stm_ctxts[j].WAL_count)
					(*sociable_mechs[j]->stm_acquire_commit_lock)();
			}
		}
		else {
		/*
		 *	sort WALs into ascending order
		 */
			for (i = 0, j = ordered_mechs[i]; i < sociable_next_mech; j = ordered_mechs[++i]) {
				wal = &MY_CXT.stm_ctxts[j];
				if (wal->WAL_count)
			 		stm_sort_WAL(aTHX_ wal, sociable_mechs[j]->level);
		 	}
		}
		/*
		 *	acquire all spinlocks and check for conflicts.
		 *	On conflict, we'll release the spinlocks we currently hold,
		 *	and then progressively scan for remaining elements for which
		 *	we *may* be owner, then spinlock and release them.
		 *
		 *	Note: we acquire spinlocks from the highest levels first,
		 *	in order to avoid holding spinlocks on lower level
		 *	variables for extended periods while trying to acquire
		 *	the higher level locks
		 */
		acquired = 1;
		for (i = 0, j = ordered_mechs[i]; i < sociable_next_mech; j = ordered_mechs[++i]) {
			wal = &MY_CXT.stm_ctxts[j];
			if (wal->WAL_count && 
				!(*sociable_mechs[j]->stm_acquire_for_commit)(aTHX_ wal)) {
				acquired = 0;
				break;
			}
		}
		if (stm_commit_with_lock) {
			for (i = sociable_next_mech - 1, j = ordered_mechs[i]; i >= 0; j = ordered_mechs[--i]) {
				wal = &MY_CXT.stm_ctxts[j];
				if (wal->WAL_count)
					(*sociable_mechs[j]->stm_release_commit_lock)();
			}
		}
		if (!acquired) {
		/*
		 *	something required a restart:
		 *		release all spinlocks and clear logs
		 *		*in reverse order of acquisition*
		 *		(in order to release lower levels before higher)
		 */
			stm_clear_all_logs(aTHX_ 0);
		 	Perl_croak(aTHX_ SOC_STM_RESTART_MSG);
		}
		/*
		 *	we're current, so commit the changes; but call commit closures first;
		 *	if any commit closure throws error then invoke restart or rollback
		 */
		MY_CXT.stm_reconciled = TRUE;
		if (!stm_call_closures(aTHX_ SOC_STM_COMMIT)) {
			char *msg = SvPV(ERRSV, n_a);
			MY_CXT.stm_reconciled = FALSE;
			stm_clear_all_logs(aTHX_ strncmp(msg, SOC_STM_RESTART_MSG, strlen(SOC_STM_RESTART_MSG)));
		 	Perl_croak(aTHX_ msg);
		}
		for (i = 0, j = ordered_mechs[i]; i < sociable_next_mech; j = ordered_mechs[++i]) {
			wal = &MY_CXT.stm_ctxts[j];
			mech = sociable_mechs[j];
			if (!wal->WAL_count)
				continue;
			for (k = 0; k < wal->WAL_count; k++) {
				if (SOC_STM_WAS_WRITTEN(wal->WAL_proxies[k]->elemp))
					(*mech->stm_merge_element)(aTHX_ &wal->WAL[k], wal->WAL_proxies[k]->elemp);
			}
		}
		/*
		 *	to guarantee serializability, don't release until fully committed
		 */
		stm_clear_all_logs(aTHX_ 1);
		MY_CXT.stm_reconciled = FALSE;
		XSRETURN_YES;


#
#	rollback the transaction
#
void
sociable_rollback()
	CODE:
		dTHX;
		dMY_CXT;
		if (!MY_CXT.stm_xact_count)
			Perl_croak(aTHX_ "Not currently in a transaction.");
		/*
		 *	invoke all rollback closures
		 */
		MY_CXT.stm_reconciled = TRUE;
		stm_call_closures(aTHX_ SOC_STM_ROLLBACK);
		MY_CXT.stm_reconciled = FALSE;
		XSRETURN_UNDEF;


#
#	restart the transaction
#
void
sociable_restart()
	CODE:
		dTHX;
		dMY_CXT;
		if (!MY_CXT.stm_xact_count)
			Perl_croak(aTHX_ "Not currently in a transaction.");
		/*
		 *	invoke all restart closures
		 */
		MY_CXT.stm_reconciled = TRUE;
		if (stm_call_closures(aTHX_ SOC_STM_RESTART)) {
			MY_CXT.stm_reconciled = FALSE;
			XSRETURN_YES;
		}
		/*
		 *	a restart closure invoked rollback
		 */
		stm_call_closures(aTHX_ SOC_STM_ROLLBACK);
		MY_CXT.stm_reconciled = FALSE;
		XSRETURN_UNDEF;


#
#	clone an entire array/hash for optimal STM processing
#	NOTE: not yet implemented
#
void
sociable_stm_clone(SV *sv)
	CODE:
		if (SvTYPE(sv) == SVt_PVAV) {
			XSRETURN_YES;
		}
		else if (SvTYPE(sv) == SVt_PVHV) {
			XSRETURN_YES;
		}
		else if (SvTYPE(sv) == SVt_PV) {
		}

		XSRETURN_YES;


#
#	report/adjust STM transaction timeout
#	NOTE: timeout not yet supported
#
void
sociable_stm_timeout(...)
	CODE:
		/*
		 *	probably should spinlock this...
		 */
		/*
		int old = 0;
		int new_to = 0;
		if (items) {
			if (!SvIOK(ST(0)))
				Perl_croak(aTHX_ "Argument to sociable_stm_timeout() must be positive integer.");
			new_to = SvIV(ST(0));
			SOC_SPIN_LOCK(&stm_ctxt_lock);
			old = stm_timeout;
			stm_timeout = new_to;
			SOC_SPIN_RELEASE(&stm_ctxt_lock);
		}
		else {
			SOC_SPIN_LOCK(&stm_ctxt_lock);
			old = stm_timeout;
			SOC_SPIN_RELEASE(&stm_ctxt_lock);
		}
		ST(0) = sv_2mortal(newSViv(old));
		*/
		XSRETURN_UNDEF;


#
#	set STM commit protocol to locked
#
void
sociable_stm_locked()
	CODE:
		int old = 0;
		int i;
		old = stm_commit_with_lock;
		stm_commit_with_lock = 1;
		for (i = 0; i < sociable_next_mech; i++)
			(*sociable_mechs[ordered_mechs[i]]->commit_with_lock)(1);
		ST(0) = sv_2mortal(newSVpv((old ? "locked" : "lockfree"), 0));
		XSRETURN(1);


#
#	set STM commit protocol to lockfree
#
void
sociable_stm_lockfree()
	CODE:
		int old = 0;
		int i;
		old = stm_commit_with_lock;
		stm_commit_with_lock = 0;
		for (i = 0; i < sociable_next_mech; i++)
			(*sociable_mechs[ordered_mechs[i]]->commit_with_lock)(0);
		ST(0) = sv_2mortal(newSVpv((old ? "locked" : "lockfree"), 0));
		XSRETURN(1);


#
#	return current commit and conflict protocols
#
void
sociable_stm_protocol()
	CODE:
		char *protocol;
		protocol = stm_commit_with_lock ?
			(stm_eager_detect ? "locked eager" : "locked lazy") :
			(stm_eager_detect ? "lockfree eager" : "lockfree lazy");
		ST(0) = sv_2mortal(newSVpv(protocol, strlen(protocol)));


#
#	set STM conflict detector protocol to eager
#
void
sociable_stm_eager()
	CODE:
		char *old;
		int i;
		old = stm_eager_detect ? "eager" : "lazy";
		stm_eager_detect = 1;
		for (i = 0; i < sociable_next_mech; i++)
			(*sociable_mechs[ordered_mechs[i]]->eager_detect)(1);
		ST(0) = sv_2mortal(newSVpv(old, strlen(old)));
		XSRETURN(1);


#
#	set STM conflict detector protocol to lazy
#
void
sociable_stm_lazy()
	CODE:
		char *old;
		int i;
		old = stm_eager_detect ? "eager" : "lazy";
		stm_eager_detect = 0;
		for (i = 0; i < sociable_next_mech; i++)
			(*sociable_mechs[ordered_mechs[i]]->eager_detect)(0);
		ST(0) = sv_2mortal(newSVpv(old, strlen(old)));
		XSRETURN(1);


#
#	return current WAL as shallow list as follows:
#	(
#		current thread's owner value (aka stm context ptr),
#		current level of transactions,
#		current karma,
#		current number of concurrent transactions,
#		{
#			stringified sociable ptr => [
#				$type,
#				$addr,
#				$owner,
#				$seqno_hi,
#				$seqno_lo,
#				$cycleno,
#				$pkgname,
#				[
#					$private_addr,
#					$seqno_hi,
#					$seqno_lo,
#					$cycleno,
#					read/write flag,
#					has_writes flag
#				]
#			]
#		}
#	)
#	
#
void
sociable_stm_get_log()
	CODE:
		HV *stmlog;
		int i, j;
		sociable_stm_bucket_t *bucket;
		sociable_stm_proxy_t *proxy;
		sociable_structure_t *structp;
		sociable_element_t *elemp;
		sociable_header_t *header;
		sociable_storage_mech_t *mech;
		AV *logentry;
		AV *priventry;
 		dMY_CXT;

 		stmlog = newHV();

		for (i = 0; i < SOCIABLE_BUCKETS; i++) {
			bucket = &MY_CXT.stm_referents[i];
			for (j = 0; j < bucket->count; j++) {
				elemp = bucket->bucket[j].priv.elemp;
				logentry = newAV();
				priventry = newAV();
				if (SOC_IS_STRUCTURE(elemp)) {
					structp = (sociable_structure_t *)elemp;
					header = &structp->header.header;
					mech = sociable_mechs[structp->header.mechid];
					if (SvTYPE(header) == SVt_PVAV) {
						av_push(logentry, newSVpv("ARRAY", 0));
					}
					else {
						av_push(logentry, newSVpv("HASH", 0));
					}
					av_push(logentry, newSVpvf("%p", structp));
				}
				else {
					header = &elemp->header.header;
					mech = sociable_mechs[elemp->header.mechid];
					av_push(logentry, newSVpv("SCALAR", 0));
				}
				av_push(logentry, newSVpv(mech->name, 0));
				av_push(logentry, newSViv(header->updcnthi));
				av_push(logentry, newSViv(header->updcntlo));
				av_push(logentry, newSViv(header->cycleno));
				/*
				 * don't know how to do this w/ foreign elements, disable for now
				 *
				if (native->pkg_name) {
					av_push(logentry, newSVpv(native->pkg_name, 0));
				}
				else {
					av_push(logentry, newSVpv("undef", 0));
				}
				*/
				av_push(priventry, newSVpvf("%p", proxy));
				elemp = proxy->priv.elemp;
				av_push(priventry, newSVpv(sociable_mechs[elemp->header.mechid]->name, 0));
				av_push(priventry, newSViv(elemp->header.header.updcnthi));
				av_push(priventry, newSViv(elemp->header.header.updcntlo));
				av_push(priventry, newSViv(elemp->header.header.cycleno));
				if (SOC_STM_WAS_READ(elemp)) {
					if (SOC_STM_WAS_WRITTEN(elemp))
						av_push(priventry, newSVpv("READ|WRITE", 0));
					else
						av_push(priventry, newSVpv("READ", 0));
				}
				else if (SOC_STM_WAS_WRITTEN(elemp))
					av_push(priventry, newSVpv("WRITE", 0));
				else
					av_push(priventry, newSVpv("NONE", 0));
				if (SOC_STM_HAS_WRITES(elemp))
					av_push(priventry, newSVpv("HAS WRITES", 0));
				else
					av_push(priventry, newSVpv("NO WRITES", 0));

				av_push(logentry, newRV_inc((SV *)logentry));
				/*
				 *	again, not sure how to handle for foreign
				 *
				sprintf(socptr, "%p", proxy->header.sociable);
				hv_store(stmlog, socptr, strlen(socptr), (SV *)newRV_inc((SV *)logentry), 0);
				*/
			}
		}
		/*
		 *	NEEDS REWRITE!!!
		j = 0;
		for (i = 0; i < sociable_next_mech; i++) {
			if (sociable_mechs[i] == NULL)
				continue;
			ST(j) = sv_2mortal(newSVpvf("%p", &MY_CXT.stm_ctxts[i]));
			if (MY_CXT.stm_ctxt) {
				ST(j+1) = sv_2mortal(newSViv(MY_CXT.stm_xact_count));
				ST(j+2) = sv_2mortal(newSViv(MY_CXT.stm_karma));
				ST(j+3) = sv_2mortal(newSViv(stm_inuse));
				ST(j+4) = newRV_inc((SV*)stmlog);
			}
			else {
				ST(j+1) = sv_2mortal(newSViv(0));
				ST(j+2) = sv_2mortal(newSViv(0));
				ST(j+3) = sv_2mortal(newSViv(stm_inuse));
				ST(j+4) = &PL_sv_undef;
			}
		}
		*/
		XSRETURN_UNDEF;


BOOT:
{
	int i;
	MY_CXT_INIT;
	MY_CXT.lockchain = NULL;
	MY_CXT.svtmp_count = 0;
#ifdef SOCIABLE_STM
	for (i = 0; i < SOCIABLE_MECHS; i++) {
		MY_CXT.stm_ctxts[i].WAL_count = 0;
		MY_CXT.stm_ctxts[i].WAL = NULL;
		MY_CXT.stm_ctxts[i].WAL_proxies = NULL;
	}
	MY_CXT.stm_xact_avail = SOC_STM_EXTENT;
	CRT_Newz(MY_CXT.stm_commits, SOC_STM_EXTENT, SV *);
	CRT_Newz(MY_CXT.stm_restarts, SOC_STM_EXTENT, SV *);
	CRT_Newz(MY_CXT.stm_rollbacks, SOC_STM_EXTENT, SV *);
	MY_CXT.stm_reconciled = FALSE;
#endif
	sociable_init(aTHX);
}
