#ifndef _CRTHEAP_H

#include "socstoremech.h"

#ifndef HvNAME_get
#  define HvNAME_get(hv)        (0 + ((XPVHV*)SvANY(hv))->xhv_name)
#endif

/*
 * default number of element slots to allocate;
 * can be overridden
 */
#define SOCIABLE_DFLT_ELEMS (1024)

/*
 * default number of structure slots to allocate;
 * can be overridden
 */
#define SOCIABLE_DFLT_STRUCTS (64)

/*
 *	default number of elements for an array
 *	also used as the extent when increasing size;
 *	can be overridden
 */
#define SOCIABLE_DFLT_ARRAYSZ (10)
/*
 *	number of spare slots included when creating/extending an array;
 *	helps avoid frequent re-alloc/copying
 */
#define SOCIABLE_ARRAY_SPARE (10)

/*
 *	number of buckets for hashes...hv.c/.h are nearly
 *	mute on their hash buckets (turns out they start with
 *	8 buckets...and somehow grow ??)
 *	we'll see what 23 buckets does, tho it might
 *	be useful to use bigger values, assuming their hash
 *	function is reasonably random;
 *	can be overridden
 */
#define SOCIABLE_BUCKETS (23)
/*
 * lock management structure; same as threads::shared
 */
typedef struct {
    perl_mutex          mutex;
    PerlInterpreter    *owner;
    I32                 locks;
    perl_cond           cond;
#ifdef SOCIABLE_DEBUG
    char *              file;
    int                 line;
#endif
} crt_recursive_lock_t;

/*
 * User-level locks:
 *	This structure is attached (using ext magic) to any sociable SV that
 *	is used by user-level locking or condition code
 */
typedef struct {
    crt_recursive_lock_t    lock;           /* For user-level locks */
    perl_cond           user_cond;      /* For user-level conditions */
} crt_user_lock_t;

typedef struct {
	sociable_element_t elem;
	crt_user_lock_t lock;
} crt_element_t;

typedef struct {
	sociable_structure_t structure;
	crt_user_lock_t lock;
} crt_structure_t;

/*
 *	convenience macros to enter each global lock
 */
#ifdef SOCIABLE_DEBUG_LOCKS
#define ENTER_ELEM_POOL_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX, &crt_elements->lock, __LINE__);\
    } STMT_END
#else
#define ENTER_ELEM_POOL_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX, &crt_elements->lock);\
    } STMT_END
#endif

#define LEAVE_ELEM_POOL_LOCK \
    STMT_START { \
        crt_recursive_lock_release(aTHX, &crt_elements->lock);\
    } STMT_END

#ifdef SOCIABLE_DEBUG_LOCKS
#define ENTER_STRUCT_POOL_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX, &crt_structures->lock, __LINE__);\
    } STMT_END
#else
#define ENTER_STRUCT_POOL_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX, &crt_structures->lock);\
    } STMT_END
#endif

#define LEAVE_STRUCT_POOL_LOCK \
    STMT_START { \
        crt_recursive_lock_release(aTHX, &crt_structures->lock);\
    } STMT_END

#ifdef SOCIABLE_DEBUG_LOCKS
#define ENTER_TSDQ_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX, &crt_tsdq_global_lock, __LINE__);\
    } STMT_END
#else
#define ENTER_TSDQ_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX, &crt_tsdq_global_lock);\
    } STMT_END
#endif

#define LEAVE_TSDQ_LOCK \
    STMT_START { \
        crt_recursive_lock_release(aTHX, &crt_tsdq_global_lock);\
    } STMT_END

typedef struct crt_queue_t crt_queue_t;

#ifdef SOCIABLE_DEBUG_LOCKS
#define ENTER_MECH_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX_ &sociable_mech_lock, __LINE__);\
    } STMT_END
#else
#define ENTER_MECH_LOCK \
    STMT_START { \
        crt_recursive_lock_acquire(aTHX_ &sociable_mech_lock);\
    } STMT_END
#endif

#define LEAVE_MECH_LOCK \
    STMT_START { \
        crt_recursive_lock_release(aTHX_ &sociable_mech_lock);\
    } STMT_END

/*
 *	element pool structure:
 *	uses a FIFO structure to minimize the possibility
 *	of immediately recycling elements (tho that shouldn't hurt)
 */
typedef struct {
	crt_recursive_lock_t lock;	/* Mutex protecting the sociable map */
	crt_element_t *free_map;	/* head of free elements list */
	crt_element_t *free_tail;	/* tail of free elements */
	I32 total_elems;		/* total elements created (both free & inuse) */
	I32 avail_elems;		/* number of elements in freelist */
} crt_elem_pool_t;

/*
 *	structure pool structure:
 *	uses a FIFO structure to minimize the possibility
 *	of immediately recycling elements (tho that shouldn't hurt)
 */
typedef struct {
	crt_recursive_lock_t lock;	/* Mutex protecting the sociable map */
	int extent;				/* default extent */
	int himark;
	crt_structure_t *free_map;	/* head of free elements list */
	crt_structure_t *free_tail;	/* tail of free elements */
	I32 total_structs;		/* total structures created (both free & inuse) */
	I32 avail_structs;		/* number of structures in freelist */
} crt_struct_pool_t;

typedef struct {
	sociable_storage_mech_t mech;
	crt_elem_pool_t elements;
	crt_struct_pool_t structures;
} crt_storage_mech_t;

extern crt_storage_mech_t crt_mech;
extern bool crt_inited;

int crt_init();
#ifdef SOCIABLE_DEBUG_LOCKS
void crt_recursive_lock_acquire(pTHX_ void *in_lock, int line);
#else
void crt_recursive_lock_acquire(pTHX_ void *in_lock);
#endif

void crt_recursive_lock_release(pTHX_ void *in_lock);

#endif