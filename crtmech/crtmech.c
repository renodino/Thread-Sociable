/*
 * CRTHeap.xs - CRT Heap storage mechanism for Thread::Sociable
 */

#define PERL_NO_GET_CONTEXT
#include "crt_memmgt.h"
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#ifdef HAS_PPPORT_H
#include "ppport.h"
#endif
#include "sociable.h"
#include "socstoremech.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "crtmech.h"

#define CRT_SWAP_FLAG(flag, val) \
	bool old = crt_mech.flag; \
	crt_mech.flag = val; \
	return old

crt_storage_mech_t crt_mech;
static bool crt_inited = false;

static crt_recursive_lock_t crt_commit_lock;
/*
 *	 heap mgmt methods
 */
STATIC int
crt_mem_alloc(sociable_locator_t *loc, int size)
{
	loc->un.elemp = crt_malloc(size);
	return (loc->un.elemp ? 1 : 0);
}

STATIC int
crt_mem_free(sociable_locator_t *loc)
{
	loc->un.elemp = crt_free(loc->un.elemp);
	return 1;
}

STATIC int
crt_mem_realloc(sociable_locator_t *loc, int cursize, int newsize)
{
	loc->un.elemp = crt_realloc(loc->un.elemp, cursize, 1, newsize);
	return (loc->un.elemp ? 1 : 0);
}

STATIC int
crt_mem_strdup(sociable_locator_t *loc, char *str)
{
	loc->un.elemp = crt_strdup(str);
	return (loc->un.elemp ? 1 : 0);
}

STATIC int
crt_mem_set(sociable_locator_t *loc, char fill, int size)
{
	memset(loc->un.elemp, fill, size);
	return size;
}

STATIC int
crt_mem_read(void *to, sociable_locator_t *from, int size)
{
	memcpy(to, from->un.elemp, size);
	return size;
}

STATIC int
crt_mem_write(sociable_locator_t *to, void *from, int size)
{
	memcpy(to->un.elemp, from, size);
	return size;
}

STATIC int
crt_mem_alloc_dbg(sociable_locator_t *loc, int size, int line)
{
	loc->un.elemp = crt_malloc_dbg(size, line);
	return (loc->un.elemp ? 1 : 0);
}

STATIC int
crt_mem_free_dbg(sociable_locator_t *loc, int line)
{
	loc->un.elemp = crt_free_dbg(loc->un.elemp, line);
	return 1;
}

STATIC int
crt_mem_realloc_dbg(sociable_locator_t *loc, int cursize, int newsize, int line)
{
	loc->un.elemp = crt_realloc_dbg(loc->un.elemp, cursize, 1, newsize, line);
	return (loc->un.elemp ? 1 : 0);
}

STATIC int
crt_mem_strdup_dbg(sociable_locator_t *loc, char *str, int line)
{
	loc->un.elemp = crt_strdup_dbg(str, line);
	return (loc->un.elemp ? 1 : 0);
}

STATIC int
crt_mem_check()
{
	inuse_t *p = inuse;
	for (; p; p = p->next)
		printf("\n*** LEFTOVER %p FROM %d\n", p, p->line);
}

/**************************************************************
 **************************************************************
 * ELEMENT POOL METHODS
 **************************************************************
 **************************************************************/
/*
 *	add an extent to the element pool;
 *	extensions are added at head;
 *	assumes lock is already held
 */
STATIC sociable_element_t *
crt_extend_elements(I32 min_elems)
{
	crt_element_t *elems = NULL;
	crt_elem_pool_t *elements = &crt_mech.elements;
	I32 i;

	while (elements->avail_elems < min_elems) {
		CRT_Newz(elems, SOCIABLE_DFLT_ELEMS, crt_element_t);
		if (elems == NULL)
			Perl_croak_nocontext("CRTHeap: Unable to extend sociable elements.");
		for (i = 0; i < SOCIABLE_DFLT_ELEMS - 1; i++) {
			elems[i].elem.NEXT_PTR = &elems[i + 1];
			elems[i].elem.SOCHDR.soc_flags = SOC_STMABLE | SOC_NATIVE;
			elems[i].elem.header.mechid = crt_mech.mech_index;
		}

		elems[i].elem.NEXT_PTR = elements->free_map;
		elements->free_map = elems;
		elements->total_elems += SOCIABLE_DFLT_ELEMS;
		elements->avail_elems += SOCIABLE_DFLT_ELEMS;
		if (elements->free_tail == NULL)
			elements->free_tail = &elems[i];
	}
/*
 *	return the first one
 */
	return (sociable_element_t *)elements->free_map;
}

/*
 *	allocate a single sociable element from pool
 */
STATIC sociable_element_t *
crt_allocate(pTHX)
{
	sociable_element_t *elemp = NULL;
	crt_elem_pool_t *elements = &crt_mech.elements;

	ENTER_ELEM_POOL_LOCK;
	elemp = (elements->free_map == elements->free_tail) ?
		crt_extend_elements(1) :
		(sociable_element_t *)elements->free_map;

	elements->free_map = elemp->NEXT_PTR;
	if (elemp == elements->free_tail)
		elements->free_tail = NULL;
	elements->avail_elems--;
	LEAVE_ELEM_POOL_LOCK;
	elemp->PREV_PTR = elemp->NEXT_PTR = NULL;
/*
 *	force seqno to nonzero
 */
	elemp->SOCHDR.updcntlo = 1;
	elemp->SOCHDR.updcnthi = 0;
	return elemp;
}

/*
 *	fill an extent from the element pool
 */
STATIC void
crt_fill_extent(pTHX_ sociable_struct_extent_t *extent)
{
	sociable_element_t *cluster, *ptr;
	crt_elem_pool_t *elements = &crt_mech.elements;
	I32 i = 0;
/*
 *	allocate a cluster of elements
 */
	ENTER_ELEM_POOL_LOCK;
/*
 *	if not enough in freepool, extend it
 */
	if (elements->avail_elems < extent->extent)
		crt_extend_elements(extent->extent);

	cluster = ptr = (sociable_element_t *)elements->free_map;
	for (i = 0; (i < extent->extent) && (ptr->NEXT_PTR); ptr = ptr->NEXT_PTR, i++);
	elements->free_map = ptr->NEXT_PTR;

	if (elements->free_tail == ptr)
		elements->free_tail = NULL;
	elements->avail_elems -= extent->extent;

	LEAVE_ELEM_POOL_LOCK;
	ptr->NEXT_PTR = NULL;
	if (extent->tail.elemp)
		extent->tail.elemp->NEXT_PTR = cluster;
	else
		extent->pool.elemp = cluster;
	extent->tail = ptr;
	extent->pool_cnt += extent->extent;
}

/*
 *	allocate a cluster of elements from the pool
 */
STATIC sociable_element_t *
crt_alloc_cluster(pTHX_ int count)
{
	sociable_element_t *cluster, *ptr;
	crt_elem_pool_t *elements = &crt_mech.elements;
	I32 i = 0;
/*
 *	allocate a cluster of elements
 */
	ENTER_ELEM_POOL_LOCK;
/*
 *	if not enough in freepool, extend it
 */
	if (elements->avail_elems < count)
		crt_extend_elements(count);

	cluster = ptr = (sociable_element_t *)elements->free_map;
	for (i = 0; i < count; ptr = ptr->NEXT_PTR, i++) {
/*
 *	force seqno to nonzero
 */
		ptr->SOCHDR.updcntlo = 1;
		ptr->SOCHDR.updcnthi = 0;
	};
	elements->free_map = ptr ? ptr->NEXT_PTR : NULL;

	if (elements->free_tail == ptr)
		elements->free_tail = NULL;
	elements->avail_elems -= count;

	LEAVE_ELEM_POOL_LOCK;
	if (ptr)
		ptr->NEXT_PTR = NULL;
	return cluster;
}

STATIC sociable_element_t *
crt_free_cluster(pTHX_ sociable_element_t *cluster, sociable_element_t *tail, I32 count)
{
	crt_elem_pool_t *elements = &crt_mech.elements;
	ENTER_ELEM_POOL_LOCK;
	if (elements->free_tail)
		elements->free_tail->elem.NEXT_PTR = cluster;
	elements->free_tail = (crt_element_t *)tail;
	if (!elements->free_map)
		elements->free_map = (crt_element_t *)cluster;
	elements->avail_elems += count;
	LEAVE_ELEM_POOL_LOCK;
	return NULL;
}

STATIC sociable_element_t *
crt_extent_alloc(pTHX_ sociable_struct_extent_t *extent, I32 count)
{
	sociable_element_t *cluster, *ptr;
	int i;
	if (extent->pool_cnt < count)
		crt_fill_extent(aTHX_ extent);
	cluster = ptr = extent->pool.elemp;
	for (i = 0; i < count; ptr = ptr->NEXT_PTR, i++) {
/*
 *	force seqno to nonzero
 */
		ptr->SOCHDR.updcntlo = 1;
		ptr->SOCHDR.updcnthi = 0;
	};
	extent->pool.elemp = ptr->NEXT_PTR;
	extent->pool_cnt -= count;
	if (extent->tail.elemp == ptr)
		extent->tail.elemp = NULL;
	return cluster;
}

/**************************************************************
 **************************************************************
 * STRUCTURE POOL METHODS
 **************************************************************
 **************************************************************/
/*
 *	add an extent to the pool;
 *	extensions are added at head;
 *	assumes lock is already held
 */
STATIC sociable_structure_t *
crt_extend_structures(I32 min_structs)
{
	sociable_structure_t *elems = NULL;
	sociable_structure_t *elemp = NULL;
	crt_struct_pool_t *structures = &crt_mech.structures;
	I32 i;

	while (structures->avail_structs < min_structs) {
		CRT_Newz(elems, SOCIABLE_DFLT_STRUCTS, crt_structure_t);
		if (elems == NULL)
			Perl_croak_nocontext("Unable to extend sociable structures.");
		for (i = 0; i < SOCIABLE_DFLT_STRUCTS - 1; i++) {
			elems[i].structure.NEXT_PTR = (sociable_element_t *)&elems[i + 1];
			elems[i].structure.SOCHDR.soc_flags = SOC_STMABLE | SOC_NATIVE | SOC_STRUCTURE;
			elems[i].structure.header.mechid = crt_mech.mechid;
		}

		elems[i].NEXT_PTR = (sociable_element_t *)structures->free_map;
		structures->free_map = (crt_structure_t *)elems;
		structures->total_structs += SOCIABLE_DFLT_STRUCTS;
		structures->avail_structs += SOCIABLE_DFLT_STRUCTS;
		if (structures->free_tail == NULL)
			structures->free_tail = &elems[i];
	}
/*
 *	return the first one
 */
	return (sociable_structure_t *)structures->free_map;
}

/*
 *	allocate a single sociable structure from pool
 */
STATIC sociable_structure_t *
crt_struct_allocate(pTHX)
{
	sociable_structure_t *elemp = NULL;
	crt_struct_pool_t *structures = &crt_mech.structures;
	ENTER_STRUCT_POOL_LOCK;
	elemp = (structures->free_map == structures->free_tail) ?
		crt_extend_structures(1) :
		(sociable_structure_t *)structures->free_map;

	structures->free_map = (crt_structure_t *)elemp->NEXT_PTR;
	if (elemp == structures->free_tail)
		structures->free_tail = NULL;
	structures->avail_structs--;
	LEAVE_STRUCT_POOL_LOCK;
	elemp->PREV_PTR = elemp->NEXT_PTR = NULL;
/*
 *	force seqno to nonzero
 */
	elemp->SOCHDR.updcntlo = 1;
	elemp->SOCHDR.updcnthi = 0;

	return (sociable_structure_t *)elemp;
}

/*
 *	methods for maintaining hash/array extents
 */
STATIC void
crt_trim_extent(pTHX_ sociable_struct_extent_t *extent)
{
	sociable_element_t *cluster;
	sociable_element_t *ptr;
	I32 count = extent->extent;
/*
 *	free a cluster of elements
 */
	cluster = ptr = extent->pool.elemp;
	while (count && (ptr != NULL)) {
		ptr = ptr->NEXT_PTR;
		count--;
	}

	if (ptr->NEXT_PTR != NULL) {
		ptr->NEXT_PTR = crt_free_cluster(aTHX_ ptr->NEXT_PTR, extent->tail.elemp, extent->pool_cnt - extent->extent);
		extent->tail.elemp = ptr;
	}
	extent->pool_cnt = extent->extent;
}

STATIC void
crt_get_extent(pTHX_ sociable_proxy_t *proxy)
{
	return proxy->elemp->extent.extent;
}

STATIC void
crt_get_himark(pTHX_ sociable_proxy_t *proxy)
{
	return proxy->elemp->extent.himark;
}

STATIC void
crt_set_extent(pTHX_ sociable_proxy_t *proxy, int extent, int himark)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	int old_himark = elemp->extent.himark;
	elemp->extent.extent = extent;
	elemp->extent.himark = himark;

	if (elemp->extent.pool_cnt > himark)
		crt_trim_extent(aTHX_ &elemp->extent);
}

STATIC void
crt_default_extent(pTHX_ int extent, int himark)
{
	crt_mech.storage.extent = extent;
	crt_mech.storage.himark = himark;
}

/**************************************************************
 **************************************************************
 * LOCK METHODS
 **************************************************************
 **************************************************************/
STATIC int
crt_is_lockable(sociable_proxy_t *elemp)
{
	return (proxy->elemp->SOCHDR.soc_flags & SOC_LOCKABLE);
}

STATIC void
crt_create_lock(sociable_element_t *elemp)
{
	crt_element_t *crtp = (crt_element_t *)elemp;
	ENTER_STRUCT_POOL_LOCK;
	crt_recursive_lock_init(aTHX_ &crtp->lock.lock);
	COND_INIT(&crtp->lock.user_cond);
	LEAVE_STRUCT_POOL_LOCK;
	elemp->SOCHDR.soc_flags |= SOC_LOCKABLE;
}

/*
 *	create and init a lock structure
 */
STATIC void
crt_recursive_lock_init(pTHX_ crt_recursive_lock_t *lock)
{
	// printf("\n*** Initing lock %p!!!\n", lock);
    Zero(lock,1,crt_recursive_lock_t);
    MUTEX_INIT(&lock->mutex);
    COND_INIT(&lock->cond);
}

/*
 *	destroy/remove a lock structure
 */
STATIC void
crt_recursive_lock_destroy(pTHX_ crt_recursive_lock_t *lock, int line)
{
	// printf("\n*** Destroying lock %p at %d!!!\n", lock, line);
    MUTEX_DESTROY(&lock->mutex);
    COND_DESTROY(&lock->cond);
}

/*
 *	release a lock
 */
void
crt_recursive_lock_release(pTHX_ void *in_lock)
{
	crt_recursive_lock_t *lock = (crt_recursive_lock_t *)in_lock;

	SOCIABLE_TRACELOCK(" *** in crt_recursive_lock_release from %d\n", lock->line);

     MUTEX_LOCK(&lock->mutex);
    if (lock->owner != aTHX) {
		SOCIABLE_TRACELOCK(" *** crt_recursive_lock_release: not lock owner from %d!!!\n", lock->line);
    }
    else if (--lock->locks == 0) {
		SOCIABLE_TRACELOCK(" *** crt_recursive_lock_release: we're lock owner from %d\n", lock->line);

        lock->owner = NULL;
        COND_SIGNAL(&lock->cond);
    }
    MUTEX_UNLOCK(&lock->mutex);
	SOCIABLE_TRACELOCK(" *** crt_recursive_lock_release: lock released!!! from %d\n", lock->line);

}

/*
 *	acquire a lock
 */
#ifdef SOCIABLE_DEBUG_LOCKS
void
crt_recursive_lock_acquire(pTHX_ void *in_lock, int line)
#else
void
crt_recursive_lock_acquire(pTHX_ void *in_lock)
#endif
{
	crt_recursive_lock_t *lock = (crt_recursive_lock_t *)in_lock;
    assert(aTHX);
	SOCIABLE_TRACELOCK(" *** in crt_recursive_lock_aquire from %d\n", line);

    MUTEX_LOCK(&lock->mutex);
    if (lock->owner == aTHX) {
		SOCIABLE_TRACELOCK(" *** crt_recursive_lock_aquire: we're owner, increment from %d\n", line);
        lock->locks++;
    }
    else {
		SOCIABLE_TRACELOCK(" *** crt_recursive_lock_aquire: waiting for lock release at %d\n", line);
        while (lock->owner) {
            SOCIABLE_TRACELOCK_4(" %p waiting - owned by %p %s:%d\n",
                      aTHX, lock->owner, lock->file, lock->line);
            COND_WAIT(&lock->cond,&lock->mutex);
        }
        lock->locks = 1;
        lock->owner = aTHX;
#ifdef SOCIABLE_DEBUG_LOCKS
        lock->line  = line;
#endif
    }
    MUTEX_UNLOCK(&lock->mutex);
	SOCIABLE_TRACELOCK(" *** crt_recursive_lock_aquire: released mutex at %d\n", line);
	SOCIABLE_TRACELOCK(" *** crt_recursive_lock_aquire: lock owner is %p\n", lock->owner);
/*
 *	causes recursive_lock_release to be called w/ lock
 *	on exit from the current scope level
 */
	if ((lock != &elements->lock) &&
		(lock != &tsdq_global_lock)) {
    	SAVEDESTRUCTOR_X(crt_recursive_lock_release,lock);
    }
}

STATIC void
crt_cond_wait(pTHX sociable_proxy_t *proxy, sociable_proxy_t *cond_proxy)
{
	crt_element_t *elemp = (crt_element_t *)proxy->elemp;
	crt_element_t *cond_elemp = cond_proxy ? (crt_element_t *)cond_proxy->elemp : NULL;
	perl_cond* user_cond;
	int locks;
	crt_user_lock_t *ul = &elemp->lock;

	if (ul == NULL)
		Perl_croak(aTHX_ "cond_wait can only be used on locked sociable values");

	user_cond = &ul->user_cond;
	if (cond_elemp != NULL) {
		ul = &cond_elemp->lock;
		if (ul == NULL)
			Perl_croak(aTHX_ "cond_wait can only be used on locked sociable values");
	}

	SOCIABLE_TRACE_2("*** cond_wait: lock holder is %p and I am %p\n", ul->lock.owner, aTHX);
	if (ul->lock.owner != aTHX)
		Perl_croak(aTHX_ "A thread must hold the lock on an object before it can cond_wait on it");
	MUTEX_LOCK(&ul->lock.mutex);
	ul->lock.owner = NULL;
	locks = ul->lock.locks;
	ul->lock.locks = 0;

/*
 *	Since we are releasing the lock here, we need to tell other
 *	waiters to try to attemp to acquire it
 */
	SOCIABLE_TRACE_1("*** cond_wait: before COND_SIGNAL for %p\n", &ul->lock.cond);
	COND_SIGNAL(&ul->lock.cond);
	SOCIABLE_TRACE_2("*** cond_wait: after COND_SIGNAL for %p mutex %p\n", &ul->lock.cond, &ul->lock.mutex);
	COND_WAIT(user_cond, &ul->lock.mutex);
	SOCIABLE_TRACE_1("*** cond_wait: after COND_WAIT for %p\n", &ul->lock.mutex);
	while(ul->lock.owner != NULL) {
		/* OK -- must reacquire the lock */
		COND_WAIT(&ul->lock.cond, &ul->lock.mutex);
	}
	SOCIABLE_TRACE_1("*** cond_wait: after COND_WAIT loop %p\n", &ul->lock.mutex);
	ul->lock.owner = aTHX;
	ul->lock.locks = locks;
	MUTEX_UNLOCK(&ul->lock.mutex);
}

#ifdef WIN32

typedef union {
    unsigned __int64	ft_i64;
    FILETIME		ft_val;
} FT_t;

/* Number of 100 nanosecond units from 1/1/1601 to 1/1/1970 */
#ifdef __GNUC__
#define Const64(x)	x##LL
#else
#define Const64(x)	x##i64
#endif
# define EPOCH_BIAS Const64(116444736000000000)

/*
 *	don't give a damn about all the fancy drift and skew adjustments
 *	just give us a reasonable hires timestamp
 */
STATIC double hires_time()
{
    FT_t ft;
    unsigned long tv_sec;
    unsigned long tv_usec;
    double retval;

	GetSystemTimeAsFileTime(&ft.ft_val);

    /* seconds since epoch */
    tv_sec = (long) ( (ft.ft_i64 - EPOCH_BIAS) / Const64(10000000) );

    /* microseconds remaining */
    tv_usec = (long)( (ft.ft_i64 / Const64(10)) % Const64(1000000) );

	retval = tv_sec + (tv_usec / 1000000.);

	return retval;
}

#endif

STATIC int
crt_cond_timedwait(pTHX sociable_proxy_t *proxy, double timeout, sociable_proxy_t *cond_proxy)
{
#if defined(OS2) || defined(NETWARE) || defined(FAKE_THREADS) || defined(I_MACH_CTHREADS)
    Perl_croak_nocontext("cond_timedwait not supported on this platform");
#else
	crt_element_t *elemp = (crt_element_t *)proxy->elemp;
	crt_element_t *cond_elemp = cond_proxy ? (crt_element_t *)cond_proxy->elemp : NULL;
	user_lock_t *ul = (elemp->header.soc_flags & SOC_LOCKABLE) ? &elemp->lock : NULL;
	perl_cond* user_cond;
	perl_mutex *mut;
	int locks;
    int got_it = 0;
	dTHX;

	if (ul == NULL)
		Perl_croak(aTHX_ "cond_timedwait can only be used on locked sociable values");

	user_cond = &ul->user_cond;
	if (cond_elemp != NULL) {
		ul = &cond_elemp->lock;
		if (ul == NULL)
			Perl_croak(aTHX_ "cond_timedwait can only be used on locked sociable values");
	}
	if (ul->lock.owner != aTHX)
		Perl_croak(aTHX_ "You need a lock before you can cond_wait");

	mut = &ul->lock.mutex;
	MUTEX_LOCK(mut);
	ul->lock.owner = NULL;
	locks = ul->lock.locks;
	ul->lock.locks = 0;
/*
 * Since we are releasing the lock here we need to tell other
 * people that is ok to go ahead and use it
 */
	COND_SIGNAL(&ul->lock.cond);
/*
 * 	Do OS-specific condition timed wait
 */
#ifdef WIN32

    timeout -= hires_time();
    timeout = (timeout > 0) ? timeout * 1000. : 0;

    cond->waiters++;
    MUTEX_UNLOCK(mut);
/*
 *	See comments in win32/win32thread.h COND_WAIT vis-a-vis race
 */
    switch (WaitForSingleObject(user_cond->sem, (DWORD)timeout)) {
        case WAIT_OBJECT_0:
        	got_it = 1;
        	break;

        case WAIT_TIMEOUT:
        	break;

        default:
            /* WAIT_FAILED? WAIT_ABANDONED? others? */
            Perl_croak_nocontext("panic: cond_timedwait (%ld)",GetLastError());
            break;
    }
    MUTEX_LOCK(mut);
    user_cond->waiters--;

#else         /* Hope you're I_PTHREAD! */

    struct timespec ts;

    ts.tv_sec = (long)timeout;
    timeout -= (NV)ts.tv_sec;
    ts.tv_nsec = (long)(timeout * 1000000000.0);

    switch (pthread_cond_timedwait(user_cond, mut, &ts)) {
        case 0:
        	got_it = 1;
        	break;

        case ETIMEDOUT:
        	break;

        default:
            Perl_croak_nocontext("panic: cond_timedwait");
            break;
    }

#endif /* WIN32 */
#endif /* OS2 || NETWARE || FAKE_THREADS || I_MACH_CTHREADS */

	while (ul->lock.owner != NULL) {
		/* OK -- must reacquire the lock... */
		COND_WAIT(&ul->lock.cond, mut);
	}
	ul->lock.owner = aTHX;
	ul->lock.locks = locks;
	MUTEX_UNLOCK(mut);

	return got_it;
}

STATIC int
crt_cond_signal(pTHX_ sociable_proxy_t *proxy)
{
	crt_element_t *elemp = (crt_element_t *)proxy->elemp;
	user_lock_t *ul = (elemp->SOCHDR.soc_flags & SOC_LOCKABLE) ? &elemp->lock : NULL;
	if (ckWARN(WARN_THREADS) && ul->lock.owner != aTHX) {
		Perl_warner(aTHX_ packWARN(WARN_THREADS),
			"cond_signal() called on unlocked variable");
	}
	COND_SIGNAL(&ul->user_cond);
	return 1;
}

STATIC int
crt_cond_broadcast(pTHX_ sociable_proxy_t *proxy)
{
	crt_element_t *elemp = (crt_element_t *)proxy->elemp;
	user_lock_t *ul = (elemp->SOCHDR.soc_flags & SOC_LOCKABLE) ? &elemp->lock : NULL;

	if (ckWARN(WARN_THREADS) && ul->lock.owner != aTHX) {
		Perl_warner(aTHX_ packWARN(WARN_THREADS),
			"cond_broadcast() called on unlocked variable");
	}
	COND_BROADCAST(&ul->user_cond);
	return 1;
}

STATIC bool
crt_debug(bool flag)
{
	CRT_SWAP_FLAG(debug, flag);
}

STATIC bool
crt_warn(bool flag)
{
	CRT_SWAP_FLAG(warn, flag);
}

STATIC bool
crt_warn(bool flag)
{
	CRT_SWAP_FLAG(always_lockable, flag);
}

STATIC bool crt_debug_enabled() { return crt_mech.debug; }
STATIC bool crt_warn_enabled() { return crt_mech.warn; }
STATIC bool crt_always_lockable_enabled()  { return crt_mech.always_lockable; }

STATIC void
crt_destroy(pTHX_ sociable_proxy_t *proxy)
{
	return crt_destroy_elem(aTHX_ proxy->elemp);
}
/*
 * Destroy an element.
 * Acquires global lock before freeing.
 *
 *	NOTE: No STM processing here; let caller decide
 *	what to do w/ a stale element inside a transaction
 */
STATIC void
crt_destroy_elem(pTHX_ sociable_element_t *elemp)
{
	crt_struct_extent_t *extent;
	sociable_structure_t *ahelemp = (sociable_structure_t *)elemp;
	crt_struct_pool_t *structures = &crt_mech.structures;
	crt_element_t *crtp = (crt_element_t *)elemp;
	SOCIABLE_TRACE_1("in crt_destroy for elemp %p\n", elemp);

	if (SOC_IS_LOCKABLE(elemp))
		crt_recursive_lock_destroy(aTHX_ &crtp->lock.lock, __LINE__);
	elemp->SOCHDR.soc_flags &= (~SOC_LOCKABLE);

	if ((SvTYPE(&elemp->SOCHDR) == SVt_PVAV) || (SvTYPE(&elemp->SOCHDR) == SVt_PVHV)) {
		if (SvTYPE(&elemp->SOCHDR) == SVt_PVAV)
			crt_array_clear(aTHX_ &ahelemp->un.array);
		else
			crt_hash_clear(aTHX_ &ahelemp->un.hash);
/*
 *	free up its extents as well
 */
		extent = &ahelemp->extent;
		if (extent->pool_cnt) {
			extent->pool.elemp = crt_free_cluster(aTHX_ extent->pool.elemp, extent->tail.elemp, extent->pool_cnt);
			extent->pool_cnt = 0;
		}
		elemp->SOCHDR.cycleno++;
		elemp->SOCHDR.soc_flags &= (~SOC_INUSE);
	    ENTER_STRUCT_POOL_LOCK;
/*
 * requires dealloc of the element
 */
		if (elemp->NEXT_PTR == NULL) {
			if (elemp->PREV_PTR != NULL)
				elemp->PREV_PTR->NEXT_PTR = elemp->NEXT_PTR;
		}
		else {
			elemp->NEXT_PTR->header.prev = elemp->PREV_PTR;
			if (elemp->PREV_PTR != NULL)
				elemp->PREV_PTR->NEXT_PTR = elemp->NEXT_PTRt;
		}
		elemp->PREV_PTR = elemp->NEXT_PTR = NULL;
		structures->free_tail->NEXT_PTR = (sociable_element_t *)ahelemp;
		structures->free_tail = ahelemp;
		if (!structures->free_map)
			structures->free_map = ahelemp;
		structures->avail_structs++;
	    LEAVE_STRUCT_POOL_LOCK;
		SOCIABLE_TRACE("crt_destroy: returning\n");
		return;
	}

	/* if its a hash element */
	elemp->hash_tuple.keyptr.elemp = crt_free(elemp->hash_tuple.keyptr.elemp);
	elemp->hash_tuple.keylen = 0;
	elemp->hash_tuple.hashval = 0;
 	if (SvROK(&elemp->SOCHDR)) {
		/* if its blessed */
		elemp->header.pkg_name.elemp = crt_free(elemp->header.pkg_name.elemp);
/*
 *	NOTE we don't free the referent!
 */
		memzero(&elemp->un.ref, sizeof(sociable_ref_t));
	}
	else {
		if (elemp->un.scalar.bufsz)
			elemp->un.scalar.bufptr.elemp = crt_free(elemp->un.scalar.bufptr.elemp);

		elemp->un.scalar.buflen = elemp->un.scalar.bufsz = 0;
	}
	elemp->SOCHDR.cycleno++;
	elemp->SOCHDR.soc_flags &= (~SOC_INUSE);

    ENTER_ELEM_POOL_LOCK;
   	/*
   	 * requires dealloc of the element
   	 */
	if (elemp->NEXT_PTR == NULL) {
		if (elemp->PREV_PTR != NULL)
			elemp->PREV_PTR->NEXT_PTR = elemp->NEXT_PTR;
	}
	else {
		elemp->NEXT_PTR->PREV_PTR = elemp->PREV_PTR;
		if (elemp->PREV_PTR != NULL)
			elemp->PREV_PTR->NEXT_PTR = elemp->NEXT_PTR;
	}
	elemp->PREV_PTR = elemp->NEXT_PTR = NULL;
	elements->free_tail->NEXT_PTR = elemp;
	elements->free_tail = elemp;
	if (!elements->free_map)
		elements->free_map = elemp;
	elements->avail_elems++;
    LEAVE_ELEM_POOL_LOCK;
	SOCIABLE_TRACE("crt_destroy: returning\n");
}

/*
 * Destroy an element, but leave in place.
 * Used to optimize clearing hash/array elements
 *
 *	NOTE: No STM processing here; let caller decide
 *	what to do w/ a stale element inside a transaction
 */
STATIC void
crt_destroy_insitu(pTHX_ sociable_element_t *elemp)
{
	crt_element_t *crtelemp = (crt_element_t *)elemp;
	SOCIABLE_TRACE_1("in crt_destroy_insitu for elemp %p\n", elemp);

	if (SOC_IS_LOCKABLE(elemp))
		crt_recursive_lock_destroy(aTHX_ &crtelemp->lock.lock, __LINE__);
	elemp->SOCHDR.soc_flags &= (~SOC_LOCKABLE);

	/* if its a hash element */
	elemp->hash_tuple.keyptr.elemp = crt_free(elemp->hash_tuple.keyptr.elemp);
	elemp->hash_tuple.keylen = 0;
	elemp->hash_tuple.hashval = 0;

 	if (SvROK(&elemp->SOCHDR)) {
		elemp->header.pkg_name.elemp = crt_free(elemp->header.pkg_name.elemp);
/*
 *	NOTE we don't free the referent!
 */
		memzero(&elemp->un.ref, sizeof(sociable.ref.t));
	}
	else if ((SvTYPE(&elemp->SOCHDR) == SVt_PVHV) || (SvTYPE(&elemp->SOCHDR) == SVt_PVAV))
		Perl_croak(aTHX_ "crt_destroy_insitu: unexpected element type!");
	else {
		if (elemp->un.scalar.bufsz)
			elemp->un.scalar.bufptr.elemp = crt_free(elemp->un.scalar.bufptr.elemp);

		elemp->un.scalar.buflen = elemp->un.scalar.bufsz = 0;
	}
	elemp->SOCHDR.cycleno++;
	elemp->SOCHDR.soc_flags &= (~SOC_INUSE);
	SOCIABLE_TRACE("crt_destroy_insitu: returning\n");
}

STATIC void
crt_set_elem_proxy(sociable_proxy_t *proxy, sociable_element_t *elemp)
{
	memcpy(&proxy->header, elemp->SOCHDR, sizeof(sociable_header_t));
	proxy->sociable.mechid = crt_mech.smech.mechid;
	proxy->sociable.level = crt_mech.smech.level;
	proxy->sociable.un.elemp = elemp;
	return proxy;
}

STATIC sociable_proxy_t *
crt_make_elem_proxy(sociable_element_t *elemp)
{
	sociable_proxy_t *proxy;
	CRT_Newz(proxy, 1, sociable_proxy_t);
	return crt_set_elem_proxy(proxy, elemp);
}

STATIC sociable_proxy_t *
crt_make_struct_proxy(sociable_structure_t *structp)
{
	sociable_proxy_t *proxy;
	CRT_Newz(proxy, 1, sociable_proxy_t);
	return crt_set_elem_proxy(proxy, (sociable_element_t *)structp);
}

STATIC sociable_proxy_t *
crt_create_proxy(sociable_locator_t *loc)
{
	sociable_proxy_t *proxy;
	CRT_Newz(proxy, 1, sociable_proxy_t);
	memcpy(&proxy->header, loc->un.elemp->SOCHDR, sizeof(sociable_header_t));
	memcpy(&proxy->sociable, loc, sizeof(sociable_locator_t));
	proxy->mech = &crt_mech;
	proxy->level = SOC_PROCESS_PRIVATE;
	sprintf(&proxy->loc_string, "P:%p", loc->un.elemp);
	return proxy;
}

/***********************************************************
 ***********************************************************
 * HASH MANIPULATION METHODS
 ***********************************************************
 ***********************************************************/
/*
 *	create a hash in an element
 */
STATIC sociable_proxy_t *
crt_new_hash(pTHX_ int svflags, bool withlock)
{
	sociable_structure_t *hash = crt_struct_allocate(aTHX);

	SOCIABLE_TRACE("in crt_new_hash\n");
	if (!hash)
		return NULL;

	SOCIABLE_TRACE_1("crt_new_hash: got an element %p\n", hash);
	hash->SOCHDR.sv_flags = sv->sv_flags;
	hash->SOCHDR.soc_flags |= SOC_INUSE;
	SOCIABLE_TRACE("crt_new_hash: create sociable hash\n");
	CRT_Newz(hash->un.hash.buckets.elemp, crt_hash_buckets, sociable_element_t*);
	hash->un.hash.bucket_count = crt_hash_buckets;
	hash->extent.extent = crt_mech.structures.extent;
	hash->extent.himark = crt_mech.structures.himark;
	hash->extent.pool.elemp = NULL;
	hash->extent.pool_cnt = 0;

	if (withlock)
		crt_create_lock((sociable_element_t *)hash);

	SOCIABLE_TRACE("crt_new_hash: returning\n");
	return crt_make_struct_proxy(hash);
}

/*
 *	add an (empty) element to a hash; app is responsible for locking
 */
STATIC void
crt_new_hash_element(pTHX_ sociable_hash_t *hash, char *key, I32 len, sociable_struct_extent_t *extent)
{
	sociable_element_t *h = NULL;
	UV hashval = 0;
	I32 bucket;
	sociable_addr_t *buckets = hash->buckets.elemp;

	SOCIABLE_TRACE_1("in crt_new_hash_element for key %s\n", key);
	if (extent->extent) {
		if (extent->pool_cnt == 0) {
			SOCIABLE_TRACE("crt_new_hash_element: extending extent\n");
			crt_fill_extent(aTHX_ extent);
		}
		h = extent->pool.elemp;
		extent->pool.elemp = h->NEXT_PTR;
		h->NEXT_PTR = NULL;
		extent->pool_cnt--;
	}
	else
		h = crt_allocate(aTHX);

	SOCIABLE_TRACE_1("crt_new_hash_element: got element %p\n", h);

	PERL_HASH(hashval, key, len);
	bucket = hashval % hash->bucket_count;
	h->hash_tuple.hashval = hashval;
/*
 *	OPTIMIZE: recycle old keyptr buffers if possible
 */
	CRT_Dup(h->hash_tuple.keyptr.elemp, len + 1, char, key, len);
	h->hash_tuple.keylen = len;
	if (buckets[bucket].elemp)
		buckets[bucket].elemp->PREV_PTR = h;
	h->NEXT_PTR = buckets[bucket].elemp;
	h->PREV_PTR = NULL;
	buckets[bucket].elemp = h;
	hash->keycount++;
	SOCIABLE_TRACE_1("crt_new_hash_element: returning with key %s\n", h->hash_tuple.keyptr.elemp);
}

/*
 *	remove an element from a hash; app is responsible for locking
 */
STATIC I32
crt_remove_hash_element(pTHX_ sociable_proxy_t *proxy, char *key, I32 len)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	sociable_hash_t *hash = &elemp->un.hash;
	sociable_struct_extent_t *extent = &elemp->extent;
	UV hashval = 0;
	I32 bucket;
	sociable_addr_t *buckets = hash->buckets.elemp;
	sociable_element_t *h = NULL;
	sociable_element_t *trail = NULL;
	sociable_element_t *free = NULL;

	SOCIABLE_TRACE("in crt_remove_hash_element\n");

	PERL_HASH(hashval, key, len);
	bucket = hashval % hash->bucket_count;
	h = buckets[bucket].elemp;
	while ((h != NULL) &&
		((hashval != h->hash_tuple.hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr.elemp, len)))
		h = h->NEXT_PTR;

	if (!h)
		return (0);

	if (h->NEXT_PTR) {
		h->PREV_PTR->NEXT_PTR = h->NEXT_PTR;
		if (h->NEXT_PTR)
			h->NEXT_PTR->PREV_PTR = h->PREV_PTR;
	}
	else {
		buckets[bucket].elemp = h->NEXT_PTR;
		if (h->NEXT_PTR)
			h->NEXT_PTR->PREV_PTR = NULL;
	}
	hash->keycount--;
	crt_destroy_insitu(aTHX_ h);
/*
 *	Save in extent (if any)
 */
	if (extent->extent) {
		h->NEXT_PTR = extent->pool;
		extent->pool.elemp = h;
		extent->pool_cnt++;
		if (extent->pool_cnt > extent->himark)
			crt_trim_extent(aTHX_ extent);
	}
	return (1);
}

/*
 *	lookup value for the specified key; returns the element for the value
 */
STATIC int
crt_hash_lookup(pTHX_ sociable_proxy_t *proxy, char *key, I32 len, I32 flags, sociable_proxy_t *valproxy)
{
	sociable_element_t *elemp = proxy->elemp;
	UV hashval = 0;
	I32 bucket = 0;
	sociable_element_t *h = NULL;
	sociable_hash_t *hash = &elemp->un.hash;
	sociable_struct_extent_t *extent = &elemp->extent;
	sociable_addr_t *buckets = hash->buckets.elemp;

	SOCIABLE_TRACE_1("in crt_hash_lookup: key %s\n", key);
	PERL_HASH(hashval, key, len);
	bucket = hashval %  hash->bucket_count;
	h = buckets[bucket].elemp;
	/* printf("\n***hashval %u hashbucket %u is %p\n", hashval, bucket, h); */

	while ((h != NULL) &&
		((h->hash_tuple.hashval != hashval) ||
		(len != h->hash_tuple.keylen) ||
		memcmp(key, h->hash_tuple.keyptr.elemp, len)))
		h = h->header.next;
	/*
	 *	create element if not found and flags set
	 */
	if (h == NULL) {
		if (!flags)
			return 0;

		SOCIABLE_TRACE_1("crt_hash_lookup: key %s not found; create new entry\n", key);
		crt_new_hash_element(aTHX_ hash, key, len, extent);
		h = buckets[bucket].elemp;
	}
	SOCIABLE_TRACE_1("crt_hash_lookup: returning %p\n", h);
	if (valproxy)
		crt_set_elem_proxy(valproxy, h);
	return 1;
}
/*
 * clear the hash, including destroying all contents
 */
STATIC void
crt_hash_clear(pTHX_ sociable_proxy_t *proxy)
{
/*
 *	note that we assume this should destroy
 *  all the contents...ie, its an implicit
 *  discard() on all the contents...so any
 *	refs to hash elements will hereafter return undef
 *	note that we keep any extent as is, but we don't
 *	return the freed elements to the extent (for performance)
 */
 	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
 	sociable_hash_t *hash = &elemp->un.hash;
	sociable_element_t *h = NULL;
	sociable_element_t *trail = NULL;
	sociable_element_t *tail = NULL;
	sociable_element_t *freelist = NULL;
	sociable_addr_t *buckets = hash->buckets.elemp;
	I32 count = 0;
 	I32 i;

	SOCIABLE_TRACE("in crt_hash_clear\n");
	for (i = 0; i <  hash->bucket_count; i++) {
		if (buckets[i].elemp == NULL)
			continue;
		h = buckets[i].elemp;
		while (h != NULL) {
			crt_destroy_insitu(aTHX_ h);
			trail = h;
			count++;
			h = h->NEXT_PTR;
		}
		trail->NEXT_PTR = freelist;
		if (!freelist)
			tail = trail;
		freelist = buckets[i].elemp;
		buckets[i].elemp = NULL;
	}
	hash->keycount = 0;
	if (freelist)
		crt_free_cluster(aTHX_ freelist, tail, count);
	SOCIABLE_TRACE("crt_hash_clear: returning\n");
}
/***********************************************************
 ***********************************************************
 * ARRAY MANIPULATION METHODS
 ***********************************************************
 ***********************************************************/
/*
 * create a new array in specified element
 */
STATIC sociable_proxy_t *
crt_new_array(pTHX_ int svflags, bool withlock)
{
	sociable_structure_t *array = crt_struct_allocate(aTHX);
	sociable_addr_t *ary = NULL;
	if (!array)
		return NULL;

	SOCIABLE_TRACE_1("crt_new_array: got an element %p\n", array);
	array->SOCHDR.sv_flags = sv->sv_flags;
	array->SOCHDR.soc_flags |= SOC_INUSE;
	CRT_Newz(ary, SOCIABLE_DFLT_ARRAYSZ, sociable_addr_t);
	/* printf("\n*** got array space %p\n", ary); */
	array->ary_ptr.elemp = ary;
	array->ary_size = SOCIABLE_DFLT_ARRAYSZ;
	array->ary_head = 0;
	array->extent.extent = sociable_dflt_extent;
	array->extent.himark = sociable_dflt_himark;
	array->extent.pool.elemp = NULL;
	array->extent.pool_cnt = 0;

	if (withlock)
		crt_create_lock((sociable_element_t *)array);

	SOCIABLE_TRACE("leaving crt_new_array\n");
	return crt_make_struct_proxy(array);
}

/*
 * extend an array to permit pushing (or just adding new elements)
 */
STATIC sociable_addr_t *
crt_extend_array(pTHX_ crt_array_t *array, I32 needed, crt_struct_extent_t *extent)
{
	sociable_addr_t *newary;
	sociable_addr_t *oldary = array->aryptr.elemp;
	sociable_element_t *cluster, *ptr;
	int i, count;

	SOCIABLE_TRACE_2("in crt_extend_array: need %d have %d\n", needed, array->arysz);
	if (array->arysz < needed) {
/*
 *	always alloc more than needed
 */
		SOCIABLE_TRACE_2("crt_extend_array: extending from %d to %d\n", array->arysz, needed);
		array->arysz = needed + SOCIABLE_ARRAY_SPARE;
		CRT_Newz(newary, array->arysz, sociable_addr_t);
		SOCIABLE_TRACE_3("crt_extend_array: copy old array %p to new array %p for %d elems\n", oldary, newary, array->aryhead);
		if ((array->aryhead > 0) && (oldary != NULL)) {
			memcpy(newary, oldary, (sizeof(sociable_addr_t) * array->aryhead));
			SOCIABLE_TRACE_1("crt_extend_array: freeing old array %p\n", oldary);
			crt_free(oldary);
		}
		else
			array->aryhead = 0;
/*
 *	if extent provided, fill out with undefs
 */
		if (extent) {
/*
 *	in order to support STM, we must have a placeholder element for undef;
 *	we'll allocate from the extent
 */
	 		count = needed - array->aryhead;
			cluster = (extent->extent && (count <= extent->extent)) ?
				crt_extent_alloc(aTHX_ extent, count) :
				crt_alloc_cluster(aTHX_ count);
			ptr = cluster;
			for (i = array->aryhead; i < needed; i++) {
				ptr->SOCHDR.sv_flags = 0;	/* make it undef */
				newary[i].elemp = ptr;
				ptr = ptr->NEXT_PTR;
			}
		}
		array->aryptr.elemp = newary;
		array->aryhead += needed;
	}
	SOCIABLE_TRACE("crt_extend_array: returning\n");
/*
 *	in case of replacements
 */
 /*
	ary = elemp->ary_ptr;
	for (i = 1; i < items; i++)
		ary[elemp->ary_head++] = crt_new_sociable(aTHX_ ST(i), &elemp->extent);
*/
    return array->aryptr.elemp;
}

STATIC int
crt_array_get_size(pTHX_ sociable_proxy_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	return elemp->un.array.arysz;
}

/*
 *	fetch the specified element from an array;
 *	if flags set, extend the array if needed to prepare
 *	for storage
 */
STATIC int
crt_array_fetch(pTHX_ sociable_proxy_t *proxy, I32 key, I32 flags, sociable_proxy_t *valproxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	sociable_element_t *valelemp;
	sociable_array_t *array = &elemp->un.array;
	sociable_struct_extent_t *extent = &elemp->extent;
	sociable_element_t *telemp = NULL;
	sociable_addr_t *ary = array->aryptr.elemp;
/*
 * !!!Note: need destroy on remove for dequeueing
 */
	SOCIABLE_TRACE_2("in crt_array_fetch for key %d flags 0x%04X\n", key, flags);
	if (flags && ((key >= array->aryhead) || (ary[key].elemp == NULL)) ) {
		if (key >= array->arysz)
			ary = crt_extend_array(aTHX_ array, key, extent);

		array->aryhead = key + 1;
	}
	else if (key < array->aryhead)
		telemp = ary[key].elemp;

	SOCIABLE_TRACE_1("crt_array_fetch: returning telemp %p\n", telemp);
	crt_set_elem_proxy(valproxy, (sociable_element_t *)telemp);
	return 1;
}
/*
 *	delete the specified element from an array;
 */
STATIC int
crt_array_delete(pTHX_ sociable_proxy_t *proxy, I32 key)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	sociable_addr_t *ary = array->aryptr.elemp;
	sociable_array_t *array = &elemp->un.array;
/*
 * !!!Note: maybe shrink array size ???
 */
	SOCIABLE_TRACE_1("in crt_array_delete for element %d\n", key);
	if (key < array->aryhead) {
		sociable_element_t *elemp = ary[key].elemp;
		SOCIABLE_TRACE_1("crt_array_delete: got element %p\n", elemp);
		if (elemp != NULL) {
			crt_destroy_elem(aTHX_ elemp);
			SOCIABLE_TRACE("crt_array_delete: destroyed\n");
			ary[key].elemp = NULL;
			SOCIABLE_TRACE("crt_array_delete: slot cleared\n");
			if (key == (array->aryhead - 1))
				array->aryhead--;
		}
	}
	SOCIABLE_TRACE("crt_array_delete: returning\n");
	return 1;
}
/*
 *	clear an array, and destroy all the contents
 *	do we really want to destroy contents ? what if
 *	they've been ref'd ?
 */
STATIC int
crt_array_clear(pTHX_ sociable_proxy_t *proxy)
{
/*
 *	note that we assume this should destroy
 *  all the contents...ie, its an implicit
 *  discard() on all the contents...so any
 *	refs to array elements will hereafter return undef
 *	note that we keep any extent as is, but we don't
 *	return the freed elements to the extent (for performance)
 */
 	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
 	sociable_array_t *array = &elemp->un.array;
 	sociable_addr_t *ary = array->aryptr.elemp;
 	sociable_element_t *freelist = NULL;
 	sociable_element_t *tail = NULL;
 	I32 count = 0;
 	I32 i;

	SOCIABLE_TRACE("in crt_array_clear\n");

	for (i = 0; i < array->aryhead; i++) {
		if (ary[i].elemp != NULL) {
			SOCIABLE_TRACE_2("crt_array_clear: destroying element %d telemp %p\n", i, ary[i].elemp);
			crt_destroy_insitu(aTHX_ ary[i].elemp);
			ary[i].elemp->NEXT_PTR = freelist;
			if (!freelist)
				tail = ary[i].elemp;
			freelist = ary[i].elemp;
			count++;
		}
	}
	array->arysz = 0;
	array->aryhead = 0;
	array->aryptr.elemp = crt_free(array->aryptr.elemp);
	if (freelist)
		crt_free_cluster(aTHX_ freelist, tail, count);
	SOCIABLE_TRACE("crt_array_clear: returning\n");
	return 1;
}

/*
 *	Free the list of elements back to the pool
 */
STATIC void
crt_discard_list(pTHX_ sociable_element_t **telemp, int len, sociable_struct_extent_t *extent)
{
	sociable_element_t *freelist = NULL;
	sociable_element_t *tail = NULL;
	I32 count = 0;
	int i;
 	for (i = 0; i < len; i++) {
 		if (telemp[i]) {
 			telemp[i]->NEXT_PTR = freelist;
 			if (!freelist)
 				tail = telemp[i];
			freelist = telemp[i];
		}
	}
	if (extent->extent) {
		if (extent->pool)
			tail->NEXT_PTR = extent->pool;
		if (!extent->tail)
			extent->tail = tail;
		extent->pool.elemp = freelist;
		extent->pool_cnt += count;
		if (extent->pool_cnt > extent->himark)
			crt_trim_extent(aTHX_ extent);
	}
	else
		crt_free_cluster(aTHX_ freelist, tail, count);
}
/*
 *	Remove and return the specified element(s) from an array
 *	If a replacement count is provided, opens the specified
 *	number of slots at the specified offset, and fills from any replacement list.
 *  If discard is specified, the removed values are freed, rather
 *	than returned.
 *	Assumes lock is held
 */
STATIC SV *
crt_splice(pTHX_ sociable_proxy_t *proxy, I32 items, I32 key, I32 len, I32 replace, SV **replacements)
{
	sociable_structure_t *priv_structp = (sociable_structure_t *)proxy->elemp;
	AV *av;
	SV **svs;
	SV *retsv, *errsv;
	sociable_element_t **telemp;
	sociable_element_t *selemp;
	sociable_addr_t *ary;
	int i,j;
	int inserts = replace;
	char loc_string[24];

	if (!priv_structp)
		return NULL;

	ary = priv_structp->ary_ptr.elemp;

	SOCIABLE_TRACE_5("in crt_splice for array %p key %d len %d replace %d replacements %p\n",
		&priv_structp->un.array, key, len, replace, replacements);
/*
 *	if items == -1, then its for a PUSH/UNSHIFT: splice onto end
 */
 	if (items == -1) {
 		key = (key < 0) ? priv_structp->ary_head : 0;
		retsv = &PL_sv_undef;
	}
 	else {
/*
 *	can't validate params until we've logged/acquired
 */
		if (key < 0)
			key = priv_structp->ary_head + key;
		else if (key >= priv_structp->ary_head) {
			Perl_warn(aTHX_ "Offset exceeds array length");
			key = priv_structp->ary_head;
		}
/*
 *	SPLICE and no LENGTH param was specified, use remainder
 */
		if (items <= 2)
			len = priv_structp->ary_head - key;
/*
 *	if LENGTH is negative,
 */
		if (len < 0)
			len = (priv_structp->ary_head + len) - key;
/*
 *	if LENGTH exceeds current array length, then adjust
 */
		if (len > priv_structp->ary_head - key)
			len = priv_structp->ary_head - key;
/*
 *	if OFFSET exceeds current length, throw error
 */
		if (key >= priv_structp->ary_head) {
 			errsv = get_sv("@", TRUE);
 			sv_setpv(errsv, "Thread::Sociable::SPLICE: OFFSET exceeds array length.");
			return NULL;
		}
/*
 *	if OFFSET + LENGTH exceeds current length, adjust
 */
		if (key + len > priv_structp->ary_head)
			len = priv_structp->ary_head - key;

	 	if (len > 0) {
/*
 *	recover elements and convert to SV or AV
 */
	 		if (GIMME_V == G_ARRAY) {
	 			CRT_Newz(svs, len, SV *);
				for (i = 0; i < len; i++) {
#ifdef SOCIABLE_DEBUG
					sprintf(loc_string, "P:%p", ary[key + i].elemp);
#endif
					svs[i] = native_sv_from_elem(aTHX_ newSV(0), ary[key + i].elemp, loc_string);
				}

				av = av_make(len, svs);
				crt_free(svs);
			}
			else {
#ifdef SOCIABLE_DEBUG
				sprintf(loc_string, "P:%p", ary[key + len-1].elemp);
#endif
				retsv = native_sv_from_elem(aTHX_ ary[key + len-1].elemp, loc_string);
			}
		}
		else {
/*
 *	return empty list or undef if no removal
 */
 			if (GIMME_V == G_ARRAY)
				av = newAV();
			else
				retsv = &PL_sv_undef;
		}
	}	/* end if PUSH else SPLICE */

	if (replace > len) {
/*
 *	more new elems than old:
 *	for existing elems:
 *		mark elems as written and copy replacements in place
 *	for remaining replacements:
 *		create new private elems and copy
 */
		replace -= len;
		ary = crt_extend_array(aTHX_ &priv_structp->un.array, (priv_structp->ary_head + replace), &priv_structp->extent);
		for (i = priv_structp->ary_head - 1, j = i + replace; i >= key + len; i--) {
			/* swap empty new element with existing element */
			selemp = ary[j].elemp
			ary[j--].elemp = ary[i].elemp;
			ary[i].elemp = selemp;
		}
		priv_structp->ary_head += replace;
		for (i = 0; i < inserts; i++)
			native_sv_save(aTHX_ replacements[i], ary[i + key].elemp);
	}
	else { /* (replace <= len), including replace == 0 or len == 0 */
/*
 *	more old elems than new:
 *	for replacements:
 *		mark old as written and copy
 *	for remainder:
 *		move to delete list
 *
 *	copy replacements into existing slots
 */
		for (i = 0; i < replace; i++)
			native_sv_save(aTHX_ replacements[i], ary[i + key].elemp);
/*
 *	delete remainder
 */
 		if (len > i) {
			CRT_Newz(telemp, len - i, sociable_element_t*);
			for (j = 0; i < len; telemp[j++] = ary[i + key].elemp, ary[i++].elemp = NULL);
			crt_discard_list(aTHX_ telemp, len, &priv_structp->extent);
			telemp = crt_free(telemp);
		}
/*
 *	pull in rest of array (maybe shrink storage in future ?)
 */
		len -= replace;
		if (len) {
			for (i = key + replace; (i + len) < priv_structp->ary_head; i++)
				ary[i].elemp = ary[i + len].elemp;
			for (i = (priv_structp->ary_head - len - 1); i < priv_structp->ary_head; ary[i++].elemp = NULL);
			priv_structp->ary_head -= len;
		}
	}
	SOCIABLE_TRACE_1("crt_array_splice: returning %p\n", (av ? (SV *)av : retsv));
	return (av ? ((SV *)av) : retsv);
}

/***********************************************************
 ***********************************************************
 * SCALAR ELEMENT METHODS
 ***********************************************************
 ***********************************************************/

STATIC sociable_proxy_t *
crt_new_scalar(pTHX_ SV *sv, with_lock)
{
	sociable_element_t *elemp = crt_allocate(aTHX);
	char *loc_string = crt_malloc(24);

	if (!elemp)
		return NULL;

	SOCIABLE_TRACE_1("crt_new_scalar: got an element %p\n", elemp);
	elemp->SOCHDR.sv_flags = sv->sv_flags;
	elemp->SOCHDR.soc_flags |= SOC_INUSE;
/*
	if (elemp->un.scalar.bufsz)
		elemp->un.scalar.bufptr = crt_free(elemp->un.scalar.bufptr);
*/
	elemp->un.scalar.bufptr.elemp = NULL;
	elemp->un.scalar.buflen = elemp->un.scalar.bufsz = 0;
/*
 *	copy any existing value into the element
 */
	native_sv_save(aTHX_ sv, elemp);

	if (withlock)
		crt_create_lock(elemp);

    return crt_make_elem_proxy(elemp);
}

STATIC int
crt_push(pTHX_ sociable_proxy_t *proxy, int items, SV **svary)
{
	crt_splice(aTHX_ proxy, -1, -1, 0, items, svary);
}

STATIC int
crt_unshift(pTHX_ sociable_proxy_t *proxy, int items, SV **svary)
{
	crt_splice(aTHX_ proxy, -1, 0, 0, items, svary);
}

STATIC SV *
crt_pop(pTHX_ sociable_proxy_t *proxy)
{
	return crt_splice(aTHX_ proxy, 3, -1, 1, 0, NULL);
}

STATIC SV *
crt_shift(pTHX_ sociable_proxy_t *proxy)
{
	return crt_splice(aTHX_ proxy, 3, 0, 1, 0, NULL);
}

STATIC int
crt_array_elem_exists(pTHX_ sociable_proxy_t *proxy, int i)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	return ((elemp->ary_head > i) && (elemp->ary_ptr[i].elemp != NULL));
}

STATIC int
crt_hash_elem_exists(pTHX_ sociable_proxy_t *proxy, char *key, int len)
{
	return crt_hash_lookup(aTHX_ proxy, key, len, 0, NULL);
}

STATIC SV *
crt_firstkey(pTHX_ sociable_proxy_t *proxy)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	int i;
	crt_hash_t *hash = &elemp->un.hash;
	sociable_addr_t *buckets = hash->buckets.elemp;

	for (i = 0; (i < hash->bucket_count) && (buckets[i].elemp == NULL); i++);
	return (i == hash->bucket_count) ?
		&PL_sv_undef :
		newSVpv(buckets[i].elemp->hash_tuple.keyptr.elemp, buckets[i].elemp->hash_tuple.keylen);
}

STATIC SV *
crt_nextkey(pTHX_ sociable_proxy_t *proxy, char *oldkey, int oldlen)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	int i;
	crt_hash_t *hash = &elemp->un.hash;
	sociable_element_t *h = NULL;
	unsigned int hashval = 0;
	sociable_addr_t *buckets = hash->buckets.elemp;

	PERL_HASH(hashval, oldkey, oldlen);
	hashval %= hash->bucket_count;
	for (h = buckets[hashval].elemp;
		(h != NULL) && ((oldlen != h->hash_tuple.keylen) ||
			memcmp(h->hash_tuple.keyptr.elemp, oldkey, oldlen));
		h = h->NEXT_PTR);

	if (h == NULL)
		return &PL_sv_undef;

	h = h->NEXT_PTR;
	if (h == NULL) {
/*
 * end of bucket, move to next occupied
 */
		hashval++;
		for (; (hashval < (unsigned int)hash->bucket_count) &&
			(buckets[hashval].elemp == NULL); hashval++);
		if (hashval == hash->bucket_count)
			return &PL_sv_undef;
		h = buckets[hashval].elemp;
	}
	return newSVpv(h->hash_tuple.keyptr.elemp, h->hash_tuple.keylen);
}

STATIC int
crt_extend(pTHX_ sociable_proxy_t *proxy, int count)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	crt_array_t *array = &elemp->un.array;

	crt_extend_array(aTHX_ array, count, &elemp->extent);
	return 1;
}

STATIC int
crt_storesize(pTHX_ sociable_proxy_t *proxy, int count)
{
	sociable_structure_t *elemp = (sociable_structure_t *)proxy->elemp;
	crt_array_t *array = &elemp->un.array;
	sociable_addr_t *ary = NULL;
	int i;

	if (count > elemp->ary_head)
		crt_extend_array(aTHX_ array, count, &elemp->extent);
	else {
/*
 * what to do with the elements we trim ? if there are
 * ref's to them, then destroy would be an issue...
 * !!!WE NEED TO REVISIT THIS!!! We should be freeing the elements here
 * !!!IF we free elements, then we need to log the array
 */
		ary = elemp->ary_ptr.elemp;
		for (i = elemp->ary_head - 1; i >= count; ary[i--].elemp = NULL);
		elemp->ary_head = count;
	}
	return elemp->ary_head;
}

STATIC int
crt_is_tied(sociable_proxy_t * proxy)
{
	return 0;
}

STATIC void
crt_get_properties(HV *mechhv)
{
	return;
}

STATIC SV *
crt_get_property(char *property)
{
	return &PL_sv_undef;
}

STATIC SV *
crt_set_property(char *property, SV *val)
{
	return &PL_sv_yes;
}

STATIC void *
crt_addr_to_ptr(sociable_addr_t *addr)
{
	return addr->elemp;
}

STATIC int
crt_stm_alloc_ctxt(sociable_addr_t *ptr)
{
	memzero(ptr, sizeof(sociable_addr_t));
	CRT_Newz(ptr->elemp, 1, sociable_stm_ctxt_t);
	return (ptr->elemp != NULL);
}

STATIC void
crt_stm_free_ctxt(sociable_addr_t *ptr)
{
	crt_free(ptr->elemp);
}

STATIC void
crt_acquire_commit_lock()
{
#ifdef SOCIABLE_DEBUG_LOCKS
	crt_recursive_lock_acquire(&crt_commit_lock, __LINE__);
#else
	crt_recursive_lock_acquire(&crt_commit_lock);
#endif
}

STATIC void
crt_release_commit_lock()
{
	crt_recursive_lock_release(&crt_commit_lock);
}

STATIC void
crt_commit_with_lock(int flag)
{
	crt_mech.smech.lockfree =  !flag;
}

STATIC void
crt_eager_detect(int flag)
{
	crt_mech.smech.detector = flag
		? &native_eager
		: &native_lazy;
	crt_mech.smech.eager = flag;
}

int
crt_init()
{
	memzero(crt_mech, sizeof(crt_storage_mech_t));
	/*
	 *	install all our methods
	 */
	crt_mech.name = crt_strdup(SvPV_nolen(name));
	crt_mech.classname = crt_strdup(SvPV_nolen(pkgname));
	crt_mech.can_stm = TRUE;
	crt_mech.can_queue = TRUE;
	crt_mech.debug = FALSE;
	crt_mech.warn = FALSE;
	crt_mech.always_lockable = FALSE;
	crt_mech.eager = TRUE;
	crt_mech.lockfree = FALSE;
	crt_mech.level = SOC_PROCESS_PRIVATE_MECH;
	crt_mech.mechid = mechindex;
	crt_mech.detector = &native_eager;

	crt_mech.is_lockable = &crt_is_lockable;
/*
	crt_mech.lock_alloc = &crt_lock_alloc;
	crt_mech.lock_free = &crt_lock_free;
*/
	crt_mech.lock = &crt_recursive_lock_acquire;
	crt_mech.unlock = &crt_recursive_lock_release;
	crt_mech.cond_wait = &crt_cond_wait;
	crt_mech.cond_timedwait = &crt_cond_timedwait;
	crt_mech.cond_signal = &crt_cond_signal;
	crt_mech.cond_broadcast = &crt_cond_broadcast;

	crt_mech.create_proxy = &crt_create_proxy;
	crt_mech.new_array = &crt_new_array;
	crt_mech.new_hash = &crt_new_hash;
	crt_mech.new_scalar = &crt_new_scalar;
	crt_mech.destroy = &crt_destroy;

	crt_mech.set_debug = &crt_debug;
	crt_mech.set_warn = &crt_warn;
	crt_mech.set_always_lockable = &crt_always_lockable;

	crt_mech.debug_enabled = &crt_debug_enabled;
	crt_mech.warn_enabled = &crt_warn_enabled;
	crt_mech.always_lockable_enabled = &crt_always_lockable_enabled;

	crt_mech.get_extent = &crt_get_extent;
	crt_mech.get_himark = &crt_get_himark;
	crt_mech.set_extent = &crt_set_extent;
	crt_mech.default_extent = &crt_default_extent;
/*
 *	structure tie methods
 */
	crt_mech.array_fetch			= &crt_array_fetch;
	crt_mech.hash_lookup			= &crt_hash_lookup;
	crt_mech.array_delete			= &crt_array_delete;
	crt_mech.remove_hash_element	= &crt_remove_hash_element;
	crt_mech.array_get_size		= &crt_array_get_size;
	crt_mech.array_clear			= &crt_array_clear;
	crt_mech.hash_clear			= &crt_hash_clear;
	crt_mech.splice				= &crt_splice;
	crt_mech.push					= &crt_push;
	crt_mech.pop					= &crt_pop;
	crt_mech.shift				= &crt_shift;
	crt_mech.unshift				= &crt_unshift;
	crt_mech.extend				= &crt_extend;
	crt_mech.storesize			= &crt_storesize;
	crt_mech.array_elem_exists	= &crt_array_elem_exists;
	crt_mech.hash_elem_exists		= &crt_hash_elem_exists;
	crt_mech.firstkey				= &crt_firstkey;
	crt_mech.nextkey				= &crt_nextkey;

	crt_mech.stm_acquire_commit_lock = &crt_acquire_commit_lock;
	crt_mech.stm_release_commit_lock = &crt_release_commit_lock;
	crt_mech.commit_with_lock		= &crt_commit_wth_lock;
	crt_mech.eager_detect			= &crt_eager_detect;

	crt_mech.stm_save_referent	= &native_save_referent;
	crt_mech.stm_merge_element	= &native_merge_element;
	crt_mech.stm_acquire_for_commit = &private_acquire_for_commit;
/*
 *	allocate a transaction context for the specified mechanism
 */
	crt_mech.stm_alloc_ctxt = &crt_stm_alloc_ctxt;
	crt_mech.stm_free_ctxt = &crt_stm_free_ctxt;

	crt_mech.locked_commit = &crt_locked_commit;
	crt_mech.lockfree_commit = &crt_lockfree_commit;

	crt_mech.get_properties = &crt_get_properties;
	crt_mech.get_property = &crt_get_property;
	crt_mech.set_property = &crt_set_property;
	crt_mech.is_tied = &crt_is_tied;
	crt_mech.addr_to_ptr = &crt_addr_to_ptr;

	crt_mech.mem_alloc = &crt_mem_alloc;
	crt_mech.mem_free = &crt_mem_free;
	crt_mech.mem_realloc = &crt_mem_realloc;
	crt_mech.mem_strdup = &crt_mem_strdup;
	crt_mech.mem_set = &crt_mem_set;
	crt_mech.mem_read = &crt_mem_read;
	crt_mech.mem_write = &crt_mem_write;

	crt_mech.mem_alloc_dbg = &crt_mem_alloc_dbg;
	crt_mech.mem_free_dbg = &crt_mem_free_dbg;
	crt_mech.mem_realloc_dbg = &crt_mem_realloc_dbg;
	crt_mech.mem_strdup_dbg = &crt_mem_strdup_dbg;
	crt_mech.mem_check = &crt_mem_check;
/*
 *	we're native, so we don't need these
 */
	crt_mech.spinlock_acquire = NULL;
	crt_mech.spinlock_release = NULL;
	crt_mech.get_scalar_len = NULL;
	crt_mech.clear_scalar = NULL;
	crt_mech.get_sv_type = NULL;
	crt_mech.is_elem_updated = NULL;
	crt_mech.incr_elem_seqno = NULL;
	crt_mech.bless = NULL;
	crt_mech.is_encoded = NULL;
	crt_mech.set_encoding = NULL;
	crt_mech.sv_recover = NULL;
	crt_mech.sv_save = NULL;
	crt_mech.get_seqno = NULL;
	crt_mech.get_seqnolo = NULL;
	crt_mech.get_seqnohi = NULL;
	crt_mech.expired = NULL;
	crt_mech.get_reference = NULL;
	crt_mech.is_rv = NULL;
	crt_mech.is_same_elem = NULL;
	crt_mech.get_rv = NULL;
	crt_mech.get_pkgname = NULL;
	crt_mech.get_array_head = NULL;
	crt_mech.set_ref = NULL;
	crt_mech.sv_ok = NULL:

	crt_mech.stm_update_karma = NULL;
	crt_mech.stm_get_karma = NULL;
	crt_mech.stm_make_owner = NULL;
	crt_mech.stm_am_owner = NULL;
	crt_mech.stm_get_owner = NULL;
	/*
	 *	create initial pools
	 */
	crt_extend_elements(1);
	crt_extend_structures(1);
	/*
	 *	initialize global locks
	 */
	crt_recursive_lock_init(aTHX_ &mech->elements.lock);
	crt_recursive_lock_init(aTHX_ &mech->structures.lock);
	crt_recursive_lock_init(aTHX_ &mech->tsdq_global_lock);
	crt_recursive_lock_init(aTHX_ &crt_commit_lock);
	return 1;
}